node {
    
    def userInput = true

    deleteDir() //delete workspace before build
    
    stage('Clone Repo') {
        
        checkout scm
    }
    
    stage('Choose to include Integration Tests') {
        try {
            timeout(time: 15, unit: 'SECONDS') { 
                input(message: 'Run Integration Tests in this build?', parameters: [])
                userInput = true
            } 
        } 
        catch(error) {
            def user = error.getCauses()[0].getUser()
            if('SYSTEM' == user.toString()) { // SYSTEM means timeout.
                echo "Default action taken: Including Integration Tests"
                userInput = true
            } 
            else {
                echo "Skipping Integration Tests"
                userInput = false
            }
        }
    }

    try {
        stage('Build model') { 
            sh '''
                cd ./docker/
                docker build --no-cache -t switchgroup1/projmanappmysqlrel:`date +"%Y-%m-%d_%H.%m"` -t switchgroup1/projmanappmysqlrel:latest -f DockerfileMySQL .
            '''
        }
        
        stage('Build frontend') {
            sh '''
                cd ./frontend/project-management-app/
                docker build --no-cache -t switchgroup1/frontend:`date +"%Y-%m-%d_%H.%m"` -t switchgroup1/frontend:latest -f Dockerfile .
            '''
        }

        stage('Unit Tests') {
            docker.image('switchgroup1/projmanappmysqlrel:latest').inside() {   
                sh 'mvn test' 
            }
        }

        stage('Integration Tests') {
            if (userInput == true) {
                docker.image('switchgroup1/projmanappmysqlrel:latest').inside() { 
                    sh 'mvn integration-test -Dskip.surefire.tests'
                }
            }
        }
        
        stage('Package') {
            docker.image('switchgroup1/projmanappmysqlrel:latest').inside() {   
                sh 'mvn package -Dskip.surefire.tests'
            }
        }  

        stage('Push model and frontend to Docker Hub') {

            withCredentials([usernamePassword(credentialsId: 'group1-dockerhub', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                sh '''
                    echo $PASSWORD | docker login -u $USERNAME --password-stdin
                    
                    docker push $(docker images | grep "/projmanappmysqlrel" | grep `date +"%Y-%m-"` | awk -F ' ' '{print ($1 ":" $2)}')
                    docker push $(docker images | grep "/frontend" | grep `date +"%Y-%m-"` | awk -F ' ' '{print ($1 ":" $2)}')
                    docker push switchgroup1/projmanappmysqlrel:latest
                    docker push switchgroup1/frontend:latest
                '''
            }
        }

        stage('Deploy to Microsoft Azure') {

            sh '''
                ssh group1@switch-group1.cloudapp.net < deployAzure.sh
            '''
        }

        stage('Cleanup') {
        
            sh '''
                docker images | grep "switchgroup1" | awk -F ' ' '{print $3}' | xargs docker rmi -f || true
        
            '''  
        }  
    }

    catch (exc) {
        sh '''
            docker images | grep "switchgroup1" | awk -F ' ' '{print $3}' | xargs docker rmi -f || true
        '''  
     }    
}