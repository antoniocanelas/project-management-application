package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;

import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.services.TaskService;

@Component
public class ReportCostController {

    ProjectService projectService;
    TaskService taskService;
    private static final String TASK = "Task ID: ";
    private static final String COST = "Cost: ";

    public ReportCostController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Get cost in certain project.
     * @param projectId
     * @return cost
     */
    public double getProjectCostSoFar(String projectId) {
        
        return projectService.calculateTotalCostSoFar(projectId);
    }

    /**
     * Get the project chosen unit.
     * @param projectId
     * @return project unit.
     */
    public String getUnit(String projectId) {
        return projectService.getProjectByID(projectId).getUnit().toString();
    }

    /**
     * Method to list a detailed cost so far.
     * @param projectId
     * @return cost so far.
     */
    public String listCostSoFarDetails(String projectId) {
    	StringBuilder sb = new StringBuilder();
    	String separatorProjectCostDetails = "_________________________________";
    	String fourLinesFeed = "\n\n\n\n";
    	Project project1 = projectService.getProjectByID(projectId);
    	sb.append(fourLinesFeed);
    	sb.append(separatorProjectCostDetails);
    	sb.append("\n\nPROJECT ").append(projectId).append(" COST DETAILS\n");
    	sb.append(separatorProjectCostDetails).append("\n\n");
    	for (Task t : taskService.getProjectTasks(project1))
    	{
    		sb.append(TASK).append(t.getId()).append("\n");
            sb.append(COST).append(project1.calculateCostSoFarOfTask(t)).append("\n");
            sb.append("\n");
    	}
        sb.append(separatorProjectCostDetails).append("\n\n");
        return sb.toString();
    }
}
