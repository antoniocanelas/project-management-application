package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.services.TaskService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class TaskController {

    ProjectService projectService;
    TaskService taskService;

    public TaskController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Defines dependencies between tasks in a project.
     * @param projectId
     * @param taskToAddDependency
     * @param dependentTask
     * @return a string saying if task dependency was added, if task dependency
     * already exists, if task can´t depend on itself, or if task dependency
     * was successfully added
     */
    public String addTaskDependency(String projectId, String taskToAddDependency, String dependentTask) {

        Project project = projectService.getProjectByID(projectId);

        Task task1 = project.findTaskByID(taskToAddDependency);
        Task task2 = project.findTaskByID(dependentTask);

        if (task2 == null || task1 == null)
            return "No task dependency added\n\n";
        else if (task1.getTaskDependencies().contains(task2))
            return "Task dependency already exists\n\n";
        else if (task1.equals(task2))
            return "Task can´t depend on itself\n\n";
        else if (task2.getTaskDependencies().contains(task1))
            return task2.getId() + " already depends on " + task1.getId() + ". No dependency added.";
        else {
            task1.addTaskDependency(task2);
            taskService.updateTask(task1);
            return "Task dependency successfully added\n\n";
        }
    }

    /**
     * Checks if a task can start or not.
     * @param task
     * @return a string saying if a task can start or not.
     */
    public String taskCanStart(String task) {

        Task taskToStart = taskService.findTaskByID(task);

        if (taskToStart.canStart())
            return "Task can start\n\n";
        else
            return "Task can not start\n\n";
    }

    /**
     * Get task ID.
     * @param taskID
     * @return a string showing the task ID.
     */
    public String getTask(String taskID) {

        return taskService.findTaskByID(taskID).getId();
    }

    /**
     * Get the project unit (hours or days).
     * @param projectId
     * @return a string showing the project unit.
     */
    public String getProjectUnit(String projectId) {
        return projectService.getProjectByID(projectId).getUnit().toString();
    }

    /**
     * Verify if user input related to days is valid (prevent negative numbers, but
     * zero is included).
     * @param days
     * @return true if it is valid, false otherwise.
     */
    public boolean isValidDay(int days) {
        Pattern pattern = Pattern.compile("^\\d+$");
        Matcher matcher = pattern.matcher(String.valueOf(days));
        return matcher.matches();
    }

    /**
     * Show list of projects based on its ID and title.
     * @return list of projects based on its ID and title.
     */
    public StringBuilder projectIdAndTitle() {
        StringBuilder projectIdTitle = new StringBuilder();

        for (Project p : projectService.getProjectList()) {
            projectIdTitle.append("ID: ").append(p.getId()).append(" Title: ").append(p.getName());
        }
        return projectIdTitle;
    }
}
