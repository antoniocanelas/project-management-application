package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;

import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.model.user.User;

@Component
public class ProjectController {

    private UserService userService;
    private ProjectService projectService;
    private ProjectCollaboratorService projCollabRegistry;
    private String email = " Email: ";
    private String projectStatus = "Project Status: ";
    private String projectDescription = "Project description: ";
    private String projectManager = "\t\tProject Manager: ";
    private String projectName = "\t\tProject name: ";
    private String projectCostCalculationMethod = "\t\tProject Cost Calculation Method: ";
    private static final String PROJECT_ID = "\nProject ID: ";  

    public ProjectController(UserService userService, ProjectService projectService, ProjectCollaboratorService projCollabRegistry) {
        this.userService = userService;
        this.projectService = projectService;
        this.projCollabRegistry = projCollabRegistry;
    }

    /**
     * Changes the project's Project Manager.
     * @param projectId Project ID
     * @param userEmail
     * @return String with a success or fail message
     */
    public String changeProjectManager(String projectId, String userEmail) {
        if ("".equals(userEmail) || "".equals(projectId))
            return "Project manager did not change because entered invalid information";
        User user = userService.searchUserByEmail(userEmail);
        Project project = projectService.getProjectByID(projectId);

        if (project == null)
            return "\nNo project found with the introduced ID";

        if (project.getProjectManager().getUser().getEmail().equals(userEmail))
            return "\nThis user is already the project manager the project with the ID " + projectId + ".";

        if (projCollabRegistry.setProjectManager(project, user)) {
            projectService.updateProject(project);
            return "\nProject manager of the project with the ID " + projectId
                    + " has been changed to the project collaborator which email is " + userEmail + ".";
        }
        return "\nProject manager did not change because of one of the following reasons:\n"
                + "- the user is not registered.\n" + "- the user is not a project collaborator.\n"
                + "- the user is not active.\n\n";
    }

    /**
     * Lists all active projects.
     * @return String representing a list of all active projects.
     */
    public String listAllActiveProjects() {

        StringBuilder sb = new StringBuilder();
        for (Project project : projectService.listAllActiveProjects()) {
            sb.append(PROJECT_ID).append(project.getId()).append("\t");
            sb.append(projectStatus).append(project.getStatus()).append("\n");
            sb.append(projectName).append(project.getName()).append("\t");
            sb.append(projectDescription).append(project.getDescription()).append("\n");
            sb.append(projectCostCalculationMethod).append(project.getCostCalculatorPersistence()).append("\n");
            sb.append(projectManager).append(project.getProjectManager().getUser().getName()).append(email).append(project.getProjectManager().getUser().getEmail()).append("\n");
            sb.append("______________________________________________________________________________\n");
        }
        sb.append("\n\n");
        return sb.toString();
    }

    /**
     * Lists all project manager projects.
     * @param userEmail
     * @return String showing a list of all project manager projects.
     */
    public String listAllProjectManagerProjects(String userEmail) {

        StringBuilder sb = new StringBuilder();

        User user = userService.searchUserByEmail(userEmail);
        int count = 0;

        for (Project project : projectService.projectManagerProjectsList(user)) {
            sb.append(PROJECT_ID).append(project.getId()).append("\t");
            sb.append(projectStatus).append(project.getStatus()).append("\n");
            sb.append(projectName).append(project.getName()).append("\t");
            sb.append(projectDescription).append(project.getDescription()).append("\n");

            sb.append(projectManager).append(project.getProjectManager().getUser().getName()).append(email).append(project.getProjectManager().getUser().getEmail()).append("\n");

            count++;
        }
        if (count == 0) {
            sb.append("This user has no projects as Project Manager!\n\n");
        }

        return sb.toString();
    }

    /**
     * Get project info (name and ID)
     * @param projectId
     * @return a String with project info.
     */
    public String getProjectInfo(String projectId) {
        StringBuilder sb = new StringBuilder();
        Project project = projectService.getProjectByID(projectId);

        sb.append(PROJECT_ID).append(project.getId()).append("\t");
        sb.append(projectStatus).append(project.getStatus()).append("\n");
        sb.append(projectName).append(project.getName()).append("\t");
        sb.append(projectDescription).append(project.getDescription()).append("\n");
        sb.append(projectManager).append(project.getProjectManager().getUser().getName()).append(email).append(project.getProjectManager().getUser().getEmail());

        return sb.toString();
    }
}
