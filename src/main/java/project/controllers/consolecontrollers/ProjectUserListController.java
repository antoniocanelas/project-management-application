package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.user.User;

import java.util.List;

@Component
public class ProjectUserListController {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;

    public ProjectUserListController(UserService userService, ProjectService projectService, ProjectCollaboratorService projectCollaboratorService) {
        this.userService = userService;
        this.projectService = projectService;
        this.projectCollaboratorService = projectCollaboratorService;
    }

    /**
     * Get all project collaborators.
     * @param projectId
     * @return a list of project collaborators.
     */
    public String listActiveProjectCollaborators(String projectId) {
        List<ProjectCollaborator> list = projectCollaboratorService.listProjectCollaboratorsByProject(projectId);

        if (list.isEmpty()) {
            return "There are no Collaborators available in this Project";
        }
        return formatListCollaborators(list);
    }

    /**
     * Get all unassigned project collaborators.
     * @param projectId
     * @return a list of unassigned project collaborators.
     */
    public String getUnassignedProjectCollaboratorList(String projectId) {
        Project project1 = projectService.getProjectByID(projectId);
        List<ProjectCollaborator> list = project1.listUnassignedProjectCollaborator();

        if (list.isEmpty()) {

            return "There are no Unassinged Collaborators in this Project";
        }
        return formatListCollaborators(list);
    }

    /**
     * Method to format how collaborators are displayed.
     * @param list of project collaborators
     * @return
     */
    public String formatListCollaborators(List<ProjectCollaborator> list) {

        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (ProjectCollaborator p : list) {
            count++;
            User u = p.getUser();
            sb.append("\n[").append(count).append("] Name: ").append(u.getName());
            sb.append("\n    Email: ").append(u.getEmail()).append("\n");
        }
        return sb.toString();
    }
}
