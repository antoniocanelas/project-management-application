package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.user.Address;
import project.model.user.User;

@Component
public class UserDataController {

    UserService userService;

    public UserDataController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Set a name,
     * @param name
     * @param email
     */
    public void editName(String name, String email) {

        User user = userService.searchUserByEmail(email);
        user.setName(name);

        userService.updateUser(user);
    }

    /**
     * Get users name (by his email).
     * @param email
     * @return
     */
    public String getName(String email) {

        User user = userService.searchUserByEmail(email);
        return user.getName();
    }

    /**
     * Get users phone(by his email).
     * @param email
     * @return
     */
    public String getPhone(String email) {
        User user = userService.searchUserByEmail(email);
        return user.getPhone();
    }

    /**
     * Get users nif (by his email).
     * @param email
     * @return
     */
    public String getTpi(String email) {
        User user = userService.searchUserByEmail(email);
        return user.getTaxPayerId();
    }

    /**
     * Get users birth (by his email).
     * @param email
     * @return
     */
    public String getBirthDate(String email) {
        User user = userService.searchUserByEmail(email);
        return user.getBirthDate().toString();
    }

    /**
     * Get users address list (by his email).
     * @param email
     * @return
     */
    public String getAddressList(String email) {
        User user = userService.searchUserByEmail(email);
        return user.getAddressList().toString();
    }

    /**
     * Get users address ID (by his email and address ID).
     * @param email
     * @param id
     * @return
     */
    public String getAddressId(String email, String id) {
        User user = userService.searchUserByEmail(email);
        if (user.getAddressById(id) == null)
        	return null;
        return user.getAddressById(id).getAddressDescription();
    }

    /**
     * Get users street (by his email and address ID).
     * @param email
     * @param id
     * @return
     */
    public String getAddressStreet(String email, String id) {
        User user = userService.searchUserByEmail(email);
        return user.getAddressById(id).getStreet();
    }

    /**
     * Get users city (by his email and address ID).
     * @param email
     * @param id
     * @return
     */
    public String getAddressCity(String email, String id) {
        User user = userService.searchUserByEmail(email);
        return user.getAddressById(id).getCity();
    }

    /**
     * Get users postal code (by his email and address ID).
     * @param email
     * @param id
     * @return
     */
    public String getAddressPostalCode(String email, String id) {
        User user = userService.searchUserByEmail(email);
        return user.getAddressById(id).getPostalCode();
    }

    /**
     * Get users country (by his email and address ID).
     * @param email
     * @param id
     * @return
     */
    public String getAddressCountry(String email, String id) {
        User user = userService.searchUserByEmail(email);
        return user.getAddressById(id).getCountry();
    }

    /**
     * Edit users address ID (by his email, old address ID and new address ID).
     * @param email
     * @param id
     * @param id2
     */
    public void editAddressId(String email, String id, String id2) {
        User user = userService.searchUserByEmail(email);
        Address address = user.getAddressById(id);
        address.setAddressDescription(id2);
        userService.updateUser(user);

    }

    /**
     * Edit users street (by his email, old address ID and new street).
     * @param email
     * @param id
     * @param street
     */
    public void editAddressStreet(String email, String id, String street) {
        User user = userService.searchUserByEmail(email);
        Address address = user.getAddressById(id);
        address.setStreet(street);
        userService.updateUser(user);
    }

    /**
     * Edit users city (by his email, old address ID and new city).
     * @param email
     * @param id
     * @param city
     */
    public void editAddressCity(String email, String id, String city) {
        User user = userService.searchUserByEmail(email);
        Address address = user.getAddressById(id);
        address.setCity(city);
        userService.updateUser(user);
    }

    /**
     * Edit users postal code (by his email, old address ID and postal code).
     * @param email
     * @param id
     * @param postalCode
     */
    public void editAddressPostalCode(String email, String id, String postalCode) {
        User user = userService.searchUserByEmail(email);
        Address address = user.getAddressById(id);
        address.setPostalCode(postalCode);
        userService.updateUser(user);
    }

    /**
     * Edit users country (by his email, old address ID and new country).
     * @param email
     * @param id
     * @param country
     */
    public void editAddressCountry(String email, String id, String country) {
        User user = userService.searchUserByEmail(email);
        Address address = user.getAddressById(id);
        address.setCountry(country);
        userService.updateUser(user);
    }

    /**
     * Set user active
     * @param email
     */
    public void setActive(String email) {
        User user = userService.searchUserByEmail(email);
        user.setActive();

        userService.updateUser(user);
    }
    
    
    /**
     * Set new password
     * @param email
     * @param newPassword
     */
    public void setNewPassword(String email, String newPassword) {
        User user = userService.searchUserByEmail(email);
        user.setPassword(newPassword);

        userService.updateUser(user);
    }
}
