package project.controllers.consolecontrollers;

import java.util.List;

import org.springframework.stereotype.Component;

import project.model.project.ProjectService;

@Component
public class ChooseCostCalculationController {

	private ProjectService projectService;
	
	public ChooseCostCalculationController(ProjectService projectService) {
		this.projectService = projectService;
	}
	
	/**
	 * Requests projectService to list all Project's cost calculation options.
	 * @param projectId
	 * @return
	 */
	public List<String> listProjectCostCalculationOptions(String projectId) {
		
		return projectService.listProjectCostCalculationOptions(projectId);
	}

	/**
	 * Requests projectService to set a project's cost calculation method.
	 * @param projectId
	 * @param choice Cost calculation choice. 
	 */
	public void setProjectCostCalculationMechanism(String projectId, String choice) {
			
		 projectService.setProjectCostCalculationMechanism(projectId, choice);
	}
	
	/**
	 * Requests projectService to get the project's cost calculation method selected.
	 * @param projectId
	 */
	public String getSelectedProjectCostCalculationMechanism(String projectId) {
			
		 return projectService.getProjectByID(projectId).getCostCalculatorPersistence();
	}
}
