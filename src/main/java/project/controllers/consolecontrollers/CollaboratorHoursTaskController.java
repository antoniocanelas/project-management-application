package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project.ProjectUnits;
import project.model.project.ProjectService;
import project.model.user.User;

@Component
public class CollaboratorHoursTaskController {

    UserService userService;
    ProjectService projectService;

    public CollaboratorHoursTaskController(UserService userService, ProjectService projectService) {
        this.userService = userService;
        this.projectService = projectService;
    }

    /**
     * Calculates the collaborator's total number of hours in completed tasks last month.
     * @param email Parameter to find the collaborator.
     * @return Total number of hours.
     */
    public String calculateCollaboratorsHoursCompletedTasksLastMonthInAllProjects(String email) {

        User user = userService.searchUserByEmail(email);

        double hours = projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user,ProjectUnits.HOURS);
        return "Total hours spent in all projects: " + hours;
    }

    /**
     * Calculates the collaborator's average hours spent per task in completed tasks last month.
     * @param email Parameter to find the collaborator.
     * @return Average hours spent per task.
     */
    public String calculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjects(String email) {

        User user = userService.searchUserByEmail(email);

        double hours = projectService.calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(user);
        return "Average hours per completed task was: " + hours;
    }

}
