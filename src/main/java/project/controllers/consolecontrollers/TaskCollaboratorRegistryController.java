package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;

@Component
public class TaskCollaboratorRegistryController {

    UserService userService;
    ProjectService projectService;

    public TaskCollaboratorRegistryController(UserService userService, ProjectService projectService) {
        this.userService = userService;
        this.projectService = projectService;
    }

    /**
     * Lists collaborator assignment requests.
     * @param projID
     * @return String list assignment requests
     */
    public String listAssignmentRequests(String projID) {

        Project proj = projectService.getProjectByID(projID);
        StringBuilder sb = new StringBuilder();

        if (proj == null || proj.listAssignmentRequests().isEmpty())
            return "\nNo requests waiting for approval.\n\n";

        for (TaskCollaboratorRegistry tcr : proj.listAssignmentRequests()) {

            sb.append("\nTask Registry ID: ").append(tcr.getTaskCollaboratorRegistryID());
            sb.append("\nProject ROLE_COLLABORATOR" + ": ").append(tcr.getProjectCollaborator().getUser().getName());
            sb.append("\nRequest Status: ").append(tcr.getRegistryStatus()).append("\n");

        }

        return sb.toString();
    }

    /**
     * Cancel an assignment request
     * @param projID
     * @param taskId
     * @param userEmail
     * @return String with success or fail message
     */
    public String cancelAssignement(String projID, String taskId, String userEmail) {

        String message = "Not able to cancel assignmet due to one of the following reasons\n"
                + "User not found, task not found or task already cancelled ";

        if (userEmail == null) {
            return message;
        }

        Project project = projectService.getProjectByID(projID);
        Task task = project.findTaskByID(taskId);
        User user = userService.searchUserByEmail(userEmail);
        ProjectCollaborator projCollab = project.findProjectCollaborator(user);
        if (task == null || task.getLastTaskCollaboratorRegistryOf(projCollab)
                .getRegistryStatus() == RegistryStatus.ASSIGNMENTCANCELLED) {
            return message;
        } else {
            task.cancelAssignmentRequest(projCollab);
            projectService.updateProject(project);
        }
        return "Assignment cancelled.";
    }

    /**
     * Approve assignment request
     * @param projID
     * @param taskId
     * @param userEmail
     * @return String with success or fail message
     */
    public String approveAssignement(String projID, String taskId, String userEmail) {

        String message = "Not able to approve assignmet due to one of the following reasons\n"
                + "User not found, task not found or task already cancelled ";

        if (userEmail == null) {
            return message;
        }

        Project project = projectService.getProjectByID(projID);
        Task task = project.findTaskByID(taskId);
        User user = userService.searchUserByEmail(userEmail);
        ProjectCollaborator projCollab = project.findProjectCollaborator(user);

        if (user == null || task == null || task.getLastTaskCollaboratorRegistryOf(projCollab)
                .getRegistryStatus() == RegistryStatus.ASSIGNMENTCANCELLED) {
            return message;
        } else {
            task.addProjectCollaborator(projCollab);
            projectService.updateProject(project);
        }
        return "Assignment approved.";
    }

    /**
     * List all removal requests.
     * @param projID
     * @return a String with a sucess or insucess message.
     */
    public String listRemovalRequests(String projID) {

        Project proj = projectService.getProjectByID(projID);
        StringBuilder sb = new StringBuilder();

        if (proj == null)
            return "\nNo requests waiting for removal.\n\n";

        for (TaskCollaboratorRegistry tcr : proj.listRemovalRequests()) {

            sb.append("\nTask Registry ID: ").append(tcr.getTaskCollaboratorRegistryID());
            sb.append("\nProject ROLE_COLLABORATOR" + ": ").append(tcr.getProjectCollaborator().getUser().getName());
            sb.append("\nRequest Status: ").append(tcr.getRegistryStatus()).append("\n");

        }
        return sb.toString();
    }

    /**
     * Approve removals.
     * @param projID
     * @param taskId
     * @param userEmail
     * @return a String with a sucess or insucess message.
     */
    public String approveRemoval(String projID, String taskId, String userEmail) {

        String errMessage = "Not able to approve removal due to one of the following reasons\n"
                + "User not found, task not found or user already removed from task ";

        if (userEmail == null) {
            return errMessage;
        }
        Project project = projectService.getProjectByID(projID);
        Task task = project.findTaskByID(taskId);
        User user = userService.searchUserByEmail(userEmail);

        ProjectCollaborator projCollab = project.findProjectCollaborator(user);

        if (user == null || task == null || task.getLastTaskCollaboratorRegistryOf(projCollab)
                .getRegistryStatus() == RegistryStatus.REMOVALAPPROVED) {

            return errMessage;
        } else {
            task.removeProjectCollaborator(projCollab);
            projectService.updateProject(project);
        }
        return "Removal approved.";
    }

    /**
     * Cancel removals.
     * @param projID
     * @param taskId
     * @param userEmail
     * @returna String with a sucess or insucess message.
     */
    public String cancelRemoval(String projID, String taskId, String userEmail) {

        String errorMessage = "Not able to cancel remove due to one of the following reasons\n"
                + "User not found, task not found or user already removed from task";

        if (userEmail == null) {
            return errorMessage;
        }
        User user = userService.searchUserByEmail(userEmail);
        Project project = projectService.getProjectByID(projID);
        ProjectCollaborator projCollab = project.findProjectCollaborator(user);
        Task task = project.findTaskByID(taskId);

        if (user == null || task == null || task.getLastTaskCollaboratorRegistryOf(projCollab)
                .getRegistryStatus() == RegistryStatus.REMOVALAPPROVED) {
            return errorMessage;
        } else {
            task.cancelRemovalRequest(projCollab);
            projectService.updateProject(project);
        }
        return "Removal cancelled.";
    }
}
