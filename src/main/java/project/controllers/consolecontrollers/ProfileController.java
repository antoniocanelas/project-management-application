package project.controllers.consolecontrollers;

import project.model.UserService;
import project.model.user.User;

public class ProfileController {

    UserService userService;

    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Sets the user profile as ROLE_DIRECTOR
     * @param userEmail
     * @return String with a success or fail message
     */
    public String setProfileDirector(String userEmail) {

        User user = userService.searchUserByEmail(userEmail);

        String result = userService.setUserProfileDirector(user);
        if (user != null) {
            userService.updateUser(user);
        }
        return result;
    }

    /**
     * Sets the user profile as ROLE_COLLABORATOR
     * @param userEmail
     * @return String with a success or fail message
     */
    public String setProfileCollaborator(String userEmail) {

        User user = userService.searchUserByEmail(userEmail);

        String result = userService.setUserProfileCollaborator(user);
        if (user != null) {
            userService.updateUser(user);
        }
        return result;
    }
}
