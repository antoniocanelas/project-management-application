package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.services.TaskService;

@Component
public class TaskStateController {

    ProjectService projectService;
    TaskService taskService;

    public TaskStateController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /**
     * Set task cancelled.
     * @param projectId id
     * @param taskId    id
     * @return a string saying if a task was set as cancelled.
     */
     public String setTaskCancelled(String projectId, String taskId) {

        Task task = projectService.getProjectByID(projectId).findTaskByID(taskId);
        if (task == null) {

            return "TASK WAS NOT FOUND";
        }
        task.setTaskCancelled();

        if (task.getTaskState().isOnCancelledState()) {
            taskService.updateTask(task);
            return "TASK WAS SUCCESSFULLY CANCELLED";
        }
        return "IS NOT POSSIBLE TO CANCEL THIS TASK";
     }

    /**
     * Set task as completed.
     * @param projectId id
     * @param taskId    id
     * @return a string saying if a task was set as completed.
     */
    public String setTaskCompleted(String projectId, String taskId) {
        Task task = projectService.getProjectByID(projectId).findTaskByID(taskId);
        if (task == null) {
            return "TASK WAS NOT FOUND";
        }
        task.setTaskCompleted();

        if (task.isCompleted()) {
            taskService.updateTask(task);
            return "TASK " + taskId + " IS COMPLETED\n";
        } else
            return "IT IS NOT POSSIBLE TO SET THIS TASK AS COMPLETED\n";
    }
}
