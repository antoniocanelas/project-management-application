package project.controllers.consolecontrollers;

import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

import java.util.ArrayList;
import java.util.List;

public class UserRegistryController {

    private static final String NAME = "User name: ";
    private static final String EMAIL = "User email: ";
    private static final String SPROFILE = "User profile: ";
    private static final String STATE = "User state: ";
    UserService userService;

    public UserRegistryController(UserService userService) {
        this.userService = userService;
    }

    /**
     * List all company users
     * @return String representing a list of all users
     */
    public String listAllUsers() {

        StringBuilder sb = new StringBuilder();
        List<User> userList = userService.listAllUsers();

        userList.sort((User u1, User u2) -> u1.getEmail().compareTo(u2.getEmail()));

        for (User user : userList) {
            sb.append(NAME).append(user.getName()).append("\n");
            sb.append(EMAIL).append(user.getEmail()).append("\n");
            sb.append(SPROFILE).append(user.getRoles()).append("\n");
            if (user.isActive()) {
                sb.append(STATE + "Active" + "\n");
            } else {
                sb.append(STATE + "Inactive" + "\n");
            }
        }
        return sb.toString();
    }

    /**
     * Search users by email.
     * @param userEmail
     * @return a string with informations about the user, like name, email or
     * profile.
     */
    public String searchUserByEmail(String userEmail) {
        User user = userService.searchUserByEmail(userEmail);

        if (!userService.listAllUsers().contains(user))
            return "User not found.";

        StringBuilder sb = new StringBuilder();

        sb.append(NAME).append(user.getName()).append("\n");
        sb.append(EMAIL).append(user.getEmail()).append("\n");
        sb.append(SPROFILE).append(user.getRoles()).append("\n");

        return sb.toString();
    }

    /**
     * Search users by profile.
     * @param userProfile
     * @return a string with informations about the user, like name, email or
     * profile.
     */
    public String searchUserByProfile(String userProfile) {

        StringBuilder sb = new StringBuilder();

        RoleName profile = Enum.valueOf(RoleName.class, userProfile);

        List<User> profileList = new ArrayList<>(userService.searchUserByProfile(new Role(profile)));

        if (profileList.isEmpty())
            return "There are not users with that profile.";

        for (User user : profileList) {

            sb.append("\n" + NAME).append(user.getName()).append("\n");
            sb.append(EMAIL).append(user.getEmail()).append("\t\t");
            sb.append(SPROFILE).append(user.getRoles()).append("\n");
        }
        return sb.toString();
    }
}
