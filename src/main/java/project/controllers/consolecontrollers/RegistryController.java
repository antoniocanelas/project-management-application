package project.controllers.consolecontrollers;

import org.springframework.stereotype.Component;

import project.dto.EmailMessageDTO;
import project.services.EmailService;
import project.model.UserService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RegistryController {

	UserService userService;
	EmailService emailService;
	EmailMessageDTO emailMessageDTO;
	
	public RegistryController(UserService userService, EmailService emailService) {
		this.userService = userService;
		this.emailService = emailService;
	}

	/**
	 * Method to register users, based in some parameters.
	 * @param name
	 * @param phone
	 * @param email
	 * @param tin
	 * @param birthDate
	 * @param id
	 * @param street
	 * @param postalCode
	 * @param city
	 * @param country
	 * @param password
	 * @return sucess if user is registered, false otherwise.
	 * @throws AddressException
	 */
	public boolean registerUser(String name, String phone, String email, String tin, LocalDate birthDate, String id,
			String street, String postalCode, String city, String country, String password) throws AddressException {
		
		boolean success = userService.addUser(name, phone, email, tin, birthDate, id, street, postalCode, city,
				country);
		User user1 = userService.searchUserByEmail(email);
		user1.setPassword(password);
		user1.setInactive();
		userService.updateUser(user1);
		emailMessageDTO = new EmailMessageDTO();
		emailMessageDTO.setTo(email);
		emailMessageDTO.setSubject("Confirmation e-mail from Group 1 Project Management Application");
		emailMessageDTO.setMessage("Your confirmation code is: " + user1.getUserLoginData().getConfirmationToken() + "\n\n");
		emailService.sendSimpleMessage(emailMessageDTO);
		return success;
	}

	/**
	 * Check if email is valid (if has "@", .com for example, etc).
	 * @param email
	 * @return true if mail is valid, false otherwise.
	 */
	public boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	/**
	 * Check if date is valid(needs to be in this format dd/mm/yyyy).
	 * @param date
	 * @return true if date is valid, false otherwise.
	 */
	public boolean isValidDate(String date) {
		Pattern pattern = Pattern.compile("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$");
		Matcher matcher = pattern.matcher(date);
		return matcher.matches();
	}

	/**
	 * Check if user input is constituted by numbers only, not symbols or letters.
	 * @param number
	 * @return true if it is valid, false otherwise.
	 */
	public boolean isValidNumber(String number) {
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(number);
		return matcher.matches();
	}

	/**
	 * Check if user input is constituted by letters only, not symbols or numbers.
	 * @param letters
	 * @return true if it is valid, false otherwise.
	 */
	public boolean isValidString(String letters) {
		Pattern pattern = Pattern.compile("[A-Za-zÀ-ú]+");
		Matcher matcher = pattern.matcher(letters);
		return matcher.matches();
	}

	/**
	 * Check if a profile is valid.
	 * @param profile
	 * @return true if it is valid, false otherwise.
	 */
	public boolean isValidProfile(String profile) {
		return ("COLLABORATOR".equals(profile) || "DIRECTOR".equals(profile) || "ADMINISTRATOR".equals(profile)
				|| "REGISTEREDUSER".equals(profile));
	}
}
