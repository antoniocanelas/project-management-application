package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.services.TaskProjectCollaboratorService;

@RestController
public class RequestAddOrRemoveTaskFromCollaboratorRESTController {

    TaskProjectCollaboratorService taskProjectCollaboratorService;

    public RequestAddOrRemoveTaskFromCollaboratorRESTController(TaskProjectCollaboratorService taskProjectCollaboratorService) {
        this.taskProjectCollaboratorService = taskProjectCollaboratorService;
    }

    /**
     * Add task to a collaborator.
     *
     * @param taskProjectCollaboratorRestDTO
     * @param taskId
     * @return a string saying if collaborator was added or not.
     */
    @PreAuthorize("isTaskCollaborator(#taskId)")
    @PostMapping("/tasks/{id}/request-assignment")//US302V02
    public  ResponseEntity<TaskProjectCollaboratorRestDTO> requestAddTaskToCollaborator(@RequestBody TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, @PathVariable("id") String taskId) {
        taskProjectCollaboratorRestDTO.setTaskId(taskId);

        if (taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO)) {
            return new ResponseEntity<>( HttpStatus.OK);
        }
        return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
    }

    /**
     * Remove task from a collaborator.
     *
     * @param taskProjectCollaboratorRestDTO
     * @param taskId
     * @return a string saying if collaborator was remove or not.
     */
    @PreAuthorize("isTaskCollaborator(#taskId)")
    @PutMapping("/tasks/{id}/request-removal")
    public ResponseEntity<TaskProjectCollaboratorRestDTO> requestRemoveTaskFromCollaborator(@RequestBody TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, @PathVariable("id") String taskId) {
        taskProjectCollaboratorRestDTO.setTaskId(taskId);

        if (taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
