package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import project.dto.rest.ProjectDTO;
import project.services.ProjectUserService;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CreateProjectRestController {
    ProjectUserService projectUserService;
    ProjectDTO projectOutDTO;

    public CreateProjectRestController(ProjectUserService projectUserService) {
        this.projectUserService = projectUserService;
    }

    /**
     * Auxiliary method that adds HATEOAS self links to projectDTO elements of a given list
     *
     * @param projectDTOList
     * @return list with self links added.
     */
    public static List<ProjectDTO> addSelfLinkToEachElement(List<ProjectDTO> projectDTOList) {

        for (ProjectDTO projectDTO : projectDTOList) {
            projectDTO.add(linkTo(methodOn(CreateProjectRestController.class).getProject(projectDTO.getProjectId()))
                    .withSelfRel());
        }
        return projectDTOList;
    }

    /**
     * Link to create project
     *
     * @param projectInDTO Project information
     * @return projectDTO with information/message.
     */
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    @PostMapping("/projects")
    public ResponseEntity<ProjectDTO> createProject(@RequestBody ProjectDTO projectInDTO) {
        projectOutDTO = projectUserService.createProject(projectInDTO);
        if (projectOutDTO == null) {
            return new ResponseEntity<>(insertErrorMessage(), HttpStatus.BAD_REQUEST);
        }

        projectOutDTO.add(linkTo(methodOn(CreateProjectRestController.class).getProject(projectInDTO.getProjectId())).withSelfRel().withRel("self"));
        projectOutDTO.add(linkTo(methodOn(CreateProjectRestController.class).listAllProjects()).withSelfRel().withRel("listAll"));

        return new ResponseEntity<>(projectOutDTO, HttpStatus.CREATED);
    }

    private ProjectDTO insertErrorMessage() {
        projectOutDTO = new ProjectDTO();
        projectOutDTO.setMessageError("Bad data entry!");
        return projectOutDTO;
    }

    /**
     * Method that get a project.
     *
     * @param projecId ID of project
     * @return desired project.
     */
    @GetMapping("/projects/{projecId}")
    @PreAuthorize("isProjectManager(#projecId)")
    public ProjectDTO getProject(@PathVariable("projecId") String projecId) {

        return projectUserService.getProject(projecId);
    }

    /**
     * Method that get all project.
     *
     * @return all projects.
     */
    @GetMapping("/projects")
    @PreAuthorize("hasRole('ROLE_COLLABORATOR') || hasRole('ROLE_ADMINISTRATOR') || hasRole('ROLE_DIRECTOR')")//DODO Create Permission
    public ResponseEntity<List<ProjectDTO>> listAllProjects() {

        List<ProjectDTO> projectDTOList = projectUserService.listAllProjects();
        addSelfLinkToEachElement(projectDTOList);
        return new ResponseEntity<>(projectDTOList, HttpStatus.OK);
    }
}
