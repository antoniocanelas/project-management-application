package project.controllers.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

@RestController
public class ProjectManagerTaskRequestsControllerREST {

	ProjectService projectService;
	TaskProjectCollaboratorService taskProjectCollaboratorService;
	UserService userService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;

	public ProjectManagerTaskRequestsControllerREST (ProjectService projectService, TaskProjectCollaboratorService
			taskProjectCollaboratorService, UserService userService, TaskService taskService,  ProjectCollaboratorService projectCollaboratorService) {
		this.projectService = projectService;
		this.taskProjectCollaboratorService = taskProjectCollaboratorService;
		this.userService = userService;
		this.taskService= taskService;
		this.projectCollaboratorService=projectCollaboratorService;
	}


	/**
	 * Get a list of collaborators assignment requests for all tasks in a project
	 * @param projectId
	 * @return list of assignment requests
	 */
	@PreAuthorize("isProjectManager(#projectId)")
	@GetMapping("/projects/{projectId}/assignmentrequests")
	public HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> listAssignmentRequests(@PathVariable("projectId") String projectId) {

		List<TaskCollaboratorRegistryRESTDTO> assignmentList = projectService.listAssignmentRequestsToDTO(projectId);
		for (TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO : assignmentList) {
			taskCollaboratorRegistryRESTDTO.add(linkTo(methodOn(ProjectManagerTaskRequestsControllerREST.class).listAssignmentRequests(projectId)).withSelfRel());
		}
		return new ResponseEntity<>(assignmentList, HttpStatus.OK);
	}


	/**
	 * Get a list of collaborators removal requests for all tasks in a project
	 * @param projectId
	 * @return list of removal requests
	 */
	@PreAuthorize("isProjectManager(#projectId)")
	@GetMapping("/projects/{projectId}/removalrequests")
	public HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> listRemovalRequests(@PathVariable("projectId") String projectId) {

		List<TaskCollaboratorRegistryRESTDTO> removalList = projectService.listRemovalRequestsToDTO(projectId);
		for (TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO : removalList) {
			taskCollaboratorRegistryRESTDTO.add(linkTo(methodOn(ProjectManagerTaskRequestsControllerREST.class).listRemovalRequests(projectId)).withSelfRel());
		}
		return new ResponseEntity<>(removalList, HttpStatus.OK);
	}

	/**
	 * Get a list of collaborators completed requests for all tasks in a project
	 * @param projectId
	 * @return list of completed requests
	 */
	@PreAuthorize("isProjectManager(#projectId)")
	@GetMapping("/projects/{projectId}/completedrequests")
	public HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> listCompletedRequests(@PathVariable("projectId") String projectId) {

		List<TaskCollaboratorRegistryRESTDTO> completedList = taskProjectCollaboratorService.listCompletedRequestsToDTO(projectId);
		String username;
		for (TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO : completedList) {
			username = userService.searchUserByEmail(taskCollaboratorRegistryRESTDTO.getProjectCollaboratorEmail()).getName();
			taskCollaboratorRegistryRESTDTO.setProjectCollaboratorName(username);
			taskCollaboratorRegistryRESTDTO.add(linkTo(methodOn(ProjectManagerTaskRequestsControllerREST.class).listRemovalRequests(projectId)).withSelfRel());
		}

		return new ResponseEntity<>(completedList, HttpStatus.OK);
	}

	/**
	 * Cancel a assignment request from a collaborator
	 * @param projectId, taskCollaboratorRegistryRESTDTO
	 * @return task collaborator registry DTO
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@PutMapping("/projects/{projectId}/cancelassignment")
	public HttpEntity<TaskCollaboratorRegistryRESTDTO> cancelAssignmentRequest(@PathVariable("projectId") String projectId, @RequestBody TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

		TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryDTOOut = taskProjectCollaboratorService.cancelAssignmentRequestToDTO(taskCollaboratorRegistryRESTDTO);
		taskCollaboratorRegistryDTOOut.add(linkTo(methodOn(ProjectManagerTaskRequestsControllerREST.class).cancelAssignmentRequest(projectId, taskCollaboratorRegistryRESTDTO)).withSelfRel());

		return new ResponseEntity<>(taskCollaboratorRegistryDTOOut, HttpStatus.OK);
	}

	/**
	 * Cancel a removal request from a collaborator
	 * @param projectId, taskCollaboratorRegistryRESTDTO
	 * @return task collaborator registry DTO
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@PutMapping("/projects/{projectId}/cancelremoval")
	public HttpEntity<TaskCollaboratorRegistryRESTDTO> cancelRemovalRequest(@PathVariable("projectId") String projectId, @RequestBody TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

		TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryDTO = taskProjectCollaboratorService.cancelRemovalRequestToDTO(taskCollaboratorRegistryRESTDTO);
		taskCollaboratorRegistryDTO.add(linkTo(methodOn(ProjectManagerTaskRequestsControllerREST.class).cancelAssignmentRequest(projectId, taskCollaboratorRegistryRESTDTO)).withSelfRel());

		return new ResponseEntity<>(taskCollaboratorRegistryDTO, HttpStatus.OK);
	}

	/**
	 * Cancel a removal request from a collaborator
	 * @param taskId, taskCollaboratorRegistryRESTDTO
	 * @return task collaborator registry DTO
	 */
	//@PreAuthorize("isProjectManager(#projectId)")
	@PutMapping("/tasks/{taskId}/approvecompleted")
	public ResponseEntity<TaskRestDTO> approveCompletedRequest(@PathVariable("taskId") String taskId) {


		return new ResponseEntity<>(taskService.markTaskCompleted(taskId), HttpStatus.OK);
	}


	/**
	 * Cancel a completed request from a collaborator
	 * @param taskId
	 * @return task rest DTO
	 */

	

	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@PutMapping("/tasks/{taskId}/cancelcompleted")
	public ResponseEntity<TaskRestDTO> cancelCompletedRequest(@PathVariable("taskId") String taskId) {


		return new ResponseEntity<>(taskService.cancelMarkTaskCompleted(taskId), HttpStatus.OK);
	}


}
