package project.controllers.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectCollaboratorRestDTO;
import project.services.ProjectUserService;

@RestController
public class AddOrRemoveProjectCollaboratorControllerREST {

    ProjectUserService projectUserService;

    public AddOrRemoveProjectCollaboratorControllerREST(ProjectUserService projectUserService) {
        this.projectUserService = projectUserService;
    }

    /**
     * Add project collaborator to project.
     *
     * @param projectCollaboratorRestDTO
     * @param projectId
     * @return a string saying if collaborator was added or not.
     */
    @PreAuthorize("isProjectManager(#projectId)")
    @PostMapping(value = "/projects/{projectId}")
    public HttpEntity<ProjectCollaboratorRestDTO> addCollaboratorToProject(@RequestBody ProjectCollaboratorRestDTO projectCollaboratorRestDTO, @PathVariable("projectId") String projectId) {

        ProjectCollaboratorRestDTO projectCollaboratorRestDTOOut = projectUserService.addProjectCollaboratorToProjectRest(projectCollaboratorRestDTO, projectId);
        if (projectCollaboratorRestDTOOut.getProjectId() == null) {
            return new ResponseEntity<>(projectCollaboratorRestDTOOut, HttpStatus.BAD_REQUEST);
        }
        projectCollaboratorRestDTOOut.add(linkTo(methodOn(AddOrRemoveProjectCollaboratorControllerREST.class).addCollaboratorToProject(projectCollaboratorRestDTOOut, projectId)).withSelfRel());
        return new ResponseEntity<>(projectCollaboratorRestDTOOut, HttpStatus.CREATED);
    }
    
    
   
}
