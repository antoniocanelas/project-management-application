package project.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectCollaboratorRestDTO;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
@RestController
public class ListProjectCollaboratorsToAddTaskRESTController {

	
	ProjectService projectService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;
	
	    public ListProjectCollaboratorsToAddTaskRESTController(ProjectService projectService, TaskService taskService,
			ProjectCollaboratorService projectCollaboratorService) {
	
		this.projectService = projectService;
		this.taskService = taskService;
		this.projectCollaboratorService = projectCollaboratorService;
	}


		

	  
	    @GetMapping("/tasks/{id}/collaboratorsavailable")
	      public HttpEntity<List<ProjectCollaboratorRestDTO>> listCollaboratorsNotInTask( @PathVariable("id") String taskId) {
	    	 Task task = taskService.findTaskByID(taskId);
	    	String projectId=task.getProject().getId();
	        Project project1 = projectService.getProjectByID(projectId);
        	
        	List<ProjectCollaboratorRestDTO> collaboratorsNotInTask = new ArrayList<>();
        	List<ProjectCollaborator>collaboratorsInProject =  projectCollaboratorService.listProjectCollaboratorsByProject(projectId);

	        for (ProjectCollaborator col : collaboratorsInProject) {
	        	ProjectCollaboratorRestDTO dto= new ProjectCollaboratorRestDTO();
	            if (!task.hasProjectCollaborator(col) && !project1.getProjectManager().equals(col)) { //TOCHECK: Verificar se a verificação do project manager é necessária

	            	 dto.setUserId(col.getUser().getEmail());
	            	 dto.setUserName(col.getUser().getName());
	            	 dto.setProjectId(col.getProject().getId());
	            	 dto.setCost(col.getCurrentCost());
	            	 
	            	 
	                collaboratorsNotInTask.add(dto);
	            }
	        }
	        
	        return new ResponseEntity<>(collaboratorsNotInTask, HttpStatus.OK);


}
	 
}
