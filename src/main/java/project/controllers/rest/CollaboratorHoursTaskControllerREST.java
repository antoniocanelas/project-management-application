package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskHoursRestDTO;
import project.model.UserService;
import project.model.project.Project.ProjectUnits;
import project.model.project.ProjectService;
import project.model.user.User;

@RestController
public class CollaboratorHoursTaskControllerREST {
	UserService userService;
	ProjectService projectService;

	public CollaboratorHoursTaskControllerREST(UserService userService, ProjectService projectService) {
		this.userService = userService;
		this.projectService = projectService;
	}
	
	/**
	 * Method that get the collaborator's total number of hours in completed tasks last month.
	 * @param userId Parameter to find the collaborator.
	 * @return Total number of hours.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	@GetMapping(value = "/users/{userId}/hours")
	public ResponseEntity<TaskHoursRestDTO> collaboratorHoursInCompletedTasksLastMonth(@PathVariable String userId) {
		User user = userService.searchUserByEmail(userId);
		TaskHoursRestDTO hours = new TaskHoursRestDTO();
		
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		hours.setHours(projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user,ProjectUnits.HOURS));
		hours.setPeopleMonth(projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user,ProjectUnits.PEOPLE_MONTH));

		return new ResponseEntity<>(hours, HttpStatus.OK);
	}
}
