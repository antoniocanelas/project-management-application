package project.controllers.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.LoadUserDataDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.payload.ApiAuthResponse;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.ui.console.PopulateFileDataUI;

@RestController
public class LoadDataControllerREST {

	UserService userService;
	ProjectService projectService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;

	public LoadDataControllerREST(UserService userService, ProjectService projectService, TaskService taskService,
			ProjectCollaboratorService projectCollaboratorService) {
		this.projectService = projectService;
		this.userService = userService;
		this.taskService = taskService;
		this.projectCollaboratorService = projectCollaboratorService;
	}

	/**
	 * Post method to load users from XML file
	 *
	 * @param filename
	 * @return a list of users loaded from a XML file
	 * @throws Exception
	 */
	public HttpEntity<List<UserRestDTO>> loadUserDataFromXMLFile(@RequestBody String filename) throws Exception {
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("inputFile"));

		if (StringUtils.isEmpty(loadUserDataDTO.getFileName()))
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		userService.usersPopulateDatabaseFromFile(loadUserDataDTO.getFileName());
		List<UserRestDTO> usersList = userService.listAllUsersToUI();
		for (UserRestDTO userRestDTO : usersList) {
			userRestDTO.add(
					linkTo(methodOn(LoadDataControllerREST.class).loadUserDataFromXMLFileRest(filename)).withSelfRel());
		}
		return new ResponseEntity<>(usersList, HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
	@PostMapping(value = "/users/populate")
	public HttpEntity<List<UserRestDTO>> loadUserDataFromXMLFileRest(@RequestBody String body) throws Exception {

		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(body);
		loadUserDataDTO.setFileName(obj.getString("filename"));
		String url = obj.getString("path");

		if (StringUtils.isEmpty(loadUserDataDTO.getFileName()))
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		InputStream inputStream = getInputStreamFromUrl(url);
		File file = writeFileWithInputStreamFromUrl(inputStream, loadUserDataDTO.getFileName());

		if (file.getName().startsWith("Utilizador")) {
			userService.usersPopulateDatabaseFromFileRest(file.getAbsolutePath());
			List<UserRestDTO> usersList = userService.listAllUsersToUI();
			for (UserRestDTO userRestDTO : usersList) {
				userRestDTO.add(
						linkTo(methodOn(LoadDataControllerREST.class).loadUserDataFromXMLFileRest(body)).withSelfRel());
			}
			Files.delete(file.toPath());
			return new ResponseEntity<>(usersList, HttpStatus.CREATED);
		}
		Files.delete(file.toPath());
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
	@PostMapping(value = "/projects/populate")
	public HttpEntity<ApiAuthResponse> loadProjectDataFromXMLFileRest(@RequestBody String body) throws Exception {

		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(body);
		loadUserDataDTO.setFileName(obj.getString("filename"));
		String url = obj.getString("path");

		if (StringUtils.isEmpty(loadUserDataDTO.getFileName()))
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		InputStream inputStream = getInputStreamFromUrl(url);
		File file = writeFileWithInputStreamFromUrl(inputStream, loadUserDataDTO.getFileName());

		if (file.getName().startsWith("Projeto")) {
			PopulateFileDataUI populateProject = new PopulateFileDataUI(userService, projectService, taskService,
					projectCollaboratorService);
			boolean importSuccess = populateProject.projectPopulateDatabaseFromFile(file.getAbsolutePath());

			Files.delete(file.toPath());
			if (!importSuccess) {
				return new ResponseEntity<>(new ApiAuthResponse(false, null), HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(new ApiAuthResponse(true, null), HttpStatus.CREATED);
		} else {
			Files.delete(file.toPath());
			return new ResponseEntity<>(new ApiAuthResponse(false, null), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Given a valid URL, this method returns an input stream from that URL.
	 * 
	 * @param location
	 *            string containing the url.
	 * @return
	 * @throws IOException
	 */
	public InputStream getInputStreamFromUrl(String location) throws IOException {
		URL url = new URL(location);
		URLConnection connection = url.openConnection();
		return connection.getInputStream();
	}

	/**
	 * Creates a file with the name given by parameter "name" and writes the
	 * contents of the inputStream into it.
	 * 
	 * @param inputStream
	 *            inputStream from URL
	 * @param name
	 *            file name
	 * @return
	 * @throws IOException
	 */
	public File writeFileWithInputStreamFromUrl(InputStream inputStream, String name) throws IOException {

		File file = new File(name);
		FileOutputStream fos = new FileOutputStream(file);

		try {
			byte[] buf = new byte[512];
			while (true) {
				int len = inputStream.read(buf);
				if (len == -1) {
					break;
				}
				fos.write(buf, 0, len);
			}
			inputStream.close();
			fos.flush();
			fos.close();
		} finally {
			fos.close();
		}
		return file;
	}
}
