package project.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.user.User;

@RestController
public class CollaboratorProjectListRestController {

	UserService userService;
	ProjectService projectService;
	
	public CollaboratorProjectListRestController( UserService userService, ProjectService projectService) {
		this.userService = userService;
		this.projectService = projectService;
	}
	
	/**
	 * Method to get information about projects from a user. 
	 * @param email
	 *            user's email to search for.
	 * @return list of project collaborator's projects.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	@GetMapping("/users/{id}/projects")
	public List<ProjectDTO> getCollaboratorProjects(@PathVariable("id") String email) {

		List<ProjectDTO> projectAsCollaborator = new ArrayList<>();
		
		User user = userService.searchUserByEmail(email);

		for ( ProjectDTO dto: projectService.listAllUserProjects(user)){
			
			if(!dto.getUserEmail().equals(user.getEmail())) {
				
				projectAsCollaborator.add(dto);
			}
			
		}
		return projectAsCollaborator;
	}
	
}
