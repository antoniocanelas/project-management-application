package project.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectDTO;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

@RestController

public class CollaboratorTaskRequestsControllerREST {
	
	ProjectService projectService;
	TaskProjectCollaboratorService taskProjectCollaboratorService;
	UserService userService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;

	public CollaboratorTaskRequestsControllerREST (ProjectService projectService, TaskProjectCollaboratorService
			taskProjectCollaboratorService, UserService userService, TaskService taskService,  ProjectCollaboratorService projectCollaboratorService) {
		this.projectService = projectService;
		this.taskProjectCollaboratorService = taskProjectCollaboratorService;
		this.userService = userService;
		this.taskService= taskService;
		this.projectCollaboratorService=projectCollaboratorService;
	}
	
	
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/users/{id}/requests")
	public HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> listColaboratorAssignmentRequests(@PathVariable("id") String email) {

		User user= userService.searchUserByEmail(email);
		
		List<ProjectDTO> userProjects=projectService.listAllUserProjects(user);
		
		 List<TaskCollaboratorRegistryRESTDTO> requestlist= new ArrayList<>();
		 
		 for(ProjectDTO p : userProjects) {
			 
			 requestlist.addAll( projectService.listAssignmentRequestsToDTO(p.getProjectId()));
			 requestlist.addAll( projectService.listRemovalRequestsToDTO(p.getProjectId()));
			requestlist.addAll(taskProjectCollaboratorService.listCompletedRequestsToDTO(p.getProjectId()));

		 }
		 
		 List<TaskCollaboratorRegistryRESTDTO> collabList= new ArrayList<>();

		 for(TaskCollaboratorRegistryRESTDTO tcr :requestlist) {
			 
			 
			 if(tcr.getProjectCollaboratorEmail().equals(email)){
				 
				 collabList.add(tcr);
				 
			 }
		 }
		return new ResponseEntity<>(collabList, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@PutMapping("/tasks/{taskId}/makeremoval")
	public HttpEntity<TaskCollaboratorRegistryRESTDTO> makeRemovalRequest(@PathVariable("taskId") String taskId, @RequestBody TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

		Task task= taskService.findTaskByID(taskId);

		ProjectCollaborator colab= projectCollaboratorService.getProjectCollaborator(task.getProject().getId(), taskCollaboratorRegistryRESTDTO.getProjectCollaboratorEmail());
		task.getLastTaskCollaboratorRegistryOf(colab).setRegistryStatus(RegistryStatus.REMOVALREQUEST);
		taskService.updateTask(task);
		taskCollaboratorRegistryRESTDTO.setRegistryStatus(task.getLastTaskCollaboratorRegistryOf(colab).getRegistryStatus().toString());

		return new ResponseEntity<>(taskCollaboratorRegistryRESTDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@PutMapping("/tasks/{taskId}/makeassignement")
	public HttpEntity<TaskCollaboratorRegistryRESTDTO> makeAssignementRequest(@PathVariable("taskId") String taskId, @RequestBody TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

		Task task= taskService.findTaskByID(taskId);

		ProjectCollaborator colab= projectCollaboratorService.getProjectCollaborator(task.getProject().getId(), taskCollaboratorRegistryRESTDTO.getProjectCollaboratorEmail());
		taskService.addProjectCollaboratorToTask(colab, task);
		task.getLastTaskCollaboratorRegistryOf(colab).setRegistryStatus(RegistryStatus.ASSIGNMENTREQUEST);
		taskService.updateTask(task);
		taskCollaboratorRegistryRESTDTO.setRegistryStatus(task.getLastTaskCollaboratorRegistryOf(colab).getRegistryStatus().toString());
		
		
		return new ResponseEntity<>(taskCollaboratorRegistryRESTDTO, HttpStatus.OK);
	}

}
