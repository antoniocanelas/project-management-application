package project.controllers.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskCostRestDTO;
import project.model.project.ProjectService;

@RestController
public class ReportCostRESTController {

    ProjectService projectService;

    public ReportCostRESTController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Method to display the running costs from a predetermined project. (Part of US390)
     *
     * @param projectId The project's ID
     * @return The calculated project's running costs.
     */
    @PreAuthorize("isProjectManager(#projectId) OR hasRole('ROLE_DIRECTOR')")
    @RequestMapping("/projects/{id}/costs")//DODO Check Permissions
    public ResponseEntity<Double> getProjectCostsSoFar(@PathVariable("id") String projectId) {

        return new ResponseEntity<>(projectService.calculateTotalCostSoFar(projectId), HttpStatus.OK);

    }
    
    /**
     * Requests projectService to set a project's cost calculation method.
     * @param projectId
     */
    @PreAuthorize("isProjectManager(#projectId) OR hasRole('ROLE_DIRECTOR')")
    @GetMapping("/projects/{projectId}/tasks/costs")
    public ResponseEntity<List<TaskCostRestDTO>> listProjectTasksCosts(@PathVariable("projectId") String projectId) {

        List<TaskCostRestDTO> taskCosts;
        taskCosts = projectService.getProjectTasksCostToDTO(projectId);
      
        return new ResponseEntity<>(taskCosts, HttpStatus.OK);

    }
}
