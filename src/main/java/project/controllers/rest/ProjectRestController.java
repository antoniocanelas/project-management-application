package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import project.dto.rest.AvailableCostOptionsDTO;
import project.dto.rest.ProjectDTO;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.TaskProjectCollaboratorService;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ProjectRestController {

    ProjectService projectService;
    TaskProjectCollaboratorService taskProjectCollaboratorService;

    public ProjectRestController(ProjectService projectService,TaskProjectCollaboratorService taskProjectCollaboratorService) {
        this.projectService = projectService;
        this.taskProjectCollaboratorService = taskProjectCollaboratorService;
    }

    /**
     * Lists all projects according to its active status.
     *
     * @param isActive active status
     * @return Response Entity with status and list of projects
     */
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    @GetMapping(value = "/projects", params = "isActive")
    public ResponseEntity<List<ProjectDTO>> listProjectsByActiveStatus(@RequestParam(value = "isActive", required = true) boolean isActive) {

        List<ProjectDTO> projectList = projectService.listProjectsByActiveStatus(isActive);

        for (ProjectDTO projDTO : projectList) {
            projDTO.add(linkTo(methodOn(ProjectRestController.class).getProjectDTOById(projDTO.getProjectId())).withSelfRel());
        }
        return new ResponseEntity<>(projectList, HttpStatus.OK);
    }

    /**
     * Gets a project with the given ID.
     *
     * @param projectId
     * @return the project and status OK if successful, HttpStatus.NOT_FOUND and empty body otherwise.
     */
    //@PreAuthorize("hasRole('ROLE_DIRECTOR')")//DODO Check Permissions
    @GetMapping(value = "/projects/{projectId}/show")
    public ResponseEntity<ProjectDTO> getProjectDTOById(@PathVariable("projectId") String projectId) {

    	Project p = projectService.getProjectByID(projectId);
       
        if (p == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        ProjectDTO projDTO = p.toDTO();
        int requests = 0; 
        requests = projectService.listAssignmentRequestsToDTO(p.getId()).size()
				+projectService.listRemovalRequestsToDTO(p.getId()).size()
				+taskProjectCollaboratorService.listCompletedRequestsToDTO(p.getId()).size();
        projDTO.setRequests(requests);
        projDTO.add(linkTo(methodOn(ProjectRestController.class).getProjectDTOById(projectId)).withSelfRel());
        return new ResponseEntity<>(projDTO, HttpStatus.OK);
    }

    /**
     * Gets all available cost options.
     *
     * @return the projects cost options and status OK if successful, HttpStatus.NOT_FOUND and empty body otherwise.
     */
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    @GetMapping(value = "/projects/availableCostOptions")
    public ResponseEntity<AvailableCostOptionsDTO> getAvailableCostOptions() {

        AvailableCostOptionsDTO availableCostOptions = projectService.listAllCostCalculationOptionsToDTO();

        availableCostOptions.add(linkTo(methodOn(ProjectRestController.class).getAvailableCostOptions()).withSelfRel());
        return new ResponseEntity<>(availableCostOptions, HttpStatus.OK);
    }
}
