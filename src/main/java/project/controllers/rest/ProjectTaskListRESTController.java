package project.controllers.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskRestDTO;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.security.CurrentUser;
import project.security.UserPrincipal;
import project.services.TaskService;
import project.util.TaskLinkBuilderFactory;

@RestController
public class ProjectTaskListRESTController {

	ProjectService projectService;
	TaskService taskService;

	String fileName = "taskactions.config";

	public ProjectTaskListRESTController(ProjectService projectService, TaskService taskService) {
		this.projectService = projectService;
		this.taskService = taskService;
	}

	/**
	 * Method to get information about completed tasks last month from a user.
	 * (Solves US210)
	 * 
	 * @param email
	 *            user's email to search for.
	 * @return object with a list of project collaborator's completed tasks last
	 *         month.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{projectId}/tasks/completedlastmonth")
	public ResponseEntity<List<TaskRestDTO>> getProjectCompletedTasksLastMonth(
			@PathVariable("projectId") String projectId, @CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);

		List<TaskRestDTO> taskRestDTOList = taskService.listProjectCompletedTasksLastMonth(project);

		for (TaskRestDTO taskRestDTO : taskRestDTOList) {
			taskRestDTO.add(linkTo(methodOn(ProjectTaskListRESTController.class)
					.getProjectCompletedTasksLastMonth(projectId, userPrincipal)).withSelfRel());

		}

		return new ResponseEntity<>(taskRestDTOList, HttpStatus.OK);
	}

	/**
	 * Method that lists a project's completed tasks. (Resolves US370)
	 *
	 * @param projectId
	 *            ID of project
	 * @return String containing the completed task's information.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{projectId}/tasks/completed")
	public ResponseEntity<List<TaskRestDTO>> listProjectCompletedTasks(@PathVariable("projectId") String projectId,
			@CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);

		List<TaskRestDTO> taskRestDTOList = taskService.listProjectCompletedTasks(project);

		for (TaskRestDTO taskRestDTO : taskRestDTOList) {
			taskRestDTO.add(linkTo(
					methodOn(ProjectTaskListRESTController.class).listProjectCompletedTasks(projectId, userPrincipal))
							.withSelfRel());

		}

		return new ResponseEntity<>(taskRestDTOList, HttpStatus.OK);
	}

	/**
	 * Method that lists a project's initiated but not completed tasks. (Responds to
	 * US372)
	 *
	 * @param projectId
	 *            ID of project
	 * @return list containing the pertinent tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{projectId}/tasks/initiated-not-completed")
	public ResponseEntity<List<TaskRestDTO>> getProjectInitiatedButNotCompletedTasks(
			@PathVariable("projectId") String projectId, @CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);

		List<TaskRestDTO> taskRestDTOList = taskService.listProjectIniciatedButNotCompletedTasks(project);

		for (TaskRestDTO taskRestDTO : taskRestDTOList) {
			taskRestDTO.add(linkTo(methodOn(ProjectTaskListRESTController.class)
					.getProjectInitiatedButNotCompletedTasks(projectId, userPrincipal)).withSelfRel());
			TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
			List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), fileName,
					userPrincipal.getListTasksWhereIsProjectManager(),
					userPrincipal.getListTasksWhereIsTaskCollaborator());
			for (Link link : links) {
				taskRestDTO.add(link);
			}
		}

		return new ResponseEntity<>(taskRestDTOList, HttpStatus.OK);
	}

	/**
	 * Method that lists a project's Cancelled tasks.
	 * 
	 * @param projectId
	 * @return list containing the pertinent tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{projectId}/tasks/cancelled")
	public ResponseEntity<List<TaskRestDTO>> getProjectCancelledTasks(@PathVariable("projectId") String projectId,
			@CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);

		List<TaskRestDTO> taskRestDTOList = taskService.listProjectCancelledTasks(project);

		for (TaskRestDTO taskRestDTO : taskRestDTOList) {
			taskRestDTO.add(linkTo(
					methodOn(ProjectTaskListRESTController.class).getProjectCancelledTasks(projectId, userPrincipal))
							.withSelfRel());

		}

		return new ResponseEntity<>(taskRestDTOList, HttpStatus.OK);
	}

	/**
	 * Method that lists a project's not completed and with predicted date of
	 * conclusion expired tasks.
	 * 
	 * @param projectId
	 * @return list containing the pertinent tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{projectId}/tasks/overdue")
	public ResponseEntity<List<TaskRestDTO>> getProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(
			@PathVariable("projectId") String projectId, @CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);

		List<TaskRestDTO> taskRestDTOList = taskService
				.listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);

		for (TaskRestDTO taskRestDTO : taskRestDTOList) {
			taskRestDTO.add(linkTo(methodOn(ProjectTaskListRESTController.class)
					.getProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(projectId, userPrincipal))
							.withSelfRel());
			TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
			List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), fileName,
					userPrincipal.getListTasksWhereIsProjectManager(),
					userPrincipal.getListTasksWhereIsTaskCollaborator());
			for (Link link : links) {
				taskRestDTO.add(link);
			}
		}

		return new ResponseEntity<>(taskRestDTOList, HttpStatus.OK);
	}

	/**
	 * Method that lists a project's initiated but not completed tasks. (Responds to
	 * US372)
	 *
	 * @param projectId
	 *            ID of project
	 * @return list containing the pertinent tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR') || hasRole('ROLE_DIRECTOR')")
	@GetMapping("/projects/{projectId}/tasks/all")
	public ResponseEntity<List<TaskRestDTO>> getProjectTasks(@PathVariable("projectId") String projectId,
			@CurrentUser UserPrincipal userPrincipal) {

		Project project = projectService.getProjectByID(projectId);
		List<TaskRestDTO> projectTasks = taskService.listTasksDTOByProject(project);

		for (TaskRestDTO taskRestDTO : projectTasks) {
			taskRestDTO
					.add(linkTo(methodOn(ProjectTaskListRESTController.class).getProjectTasks(projectId, userPrincipal))
							.withSelfRel());
			TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
			List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), fileName,
					userPrincipal.getListTasksWhereIsProjectManager(),
					userPrincipal.getListTasksWhereIsTaskCollaborator());
			for (Link link : links) {
				taskRestDTO.add(link);
			}
		}

		return new ResponseEntity<>(projectTasks, HttpStatus.OK);
	}

	/**
	 * Method to get tasks, from a specific project, without active project
	 * collaborators assigned (Solves US360)
	 *
	 * @param projectId
	 *            ID of project
	 * @return list of unassigned tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/projects/{Id}/tasks/unassigned")
	public HttpEntity<List<TaskRestDTO>> getProjectUnassignedTasks(@PathVariable("Id") String projectId,
			@CurrentUser UserPrincipal userPrincipal) {

		if (StringUtils.isEmpty(projectId)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		List<TaskRestDTO> unassignedTasks = projectService.listUnassignedTasksToDTO(projectId);

		for (TaskRestDTO taskRestDTO : unassignedTasks) {
			taskRestDTO.add(linkTo(
					methodOn(ProjectTaskListRESTController.class).getProjectUnassignedTasks(projectId, userPrincipal))
							.withSelfRel());
			TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
			List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), fileName,
					userPrincipal.getListTasksWhereIsProjectManager(),
					userPrincipal.getListTasksWhereIsTaskCollaborator());
			for (Link link : links) {
				taskRestDTO.add(link);
			}
		}
		return new ResponseEntity<>(unassignedTasks, HttpStatus.OK);
	}
}
