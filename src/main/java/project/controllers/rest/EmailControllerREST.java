package project.controllers.rest;

import org.springframework.web.bind.annotation.*;
import project.dto.EmailMessageDTO;
import project.services.EmailService;

@RestController
public class EmailControllerREST {

    private static final String EMAIL_SEND = "Email sent";
    private EmailService emailService;

    public EmailControllerREST(EmailService emailService) {
        this.emailService = emailService;
    }

    /**
     * Method to send an email to certain email.
     *
     * @param to
     * @return a message to inform user that an email was sent.
     */
    @GetMapping("/email/{emailAddress}")
    public String sendEmail(@PathVariable("emailAddress") String to) {
        emailService.sendSimpleMessage(to, "Hello World", "Hello World from Group 1 fabulous App");
        return EMAIL_SEND;
    }

    /**
     * Method to send an email to certain email.
     *
     * @return a message to inform user that an email was sent.
     */
    @GetMapping("/email")
    public String sendEmail() {
        emailService.sendSimpleMessage("switchisep@gmail.com", "Hello World", "Hello World from Group 1 fabulous App");
        return EMAIL_SEND;
    }

    /**
     * Method to send an email to certain email.
     *
     * @param emailMessageDTO
     * @return a message to inform user that an email was sent.
     */
    @PostMapping(path = "/email", consumes = "application/json", produces = "text/plain")
    public String sendEmail(@RequestBody EmailMessageDTO emailMessageDTO) {
        emailService.sendSimpleMessage(emailMessageDTO);
        return EMAIL_SEND;
    }
}
