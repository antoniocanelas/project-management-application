package project.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.rest.ReportRestDTO;
import project.services.TaskProjectCollaboratorService;

import java.util.List;

@RestController
public class CollaboratorReportsRestController {


    private TaskProjectCollaboratorService taskProjectCollaboratorService;

    public CollaboratorReportsRestController(TaskProjectCollaboratorService taskProjectCollaboratorService) {
        this.taskProjectCollaboratorService = taskProjectCollaboratorService;
    }

    @GetMapping(value = "/tasks/{taskId}/reports/{taskCollab}")
    public ResponseEntity<List<ReportRestDTO>> getReportByTaskAndCollab( @PathVariable String taskCollab, @PathVariable String taskId) {

        List<ReportRestDTO> reportList = taskProjectCollaboratorService.listReportsByTaskAndCollaborator(taskId, taskCollab);

        return new ResponseEntity<>(reportList, HttpStatus.OK);
    }



    @GetMapping(value = "/tasks/reports/{taskCollab}")
    public ResponseEntity<List<ReportRestDTO>> getReportByCollab(@PathVariable String taskCollab) {

        List<ReportRestDTO> reportList = taskProjectCollaboratorService.listReportsByCollaborator(taskCollab);

        return new ResponseEntity<>(reportList, HttpStatus.OK);
    }


}
