package project.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.ProjectDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.user.User;
import project.services.TaskProjectCollaboratorService;


@RestController
public class GetProjectManagerProjectsRESTController {

	UserService userService;
	ProjectService projectService;
	TaskProjectCollaboratorService taskProjectCollaboratorService;
	
	public GetProjectManagerProjectsRESTController(UserService userService, ProjectService projectService, TaskProjectCollaboratorService taskProjectCollaboratorService) {
		this.userService = userService;
		this.projectService = projectService;
		this.taskProjectCollaboratorService = taskProjectCollaboratorService;
	}
	
	
	/**
	 * Method to get information about projects as project manager from a user. 
	 * @param email
	 *            user's email to search for.
	 * @return list of project collaborator's projects.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	@GetMapping("/users/{id}/projectsaspm")
	public List<ProjectDTO> getProjectManagerProjects(@PathVariable("id") String email) {

		User user = userService.searchUserByEmail(email);
		int requests = 0;
		
		List<ProjectDTO> result=new ArrayList<>();
		ProjectDTO projectDTO;

		for(Project p:  projectService.projectManagerProjectsList(user)) {
			
			requests = projectService.listAssignmentRequestsToDTO(p.getId()).size()
					+projectService.listRemovalRequestsToDTO(p.getId()).size()
					+taskProjectCollaboratorService.listCompletedRequestsToDTO(p.getId()).size();
			projectDTO = p.toDTO();
			projectDTO.setRequests(requests);
			result.add(projectDTO);
			
			
		}
		
		return result;
	}
	

	
}
