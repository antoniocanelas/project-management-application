package project.controllers.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.ProjectService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.security.CurrentUser;
import project.security.UserPrincipal;
import project.services.TaskService;
import project.util.TaskLinkBuilderFactory;

@RestController
public class CollaboratorTaskListRESTController {

	UserService userService;
	ProjectService projectService;
	TaskService taskService;

	public CollaboratorTaskListRESTController(UserService userService, ProjectService projectService, TaskService taskService) {
		this.userService = userService;
		this.projectService = projectService;
		this.taskService = taskService;
	}

	/**
	 * Method to get information about pending tasks from a user. (Solves US203)
	 * @param email
	 *            user's email to search for.
	 * @return list of project collaborator's completed tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	@GetMapping("/users/{id}/tasks/pending")
	public ResponseEntity<List<TaskRestDTO>>getCollaboratorPendingTasks(@PathVariable("id") String email,@CurrentUser UserPrincipal userPrincipal) {

		User user = userService.searchUserByEmail(email);
		
		 List<TaskRestDTO> userPendingTasks=  projectService.listUserPendingTasksInAllProjects(user);
		 
		 for(TaskRestDTO taskRestDTO : userPendingTasks) {
	            taskRestDTO.add(linkTo(methodOn(CollaboratorTaskListRESTController.class).getCollaboratorPendingTasks(email, userPrincipal))
	                    .withSelfRel());
	            TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
	           
	            List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), "taskactions.config",
	                    userPrincipal.getListTasksWhereIsProjectManager(), userPrincipal.getListTasksWhereIsTaskCollaborator());
	            for (Link link : links) {
	                taskRestDTO.add(link);
	            }
	        }

	        return new ResponseEntity<>(userPendingTasks, HttpStatus.OK);
	    
	}
	
	
	/**
	 * Method to get information about all tasks from a user.
	 * @param email
	 *            user's email to search for.
	 * @return list of project collaborator's tasks.
	 */
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")
	@GetMapping("/users/{id}/tasks")
	public HttpEntity<List<TaskRestDTO>> getCollaboratorTasks(@PathVariable("id") String email,@CurrentUser UserPrincipal userPrincipal) {
		
		
		User user = userService.searchUserByEmail(email);

		List<TaskRestDTO> result= projectService.listUserAllTasksInAllProjects(user);
		
		result.addAll(projectService.listUserCompletedTasksInAllProjectsSortedByReverseOrderToDTO(user));
		
		
		 for(TaskRestDTO taskRestDTO : result) {
	            taskRestDTO.add(linkTo(methodOn(CollaboratorTaskListRESTController.class).getCollaboratorPendingTasks(email, userPrincipal))
	                    .withSelfRel());
	            TaskLinkBuilderFactory taskBuilder = new TaskLinkBuilderFactory(taskService);
	           
	            List<Link> links = taskBuilder.getAvailableLinks(taskRestDTO.getTaskId(), "taskactions.config",
	                    userPrincipal.getListTasksWhereIsProjectManager(), userPrincipal.getListTasksWhereIsTaskCollaborator());
	            for (Link link : links) {
	                taskRestDTO.add(link);
	            }
	        }

		
		return new ResponseEntity<>(result,HttpStatus.OK);
	}

	/**
	 * Method to get information about completed tasks from a user. (Solves US210)
	 * @param email user's email to search for.
	 * @return object with a list of project collaborator's completed tasks.
	 */
	@GetMapping("/users/{id}/tasks/completed")
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	public HttpEntity<List<TaskRestDTO>> getCollaboratorCompletedTasks(@PathVariable("id") String email) {

		if (StringUtils.isEmpty(email)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		List<TaskRestDTO> completedTasks = projectService.listUserCompletedTasksInAllProjectsSortedByReverseOrderToDTO(userService.searchUserByEmail(email));

		for (TaskRestDTO taskRestDTO : completedTasks) {
			taskRestDTO.add(linkTo(methodOn(CollaboratorTaskListRESTController.class).getCollaboratorCompletedTasks(email))
							.withSelfRel());
		}
		
		return new ResponseEntity<>(completedTasks, HttpStatus.OK);
	}
	
	
	/**
	 * Method to get information about completed tasks last month from a user. (Solves US210)
	 * @param email user's email to search for.
	 * @return object with a list of project collaborator's completed tasks last month.
	 */
	@GetMapping("/users/{id}/tasks/completedlastmonth")
	@PreAuthorize("hasRole('ROLE_COLLABORATOR')")//DODO Create Permission
	public HttpEntity<List<TaskRestDTO>> getCollaboratorCompletedTasksLastMonth(@PathVariable("id") String email) {

		if (StringUtils.isEmpty(email)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		List<Task> completedTasks = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(userService.searchUserByEmail(email));
		List<TaskRestDTO> completedTasksDto = new ArrayList<>();

		for (Task task : completedTasks) {
			
			completedTasksDto.add(task.toDTO());
		}
		
		for (TaskRestDTO taskRestDTO : completedTasksDto) {
			taskRestDTO.add(linkTo(methodOn(CollaboratorTaskListRESTController.class).getCollaboratorCompletedTasks(email))
							.withSelfRel());
		}
		
		return new ResponseEntity<>(completedTasksDto, HttpStatus.OK);
	}
	
	
	/**
     * Method to list all reports related to certain task.
     *
     * @param taskId
     * @return a list of reports.
     */
    @GetMapping(value = "/users/{userId}/recentActivity")
    @PreAuthorize("hasRole('ROLE_COLLABORATOR')")
    public ResponseEntity<List<TaskRestDTO>> listCollaboratorTasksOrderByLastReport(@PathVariable String userId) {
    	
    	List<TaskRestDTO> recentAtivityList = new ArrayList<>();
    	User u = userService.searchUserByEmail(userId);
    	List<TaskCollaboratorRegistry> userTcrList = projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(u);
    	
    	for(TaskCollaboratorRegistry tcr : userTcrList)
    	{
    		TaskRestDTO taskDTO;
    		Task t = taskService.findTaskByID(tcr.getTaskId());
    		taskDTO = t.toDTO();
    		if (tcr.getLastReport() != null)
    		{
    			taskDTO.setLastReportHours(tcr.getLastReport().getQuantity());
        		taskDTO.setLastReportDate(tcr.getLastReport().getStartDate());
        		taskDTO.setLastReportCreationDate(tcr.getLastReport().getReportCreationDate());
    		}

    		recentAtivityList.add(taskDTO);
    	}
    	
        return new ResponseEntity<>(recentAtivityList,HttpStatus.OK);
    }
}
