package project.jparepositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role getOneByName(RoleName roleName);
    boolean existsByName(RoleName roleName);
}