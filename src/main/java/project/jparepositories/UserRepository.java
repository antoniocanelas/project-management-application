package project.jparepositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.user.User;
import project.model.user.UserIdVO;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface UserRepository extends JpaRepository<User, UserIdVO> {
    User getOneByUserIdVO(UserIdVO userIdVO);

    boolean existsByUserIdVO(UserIdVO userIdVO);
}