package project.ui.console;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.controllers.consolecontrollers.CreateProjectController;
import project.controllers.consolecontrollers.DirectorSetProjectCostCalculationOptions;
import project.controllers.consolecontrollers.ProjectController;
import project.controllers.consolecontrollers.UserRegistryController;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import system.dto.LoginTokenDTO;

@Component
public class DirectorUI {

	static Scanner scan = new Scanner(System.in);
	static PrintStream writer = new PrintStream(System.out);
	static boolean leaveUI = false;
	LoginTokenDTO loginDto;
	UserService userService;
	ProjectService projectService;
	@Autowired // Injectar pelo construtor da classe, não por este atributo!!!
	ProjectCollaboratorService projCollabRegistry;
	ProjectController projectController;
	UserRegistryController userRegController;
	CreateProjectController createProjectController;
	DirectorSetProjectCostCalculationOptions setProjectCalculationOptions;
	String separator1 = "______________________________________________________________________________";
	String separator2 = "_______________________________________________________________";

	public DirectorUI(LoginTokenDTO loginDto, UserService userService, ProjectService projectService) {
		this.loginDto = loginDto;
		this.userService = userService;
		this.projectService = projectService;
	}

	public void showMenu() {
		String separator = "___________________________________________________________________________";
		writer.println(separator);
		writer.println("\n                            DIRECTOR MENU");
		writer.println(separator);
		showAllOptions();
		writer.println(separator + "\n\n");
	}

	private static void showAllOptions() {
		writer.println("\n[1] - CREATE PROJECTS, DEFINE PROJECT MANAGERS AND COST CALCULATION OPTIONS");
		writer.println("[2] - CHANGE PROJECT MANAGER");
		writer.println("[3] - CHANGE PROJECT COST CALCULATION AVAILABLE OPTIONS");
		writer.println("[4] - LIST ALL ACTIVE PROJECTS");
		writer.println("[ ] - PRESS OTHER KEY TO GO BACK TO PREVIOUS MENU");
	}
	public synchronized void chooseOption() {
		CreateProjectController createProjController = new CreateProjectController(userService, projectService,
				projCollabRegistry);
		UserRegistryController userRegistryController = new UserRegistryController(userService);
		projectController = new ProjectController(userService, projectService, projCollabRegistry);
		setProjectCalculationOptions = new DirectorSetProjectCostCalculationOptions(projectService);
		
		do {

			showMenu();
			String choice = scan.nextLine();

			if ("1".equals(choice)) {
				writer.println("\nEnter a project ID:");
				String projectID = scan.nextLine();
				writer.println("Enter the project name:");
				String name = scan.nextLine();
				writer.println("Enter the project manager email:");
				String userEmail = scan.nextLine();
				List<String> chosenOptions = chooseCostCalculationOptions();
				writer.println(createProjController.addProject(projectID, name, userEmail));
				setProjectCalculationOptions.setProjectListOfCostCalculationOptions(projectID, chosenOptions);
			} else if ("2".equals(choice)) {
				listAllActiveProjects();
				writer.println("\nEnter the project ID to change the project manager:");
				String projectId = scan.nextLine();
				writer.println(separator2);
				writer.println("\n                 AVAILABLE COMPANY COLLABORATORS");
				writer.println(separator2);
				writer.println(userRegistryController.searchUserByProfile("COLLABORATOR"));
				writer.println(separator2);
				writer.println("\nEnter the new project manager email:");
				String userEmail = scan.nextLine();
				writer.println(projectController.changeProjectManager(projectId, userEmail));
			} else if ("3".equals(choice)) {
				listAllActiveProjects();
				writer.println("\nEnter the project ID to change the available calculation options:");
				String projectId = scan.nextLine();
				writer.println("The current project's available calculation options are:\n");
				listProjectAvailableCalcOptions(setProjectCalculationOptions.listProjectCalculationOptions(projectId));
				List<String> chosenOptions = chooseCostCalculationOptions();
				setProjectCalculationOptions.setProjectListOfCostCalculationOptions(projectId, chosenOptions);
				writer.println("List of available calculation options successfully updated.");
			} else if ("4".equals(choice)) {
				listAllActiveProjects();
			} else {
				leaveUI = true;
			}
		} while (!leaveUI);
	}

	private List<String> chooseCostCalculationOptions() {

		String choice;
		List<String> chosenOptions = new ArrayList<>();
		List<String> calcOptions = setProjectCalculationOptions.listAllCostCalculationOptions();
		
		do {
			listCostCalculationOptions(calcOptions);

			writer.print("\nChoose calculation option: ");
			choice = scan.nextLine();

			if (!(isValidOption(choice, calcOptions.size())) || isAlreadyChosen(choice, chosenOptions, calcOptions)) {
				writer.println("\nInvalid choice. Please try again.");
				continue;
			}
			
			if (Integer.parseInt(choice) == (calcOptions.size() + 1)) {
				chosenOptions.clear();
				chosenOptions.addAll(calcOptions);
				writer.println("All calculation options were added to the list.");
				return chosenOptions;
			}
			
			if (Integer.parseInt(choice) != 0) {
				chosenOptions.add(calcOptions.get(Integer.parseInt(choice) - 1));
				writer.println(calcOptions.get(Integer.parseInt(choice) - 1) + " successfully added.");
			}
			
			if ((Integer.parseInt(choice) == 0) && chosenOptions.isEmpty()) {
				writer.println("You should choose at least one cost calculation option!");
			}

		} while (chosenOptions.isEmpty() || (!"0".equals(choice)));

		return chosenOptions;
	}

	private static void listCostCalculationOptions(List<String> calcOptions) {
		String separator = "__________________________________________";
		writer.println(separator);
		writer.println("\n      AVAILABLE CALCULATION OPTIONS");
		writer.println(separator + "\n");

		for (int i = 0; i < calcOptions.size(); i++) {
			writer.println("[" + (i + 1) + "] - " + calcOptions.get(i));
		}
		writer.println("[" + (calcOptions.size() + 1) + "] - CHOOSE ALL OPTIONS");
		writer.println("[0] - SAVE AND EXIT\n");
		writer.println(separator);
	}

	private static boolean isValidOption(String choice, int listSize) {

		for (int i = 0; i <= (listSize + 1); i++) {
			try {
				if (Integer.parseInt(choice) == i) {
					return true;
				}
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return false;
	}
	
	private static boolean isAlreadyChosen(String choice, List<String> chosenOptions, List<String> calcOptions) {
		int addAllOptionNumber = calcOptions.size() + 1;
		int choiceNumber = Integer.parseInt(choice);
		
		return((choiceNumber != 0) && (choiceNumber != addAllOptionNumber)) && 
				(chosenOptions.contains(calcOptions.get(choiceNumber - 1)));
	}
	
	private void listAllActiveProjects() {
		projectController = new ProjectController(userService, projectService, projCollabRegistry);
		writer.println(separator1);
		writer.println("\n                         LIST ALL ACTIVE PROJECTS");
		writer.println(separator1);
		writer.println((projectController.listAllActiveProjects()));
	}
	
	private static void listProjectAvailableCalcOptions(List<String> listProjectCalcOptions) {
		
		for (String option: listProjectCalcOptions) {
			System.out.println("-> " + option);
		}
	}
}
