package project.ui.console.populate;

import org.springframework.stereotype.Component;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ProjectOne {

    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    UserService userService;


    public ProjectOne(UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollaboratorService) {
        this.projectService= projectService;
        this.taskService=taskService;
        this.projectCollaboratorService=projectCollaboratorService;
        this.userService=userService;
    }



    public void projectOneData() {
        /**
         * Projects initialization, project collaborators assignment and task related
         * work
         **/



        // Create Project 1 and persist in database

        /*Project Data*/
        String projectId="10";
        String projectName="Spring Boot using Spring Security and JWT Token";
        String projectDescription="Implement authentication with a valid username and password. " +
                "The users should only enter the API after credentials validation" +
                "The API will generate a JWT authentication token and the the client side will use this token in the headers " +
                "of the requests to permit access to the resources" +
               " Configure role based authorization on the server side. Users can have one of 4 roles, each role limits access to API resources";


        projectService.addProject(projectId, projectName);
        Project project1 = projectService.getProjectByID(projectId);
        project1.setDescription(projectDescription);
        
        double globalBudget = 550;
        List<String> costOptions=  Arrays.asList("AverageCost", "MinimumTimePeriodCost", "MaximumTimePeriodCost");
        LocalDateTime projectStartDate = LocalDateTime.of(2017, 12, 18, 0, 0);
        
        project1.setGlobalBudget (globalBudget);
        project1.setActive();
        project1.setStartDate(projectStartDate);
        List<String> option = new ArrayList<>();
        option.addAll(costOptions);
        project1.setCostCalculationOptions(option);


        /*Project Team*/
        User projectManager = userService.searchUserByEmail("joaquim@switch.com");
        projectCollaboratorService.setProjectManager(project1, projectManager);


        User user1 = userService.searchUserByEmail("manuel@switch.com");
        double projCollab1Cost= 3 ;
        projectCollaboratorService.addProjectCollaborator(project1, user1,  projCollab1Cost);
        ProjectCollaborator projectCollaboratorManuel = project1.findProjectCollaborator(user1);
        projectCollaboratorManuel.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());




        User user2 = userService.searchUserByEmail("paulo@switch.com");
        double projCollab2Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1, user2,  projCollab2Cost);
        ProjectCollaborator projectCollaboratorPaulo = project1.findProjectCollaborator(user2);
        projectCollaboratorPaulo.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        User user3 = userService.searchUserByEmail("maria@switch.com");
        double projCollab3Cost= 5 ;
        projectCollaboratorService.addProjectCollaborator(project1, user3, projCollab3Cost);
        ProjectCollaborator projectCollaboratorMaria = project1.findProjectCollaborator(user3);
        projectCollaboratorMaria.getCostAndTimePeriodList().get(0).setStartDate(projectStartDate.toLocalDate());


        /*Project Tasks*/
        String taskGeneralDescription="Build an API to let users log in using their username/email and password. After validating user’s credentials, " +
                "the API should generate a JWT authentication token and return the token in the response.";

        //Task1
        String task1Title="Security basic initial setup";
        String task1Description="Add POM dependencies, Create a Role Repository, " +
                "In the Role repository is stored the possible roles an user can have \" +\n" +
                "                \", this roles will be used with spring security to manage authorities " +
                "and permissions";
        double task1UnitCost= 1.5;
        double task1EstimatedEffort= 2;
        taskService.addTask(project1, task1Title, task1Description, projectStartDate, projectStartDate.plusMonths(1), task1UnitCost, task1EstimatedEffort);
        Task task1 = project1.findTaskByTitle("Security basic initial setup");


        // task1 Collaborator 1
        taskService.addProjectCollaboratorToTask(projectCollaboratorPaulo, task1);
        TaskCollaboratorRegistry col2Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaboratorPaulo.getUser().getEmail());
        col2Task1Registry.setCollaboratorAddedToTaskDate(projectStartDate);

        //reports
        task1.addReport(projectCollaboratorPaulo, 0.3, projectStartDate.plusDays(5),  projectStartDate.plusDays(10));
        task1.addReport(projectCollaboratorPaulo, 0.8, projectStartDate.plusDays(15), projectStartDate.plusDays(20));


        // task1 Collaborator 2
        taskService.addProjectCollaboratorToTask(projectCollaboratorMaria, task1);
        TaskCollaboratorRegistry col3Task1Registry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaboratorMaria.getUser().getEmail());
        col3Task1Registry.setCollaboratorAddedToTaskDate(projectStartDate.plusDays(6));

        //reports
        task1.addReport(projectCollaboratorMaria, 0.5, projectStartDate.plusDays(17),  projectStartDate.plusDays(23));
        task1.addReport(projectCollaboratorMaria, 0.7,  projectStartDate.plusDays(28), projectStartDate.plusDays(40));


        task1.setTaskCompleted();
        task1.setEffectiveDateOfConclusion(projectStartDate.plusMonths(1));






        //Task2
        String task2Title="Configure Model Classes";

        double task2UnitCost= 1;
        double task2EstimatedEffort= 2;
        LocalDateTime taskPredictedStartDate= getStartDateFollowingTask(task1);
        taskService.addTask(project1, task2Title, taskGeneralDescription, taskPredictedStartDate,  taskPredictedStartDate.plusMonths(3), task2UnitCost, task2EstimatedEffort);
        Task task2 = project1.findTaskByTitle("Configure Model Classes");


        // task2 Collaborators 1

        taskService.addProjectCollaboratorToTask(projectCollaboratorManuel, task2);
        TaskCollaboratorRegistry col1Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaboratorManuel.getUser().getEmail());
        col1Task2Registry.setCollaboratorAddedToTaskDate(taskPredictedStartDate.plusDays(3));

        task2.addReport(projectCollaboratorManuel, 2,taskPredictedStartDate.plusDays(5), taskPredictedStartDate.plusDays(5));
        task2.addReport(projectCollaboratorManuel, 0.9, taskPredictedStartDate.plusDays(8), taskPredictedStartDate.plusDays(18));
        task2.addReport(projectCollaboratorManuel, 0.1, taskPredictedStartDate.plusMonths(1), taskPredictedStartDate.plusMonths(1).plusDays(5));



        // task2 Collaborators 2

        taskService.addProjectCollaboratorToTask(projectCollaboratorMaria, task2);
        TaskCollaboratorRegistry col3Task2Registry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaboratorMaria.getUser().getEmail());
        col3Task2Registry.setCollaboratorAddedToTaskDate(taskPredictedStartDate.plusMonths(1));


        task2.addReport(projectCollaboratorMaria, 1.2, taskPredictedStartDate.plusMonths(1).plusDays(3), taskPredictedStartDate.plusMonths(1).plusDays(4));


        //task2 status
        task2.setTaskCompleted();
        task2.setEffectiveDateOfConclusion(taskPredictedStartDate.plusMonths(3).plusDays(4));



        // task3

        String task3Title="Configuring Spring Security and JWT";

        double task3UnitCost= 4;
        double task3EstimatedEffort= 2;
        LocalDateTime task3PredictedStartDate= getStartDateFollowingTask(task2);

        taskService.addTask(project1, task3Title, taskGeneralDescription, task3PredictedStartDate, task3PredictedStartDate.plusMonths(5), task3UnitCost, task3EstimatedEffort);
        Task task3 = project1.findTaskByTitle(task3Title);
        taskService.addProjectCollaboratorToTask(projectCollaboratorManuel, task3);
        TaskCollaboratorRegistry col1Task3Registry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaboratorManuel.getUser().getEmail());
        col1Task3Registry.setCollaboratorAddedToTaskDate(task3PredictedStartDate.plusDays(4));

        //Requests
        task3.requestAddProjectCollaborator(projectCollaboratorPaulo);


        //Dependencies





        // task4
        String task4Title="Creating Custom Spring Security Classes";
        String task4Description="Build an API to let users log in using their username/email and password. After validating user’s credentials, " +
                "the API should generate a JWT authentication token and return the token in the response.";
        double task4UnitCost= 3;
        double task4EstimatedEffort=5;
        LocalDateTime task4PredictedStartDate= getStartDateFollowingTask(task3);


        taskService.addTask(project1, task4Title, task4Description, task4PredictedStartDate.plusDays(15), task4PredictedStartDate.plusMonths(2),  task4UnitCost,  task4EstimatedEffort);
        Task task4 = project1.findTaskByTitle(task4Title);
        taskService.addProjectCollaboratorToTask(projectCollaboratorManuel, task4);
        TaskCollaboratorRegistry col1Task4Registry = task4.getTaskCollaboratorRegistryByID(task4.getId() + "-" + projectCollaboratorManuel.getUser().getEmail());
        col1Task4Registry.setCollaboratorAddedToTaskDate(task4PredictedStartDate.plusDays(4));

        //Dependencies

        task4.addTaskDependency(task3);



        // task5

        String task5Title="Authentication Controller";
        String task5Description="Rest Controller.";
        double task5UnitCost= 4;
        double task5EstimatedEffort=4;

        LocalDateTime task5PredictedStartDate= getStartDateFollowingTask(task4);


        taskService.addTask(project1,  task5Title, task5Description, task5PredictedStartDate,  task5PredictedStartDate.plusMonths(1), task5UnitCost, task5EstimatedEffort);
        Task task5 = project1.findTaskByTitle(task5Title);
        taskService.addProjectCollaboratorToTask(projectCollaboratorMaria, task5);

        TaskCollaboratorRegistry col3Task5Registry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaboratorMaria.getUser().getEmail());
        col3Task5Registry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(2));

        task5.addReport(projectCollaboratorMaria, 4, LocalDateTime.now().minusDays(20), LocalDateTime.now().minusDays(7));
        task5.addReport(projectCollaboratorMaria, 6, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));





        // task6

        String task6Title="Enabling CORS";

        double task6UnitCost=2;
        double task6EstimatedEffort= 4;
        LocalDateTime task6PredictedStartDate= getStartDateFollowingTask(task5);


        taskService.addTask(project1, task6Title, taskGeneralDescription,  task6PredictedStartDate,  task6PredictedStartDate.plusMonths(6), task6UnitCost, task6EstimatedEffort);
        Task task6 = project1.findTaskByTitle(task6Title);

        taskService.addProjectCollaboratorToTask(projectCollaboratorMaria, task6);

        TaskCollaboratorRegistry col3Task6Registry = task6.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaboratorMaria.getUser().getEmail());
        col3Task6Registry.setCollaboratorAddedToTaskDate(task6PredictedStartDate);

        task6.addReport(projectCollaboratorMaria, 4, LocalDateTime.now().minusDays(16), LocalDateTime.now().minusDays(7));




        // task7

        String task7Title="Testing the Login and Signup APIs";
        String task7Description="Unit and Integrations tests should be used in this section. tests should be used in a TDD approach";
        double task7UnitCost= 1;
        double task7EstimatedEffort= 3;


        taskService.addTask(project1, task7Title, task7Description, projectStartDate, projectStartDate.plusYears(1), task7UnitCost, task7EstimatedEffort);

        Task task7 = project1.findTaskByTitle(task7Title);
        taskService.addProjectCollaboratorToTask(projectCollaboratorManuel, task7);

        TaskCollaboratorRegistry col1Task7Registry = task7.getTaskCollaboratorRegistryByID(task7.getId() + "-" + projectCollaboratorManuel.getUser().getEmail());
        col1Task7Registry.setCollaboratorAddedToTaskDate(projectStartDate);

        task7.addReport(projectCollaboratorManuel, 4, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(7));
        task7.requestRemoveProjectCollaborator(projectCollaboratorManuel);








        projectService.updateProject(project1);

        projectCollaboratorService.updateProjectCollaborator(projectCollaboratorManuel);
        projectCollaboratorService.updateProjectCollaborator(projectCollaboratorMaria);
        projectCollaboratorService.updateProjectCollaborator(projectCollaboratorPaulo);


        taskService.updateTask(task5);
        taskService.updateTask(task6);
        taskService.updateTask(task7);
        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);

    }


    public LocalDateTime getStartDateFollowingTask(Task task){
        if(task.getEfectiveDateOfConclusion()!= null){
            return task.getEfectiveDateOfConclusion();
        }
        else if(task.getPredictedDateOfConclusion()!= null){

            return task.getPredictedDateOfConclusion();
        }

        return null;
    }

}
