package project.ui.console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"project", "system"})
@EnableJpaRepositories({"project"})
@SpringBootApplication()
public class UI{

    public static void main(String[] args) {
        SpringApplication.run(UI.class);
    }
}