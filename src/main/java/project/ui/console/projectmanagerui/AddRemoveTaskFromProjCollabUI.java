package project.ui.console.projectmanagerui;

import project.controllers.consolecontrollers.AddOrRemoveTaskFromCollaboratorController;
import project.controllers.consolecontrollers.ProjectTaskListController;
import project.controllers.consolecontrollers.TaskCollaboratorRegistryController;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

public class AddRemoveTaskFromProjCollabUI {

    String lineBreak;
    ProjectTaskListController projectTaskListController;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    String projectId;
    LoginTokenDTO logintDto;
    String exit = "\n[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU\n";
    String insertTaskId = "Insert Task ID:";
    PrintStream writer = new PrintStream(System.out);
    ProjectManagerUI projManagerUI = new ProjectManagerUI(logintDto, userService, projectService, projectCollaboratorService, taskService);
    Scanner scan = new Scanner(System.in);
    TaskCollaboratorRegistryController taskCollabRegController;
    AddOrRemoveTaskFromCollaboratorController addRemoveTaskFromCollabController;

    public AddRemoveTaskFromProjCollabUI(String projectID, UserService userService, ProjectService projectService, TaskService taskService, ProjectCollaboratorService projectCollaboratorService) {

        projectId = projectID;
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectCollaboratorService = projectCollaboratorService;
        lineBreak = "\n\n\n\n";
    }

    private void showAssignmentOperationsMenu() {
        String separatorAddRemoveTaskFromProjCollab = "______________________________________________";
        String fourLineFeed = lineBreak;
        writer.println(fourLineFeed + separatorAddRemoveTaskFromProjCollab + "\n\n  MANAGE TASKS ASSIGNMENTS" + "\n" + separatorAddRemoveTaskFromProjCollab);
        writer.println("\n[1] - MANAGE TASK ASSIGNMENT REQUESTS");
        writer.println("[2] - MANAGE TASK REMOVAL REQUESTS");
        writer.println("[3] - ADD A TASK TO PROJECT COLLABORATOR");
        writer.println("[4] - REMOVE A TASK FROM PROJECT COLLABORATOR");
        writer.println(exit);
    }

    public void optionAssignmentRequests() {
        boolean loop = true;
        do {
            showAssignmentOperationsMenu();

            String option = scan.nextLine();
            switch (option) {
                case "1":
                    showAssignmentRequests();
                    break;
                case "2":

                    showRemovalRequests();
                    break;
                case "3":
                    addTaskToCollaboratorOpt();
                    break;
                case "4":
                    removeTaskFromProjectCollaborator();
                    break;
                default:
                    loop = false;
                    break;
            }
        } while (loop);
    }

    private void showAssignmentRequests() {
        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        boolean loop = true;
        do {
            String separatorAssignmentRequests = "___________________________________";
            String fourLinesFeed = lineBreak;
            writer.println(fourLinesFeed);
            writer.println(separatorAssignmentRequests);
            writer.println("\n LIST OF TASK ASSIGNMENT REQUESTS");
            writer.println(separatorAssignmentRequests);
            writer.println(taskCollabRegController.listAssignmentRequests(projectId));
            writer.println(separatorAssignmentRequests + "\n\n");
            writer.println("[1] - CANCEL AN ASSIGNMENT REQUEST");
            writer.println("[2] - APPROVE AN ASSIGNMENT REQUEST");
            writer.println(exit);
            String option = scan.nextLine();
            switch (option) {
                case "1":
                    cancelTaskAssignment();
                    break;
                case "2":
                    approveTaskAssignment();
                    break;
                default:
                    loop = false;
                    break;
            }
        } while (loop);
    }

    private void showRemovalRequests() {

        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        boolean loop = true;
        do {
            String separatorAssignmentRequests = "_______________________________________";
            String fourLinesFeed = lineBreak;
            writer.println(fourLinesFeed);
            writer.println(separatorAssignmentRequests);
            writer.println("\n LIST OF TASK ASSIGNMENT REQUESTS");
            writer.println(separatorAssignmentRequests);
            writer.println(taskCollabRegController.listRemovalRequests(projectId));
            writer.println(separatorAssignmentRequests + "\n\n");
            writer.println("[1] - CANCEL AN REMOVAL REQUEST");
            writer.println("[2] - APPROVE AN REMOVAL REQUEST");
            writer.println(exit);
            String option = scan.nextLine();
            switch (option) {
                case "1":
                    cancelTaskRemoval();
                    break;
                case "2":
                    approveTaskRemoval();
                    break;
                default:
                    loop = false;
                    break;
            }
        } while (loop);
    }

    private void removeTaskFromProjectCollaborator() {
        addRemoveTaskFromCollabController = new AddOrRemoveTaskFromCollaboratorController(userService, projectService, taskService, projectCollaboratorService);
        projectTaskListController = new ProjectTaskListController(projectService,taskService);
        String email;
        String separatorProjCollabAvailableToRemoveTask = "___________________________________________________";
        writer.println("\nProject task list\n");
        writer.println(projectTaskListController.listProjectTasksList(projectId) + "\n\n");
        writer.println(insertTaskId);
        String idTask = scan.next();
        writer.println("\n\n" + separatorProjCollabAvailableToRemoveTask);
        writer.println("\nCOLLABORATORS AVAILABLE TO REMOVE FROM TASK");
        writer.println(separatorProjCollabAvailableToRemoveTask);
        writer.println("\n" + addRemoveTaskFromCollabController.listActiveCollaboratorsInTask(projectId, idTask));
        writer.println(separatorProjCollabAvailableToRemoveTask);
        writer.println("\nInsert project collaborator email:");
        email = scan.next();
        writer.println("\n" + addRemoveTaskFromCollabController.removeTasktoCollaborator(projectId, idTask, email));
        scan.nextLine();
    }

    private void addTaskToCollaboratorOpt() {
        addRemoveTaskFromCollabController = new AddOrRemoveTaskFromCollaboratorController(userService, projectService, taskService, projectCollaboratorService);
        projectTaskListController = new ProjectTaskListController(projectService,taskService);
        String email;
        String separatorProjCollabAvailableToAddTask = "___________________________________________________";
        writer.println("\nProject task list\n");
        writer.println(projectTaskListController.listProjectTasksList(projectId) + "\n\n");
        writer.println(insertTaskId);
        String idTask = scan.next();
        writer.println("\n\n" + separatorProjCollabAvailableToAddTask);
        writer.println("\nPROJECT COLLABORATORS AVAILABLE TO ADD TO TASK");
        writer.println(separatorProjCollabAvailableToAddTask);
        writer.println("\n" + addRemoveTaskFromCollabController.listCollaboratorsNotInTask(projectId, idTask));
        writer.println(separatorProjCollabAvailableToAddTask);
        writer.println("\nInsert project collaborator email:\n");
        email = scan.next();
        writer.println("\n" + addRemoveTaskFromCollabController.addTaskToCollaborator(projectId, idTask, email));
        scan.nextLine();
    }

    private void approveTaskAssignment() {
        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        String taskId;
        String email;
        writer.println("Enter TaskID to approve task assignment to Project ROLE_COLLABORATOR\n");
        taskId = scan.nextLine();
        writer.println("Enter project collaborator email to approve assigment\n");
        email = scan.nextLine();
        writer.println(taskCollabRegController.approveAssignement(projectId, taskId, email));
    }

    private void cancelTaskAssignment() {
        String taskId;
        String email;
        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        writer.println("Enter TaskID to cancel task assignment to Project ROLE_COLLABORATOR\n");
        taskId = scan.nextLine();
        writer.println("Enter project collaborator email to cancel assigment\n");
        email = scan.nextLine();
        writer.println(taskCollabRegController.cancelAssignement(projectId, taskId, email));
    }

    private void approveTaskRemoval() {
        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        String taskId;
        String email;
        writer.println("Enter TaskID to approve task remova to Project ROLE_COLLABORATOR\n");
        taskId = scan.nextLine();
        writer.println("Enter project collaborator email to approve removal\n");
        email = scan.nextLine();
        writer.println(taskCollabRegController.approveRemoval(projectId, taskId, email));
    }

    private void cancelTaskRemoval() {
        String taskId;
        String email;
        taskCollabRegController = new TaskCollaboratorRegistryController(userService, projectService);
        writer.println("Enter TaskID to cancel task removal to Project ROLE_COLLABORATOR\n");
        taskId = scan.nextLine();
        writer.println("Enter project collaborator email to cancel removal\n");
        email = scan.nextLine();
        writer.println(taskCollabRegController.cancelRemoval(projectId, taskId, email));
    }
}


