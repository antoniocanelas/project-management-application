package project.ui.console.projectmanagerui;

import project.controllers.consolecontrollers.ProjectTaskListController;
import project.model.project.ProjectService;
import project.services.TaskService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

public class ListTasksUi {

    static LoginTokenDTO loginDto;
    static PrintStream writer = new PrintStream(System.out);
    ProjectService projectService;
    TaskService taskService;
    ProjectTaskListController projectTaskListController;
    String projectId;
    Scanner scan = new Scanner(System.in);

    public ListTasksUi(String projectId, ProjectService projectService, TaskService taskService) {
        this.projectId = projectId;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    static void showProjManagerListTaskOptionsMenu() {
        String separatorTaskLists = "____________________________________________";
        String fourLinesFeed = "\n\n\n\n";
        String twoLinesFeed = "\n\n";
        writer.println(fourLinesFeed + separatorTaskLists + twoLinesFeed + "               TASK LISTS" + "\n" + separatorTaskLists);
        writer.println("\n[1] - LIST UNASSIGNED TASKS");
        writer.println("[2] - LIST COMPLETED TASKS");
        writer.println("[3] - LIST INITIATED AND NOT COMPLETED TASKS");
        writer.println("[4] - LIST NOT INITIATED TASKS");
        writer.println("[5] - LIST NOT COMPLETED AND OVERDUE TASKS");
        writer.println("[6] - LIST CANCELLED TASKS");
        writer.println("[7] - LIST ALL PROJECT TASKS");
        writer.println("\n[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU\n");
    }

    public void tasksOptions() {
        boolean loop = true;
        projectTaskListController = new ProjectTaskListController(projectService,taskService);
        do {
            showProjManagerListTaskOptionsMenu();
            String choice = scan.nextLine();

            switch (choice) {
                case "1":
                    writer.println(projectTaskListController.lisProjectManagerTasksNoCollaborator(projectId));
                    break;
                case "2":
                    writer.println(projectTaskListController.listProjectManagerCompletedTasks(projectId));
                    break;
                case "3":
                    writer.println(projectTaskListController.listProjectManagerGetInitiatedButNotCompletedTasks(projectId));
                    break;
                case "4":
                    writer.println(projectTaskListController.listProjectManagerGetNotInitiatedTasks(projectId));
                    break;
                case "5":
                    writer.println(projectTaskListController
                            .listProjectManagerNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(projectId));
                    break;
                case "6":
                    writer.println(projectTaskListController.listProjectManagerCancelledTasks(projectId));
                    break;
                case "7":
                    writer.println(projectTaskListController.listProjectTasksListFull(projectId));
                    break;
                default:
                    loop = false;

            }
        } while (loop);

    }
}
