package project.ui.console.projectmanagerui;

import project.controllers.consolecontrollers.AddOrRemoveProjectCollaboratorController;
import project.controllers.consolecontrollers.ProjectUserListController;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

public class ProjectCollaboratorListAddRemoveUi {

    static final String ESPACE = "\n\n\n\n";
    static String projectId;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    AddOrRemoveProjectCollaboratorController addRemoveProjectCollabController;
    ProjectUserListController projectUserListController;
    LoginTokenDTO loginDto;
    PrintStream writer = new PrintStream(System.out);
    Scanner scan = new Scanner(System.in);

    public ProjectCollaboratorListAddRemoveUi(String projectId, UserService userService, ProjectService projectService, ProjectCollaboratorService projectCollaboratorService, TaskService taskService) {
        ProjectCollaboratorListAddRemoveUi.projectId = projectId;
        this.userService = userService;
        this.projectService = projectService;
        this.projectCollaboratorService = projectCollaboratorService;
        this.taskService = taskService;
    }

    public void showCollaboratorsOptionsMenu() {

        String separatorManageCollaboratorsMenu = "__________________________________________";
        writer.println(ESPACE + separatorManageCollaboratorsMenu);
        writer.println("\n         MANAGE COLLABORATORS");
        writer.println(separatorManageCollaboratorsMenu + "\n");
        writer.println("[1] ADD PROJECT COLLABORATOR");
        writer.println("[2] REMOVE PROJECT COLLABORATOR");
        writer.println("[3] LIST UNASSIGNED PROJECT COLLABORATORS");
        writer.println("[4] LIST ALL PROJECT COLLABORATORS");
        writer.println("\n[ ] OTHER KEY TO GO BACK TO PREVIOUS MENU\n");
    }

    public void optionslistInsertAndRemoveProjCollaborator() {
        boolean loop = true;
        addRemoveProjectCollabController = new AddOrRemoveProjectCollaboratorController(userService, projectService, projectCollaboratorService, taskService);
        projectUserListController = new ProjectUserListController(userService, projectService, projectCollaboratorService);
        do {
            showCollaboratorsOptionsMenu();
            String choice = scan.nextLine();

            switch (choice) {

                case "1":
                    addProjectCollaboratorToProj();
                    break;

                case "2":
                    writer.println("\nInsert ROLE_COLLABORATOR email:");
                    String email = scan.nextLine();
                    writer.println(addRemoveProjectCollabController.removeProjectCollaborator(projectId, email));
                    break;

                case "3":
                    String separatorListUnassignedProjectCollaboratorsMenu = "_____________________________________________________";
                    writer.println(ESPACE + separatorListUnassignedProjectCollaboratorsMenu);
                    writer.println("\n       LIST OF UNASSIGNED PROJECT COLLABORATORS");
                    writer.println(separatorListUnassignedProjectCollaboratorsMenu + "\n" + projectUserListController.getUnassignedProjectCollaboratorList(projectId));
                    break;
                case "4":
                    String separatorListAllProjectsCollaboratorsMenu = "_________________________________";
                    writer.println(ESPACE + separatorListAllProjectsCollaboratorsMenu);
                    writer.println("\nLIST OF ALL PROJECT COLLABORATORS");
                    writer.println(separatorListAllProjectsCollaboratorsMenu + "\n" + projectUserListController.listActiveProjectCollaborators(projectId));
                    break;

                default:
                    loop = false;
                    break;

            }
        } while (loop);

    }

    private void addProjectCollaboratorToProj() {
        addRemoveProjectCollabController = new AddOrRemoveProjectCollaboratorController(userService, projectService, projectCollaboratorService, taskService);

        String separatorListOfCollaboratorsMenu = "___________________________________________";
        writer.println(ESPACE + separatorListOfCollaboratorsMenu);
        writer.println("\n         LIST OF COLLABORATORS");
        writer.println(separatorListOfCollaboratorsMenu + "\n");
        writer.println(addRemoveProjectCollabController.listCollaboratorsNotInProject(projectId) + "\n\n");
        writer.println("Insert collaborator email:\n");
        String userEmail = scan.nextLine();
        writer.println("\nInsert collaborator cost:\n");
        String userCost = scan.nextLine();
        double cost = Double.parseDouble(userCost);
        writer.println(addRemoveProjectCollabController.addProjectCollaborator(projectId, userEmail, cost) + "\n\n");
    }
}