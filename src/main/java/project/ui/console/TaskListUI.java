package project.ui.console;

import org.springframework.stereotype.Component;

import project.controllers.consolecontrollers.*;
import project.model.UserService;
import project.model.project.ProjectService;
import project.services.TaskService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class TaskListUI {

    static ProjectTaskListController projectTaskListController;
    static PrintStream writer = new PrintStream(System.out);
    static Scanner scan = new Scanner(System.in);
    Logger logger = Logger.getAnonymousLogger();
    EditTaskListController editTaskListController;
    LoginTokenDTO loginDto;
    String showPendingTasksMessage = "Your pending tasks: \n";
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    CollaboratorTaskListController collaboratorTaskListController;
    ReportController reportController;
    CollaboratorHoursTaskController collaboratorHoursTaskController;
    String fourLinesFeed = "\n\n\n\n";

    public TaskListUI(LoginTokenDTO logindto, UserService userService, ProjectService projectService, TaskService taskService) {
        this.loginDto = logindto;
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    private static int validateReportsHours(String hours) {
        int parseHours = -1;

        try {
            parseHours = Integer.parseInt(hours);
            if (parseHours <= 0) {
                writer.println("\nInvalid number of hours.");
                return -1;
            }
        } catch (NumberFormatException nfe) {
            writer.println("\nInvalid number of hours.");
        }
        return parseHours;
    }

    public static LocalDate validateReportsDate(String date)  {

        LocalDate reportDate = LocalDate.of(0, 1, 1);

        String[] dateSplited = date.split("/");
        int day = Integer.parseInt(dateSplited[0]);
        int month = Integer.parseInt(dateSplited[1]);
        int year = Integer.parseInt(dateSplited[2]);

        if (day < 1 || day > 31 || year <= 0) {
            return reportDate;
        } else if (month < 1 || month > 12) {
            return reportDate;
        } else {
            reportDate = LocalDate.of(year, month, day);
        }

        return reportDate;
    }

    public void us203ListCollaboratorPendentTasks() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        String separator = "_____________________________________________________________";
        writer.println(separator);
        writer.println("\n                 LIST PENDING TASKS");
        writer.println(separator + "\n");
        writer.println(collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()));
        writer.println(separator + "\n\n\n");
    }

    public void us370ListProjectFinishedTaks() {

        writer.println(projectTaskListController.listProjectManagerCompletedTasks("1"));
    }

    public void us210ListCollaboratorFinishedTaks() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        String separator = "_____________________________________________________________";
        writer.println(separator);
        writer.println("\n                  LIST COMPLETED TASKS");
        writer.println(separator + "\n\n");
        writer.println(collaboratorTaskListController.getCollaboratorCompletedTasks(loginDto.getEmail()));
        writer.println(separator + fourLinesFeed);
    }

    public void us211ListCollaboratorFinishedTaksLastMonth() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        String separator = "___________________________________________________________";
        writer.println(separator);
        writer.println("\n             LIST COMPLETED TASKS LAST MONTH");
        writer.println(separator + "\n");
        writer.println(collaboratorTaskListController.getCollaboratorConcludedTasksLastMonth(loginDto.getEmail()));
        writer.println(separator + fourLinesFeed);

    }

    public void us215calculateCollaboratorsHoursCompletedTasksLastMonthInAllProjects() {
        collaboratorHoursTaskController = new CollaboratorHoursTaskController(userService, projectService);
        String separator = "_____________________________________________________";
        writer.println(separator);
        writer.println("\n    CONSULT TOTAL TIME SPENT IN TASKS LAST MONTH");
        writer.println(separator + "\n");
        writer.println(collaboratorHoursTaskController.calculateCollaboratorsHoursCompletedTasksLastMonthInAllProjects((loginDto.getEmail())));
        writer.println(separator + fourLinesFeed);
    }

    public void us216calculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjects() {
        collaboratorHoursTaskController = new CollaboratorHoursTaskController(userService, projectService);
        String separator = "_____________________________________________________";
        writer.println(separator);
        writer.println("\n   CONSULT AVERAGE TIME SPENT IN TASKS LAST MONTH");
        writer.println(separator + "\n");
        writer.println(
                collaboratorHoursTaskController.calculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjects(loginDto.getEmail()));
        writer.println(separator + fourLinesFeed);
    }

    public void listAllTasks() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        String separator = "______________________________________________________________";
        writer.println("\n\n" + separator);
        writer.println("\n                    LIST ALL TASKS");
        writer.println(separator + "\n");
        writer.println(collaboratorTaskListController.getCollabaratorsTasksInAllProjects(loginDto.getEmail()));
        writer.println(separator + fourLinesFeed);

    }

    public void markTaskCompleted() {
        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        writer.println(showPendingTasksMessage);
        writer.println(collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()));

        writer.println("Insert task ID to be marked completed: ");
        String taskID = scan.nextLine();

        writer.println(editTaskListController.setTaskCompleted(loginDto.getEmail(), taskID));
    }

    public void removeTask() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        if (collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()).isEmpty()) {
            writer.println("You have no pending tasks.");
            return;
        }
        writer.println(showPendingTasksMessage);
        writer.println(collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()));

        writer.println("Insert task ID to remove: ");
        String taskID = scan.nextLine();

        writer.println(editTaskListController.requestRemoveTaskFromProjectCollaborator(loginDto.getEmail(), taskID));
    }

    public void us208EditReport() {

        String reports;

        reportController = new ReportController(userService, projectService, taskService);
        showCollaboratorsPendingTasks();

        writer.println("Insert task ID to see its reports: ");
        String taskID = scan.nextLine();

        reports = getCollaboratorReports(taskID);

        if (reports.isEmpty()) {
            writer.println("You don't have reports associated with this task.\n");
            return;
        }

        writer.println(reports);

        writer.println("Choose report ID to edit: ");
        String reportID = scan.nextLine();

        if (!isValidReportID(loginDto.getEmail(), taskID, reportID)) {
            return;
        }

        double hoursInReport = reportController.findReportByID(loginDto.getEmail(), taskID, reportID);

        writer.println("The number of hours in this report is " + hoursInReport);
        writer.println("Enter new number of hours: ");
        String hours = scan.nextLine();

        hoursInReport = validateReportsHours(hours);

        if ((int) hoursInReport != -1)
            writer.println(reportController.us208EditReport(loginDto.getEmail(), taskID, reportID, hoursInReport));
    }

    public void addReport() {

        reportController = new ReportController(userService, projectService, taskService);

        int reportHours;
        LocalDateTime reportStartDate;
        LocalDateTime reportEndDate;
        
        showCollaboratorsPendingTasks();

        writer.println("Insert task ID to add report to: ");
        String taskID = scan.nextLine();

        writer.println("Insert number of hours to add: ");
        String hours = scan.nextLine();

        writer.println("Insert report's start date (dd/mm/yyyy)");
        String startDate = scan.nextLine();
        
        writer.println("Insert report's end date (dd/mm/yyyy)");
        String endDate = scan.nextLine();

        reportHours = validateReportsHours(hours);
        if (reportHours == -1)
            return;

        try {
            reportStartDate = validateReportsDate(startDate).atTime(0, 0);
        } catch (Exception e) {
            logger.log(Level.INFO, "an exception was thrown", e);
            writer.println("Invalid start date.\n");
            return;
        }
        
        try {
            reportEndDate = validateReportsDate(endDate).atTime(0, 0);
        } catch (Exception e) {
            logger.log(Level.INFO, "an exception was thrown", e);
            writer.println("Invalid end date.\n");
            return;
        }

        if (reportStartDate.getYear() == 0 || reportEndDate.getYear() == 0) {
            writer.println("\nInvalid date.");
            return;
        }

        writer.println(reportController.addReport(loginDto.getEmail(), taskID, reportHours, reportStartDate, reportEndDate));
        writer.println(reportController.listAllReportsByThisCollaborator(loginDto.getEmail(), taskID));
    }

    private boolean isValidReportID(String email, String taskID, String reportID) {

        reportController = new ReportController(userService, projectService, taskService);

        if ((int) (reportController.findReportByID(email, taskID, reportID)) == -1) {
            writer.println("Invalid report ID.");
            return false;
        }
        return true;
    }

    private void showCollaboratorsPendingTasks() {
        collaboratorTaskListController = new CollaboratorTaskListController(userService, projectService, taskService);
        if (collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()).isEmpty()) {
            writer.println("You have no pending tasks");
        } else {
            writer.println(showPendingTasksMessage);
            writer.println(collaboratorTaskListController.getCollaboratorPendentTasks(loginDto.getEmail()));
        }
    }

    private String getCollaboratorReports(String taskID) {

        String listReports;

        listReports = reportController.listAllReportsByThisCollaborator(loginDto.getEmail(), taskID);

        return listReports;
    }

    public void addTask() {

        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        String projID;
        String taskID;
        String addTaskMessage;

        listAllCollaboratorProjects(loginDto.getEmail());

        projID = chooseProject();

        if (projID != null) {
            listPossibleTasksToBeAssigned(projID);
            taskID = chooseTask(projID);

            if (taskID != null) {
                addTaskMessage = editTaskListController.requestAddProjectCollaboratorToTask(loginDto.getEmail(), projID, taskID);
                writer.println(addTaskMessage);
            } else {
                writer.println("Invalid task ID.\n");
            }
        } else {
            writer.println("Invalid project ID.\n");
        }
    }

    public void listAllCollaboratorProjects(String email) {

        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        writer.println(editTaskListController.listAllUserProjects(email));
    }

    private String chooseProject() {

        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        String projectID;
        writer.println("\nInsert project ID to choose one of its tasks: ");
        projectID = scan.nextLine();
        return (editTaskListController.validateProject(projectID, loginDto.getEmail()));
    }

    private void listPossibleTasksToBeAssigned(String projID) {

        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        writer.println("Tasks currently on project " + projID + ":\n");
        writer.println(editTaskListController.listAllProjectPendingTasks(projID));
    }

    public String chooseTask(String projID) {

        editTaskListController = new EditTaskListController(userService, projectService, taskService);
        String taskID;
        writer.println("\nInsert task ID be assigned to: ");
        taskID = scan.nextLine();
        return editTaskListController.validateTaskID(projID, taskID);
    }
}
