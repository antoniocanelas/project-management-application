package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.controllers.consolecontrollers.UserAddAddressController;
import project.controllers.consolecontrollers.UserDataController;
import project.model.UserService;
import project.model.project.ProjectService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

@Component
public class UserDataUI {

    static PrintStream writer = new PrintStream(System.out);
    static Scanner scan = new Scanner(System.in);
    static boolean leaveUI = false;
    UserService userService;
    ProjectService projectService;
    UserAddAddressController userAddAddressController;
    UserDataController userDataController;
    LoginTokenDTO loginTokenDto;
    @Autowired
    CollaboratorUI collUi;
    String emptyLines = "\n\n\n\n";

    public UserDataUI(LoginTokenDTO ltdto, UserService userService, ProjectService projectService) {
        this.loginTokenDto = ltdto;
        this.userService = userService;
        this.projectService = projectService;
    }

    public void home() {
        do {
            showMenu();
            chooseOption();
        } while (!leaveUI);
    }

    public void showMenu() {

        writer.println("[1] - CHECK PERSONAL DATA");
        writer.println("[2] - EDIT PERSONAL DATA");
        writer.println("[3] - ADD NEW ADDRESS");
        writer.println("[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU");
    }

    public synchronized void chooseOption() {
        userDataController = new UserDataController(userService);
        String choice = scan.nextLine();
        String separatorEditPersonalData = "____________________________________________";
        String separatorCheckPersonalData = "_________________________________________________________________";
        if ("1".equals(choice)) {
            writer.println("\n\n" + separatorCheckPersonalData);
            writer.println("\n                    CHECK PERSONAL DATA");
            writer.println(separatorCheckPersonalData + "\n");
            writer.println("Name: " + userDataController.getName(loginTokenDto.getEmail()) + "\n");
            writer.println("Phone: " + userDataController.getPhone(loginTokenDto.getEmail()) + "\n");
            writer.println("Email: " + loginTokenDto.getEmail() + "\n");
            writer.println("Tax Payer Id: " + userDataController.getTpi(loginTokenDto.getEmail()) + "\n");
            writer.println("Birth Date: " + userDataController.getBirthDate(loginTokenDto.getEmail()) + "\n");
            writer.println("Address list: " + userDataController.getAddressList(loginTokenDto.getEmail()) + "\n");
            writer.println(separatorCheckPersonalData + "\n\n");

        } else if ("2".equals(choice)) {
            writer.println(emptyLines + separatorEditPersonalData);
            writer.println("\n             EDIT PERSONAL DATA");
            writer.println(separatorEditPersonalData + "\n");
            writer.println("[1] - EDIT NAME");
            writer.println("[2] - EDIT ADDRESS");
            writer.println("[ ] - OTHER KEY TO GO BACK TO PREVIOUS MENU");
            writer.println(separatorEditPersonalData + "\n");
            String choiceFieldToEdit = scan.nextLine();
            fieldToEdit(choiceFieldToEdit);
        } else if ("3".equals(choice)) {

            addAddress();

        } else {
            leaveUI = false;

            collUi.goBackToLevel1 = true;
        }
    }

    public void fieldToEdit(String choice2) {

        userDataController = new UserDataController(userService);
        String separatorEditName = "______________________________________";
        String separatorEditAddressMenu1 = "______________________________________________________";
        String separatorEditAddressMenu2 = "_____________________________";

        if ("1".equals(choice2)) {
            writer.println(separatorEditName);
            writer.println("\n             EDIT NAME");
            writer.println(separatorEditName);
            writer.println("\nCurrent name = " + userDataController.getName(loginTokenDto.getEmail()));
            writer.println("Insert new name: ");

            String name = scan.nextLine();

            writer.println("\nConfirm new name " + name + "?\n" + "Yes[1] or No[2] ");

            String choice3 = scan.nextLine();

            if ("1".equals(choice3)) {

                userDataController.editName(name, loginTokenDto.getEmail());
                loginTokenDto.setName(name);
                writer.println("NEW NAME = " + userDataController.getName(loginTokenDto.getEmail()));

            }
        } else if ("2".equals(choice2)) {
            writer.println(emptyLines + separatorEditAddressMenu1);
            writer.println("\n             EDIT ADDRESS");
            writer.println(separatorEditAddressMenu1);
            writer.println("\nChoose an address to edit by entering the address id:");
            writer.println(userDataController.getAddressList(loginTokenDto.getEmail()));

            String addressId = scan.nextLine();
            if (userDataController.getAddressId(loginTokenDto.getEmail(), addressId)!=null)
            {
            	 writer.println(emptyLines + separatorEditAddressMenu2);
                 writer.println("\n        EDIT ADDRESS");
                 writer.println(separatorEditAddressMenu2 + "\n");
                 writer.println("[1] - EDIT ADDRESS ID");
                 writer.println("[2] - EDIT ADDRESS STREET");
                 writer.println("[3] - EDIT ADDRESS CITY");
                 writer.println("[4] - EDIT ADDRESS POSTAL CODE");
                 writer.println("[5] - EDIT ADDRESS COUNTRY");

                 String choiceAddress = scan.nextLine();

                 chooseAddressFieldsTochange(addressId, choiceAddress);
            }else {
            	writer.println("\n\nERROR");
            	writer.println("Caused by: Address can't be edited because ID doesn't exist!");
            }
        
        } else
            writer.println("No name change. Current name = " + userDataController.getName(loginTokenDto.getEmail()));
    }

    public void chooseAddressFieldsTochange(String addressId, String choiceAddress) {

        String adressId = "\nTHE NEW ADDRESS ID IS: ";
        String newStreet = "\nTHE NEW STREET IS: ";
        String newCity = "\nTHE NEW CITY IS: ";
        String newPc = "\nTHE NEW POSTAL CODE IS: ";
        String newCountry = "\nTHE NEW COUNTRY IS: ";
        String separator = "____________________________________";

        if ("1".equals(choiceAddress)) {
            writer.println(emptyLines + separator);
            writer.println("\n        EDIT ADDRESS ID");
            writer.println(separator + "\n");
            writer.println("Please enter the new id:");
            String id = scan.nextLine();
            userDataController.editAddressId(loginTokenDto.getEmail(), addressId, id);
            writer.println(adressId + userDataController.getAddressId(loginTokenDto.getEmail(), id));

        }
        if ("2".equals(choiceAddress)) {
            writer.println(emptyLines + separator);
            writer.println("\n        EDIT ADDRESS STREET");
            writer.println(separator + "\n");
            writer.println("Please enter the new Street:");
            String street = scan.nextLine();
            userDataController.editAddressStreet(loginTokenDto.getEmail(), addressId, street);
            writer.println(newStreet + userDataController.getAddressStreet(loginTokenDto.getEmail(), addressId));
        }

        if ("3".equals(choiceAddress)) {
            writer.println(emptyLines + separator);
            writer.println("\n        EDIT ADDRESS CITY");
            writer.println(separator + "\n");
            writer.println("Please enter the new City");
            String city = scan.nextLine();
            userDataController.editAddressCity(loginTokenDto.getEmail(), addressId, city);
            writer.println(newCity + userDataController.getAddressCity(loginTokenDto.getEmail(), addressId));
        }
        if ("4".equals(choiceAddress)) {
            writer.println(emptyLines + separator);
            writer.println("\n     EDIT ADDRESS POSTAL CODE");
            writer.println(separator + "\n");
            writer.println("Please enter the new postal code");
            String postalCode = scan.nextLine();
            userDataController.editAddressPostalCode(loginTokenDto.getEmail(), addressId, postalCode);
            writer.println(newPc + userDataController.getAddressPostalCode(loginTokenDto.getEmail(), addressId));
        }
        if ("5".equals(choiceAddress)) {
            writer.println(emptyLines + separator);
            writer.println("\n       EDIT ADDRESS COUNTRY");
            writer.println(separator + "\n");
            writer.println("Please enter the new Country");
            String country = scan.nextLine();
            userDataController.editAddressCountry(loginTokenDto.getEmail(), addressId, country);
            writer.println(newCountry + userDataController.getAddressCountry(loginTokenDto.getEmail(), addressId));
        }
    }

    public void addAddress() {

        userAddAddressController = new UserAddAddressController(userService);
        String separator = "___________________________________________";
        writer.println(emptyLines + separator);
        writer.println("\n             ADD NEW ADDRESS");
        writer.println(separator + "\n");
        writer.println("Insert address fields\n");
        writer.println("Please enter the new id:");
        String id = scan.nextLine();
        writer.println("Please enter the new Street:");
        String street = scan.nextLine();
        writer.println("Please enter the new City:");
        String city = scan.nextLine();
        writer.println("Please enter the new postal code:");
        String postalCode = scan.nextLine();
        writer.println("Please enter the new Country:");
        String country = scan.nextLine();
        System.out.println(userAddAddressController.addAdress(loginTokenDto.getEmail(), id, street, postalCode, city, country));
    }
}