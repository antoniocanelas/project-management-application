package project.ui.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.controllers.consolecontrollers.UserAddAddressController;
import project.controllers.consolecontrollers.UserDataController;
import project.model.UserService;
import project.model.project.ProjectService;
import system.dto.LoginTokenDTO;

import java.io.PrintStream;
import java.util.Scanner;

@Component
public class RegisteredUserUI {

    static Scanner scan = new Scanner(System.in);
    static PrintStream writer = new PrintStream(System.out);
    UserService userService;
    ProjectService projectService;
    UserAddAddressController userAddAddressController = new UserAddAddressController(userService);
    boolean leaveUI;
    UserDataController userDataController;
    @Autowired
    UserDataUI userDataUi;
    private LoginTokenDTO loginTokenDto;

    public RegisteredUserUI(LoginTokenDTO ltdto, UserService userService, ProjectService projectService) {
        this.loginTokenDto = ltdto;
        this.userService = userService;
        this.projectService = projectService;
    }

    public LoginTokenDTO getLogindto() {
        return loginTokenDto;
    }

    public void home() {

        leaveUI = false;

        do {
        	String separatorRegisteredUserMenu = "____________________________________";
            writer.println("\n\n\n\n" + separatorRegisteredUserMenu);
            writer.println("\n         REGISTERED USER MENU");
            writer.println(separatorRegisteredUserMenu + "\n");
            userDataUi.showMenu();
            chooseFirstOption();
        } while (!leaveUI);
    }

    public synchronized void chooseFirstOption() {

        String choice = scan.nextLine();

        if ("1".equals(choice)) {
            userDataController = new UserDataController(userService);
            checkPersonalData();

        } else if ("2".equals(choice)) {

            showEditPersonalDataOptions();

        } else if ("3".equals(choice)) {

            userDataUi.addAddress();

        } else {

            System.out.println("\n" + getLogindto().getName() + " has logged off.\n\n");
            leaveUI = true;

        }
    }

    private void checkPersonalData() {

        writer.println("Name: " + userDataController.getName(loginTokenDto.getEmail()) + "\n");
        writer.println("Phone: " + userDataController.getPhone(loginTokenDto.getEmail()) + "\n");
        writer.println("Email: " + loginTokenDto.getEmail() + "\n");
        writer.println("Tax Payer Id: " + userDataController.getTpi(loginTokenDto.getEmail()) + "\n");
        writer.println("Birth Date: " + userDataController.getBirthDate(loginTokenDto.getEmail()) + "\n");
        writer.println("Address list: " + userDataController.getAddressList(loginTokenDto.getEmail()) + "\n");
    }

    private void showEditPersonalDataOptions() {

        writer.println("Choose an option:\n");
        writer.println("1: Edit name");
        writer.println("2: Edit Address");
        String choiceFieldToEdit = scan.nextLine();

        userDataUi.fieldToEdit(choiceFieldToEdit);
    }

	
}
