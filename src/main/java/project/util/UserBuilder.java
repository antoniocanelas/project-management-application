package project.util;

import project.dto.AddressDTO;
import project.dto.UserDTO;
import project.model.UserService;
import project.model.user.Address;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class for building a User object from a User Data Transfer Object (DTO)
 */
public class UserBuilder {

    UserService userService;

    public UserBuilder(UserService userService) {
        this.userService = userService;
    }

    protected UserBuilder() {
    }

    private static void buildAdressList(List<AddressDTO> addressList, User user) {
        String id;
        String street;
        String postalCode;
        String city;
        String country;
        for (int i = 1; i < addressList.size(); i++) {
            AddressDTO address = addressList.get(i);
            id = user.getName() + " address " + (i + 1);
            street = address.getStreet();
            postalCode = address.getPostalCodeCity() + "-" + address.getPostalCodeAddress();
            city = address.getCity();
            country = address.getCountry();
            user.addAddress(new Address(id, street, postalCode, city, country));
        }
    }

    /**
     * Creates a User object, populates it with information from user DTO and persists it in the database
     *
     * @param userDTO
     */
    public void createAndPersistUser(UserDTO userDTO) throws AddressException {

        //Get information from userDTO
        String name = userDTO.getName();
        String phone = userDTO.getPhone();
        String tin = "123456789"; //Hard coded taxpayer identification number
        String email = userDTO.getEmail();
        String password = userDTO.getPassword();
        LocalDate birthDate = LocalDate.of(1970, 1, 1); //Hard coded birth date

        //Get first address info from userDTO address list
        List<AddressDTO> addressList = userDTO.getAddressList();
        AddressDTO firstAddress = addressList.get(0);
        String id = name + " address " + "1";
        String street = firstAddress.getStreet();
        String postalCode = firstAddress.getPostalCodeCity() + "-" + firstAddress.getPostalCodeAddress();
        String city = firstAddress.getCity();
        String country = firstAddress.getCountry();

        //Create and get user
        userService.addUser(name, phone, email, tin, birthDate, id, street, postalCode, city, country);
        User user = userService.searchUserByEmail(email);

        //Set user password
        user.setPassword(password);

        //set approved date
        user.getUserLoginData().setApprovedDateTime(LocalDateTime.now());

        //Create address list
        buildAdressList(addressList, user);

        //Update user in database
        userService.updateUser(user);

    }
}