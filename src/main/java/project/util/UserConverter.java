package project.util;

import project.dto.UserRegistryDTO;

public interface UserConverter {

    UserRegistryDTO parseToUser(String inputFile);

}
