package project.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.util.StringUtils;

public final class UserConverterFactory {

	private static String version;
	private static Logger logger = Logger.getAnonymousLogger();
	private static String pathToMapperFile = System.getProperty("user.dir")
			+ "/src/main/resources/application.properties";

	private UserConverterFactory() {
	}

	public static UserConverter createUserConverter(String inputFileComplete) {
		String extension;
		String userConverterClassName;
		String inputFile;
		UserConverter userDefaultXmlConverter = new UserXmlConverter();

		inputFile = inputFileComplete.substring(inputFileComplete.lastIndexOf('/') + 1);
		extension = inputFile.substring(inputFile.lastIndexOf('.') + 1);

		Pattern pattern = Pattern.compile("Utilizador" + ".*v[0-9]{2}");
		Matcher matcher = pattern.matcher(inputFile);
		if (matcher.find()) {
			version = matcher.group();
		}
		try {
			userConverterClassName = readConverterClass(pathToMapperFile, extension, version);
		} catch (IOException ioe) {
			logger.log(Level.WARNING, "Wrong filename, returning default converter.", ioe);
			return userDefaultXmlConverter;
		}
		try {
			userDefaultXmlConverter = contructClassFromString(userConverterClassName);
		} catch (Exception e) {
			logger.log(Level.WARNING, "Wrong class name, returning default converter.", e);
			return userDefaultXmlConverter;
		}
		return userDefaultXmlConverter;
	}

	private static UserConverter contructClassFromString(String userConverterClassName) throws Exception {

		Class<?> clazz;

		clazz = Class.forName(UserConverter.class.getPackage().getName() + "." + userConverterClassName);
		Constructor<?> construct = clazz.getConstructor();
		return (UserConverter) construct.newInstance();
	}

	private static String readConverterClass(String pathToMapperFile, String extension, String version)
			throws IOException {
		String converterUserClassName;
		String versionAndExtensionName;

		versionAndExtensionName = (StringUtils.isEmpty(version) ? extension : extension + "." + version);

		BufferedReader brUser = getBufferedReader(pathToMapperFile);

		String strUser;
		while ((strUser = brUser.readLine()) != null) {
			if (strUser.contains(versionAndExtensionName)) {
				converterUserClassName = strUser.substring(strUser.lastIndexOf('=') + 1);
				return converterUserClassName;
			}
		}
		brUser.close();
		return null;
	}

	private static BufferedReader getBufferedReader(String pathToFile) throws IOException {

		Path path = Paths.get(pathToFile);

		return Files.newBufferedReader(path);
	}

	public static void setPathToMapperFile(String pathToMapperFile) {
		UserConverterFactory.pathToMapperFile = pathToMapperFile;
	}
}
