package project.util;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.hateoas.Link;

import project.dto.rest.CreateReportRestDTO;
import project.dto.rest.EditReportQuantityDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.dto.rest.TaskRestDTO;
import project.services.TaskService;

/**
 * Class that, given a task, gets its available actions taking into
 * consideration the task's state and constructs the appropriate HATEOAS links.
 * It starts by acquiring the available actions from the task's state class, 
 * it then searches for them and extracts its relative elements from a given file.
 * With that, it constructs, using reflection, the available HATEOAS links and returns 
 * a list with them to the caller.
 *
 */
public class TaskLinkBuilderFactory {

	private Logger logger = Logger.getAnonymousLogger();
	private Map<String, String> classesPath;
	String packageOfRestControllers = "project.controllers.rest"; // not very viable

	private TaskService taskService;

	public TaskLinkBuilderFactory(TaskService taskService) {
		this.taskService = taskService;
		populateClassesPath();
	}

	/**
	 * Populates hash map classesPath. Given the name of the class for KEY, the
	 * method fills respective VALUE with the classes' path. It will be used later
	 * with reflection.
	 */
	private void populateClassesPath() {
		classesPath = new HashMap<>();
		classesPath.put("String", String.class.getName());
		classesPath.put("TaskProjectCollaboratorRestDTO", TaskProjectCollaboratorRestDTO.class.getName());
		classesPath.put("CreateReportRestDTO", CreateReportRestDTO.class.getName());
		classesPath.put("Integer", Integer.class.getName());
		classesPath.put("EditReportQuantityDTO", EditReportQuantityDTO.class.getName());
		classesPath.put("TaskRestDTO", TaskRestDTO.class.getName());
	}

	/**
	 * Given a task id and a filename, this method creates the appropriate HATEOAS links
	 * based upon the task's state.
	 * @param taskId
	 * @param filename
	 * @return
	 */
	public List<Link> getAvailableLinks(String taskId, String filename, List<String>listTasksWhereIsProjectManager, 
			List<String>listTasksWhereIsTaskCollaborator) {

		List<String> correspondingElements;
		List<Link> availableLinks = new ArrayList<>();

		List<String> availableActions = getTaskAvailableActions(taskId);

		try {
			correspondingElements = getElementsFromFile(availableActions, filename);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "No file by that name.", e);
			return availableLinks;
		}

		try {
			availableLinks = constructAvailableLinks(correspondingElements, taskId, listTasksWhereIsProjectManager, 
					listTasksWhereIsTaskCollaborator);
		} catch (ClassNotFoundException e) {
			logger.log(Level.SEVERE, "Parse error. No class found", e);
			return availableLinks;
		} catch (NoSuchMethodException me) {
			logger.log(Level.SEVERE, "Parse error. No such method.", me);
			return availableLinks;
		}
		
		return availableLinks;
	}

	/**
	 * Given task id, this method finds the task and its taskState and gets the list
	 * of the task's available actions, taking into consideration the task's state
	 * 
	 * @param taskId
	 *            Id of task
	 * @return a list of available actions.
	 */
	private List<String> getTaskAvailableActions(String taskId) {

		return taskService.findTaskByID(taskId).getTaskState().listAvailableActions();
	}

	/**
	 * Method that reads the content of a file (given its file name), compares it to
	 * task's available actions and, if found, add the respective elements to a
	 * list. The elements are needed to construct the HATEOAS links.
	 * 
	 * @param availableActions
	 *            list of actions to be searched for inside the file.
	 * @param filename
	 *            name of file.
	 * @return a list of elements pertaining to the actions found.
	 * @throws IOException
	 *             (file not found)
	 */
	private static List<String> getElementsFromFile(List<String> availableActions, String filename) throws IOException{

		List<String> correspondingElements = new ArrayList<>();

		for (String availableAction : availableActions) {
			BufferedReader taskActionsConfig = getBufferedReader(filename);
			addRespectiveElement(correspondingElements, taskActionsConfig, availableAction);
			taskActionsConfig.close();
		}

		return correspondingElements;
	}

	/**
	 * Gets a BufferedReader instance given a file's name.
	 * 
	 * @param filename
	 * @return
	 */
	private static BufferedReader getBufferedReader(String filename) throws IOException {

		Path configFilePath = Paths
				.get(System.getProperty("user.dir") + "/src/main/resources/project/modelconfigs/" + filename);
		return Files.newBufferedReader(configFilePath);
	}

	/**
	 * For each line in the file, if KEY available action is found, it adds its
	 * respective VALUE element to the list correspondingElements.
	 * 
	 * @param correspondingElements
	 *            list to add elements to.
	 * @param taskActionsConfig
	 *            BufferedReader instance to read the file.
	 * @param availableAction
	 *            parameter to be searched for
	 * @throws IOException
	 */
	private static void addRespectiveElement(List<String> correspondingElements, BufferedReader taskActionsConfig,
			String availableAction) throws IOException {

		String fileLine;

		while ((fileLine = taskActionsConfig.readLine()) != null) {
			if (fileLine.contains(availableAction)) {
				String respectiveElement = fileLine.substring(fileLine.lastIndexOf('=') + 1);
				if (!respectiveElement.isEmpty()) {
					correspondingElements.add(respectiveElement);
				}
			}
		}
	}

	/**
	 * Constructs through reflection all available links using the action's corresponding elements.
	 * @param correspondingElements Elements that are used to construct the links.
	 * @param taskId Id of task to be passed to method for link construction.
	 * @return list of all available links.
	 * @throws Exception
	 */
	private List<Link> constructAvailableLinks(List<String> correspondingElements, String taskId, 
			List<String>listTasksWhereIsProjectManager, List<String>listTasksWhereIsTaskCollaborator) throws
	ClassNotFoundException, NoSuchMethodException {
		
		String pertinentRole = getPertinentRoleFromTaskId(taskId, listTasksWhereIsProjectManager, listTasksWhereIsTaskCollaborator);
		
		List<Link> availableLinks = new ArrayList<>();
		String controllerClassName;
		String controllerMethodName;
		String hateoasType;
		String hateoasRel;
		String pertinentRoleInFile;
		
		for (String element : correspondingElements) {
			
			String[] parsedValues = getStringValues(element);
			controllerClassName = parsedValues[0];
			controllerMethodName = parsedValues[1];
			hateoasType = parsedValues[parsedValues.length - 3];
			hateoasRel = parsedValues[parsedValues.length - 2];
			pertinentRoleInFile = parsedValues[parsedValues.length - 1];
			
			if ("BOTH".equals(pertinentRole) || pertinentRole.equals(pertinentRoleInFile)) {
				Class<?> controllerClass = getControllerClass(controllerClassName);
				Class<?>[] parameterArray = getMethodParameterTypes(parsedValues);
				Method method = getRelatedMethod(controllerClass, controllerMethodName, parameterArray);
				Link link = linkTo(method, taskId).withRel(hateoasRel).withType(hateoasType);
				availableLinks.add(link);
			}

		}

		return availableLinks;
	}
	/**
	 * Method to find if a task is part of a project where a user is project manager, if it is part of a 
	 * project where the user is a task collaborator or both.
	 * @param taskId Id of task
	 * @param listTasksWhereIsProjectManager list of tasks where the user is project manager
	 * @param listTasksWhereIsTaskCollaborator list of tasks in which the user is task collaborator
	 * @return string identifying the type of role the user has for this task. In the case of having both
	 * roles - task collaborator and project manager - at the same time, the BOTH is returned.
	 */
	public String getPertinentRoleFromTaskId(String taskId, List<String>listTasksWhereIsProjectManager, List<String>listTasksWhereIsTaskCollaborator) {
		
		if(listTasksWhereIsProjectManager.contains(taskId) && listTasksWhereIsTaskCollaborator.contains(taskId)) {
			return "BOTH";
		}
		if(listTasksWhereIsProjectManager.contains(taskId)) {
			return "MANAGER";
		}
		if(listTasksWhereIsTaskCollaborator.contains(taskId)) {
			return "COLLABORATOR";
		}
		return "PROJECTCOLLABORATOR";
	}
	
	/**
	 * Given an element which is encoded as CSV, this method separates the values
	 * and returns an array of strings with the values. First value is authcontroller
	 * class name, second value is the associated method, next is its parameter
	 * types and the last value is HATEOAS rel.
	 * 
	 * @param element
	 *            element to be parsed
	 * @return
	 */
	private static String[] getStringValues(String element) {

		return element.split(",");
	}

	/**
	 * Gets the class associated with the class name.
	 * 
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 */
	private Class<?> getControllerClass(String className) throws ClassNotFoundException {

		return Class.forName(packageOfRestControllers + "." + className);
	}

	/**
	 * Given an array of strings, this method extracts and constructs, through
	 * reflection, each class that is a parameter type of a method.
	 * 
	 * @param parsedValues
	 *            array of strings with all parameter type names.
	 * @return
	 * @throws ClassNotFoundException
	 */
	private Class<?>[] getMethodParameterTypes(String[] parsedValues) throws ClassNotFoundException {

		int numberOfMethodParameters = parsedValues.length - 5;
		Class<?>[] parameterList = new Class<?>[numberOfMethodParameters];

		for (int i = 2; i < parsedValues.length - 3; i++) {
			parameterList[i - 2] = Class.forName(classesPath.get(parsedValues[i]));
		}
		return parameterList;
	}

	/**
	 * Given a class, a parameter array and the method name, it constructs the method through reflection. 
	 */
	private static Method getRelatedMethod(Class<?> controllerClass, String methodName, Class<?>[] parameterArray)
			throws NoSuchMethodException {
		return controllerClass.getMethod(methodName, parameterArray);
	}
    

    
}
