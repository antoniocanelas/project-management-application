package project.util;

import project.dto.UserRegistryDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserXmlConverter implements UserConverter {

    Logger logger = Logger.getAnonymousLogger();

    @Override
    public UserRegistryDTO parseToUser(String inputXml) {

        UserRegistryDTO userRegistryDTO = null;

        try {

            File file = new File(inputXml);
            JAXBContext jaxbContext = JAXBContext.newInstance(UserRegistryDTO.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            userRegistryDTO = (UserRegistryDTO) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return userRegistryDTO;
    }

}
