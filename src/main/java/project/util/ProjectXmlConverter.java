package project.util;

import project.dto.ProjectDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProjectXmlConverter implements ProjectConverter {

    Logger logger = Logger.getAnonymousLogger();

    @Override
    public ProjectDTO parseToProject(String inputXml) {

        ProjectDTO projectDTO = null;

        try {

            File file = new File(inputXml);
            JAXBContext jaxbContext = JAXBContext.newInstance(ProjectDTO.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            projectDTO = (ProjectDTO) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        } 
        return projectDTO;
    }

}
