package project.util;

import project.dto.*;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.Project.ProjectStatus;
import project.model.project.Project.ProjectUnits;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Class for building a Project object from a Project Data Transfer Object (DTO)
 */
public class ProjectBuilder {

    ProjectService projectService;
    UserService userService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    String lastTimeCost = "LastTimePeriodCost";

    public ProjectBuilder(ProjectService projectService, UserService userService,
                          ProjectCollaboratorService projectCollaboratorService, TaskService taskService) {
        this.projectService = projectService;
        this.userService = userService;
        this.projectCollaboratorService = projectCollaboratorService;
        this.taskService = taskService;
    }

    public ProjectBuilder() {
    }

    private static LocalDateTime validateDate(String date) {

        boolean dateIsAvailable = !(("").equals(date) || ("N/A").equalsIgnoreCase(date)
                || ("NA").equalsIgnoreCase(date));
        String[] dateStringArray;
        LocalDateTime parsedDate = null;

        if (dateIsAvailable) {
            dateStringArray = date.split("/");
            parsedDate = LocalDateTime.of(Integer.parseInt(dateStringArray[2]), Integer.parseInt(dateStringArray[1]),
                    Integer.parseInt(dateStringArray[0]), 0, 0);
        }
        return parsedDate;
    }

    private static void buildProjectCollaboratorCostAndTimePeriodList(ProjectCollaborator projectCollaborator,
                                                                      List<ProjectCollaboratorCostAndTimePeriodDTO> projectCollaboratorCostAndTimePeriodListDTO) {

        double cost;

        List<ProjectCollaboratorCostAndTimePeriod> projCollabCostAndTimeList = new ArrayList<>();

        for (ProjectCollaboratorCostAndTimePeriodDTO projectCollaboratorCostAndTimePeriodDTO : projectCollaboratorCostAndTimePeriodListDTO) {

            cost = Double.parseDouble(projectCollaboratorCostAndTimePeriodDTO.getCost());
            ProjectCollaboratorCostAndTimePeriod projectCollaboratorCostAndTimePeriod = new ProjectCollaboratorCostAndTimePeriod(
                    cost);

            String reportDate = projectCollaboratorCostAndTimePeriodDTO.getStartDate();

            LocalDate projectCollabStartDate = null;
            LocalDate projectCollabEndDate = null;

            LocalDateTime validatedDate = validateDate(reportDate);

            if (validatedDate != null) {
                projectCollabStartDate = validatedDate.toLocalDate();
            }

            reportDate = projectCollaboratorCostAndTimePeriodDTO.getEndDate();
            validatedDate = validateDate(reportDate);

            if (validatedDate != null) {
                projectCollabEndDate = validatedDate.toLocalDate();
            }
            projectCollaboratorCostAndTimePeriod.setEndDate(projectCollabEndDate);

            projectCollaboratorCostAndTimePeriod.setStartDate(projectCollabStartDate);
            projCollabCostAndTimeList.add(projectCollaboratorCostAndTimePeriod);

            projectCollaborator.setCostAndTimePeriodList(projCollabCostAndTimeList);
        }
    }

    /**
     * Creates a Project object, populates it with information from project DTO and
     * persists it in the database
     *
     * @param projectDTO
     */
    public boolean createAndPersistProject(ProjectDTO projectDTO) {
    		
    		for(ProjectCollaboratorDTO projectCollaboratorDTO: projectDTO.getProjectCollaboratorList()) {
    			if(userService.searchUserByEmail(projectCollaboratorDTO.getId()) == null) {
    				return false;
    			}
    		}
        // Get information from projectDTO
        String code = projectDTO.getId();
        String name = projectDTO.getName();

        // Create and get project
        projectService.addProject(code, name);
        Project project = projectService.getProjectByID(code);

        // Get information from projectDTO
        String description = projectDTO.getDescription();
        LocalDateTime startDate = validateDate(projectDTO.getStartDate());
        LocalDateTime finalDate = validateDate(projectDTO.getFinalDate());
        double globalBudget = projectDTO.getGlobalBudget();

        ProjectUnits unit;
        if (("PM").equalsIgnoreCase(projectDTO.getUnit())) {
            unit = ProjectUnits.PEOPLE_MONTH;
        } else {
            unit = ProjectUnits.HOURS;
        }

        ProjectStatus status;
        if (("Ativo").equalsIgnoreCase(projectDTO.getStatus())) {
            status = ProjectStatus.EXECUTION;
        } else {
            status = ProjectStatus.CLOSED;
        }

        String costCalculatorPersistence = costCalculationOptionsMapping(projectDTO.getCostCalculatorPersistence());

        // Set information in project
        project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinalDate(finalDate);
        project.setUnit(unit);
        project.setGlobalBudget(globalBudget);
        project.setStatus(status);
        project.setCostCalculatorPersistence(costCalculatorPersistence);
        project.getCostCalculator();

        // Set project's project manager
        projectCollaboratorService.setProjectManager(project,
                userService.searchUserByEmail(projectDTO.getProjectManager()));

        // Create project's cost calculation options
        List<String> calculateOptions = new ArrayList<>();
        calculateOptions.add("AverageCost");
        calculateOptions.add("FirstTimePeriodCost");
        calculateOptions.add(lastTimeCost);

        project.setCostCalculationOptions(calculateOptions);

        // Update project in database
        projectService.updateProject(project);

        // Create project's collaborators list and persist in database
        buildProjectCollaboratorList(projectDTO, project);

        // Create project's task list and persist in database
        buildTaskList(projectDTO, project);
        
        return true;
    }

    private void buildProjectCollaboratorList(ProjectDTO projectDTO, Project project) {

        double cost = 0.0;
        List<ProjectCollaboratorDTO> projectCollaboratorListDTO = projectDTO.getProjectCollaboratorList();

        for (ProjectCollaboratorDTO projectCollaboratorDTO : projectCollaboratorListDTO) {

            User user = userService.searchUserByEmail(projectCollaboratorDTO.getId());

            List<ProjectCollaboratorCostAndTimePeriodDTO> projectCollaboratorCostAndTimePeriodListDTO = projectCollaboratorDTO
                    .getCostAndTimePeriodList();
            projectCollaboratorService.addProjectCollaborator(project, user, cost);
            ProjectCollaborator projectCollaborator = projectCollaboratorService.getProjectCollaborator(project.getId(),
                    user.getEmail());

            buildProjectCollaboratorCostAndTimePeriodList(projectCollaborator,
                    projectCollaboratorCostAndTimePeriodListDTO);

            projectCollaboratorService.updateProjectCollaborator(projectCollaborator);
        }
    }

    private void buildTaskList(ProjectDTO projectDTO, Project project) {

        List<TaskDTO> taskDTOList = projectDTO.getTasks();
        for (TaskDTO taskDTO : taskDTOList) {
            taskService.addTask(project, taskDTO.getTitle(), taskDTO.getTaskId());
            Task task = taskService.findTaskByTitle(taskDTO.getTitle());

            task.setDescription(taskDTO.getDescription());
            task.setTaskStatePersisted("Created");
            task.setEstimatedEffort(taskDTO.getEstimatedEffort());
            task.setUnitCost(taskDTO.getUnitCost());

            task.setPredictedDateOfStart(validateDate(taskDTO.getPredictedDateOfStart()));
            task.setPredictedDateOfConclusion(validateDate(taskDTO.getPredictedDateOfConclusion()));

            // Add task dependencies
            buidTaskDependencies(taskDTO, task);

            // Add task taskCollaboratorRegistry
            buildTaskCollaboratorRegistry(project, taskDTO, task);

            task.setEffectiveDateOfConclusion(validateDate(taskDTO.getEffectiveDateOfConclusion()));
            taskService.updateTask(task);
        }
    }

    private void buidTaskDependencies(TaskDTO taskDTO, Task task) {
        List<String> taskDependenciesDTOList = taskDTO.getTaskDependencies();
        for (String taskDependencyId : taskDependenciesDTOList) {
            Task taskToAddDependency = taskService.findTaskByID(taskDependencyId);
            task.addTaskDependency(taskToAddDependency);
        }
    }

    private void buildTaskCollaboratorRegistry(Project project, TaskDTO taskDTO, Task task) {
        List<TaskCollaboratorDTO> taskCollaboratorsDTOList = taskDTO.getTaskCollaborators();

        for (TaskCollaboratorDTO taskCollaboratorDTO : taskCollaboratorsDTOList) {

            ProjectCollaborator projectCollaborator = projectCollaboratorService
                    .getProjectCollaborator(project.getId(), taskCollaboratorDTO.getId());
            taskService.addProjectCollaboratorToTask(projectCollaborator, task);
            List<TaskCollaboratorRegistryDTO> taskCollaboratorsRegistryDTOList = taskCollaboratorDTO
                    .getTaskCollaboratorLink();
            List<TaskCollaboratorRegistry> taskCollaboratorRegistries = task
                    .listTaskCollaboratorRegistry(projectCollaborator);
            Set<TaskCollaboratorRegistry> taskCollaboratorRegistriesSet = new LinkedHashSet<>(
                    taskCollaboratorRegistries);

            for (int j = 0; j < taskCollaboratorsRegistryDTOList.size(); j++) {

                TaskCollaboratorRegistryDTO collaboratorRegistryDTO = taskCollaboratorsRegistryDTOList.get(j);

                LocalDateTime startDate = validateDate(collaboratorRegistryDTO.getStartDate());
                LocalDateTime endDate = validateDate(taskDTO.getEffectiveDateOfConclusion());

                taskCollaboratorRegistries.get(j).setCollaboratorAddedToTaskDate(startDate);
                taskCollaboratorRegistriesSet.add(taskCollaboratorRegistries.get(j));

                List<ReportDTO> reportDTOList = collaboratorRegistryDTO.getReports();
                for (ReportDTO reportDTO : reportDTOList) {

                    LocalDateTime reportStartDate = validateDate(reportDTO.getStartDate());
                    LocalDateTime reportEndDate = validateDate(reportDTO.getEndDate());

                    double effort = reportDTO.getEffort();
                    taskCollaboratorRegistries.get(j).addReport(effort, reportStartDate, reportEndDate);

                    if (task.getEffectiveStartDate() == null) {
                        task.setEffectiveStartDate(reportStartDate);
                    }
                }
                taskCollaboratorRegistries.get(j).setCollaboratorRemovedFromTaskDate(endDate);

            }
        }

    }

    private String costCalculationOptionsMapping(String calcOptions) {

        if ("CI".equals(calcOptions)) {

            return "FirstTimePeriodCost";

        } else if ("CM".equals(calcOptions)) {

            return "AverageCost";
        }

        return lastTimeCost;
    }
}
