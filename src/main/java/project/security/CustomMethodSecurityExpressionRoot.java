package project.security;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class CustomMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
    ApplicationContext applicationContext;

    private Object filterObject;
    private Object returnObject;
    
    public CustomMethodSecurityExpressionRoot(Authentication authentication) {
        super(authentication);
    }

    public boolean isProjectManager(String projecId) {
        List<String> isProjectManagerList = ((UserPrincipal) this.getPrincipal()).getListProjectWhereProjectManager();
        for (String projectTestId : isProjectManagerList) {
            if (projecId.equals(projectTestId)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTaskCollaborator(String taskId) {
        List<String> listTasksWhereIsTaskCollaborator = ((UserPrincipal) this.getPrincipal()).getListTasksWhereIsTaskCollaborator();

        for (String taskTestId : listTasksWhereIsTaskCollaborator) {
            if (taskId.equals(taskTestId)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTaskBelongingToProjectWhereProjectManager(String taskId) {
        List<String> listTasksWhereIsProjectManager = ((UserPrincipal) this.getPrincipal()).getListTasksWhereIsProjectManager();

        for (String taskTestId : listTasksWhereIsProjectManager) {
            if (taskId.equals(taskTestId)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public Object getFilterObject() {
        return this.filterObject;
    }

    @Override
    public void setFilterObject(Object obj) {
        this.filterObject = obj;
    }

    @Override
    public Object getReturnObject() {
        return this.returnObject;
    }

    @Override
    public void setReturnObject(Object obj) {
        this.returnObject = obj;
    }

    @Override
    public Object getThis() {
        return this;
    }

}
