package project.payload;

public class ApiAuthResponse {
    private Boolean success;
    private String message;

    public ApiAuthResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ApiAuthResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ApiAuthResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}