package project.payload;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SignUpRequest {
    @NotBlank
    @Size(min = 2, max = 40)
    private String name;
    @NotBlank
    @Size(max = 40)
    @Email
    private String email;
    @NotBlank
    @Size(min = 6, max = 20)
    private String password;
    @NotBlank
    @Size(min = 6, max = 20)
    private String phone;
    @NotBlank
    @Size(min = 1, max = 9)
    private String taxPayerId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String birthDate;
    @NotBlank
    @Size(min = 1, max = 30)
    private String addressId;
    @NotBlank
    @Size(min = 3, max = 30)
    private String street;
    @NotBlank
    @Size(min = 4, max = 30)
    private String postalCode;
    @NotBlank
    @Size(min = 3, max = 20)
    private String city;
    @NotBlank
    @Size(min = 3, max = 20)
    private String country;
    @Size(min = 3, max = 20)
    private String profile;

    public SignUpRequest() {
        // Do nothing.
    }

    /**
     * Method to get name.
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method to set name.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Method to get email.
     * @return email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Method to set email.
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to get password.
     * @return password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Method to set password.
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Method to get phone.
     * @return phone.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Method to set phone.
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Method to get tax payer ID.
     * @return tax payer ID.
     */
    public String getTaxPayerId() {
        return taxPayerId;
    }

    /**
     * Method to set tax payer ID.
     * @param taxPayerId
     */
    public void setTaxPayerId(String taxPayerId) {
        this.taxPayerId = taxPayerId;
    }

    /**
     * Method to get birth date.
     * @return birth date.
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Method to set birth date.
     * @param birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Method to get address ID.
     * @return address ID.
     */
    public String getAddressId() {
        return addressId;
    }

    /**
     * Method to set address id.
     * @param addressId
     */
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    /**
     * Method to get street.
     * @return street.
     */
    public String getStreet() {
        return street;
    }

    /**
     * Method to set street.
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Method to get postal code.
     * @return postal code.
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Method to set postal code.
     * @param postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Method to get city.
     * @return city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Method to set city.
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Method to get country.
     * @return country.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Method to set country.
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Method to get profile.
     * @return profile.
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Method to set profile.
     * @param profile
     */
    public void setProfile(String profile) {
        this.profile = profile;
    }
}