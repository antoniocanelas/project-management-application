package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.rest.*;
import project.jparepositories.ProjectCollaboratorRepository;
import project.jparepositories.ProjectRepository;
import project.jparepositories.TaskRepository;
import project.jparepositories.UserRepository;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@Service
public class TaskProjectCollaboratorService {

    Logger logger = Logger.getAnonymousLogger();
    Task task;
    ProjectCollaborator projectCollaborator;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ProjectCollaboratorRepository projectCollaboratorRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserRepository userRepository;

    /**
     * Constructor for TaskProjectCollaboratorService.
     *
     * @param taskRepository
     * @param projectCollaboratorRepository
     */
    public TaskProjectCollaboratorService(TaskRepository taskRepository,
                                          ProjectCollaboratorRepository projectCollaboratorRepository, ProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectCollaboratorRepository = projectCollaboratorRepository;
        this.projectRepository = projectRepository;
    }

    /**
     * Method to add project collaborator to task.
     *
     * @param taskProjectCollaboratorRestDTO
     * @return true if he was added, false otherwise.
     */
    public boolean addProjectCollaboratorToTask(TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO) {

        String email = taskProjectCollaboratorRestDTO.getEmail();
        task = taskRepository.getOneByTaskId(taskProjectCollaboratorRestDTO.getTaskId());

        if (task == null) {
            return false;
        }

        String projectId = task.getProject().getId();
        projectCollaborator = getProjectCollaborator(projectId, email);

        if (!existsTaskAndProjectCollaborator(task, projectCollaborator)) {
            return false;
        }

        if (!task.hasProjectCollaborator(projectCollaborator)) {
            task.addProjectCollaborator(projectCollaborator);
            taskRepository.save(task);
            projectCollaboratorRepository.save(projectCollaborator);
            return true;
        }
        return false;
    }

    /**
     * Method to request to add project collaborator to task.
     *
     * @param taskProjectCollaboratorRestDTO
     * @return true if the request was sent, false otherwise.
     */
    public boolean requestAddProjectCollaboratorToTask(TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO) {
        String taskId = taskProjectCollaboratorRestDTO.getTaskId();
        projectCollaborator = projectCollaboratorRepository
                .getOneById(taskProjectCollaboratorRestDTO.getProjectCollaboratorId());
        task = taskRepository.getOneByTaskId(taskId);
        if ((projectCollaborator == null || task == null)) {
            return false;
        }

        TaskCollaboratorRegistry taskCollaboratorRegistry = getTaskCollaboratorRegistry(task, projectCollaborator);

        if (taskCollaboratorRegistry == null || taskCollaboratorRegistry.getRegistryStatus()
                .equals(TaskCollaboratorRegistry.RegistryStatus.REMOVALAPPROVED)) {
            if (taskCollaboratorRegistry == null) {
                taskCollaboratorRegistry = new TaskCollaboratorRegistry(projectCollaborator, taskId);
            }
            taskCollaboratorRegistry.setRegistryStatus(TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST);
            task.getTaskCollaboratorRegistryList().add(taskCollaboratorRegistry);
            taskRepository.save(task);
            return true;

        }
        return false;
    }

    /**
     * Method to remove project collaborator from task.
     *
     * @param taskProjectCollaboratorRestDTO
     * @param taskId
     * @return TaskProjectCollaboratorRestDTO.
     */
    public TaskProjectCollaboratorRestDTO removeProjectCollaboratorFromTaskRest(TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO, String taskId) {

        String email = taskProjectCollaboratorRestDTO.getEmail();

        task = taskRepository.getOneByTaskId(taskId);
        if (task == null) {
            return null;
        }

        String projectId = task.getProject().getId();
        projectCollaborator = getProjectCollaborator(projectId, email);

        if (!existsTaskAndProjectCollaborator(task, projectCollaborator)) {
            return taskProjectCollaboratorRestDTO;
        }

        if (task.hasProjectCollaborator(projectCollaborator)) {
            task.removeProjectCollaborator(projectCollaborator);
            projectCollaboratorRepository.save(projectCollaborator);
            taskProjectCollaboratorRestDTO.setTaskId(task.getId());
            taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        }
        return taskProjectCollaboratorRestDTO;
    }

    /**
     * Method to request to remove project collaborator from task.
     *
     * @param taskProjectCollaboratorRestDTO
     * @return true if the request was sent, false otherwise.
     */
    public boolean requestRemoveProjectCollaboratorFromTask(TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO) {
        projectCollaborator = projectCollaboratorRepository
                .getOneById(taskProjectCollaboratorRestDTO.getProjectCollaboratorId());
        task = taskRepository.getOneByTaskId(taskProjectCollaboratorRestDTO.getTaskId());

        if (!existsTaskAndProjectCollaborator(task, projectCollaborator)) {
            return false;
        }

        TaskCollaboratorRegistry taskCollaboratorRegistry = getTaskCollaboratorRegistry(task, projectCollaborator);

        if (taskCollaboratorRegistry != null && taskCollaboratorRegistry.getRegistryStatus()
                .equals(TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED)) {

            taskCollaboratorRegistry.setRegistryStatus(TaskCollaboratorRegistry.RegistryStatus.REMOVALREQUEST);
            Set<TaskCollaboratorRegistry> taskCollaboratorRegistrySet = task.getTaskCollaboratorRegistryList();
            task.setTaskCollaboratorRegistryList(taskCollaboratorRegistrySet);
            taskRepository.save(task);
            return true;
        }
        return false;
    }

    /**
     * Method to get task collaborator registry.
     *
     * @param task
     * @param projectCollaborator
     * @return task collaborator registry.
     */
    public TaskCollaboratorRegistry getTaskCollaboratorRegistry(Task task, ProjectCollaborator projectCollaborator) {
        for (TaskCollaboratorRegistry taskCollaboratorRegistry : task.getTaskCollaboratorRegistryList()) {
            if (taskCollaboratorRegistry.getProjectCollaborator().getId() == projectCollaborator.getId()) {
                return taskCollaboratorRegistry;
            }
        }
        return null;
    }

    /**
     * Method to add report.
     *
     * @param reportoRestDTO
     * @return true if the report was added, false otherwise.
     */
    public boolean addReportRest2(CreateReportRestDTO reportoRestDTO) {
        task = taskRepository.getOneByTaskId(reportoRestDTO.getTaskID());

        String email = reportoRestDTO.getProjectCollaboratorEmail();
        if (task == null) {
            return false;
        }
        String projectId = task.getProject().getId();
        projectCollaborator = getProjectCollaborator(projectId, email);

        if (projectCollaborator == null) {
            return false;
        }
        if (!task.hasProjectCollaborator(projectCollaborator)) {
            return false;
        }

        if (!task.addReportRest(projectCollaborator, reportoRestDTO.getQuantity(), reportoRestDTO.getStartDate(),
                reportoRestDTO.getEndDate())) {
            return false;
        }

        task.changeStateTo();
        taskRepository.save(task);

        return true;
    }

    /**
     * Method to list task reports.
     *
     * @param taskId
     * @return list of reports.
     */
    public List<Report> listTaskReports(String taskId) {
        task = taskRepository.getOneByTaskId(taskId);
        return task.listAllReports();
    }

    /**
     * Method to list task reports.
     *
     * @param taskId
     * @return list of reports.
     */
    public List<ReportRestDTO> listTaskReportsDTO(String taskId) {
        task = taskRepository.getOneByTaskId(taskId);
        return reportsListToDTO(task.listAllReports());
    }

    /**
     * Method to list reports by task and collaborator.
     *
     * @param taskId
     * @param email
     * @return list of reports.
     */
    public List<ReportRestDTO> listReportsByTaskAndCollaborator(String taskId, String email) {
        List<ReportRestDTO> reportCollaboratorList = new ArrayList<>();
        for (Report r : listTaskReports(taskId)) {
            if (r.getUserEmail().equals(email)) {
                reportCollaboratorList.add(r.toDTO());
            }
        }
        return reportCollaboratorList;
    }

    /**
     * Method to list reports by collaborator.
     *
     * @param taskCollab
     * @return list of reports.
     */
    public List<ReportRestDTO> listReportsByCollaborator(String taskCollab) {
        List<ReportRestDTO> reportList = new ArrayList<>();

        for (Report r : listAllReports()) {
            if (r.getUserEmail().equals(taskCollab)) {
                reportList.add(r.toDTO());
            }
        }
        return reportList;
    }

    /**
     * Method to list all reports.
     *
     * @return list of all reports.
     */
    public List<Report> listAllReports() {
        List<Report> reportList = new ArrayList<>();
        for (Task t : taskRepository.findAll()) {
            for (TaskCollaboratorRegistry tcr : t.getTaskCollaboratorRegistryList()) {
                reportList.addAll(tcr.getReportList());
            }
        }
        return reportList;
    }

    /**
     * Method to convert Report list to DTO report List.
     *
     * @return list of all reports.
     */
    public List<ReportRestDTO> reportsListToDTO(List<Report> reportList) {
        List<ReportRestDTO> reportListDTO = new ArrayList<>();
        for (Report r : reportList) {
            reportListDTO.add(r.toDTO());
        }
        return reportListDTO;
    }

    /**
     * Method to edit report.
     *
     * @param editReportQuantityDTO
     * @return true if the report was edited, false otherwise.
     */
    public boolean us208EditReport2(EditReportQuantityDTO editReportQuantityDTO) {
        task = taskRepository.getOneByTaskId(editReportQuantityDTO.getTaskID());
        String projectId = task.getProject().getId();
        String email = editReportQuantityDTO.getCollabEmail();
        projectCollaborator = getProjectCollaborator(projectId, email);

        if (!existsTaskAndProjectCollaborator(task, projectCollaborator)) {
            return false;
        }

        return us208EditReportAux(editReportQuantityDTO);
    }

    /**
     * Auxiliar method to check some conditions in order to edit reports.
     *
     * @param editReportQuantityDTO
     * @return true if conditions are satisfied, false otherwise.
     */
    private boolean us208EditReportAux(EditReportQuantityDTO editReportQuantityDTO) {
        if (task.hasProjectCollaborator(projectCollaborator)) {
            Report report = task.findReportById(editReportQuantityDTO.getReportId());
            if (report != null && task.taskCollaboratorHasReport(projectCollaborator, editReportQuantityDTO.getReportId())) {
                report.setQuantity(editReportQuantityDTO.getQuantity());
                report.setReportCreationDate(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
                taskRepository.save(task);
                return true;
            }
        }
        return false;
    }

    /**
     * Method to send a completed request.
     *
     * @param taskProjectCollaboratorRestDTO
     * @return true if the request was sent, false otherwise.
     */
    public boolean sendCompletedRequest(TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO) {
        task = taskRepository.getOneByTaskId(taskProjectCollaboratorRestDTO.getTaskId());

        projectCollaborator = getProjectCollaborator(task.getProject().getId(), taskProjectCollaboratorRestDTO.getEmail());

        if (task.requestTaskCompleted(projectCollaborator)) {
            taskRepository.save(task);
            return true;
        }

        return false;
    }

    /**
     * Method to check if exists task and project collaborator.
     *
     * @param task
     * @param projectCollaborator
     * @return true if both exists, false otherwise.
     */
    public boolean existsTaskAndProjectCollaborator(Task task, ProjectCollaborator projectCollaborator) {
        return (task != null && projectCollaborator != null);
    }

    /**
     * Method to get project collaborator.
     *
     * @param projectId
     * @param email
     * @return project collaborator
     */
    public ProjectCollaborator getProjectCollaborator(String projectId, String email) {
        for (ProjectCollaborator projCol : projectCollaboratorRepository.findAll()) {
            if (email.equals(projCol.getUser().getEmail())
                    && projectId.equals(projCol.getProject().getId())) {
                return projCol;
            }
        }
        return null;
    }

    /**
     * Method to get list of completed task requests in a project.
     *
     * @param projectId
     * @return list of completed task requests DTOs
     */
    public List<TaskCollaboratorRegistryRESTDTO> listCompletedRequestsToDTO(String projectId) {

        List<TaskCollaboratorRegistryRESTDTO> taskCollaboratorRegistryDTOList = new ArrayList<>();

        for (Task t : taskRepository.findByProject(projectRepository.getOneByCode(projectId))) {
            if (t.getRequestTaskCompleted() != null) {
                TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO = new TaskCollaboratorRegistryRESTDTO();
                taskCollaboratorRegistryRESTDTO.setTaskId(t.getId());
                taskCollaboratorRegistryRESTDTO.setProjectCollaboratorEmail(t.getRequestTaskCompleted());
                taskCollaboratorRegistryRESTDTO.setRegistryStatus("COMPLETEDREQUEST");
                taskCollaboratorRegistryDTOList.add(taskCollaboratorRegistryRESTDTO);
            }
        }
        return taskCollaboratorRegistryDTOList;
    }

    /**
     * Method to cancel a assignment request from a collaborator.
     *
     * @param taskCollaboratorRegistryRESTDTO
     * @return task registry DTO
     */
    public TaskCollaboratorRegistryRESTDTO cancelAssignmentRequestToDTO(TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTOOut = new TaskCollaboratorRegistryRESTDTO();
        Task t = taskRepository.getOneByTaskId(taskCollaboratorRegistryRESTDTO.getTaskId());
        ProjectCollaborator pc = projectCollaboratorRepository.getOneById(taskCollaboratorRegistryRESTDTO.getProjectCollaboratorId());
        t.cancelAssignmentRequest(pc);
        taskRepository.save(t);
        taskCollaboratorRegistryRESTDTOOut.setRegistryStatus(t.getLastTaskCollaboratorRegistryOf(pc).getRegistryStatus().toString());
        return taskCollaboratorRegistryRESTDTOOut;
    }

    /**
     * Method to cancel a removal request from a collaborator.
     *
     * @param taskCollaboratorRegistryRESTDTO
     * @return task registry DTO
     */
    public TaskCollaboratorRegistryRESTDTO cancelRemovalRequestToDTO(TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO) {

        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTOOut = new TaskCollaboratorRegistryRESTDTO();
        Task t = taskRepository.getOneByTaskId(taskCollaboratorRegistryRESTDTO.getTaskId());
        ProjectCollaborator pc = projectCollaboratorRepository.getOneById(taskCollaboratorRegistryRESTDTO.getProjectCollaboratorId());
        t.cancelRemovalRequest(pc);
        taskRepository.save(t);
        taskCollaboratorRegistryRESTDTOOut.setRegistryStatus(t.getLastTaskCollaboratorRegistryOf(pc).getRegistryStatus().toString());
        return taskCollaboratorRegistryRESTDTOOut;
    }
}
