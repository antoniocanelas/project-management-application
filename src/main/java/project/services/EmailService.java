package project.services;

import org.springframework.stereotype.Service;

import project.dto.EmailMessageDTO;

@Service
public interface EmailService {
	
	void sendSimpleMessage(String to, String subject, String text);
	
	void sendSimpleMessage(EmailMessageDTO emailMessageDTO);
	
}
