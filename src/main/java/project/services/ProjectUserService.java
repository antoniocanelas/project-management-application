package project.services;

import org.springframework.stereotype.Service;
import project.dto.rest.ProjectCollaboratorRestDTO;
import project.dto.rest.ProjectDTO;
import project.dto.rest.SetProjectManagerDTO;
import project.jparepositories.ProjectRepository;
import project.jparepositories.UserRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.roles.Role;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static project.model.user.roles.RoleName.ROLE_COLLABORATOR;

@Service
public class ProjectUserService {
	Project project;
	String projectId;
	String name;
	String description;
	LocalDateTime startDate;
	LocalDateTime finalDate;
	String userEmail;
	User user;
	ProjectCollaborator projectCollaborator;
	UserIdVO userIdVO;
	String projectUnits;
	double globalBudget;
	boolean isActive;
	private ProjectRepository projectRepository;
	private UserRepository userRepository;

	public ProjectUserService(ProjectRepository projectRepository, UserRepository userRepository) {
		this.projectRepository = projectRepository;
		this.userRepository = userRepository;
	}

	/**
	 * Method to create project.
	 *
	 * @param projectDTO
	 *            necessary DTO with data to create project.
	 * @return
	 */
	public ProjectDTO createProject(ProjectDTO projectDTO) {
		projectId = projectDTO.getProjectId();
		name = projectDTO.getName();
		description = projectDTO.getDescription();
		startDate = projectDTO.getStartDate();
		finalDate = projectDTO.getFinalDate();
		userEmail = projectDTO.getUserEmail();
		projectUnits = projectDTO.getUnit();
		globalBudget = projectDTO.getGlobalBudget();
		isActive = true;
		userIdVO = UserIdVO.create(userEmail);

		if (areRequiredParametersInvalid())
			return null;
		project = new Project(projectId, name, description, startDate, finalDate);

		user = userRepository.getOneByUserIdVO(userIdVO);
		assignProjectManagerAndAddToProjectCollaboratorList(project, user);

		fillOptionalParameters(projectDTO);

		projectRepository.save(project);
		return project.toDTO();
	}

	/**
	 * Method to fill optional parameters in DTO
	 *
	 * @param projectDTO
	 *            DTO to create Project
	 */
	private void fillOptionalParameters(ProjectDTO projectDTO) {
		if (projectDTO.getUnit() == "PEOPLE_MONTH") {
			project.setUnit(Project.ProjectUnits.PEOPLE_MONTH);
		}
		if (!Double.isNaN(projectDTO.getGlobalBudget())) {
			project.setGlobalBudget(projectDTO.getGlobalBudget());
		}
	}

	/**
	 * @return Return true if invalid para exists, false otherwise.
	 */
	private boolean areRequiredParametersInvalid() {
		userIdVO = UserIdVO.create(userEmail);
		if (!userRepository.existsById(userIdVO)) {
			return true;
		}
		user = userRepository.getOneByUserIdVO(userIdVO);

		if (!user.isActive())
			return true;

		if (projectId == null) {
			return true;
		}
		return projectRepository.existsById(projectId);
	}

	/**
	 * Sets the project's Project Manager and adds to ProjectCollaboratorList.
	 *
	 * @param project
	 *            Desired project to assign PM.
	 * @param user
	 *            User to be "promoted" to Project Manager.
	 * @return true if setting was successful, false otherwise.
	 */
	private void assignProjectManagerAndAddToProjectCollaboratorList(Project project, User user) {

		projectCollaborator = new ProjectCollaborator(project, user, 0);
		project.addProjectCollaborator(projectCollaborator);
		project.setProjectManager(projectCollaborator);
	}

	/**
	 * Set Project Manager based on SetProjectManagerDTO.
	 * 
	 * @param setProjectManagerInDTO
	 * @return null if conditions are not satisfied, or if they are, the new project
	 *         manager is set.
	 */
	public SetProjectManagerDTO setProjectManager(SetProjectManagerDTO setProjectManagerInDTO) {

		projectId = setProjectManagerInDTO.getProjectId();
		String email = setProjectManagerInDTO.getEmail();
		userIdVO = UserIdVO.create(email);

		project = projectRepository.getOneByCode(projectId);
		user = userRepository.getOneByUserIdVO(userIdVO);
		ProjectCollaborator collaborator = new ProjectCollaborator(project, user, 0);

		if (!userRepository.existsById(userIdVO)) {
			return null;
		}
		if (!projectRepository.existsById(projectId)) {
			return null;
		}
		if (!user.isActive())
			return null;

		if (!project.hasActiveProjectCollaborator(collaborator))
			project.addProjectCollaborator(collaborator);

		project.setProjectManager(collaborator);
		projectRepository.save(project);
		return setProjectManagerInDTO;
	}

	/**
	 * Adds a collaborator to the project.
	 *
	 * @return true if adding to the project was successful, false otherwise.
	 */
	// DODO project manager role action
	public ProjectCollaboratorRestDTO addProjectCollaboratorToProjectRest(
			ProjectCollaboratorRestDTO projectCollaboratorRestDTO, String projectId) {
		userIdVO = UserIdVO.create(projectCollaboratorRestDTO.getUserId());

		user = userRepository.getOneByUserIdVO(userIdVO);
		project = projectRepository.getOneByCode(projectId);

		if (user == null || project == null) {
			return projectCollaboratorRestDTO;
		}

		if (project.findProjectCollaborator(user) != null) {
			return projectCollaboratorRestDTO;
		}

		double cost = projectCollaboratorRestDTO.getCost();

		if (project.getProjectManager() != null
				&& (user.isActive() && user.getRoles().contains(new Role(ROLE_COLLABORATOR)))) {
			projectCollaborator = new ProjectCollaborator(project, user, cost);
			project.addProjectCollaborator(projectCollaborator);
			projectRepository.save(project);
			projectCollaboratorRestDTO.setProjectId(project.getId());
		}
		return projectCollaboratorRestDTO;
	}

	/**
	 * Method to get project.
	 *
	 * @param projectId
	 *            The project's ID
	 * @return The project DTO.
	 */
	public ProjectDTO getProject(String projectId) {

		return projectRepository.getOneByCode(projectId).toDTO();
	}

	/**
	 * Method to list all projects DTO's.
	 *
	 * @return All project's DTO.
	 */
	public List<ProjectDTO> listAllProjects() {
		List<ProjectDTO> projectDTOList = new ArrayList<>();
		for (Project proj : projectRepository.findAll()) {
			projectDTOList.add(proj.toDTO());
		}
		return projectDTOList;
	}
}