package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.jparepositories.ProjectCollaboratorRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectCollaboratorService {

    @Autowired
    private ProjectCollaboratorRepository projectCollaboratorRepository;

    public ProjectCollaboratorService(ProjectCollaboratorRepository projectCollaboratorRepository) {
        this.projectCollaboratorRepository = projectCollaboratorRepository;
    }

    /**
     * Instantiates and adds a task to the project's task list.
     *
     * @return true if adding to the list was successful, false otherwise.
     */
    public boolean addProjectCollaborator(Project project, User user, double cost) {

        if (project.getProjectManager() == null) {
            return false;
        }
        if (!user.isActive() || !user.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR))) {
            return false;
        }

        if (!listUsersByProject(project.getId()).contains(user)) {
            ProjectCollaborator projectCollaborator = new ProjectCollaborator(project, user, cost);
            project.addProjectCollaborator(projectCollaborator); //ver método, só precisa de adicionar, a verificação se está na lista já foi feita.
            projectCollaboratorRepository.save(projectCollaborator);
            return true;
        }
        if (!listActiveUsersByProject(project.getId()).contains(user)) {
            ProjectCollaborator projectCollaborator = getProjectCollaborator(project.getId(), user.getEmail());
            projectCollaborator.addNewCostAndAssociatedPeriod(cost);
            return true;
        }
        return false;
    }

    /**
     * Sets the project's Project Manager.
     *
     * @param user User to be "promoted" to Project Manager.
     * @return true if setting was successful, false otherwise.
     */
    public boolean setProjectManager(Project project, User user) {
        ProjectCollaborator collaborator = new ProjectCollaborator(project, user, 0);
        if (user == null)
            return false;
        if (!user.isActive())
            return false;

        if (project.getProjectManager() != null) {
            project.removeProjectCollaborator((project.findProjectCollaborator(project.getProjectManager().getUser())));
        }
        if (!project.hasActiveProjectCollaborator(collaborator))
            project.addProjectCollaborator(collaborator);

        project.setProjectManager(collaborator);
        projectCollaboratorRepository.save(collaborator);
        return true;
    }

    /**
     * Method to update project collaborator.
     *
     * @param projectCollaborator
     */
    public void updateProjectCollaborator(ProjectCollaborator projectCollaborator) {

        if (projectCollaboratorRepository.existsById(projectCollaborator.getId())) {
            projectCollaboratorRepository.save(projectCollaborator);
        }
    }

    /**
     * Method to get project collaborator
     *
     * @param projectId
     * @param email
     * @return project collaborator linked to passed email&projectID or null.
     */
    public ProjectCollaborator getProjectCollaborator(String projectId, String email) {
        for (ProjectCollaborator projectCollaborator : projectCollaboratorRepository.findAll()) {
            if (email.equals(projectCollaborator.getUser().getEmail())
                    && projectId.equals(projectCollaborator.getProject().getId())) {
                return projectCollaborator;
            }
        }
        return null;
    }

    /**
     * Method to remove project collaborator.
     *
     * @param project
     * @param email
     * @return true if project collaborator was removed, false otherwise.
     */
    public boolean removeProjectCollaborator(Project project, String email) {
        ProjectCollaborator projectCollaborator = getProjectCollaborator(project.getId(), email);
        if (projectCollaborator != null) {
            project.removeProjectCollaborator(projectCollaborator);
            projectCollaboratorRepository.save(projectCollaborator);
            return true;
        }
        return false;
    }

    /**
     * Method to list project collaborators related to certain project.
     *
     * @param projectId
     * @return a list of project collaborators (related to passed project).
     */
    public List<ProjectCollaborator> listProjectCollaboratorsByProject(String projectId) {
        List<ProjectCollaborator> projectCollaboratorList = new ArrayList<>();

        for (ProjectCollaborator projectCollaborator : projectCollaboratorRepository.findAll()) {
            if (projectId.equals(projectCollaborator.getProject().getId()) && projectCollaborator.isActiveInProject()) {
                projectCollaboratorList.add(projectCollaborator);
            }
        }
        return projectCollaboratorList;
    }

    /**
     * Method to list users related to certain project.
     *
     * @param projectId
     * @return a list of users (related to passed project).
     */
    public List<User> listUsersByProject(String projectId) {
        List<User> projectCollaboratorList = new ArrayList<>();

        for (ProjectCollaborator projectCollaborator : projectCollaboratorRepository.findAll()) {
            if (projectId.equals(projectCollaborator.getProject().getId())) {
                projectCollaboratorList.add(projectCollaborator.getUser());
            }
        }
        return projectCollaboratorList;
    }

    /**
     * Method to get project collaborator.
     *
     * @param id
     * @return project collaborator (related to passed ID).
     */
    public ProjectCollaborator getProjectCollaborator(int id) {
        return projectCollaboratorRepository.getOneById(id);

    }

    /**
     * Method to list active users related to certain project.
     *
     * @param projectId
     * @return a list of active users (related to passed project).
     */
    public List<User> listActiveUsersByProject(String projectId) {
        List<User> projectCollaboratorList = new ArrayList<>();

        for (ProjectCollaborator projectCollaborator : projectCollaboratorRepository.findAll()) {
            if (projectId.equals(projectCollaborator.getProject().getId()) && projectCollaborator.isActiveInProject()) {
                projectCollaboratorList.add(projectCollaborator.getUser());
            }
        }
        return projectCollaboratorList;
    }

}