package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressDTO {

    @XmlElement(name = "localidade")
    private String city;

    @XmlElement(name = "pais")
    private String country;

    @XmlElement(name = "cp_loc")
    private String postalCodeCity;

    @XmlElement(name = "cp_num")
    private String postalCodeAddress;

    @XmlElement(name = "rua")
    private String street;

    protected AddressDTO() {
    }

    public AddressDTO(String city, String country, String postalCodeCity, String postalCodeAddress, String street) {
        this.city = city;
        this.country = country;
        this.postalCodeCity = postalCodeCity;
        this.postalCodeAddress = postalCodeAddress;
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPostalCodeCity() {
        return postalCodeCity;
    }

    public String getPostalCodeAddress() {
        return postalCodeAddress;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[lista_enderecos=[endereco_postal=");
        builder.append(city);
        builder.append(", pais=");
        builder.append(country);
        builder.append(", cp_loc=");
        builder.append(postalCodeCity);
        builder.append(", cp_num=");
        builder.append(postalCodeAddress);
        builder.append(", rua=");
        builder.append(street);
        builder.append("]");
        return builder.toString();
    }

}
