package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UserDTO {

    @XmlElement(name = "nome_utilizador")
    private String name;

    @XmlElement(name = "email_utilizador")
    private String email;

    @XmlElementWrapper(name = "lista_enderecos")
    @XmlElement(name = "endereco_postal")
    private List<AddressDTO> addressList;

    @XmlElement(name = "telefone")
    private String phone;

    @XmlElement(name = "password")
    private String password;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public List<AddressDTO> getAddressList() {
        return addressList;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[utilizador=[nome_utilizador=");
        builder.append(name);
        builder.append(", email_utilizador=");
        builder.append(email);
        builder.append(", lista_enderecos=");
        builder.append(addressList);
        builder.append(", telefone=");
        builder.append(phone);
        builder.append(", password=");
        builder.append(password);
        builder.append("]");
        return builder.toString();
    }

}
