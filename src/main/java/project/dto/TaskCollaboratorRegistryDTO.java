package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class TaskCollaboratorRegistryDTO {

    @XmlElement(name = "data_inicio")
    private String startDate;

    @XmlElement(name = "data_fim")
    private String endDate;

    @XmlElementWrapper(name = "lista_report_tarefa")
    @XmlElement(name = "report")
    private List<ReportDTO> reports;

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public List<ReportDTO> getReports() {
        return reports;
    }
}
