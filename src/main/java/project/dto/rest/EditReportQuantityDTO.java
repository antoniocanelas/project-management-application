package project.dto.rest;

public class EditReportQuantityDTO {
	private double quantity;
	private String taskID;
	private String reportId;
	private String collabEmail;

	/**
	 * Method to get collaborator email.
	 * @return collaborator email.
	 */
	public String getCollabEmail() {
		return collabEmail;
	}

	/**
	 * Method to set collaborator email.
	 * @param collabEmail
	 */
	public void setCollabEmail(String collabEmail) {
		this.collabEmail = collabEmail;
	}

	/**
	 * Method to get report quantity.
	 * @return quantity.
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * Method to set report quantity.
	 * @param quantity
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	/**
	 * Method to get task ID.
	 * @return task ID.
	 */
	public String getTaskID() {
		return taskID;
	}

	/**
	 * Method to set task ID.
	 * @param taskID
	 */
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	/**
	 * Method to get report ID.
	 * @return report ID.
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * Method to set report ID.
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

}
