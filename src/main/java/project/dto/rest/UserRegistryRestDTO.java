package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;

public class UserRegistryRestDTO extends ResourceSupport {

    private String name;
    private String email;
    private String phone;
    private String taxPayerId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    private String addressId;
	private String street;
    private String postalCode;
    private String city;
    private String country;
    private String password;
    private String profile;
    

    public void setPassword(String password) {
    	this.password = password;
    }
    
    public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setTin(String tin) {
		this.taxPayerId = tin;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
    
    public String getPassword() {
    	return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getTin() {
    	return taxPayerId;
    }
    
    public LocalDate getBirthDate() {
    	return birthDate;
    }
    
    public String getAddressId() {
    	return addressId;
    }
    
    public String getStreet() {
    	return street;
    }
    
    public String getPostalCode() {
    	return postalCode;
    }
    
    public String getCity() {
    	return city;
    }
    
    public String getCountry() {
    	return country;
    }
    
    public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile=profile;
	}

	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[utilizador=[nome_utilizador=");
        builder.append(name);
        builder.append(", email_utilizador=");
        builder.append(email);
        builder.append(", telefone=");
        builder.append(phone);
        builder.append(", tin=");
        builder.append(taxPayerId);
        builder.append(", birthdate=");
        builder.append(birthDate);
        builder.append(", profile=");
        builder.append(profile);
        builder.append("]");
        return builder.toString();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		
		UserRegistryRestDTO other = (UserRegistryRestDTO) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}


	
}
