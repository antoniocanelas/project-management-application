package project.dto.rest;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;

public class TaskProjectCollaboratorRestDTO extends ResourceSupport {
    private int projectCollaboratorId;
    private String taskId;
    private String email;

    @JsonCreator
	public TaskProjectCollaboratorRestDTO() {
		//Is this constructor really necessary?
	}

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
    
    @Override
	public String toString() {
		return "TaskProjectCollaboratorRestDTO [projectCollaboratorId=" + projectCollaboratorId + ", taskId=" + taskId
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + projectCollaboratorId;
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		TaskProjectCollaboratorRestDTO other = (TaskProjectCollaboratorRestDTO) obj;
		if (projectCollaboratorId != other.projectCollaboratorId)
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		return true;
	}

	public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public int getProjectCollaboratorId() {
        return projectCollaboratorId;
    }

    public void setProjectCollaboratorId(int projectCollaboratorId) {
        this.projectCollaboratorId = projectCollaboratorId;
    }

}
