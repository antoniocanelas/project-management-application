package project.dto.rest;

public class TaskHoursRestDTO {
	
	double hours;
	double peopleMonth;

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	public double getPeopleMonth() {
		return peopleMonth;
	}

	public void setPeopleMonth(double peopleMonth) {
		this.peopleMonth = peopleMonth;
	}
	
	

}
