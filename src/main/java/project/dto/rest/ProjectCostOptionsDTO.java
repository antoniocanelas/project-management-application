package project.dto.rest;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

public class ProjectCostOptionsDTO extends ResourceSupport {

    private String projectId;
    private List<String> costCalculationOptions = new ArrayList<>();

    public List<String> getCostCalculationOptions() {
        return costCalculationOptions;
    }

    public void setCostCalculationOptions(List<String> costCalculationOptions) {
        this.costCalculationOptions = costCalculationOptions;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((costCalculationOptions == null) ? 0 : costCalculationOptions.hashCode());
        result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProjectCostOptionsDTO other = (ProjectCostOptionsDTO) obj;
        if (costCalculationOptions == null) {
            if (other.costCalculationOptions != null)
                return false;
        } else if (!costCalculationOptions.equals(other.costCalculationOptions))
            return false;
        if (projectId == null) {
            if (other.projectId != null)
                return false;
        } else if (!projectId.equals(other.projectId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SetProjectCostOptionsDTO [projectId=" + projectId + ", costCalculationOptions=" + costCalculationOptions
                + "]";
    }

}
