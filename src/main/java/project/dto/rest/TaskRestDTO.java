package project.dto.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRestDTO extends ResourceSupport {


    private String projectId;
    private String taskId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateOfCreation;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime effectiveDateOfConclusion;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime effectiveStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime predictedDateOfConclusion;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime predictedDateOfStart;
    private String title;
    private String description;
    private double estimatedEffort;
    private String completedRequest;
    private String message;
    private double unitCost;
    private String state;
    private List<String> dependenciesId = new ArrayList<>();
    private String ganttTaskDependencies;
    private double lastReportHours;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastReportCreationDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastReportDate;

    @JsonCreator
    public TaskRestDTO() {
        //Is this constructor really necessary?
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getEstimatedEffort() {
        return estimatedEffort;
    }

    public void setEstimatedEffort(double estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }

    public LocalDateTime getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDateTime dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public LocalDateTime getEffectiveDateOfConclusion() {
        return effectiveDateOfConclusion;
    }

    public void setEffectiveDateOfConclusion(LocalDateTime effectiveDateOfConclusion) {
        this.effectiveDateOfConclusion = effectiveDateOfConclusion;
    }

    public LocalDateTime getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(LocalDateTime effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public LocalDateTime getPredictedDateOfConclusion() {
        return predictedDateOfConclusion;
    }

    public void setPredictedDateOfConclusion(LocalDateTime predictedDateOfConclusion) {
        this.predictedDateOfConclusion = predictedDateOfConclusion;
    }

    public LocalDateTime getPredictedDateOfStart() {
        return predictedDateOfStart;
    }

    public void setPredictedDateOfStart(LocalDateTime predictedDateOfStart) {
        this.predictedDateOfStart = predictedDateOfStart;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<String> getDependenciesId() {
        return dependenciesId;
    }

    public void setDependenciesId(List<String> dependenciesId) {
        this.dependenciesId = dependenciesId;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCompletedRequest() {
        return completedRequest;
    }

    public void setCompletedRequest(String completedRequest) {
        this.completedRequest = completedRequest;
    }

    public double getLastReportHours() {
		return lastReportHours;
	}

	public void setLastReportHours(double lastReportHours) {
		this.lastReportHours = lastReportHours;
	}

	public LocalDateTime getLastReportDate() {
		return lastReportDate;
	}

	public void setLastReportDate(LocalDateTime lastReportDate) {
		this.lastReportDate = lastReportDate;
	}
	
	

	public LocalDateTime getLastReportCreationDate() {
		return lastReportCreationDate;
	}

	public void setLastReportCreationDate(LocalDateTime lastReportCreationDate) {
		this.lastReportCreationDate = lastReportCreationDate;
	}

	public String getGanttTaskDependencies() {
        return ganttTaskDependencies;
    }

    public void setGanttTaskDependencies(String ganttTaskDependencies) {
        this.ganttTaskDependencies = ganttTaskDependencies;
    }

    @Override
    public String toString() {
        return "TaskRestDTO [projectId=" + projectId + ", taskId=" + taskId + ", dateOfCreation=" + dateOfCreation
                + ", effectiveDateOfConclusion=" + effectiveDateOfConclusion + ", effectiveStartDate="
                + effectiveStartDate + ", predictedDateOfConclusion=" + predictedDateOfConclusion
                + ", predictedDateOfStart=" + predictedDateOfStart + ", title=" + title + ", description=" + description
                + ", estimatedEffort=" + estimatedEffort + ", completedRequest=" + completedRequest + ", unitCost="
                + unitCost + ", state=" + state + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaskRestDTO)) {
            return false;
        }
        TaskRestDTO that = (TaskRestDTO) o;
        return Objects.equals(taskId, that.taskId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), taskId);
    }
}
