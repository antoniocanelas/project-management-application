package project.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProjectCollaboratorCostAndTimePeriodDTO {

    @XmlElement(name = "data_inicio")
    private String startDate;

    @XmlElement(name = "data_fim")
    private String endDate;

    @XmlElement(name = "custo_unitario_colaborador")
    private String cost;

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getCost() {
        return cost;
    }

}
