package project.model.projectcollaborator;

import java.util.UUID;

public class ProjectCollaboratorIdVO {
    private UUID projectCollaboratorId;

    public ProjectCollaboratorIdVO() {
        this.projectCollaboratorId = UUID.randomUUID();
    }

    public UUID getProjectCollaboratorId() {
        return projectCollaboratorId;
    }
}
