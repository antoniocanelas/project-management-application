package project.model.user;

import java.time.LocalDateTime;
import java.util.Random;

import javax.persistence.Embeddable;

@Embeddable
public class UserLoginData {

	private String confirmationToken;
	private LocalDateTime lastLoginDateTime;
	private LocalDateTime approvedDateTime;
		
	public UserLoginData() {
		Random r = new Random();

		this.confirmationToken =  String.format("%04d", r.nextInt(10000));
	}
	
	public String getConfirmationToken() {
		return confirmationToken;
	}
	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}
	public LocalDateTime getLastLoginDate() {
		return lastLoginDateTime;
	}
	public void setLastLoginDate(LocalDateTime lastLoginDate) {
		this.lastLoginDateTime = lastLoginDate;
	}
	
	public LocalDateTime getApprovedDateTime() {
		return approvedDateTime;
	}

	public void setApprovedDateTime(LocalDateTime approvedDateTime) {
		this.approvedDateTime = approvedDateTime;
	}
}
