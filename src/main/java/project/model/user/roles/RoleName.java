package project.model.user.roles;

public enum  RoleName {
    ROLE_ADMINISTRATOR, ROLE_COLLABORATOR, ROLE_DIRECTOR, ROLE_REGISTEREDUSER
}
