package project.model.user;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import project.dto.rest.UserRestDTO;
import project.model.user.roles.*;
import system.dto.LoginTokenDTO;

import javax.mail.internet.AddressException;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to create users and manage their personal data, profile and status.
 */

@Entity
@Proxy(lazy = false)

public class User {

    @Transient
    Logger logger = Logger.getAnonymousLogger();
    @EmbeddedId
    private UserIdVO userIdVO;
    @Embedded
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Address> addressList = new ArrayList<>();
    private LocalDate birthDate;
    @Transient
    private EmailAddress emailAddress;
    @Column(nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isActive;
    private String name;
    private String password;
    private String phone;
    private String taxPayerId;
    @Embedded
    private UserLoginData userLoginData;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new LinkedHashSet<>();
    private String username;

    /**
     * Constructs a User.
     *
     * @param name
     * @param phone
     * @param userEmail
     * @param tin
     * @param birthDate
     * @param id
     * @param street
     * @param postalCode
     * @param city
     * @param country
     * @throws AddressException
     */
    public User(String name, String phone, String userEmail, String tin, LocalDate birthDate, String id, String street,
                String postalCode, String city, String country) throws AddressException {
        this.name = name;
        this.phone = phone;
        taxPayerId = tin;
        this.birthDate = birthDate;
        Address address = createAddress(id, street, postalCode, city, country);
        addressList.add(address);
        isActive = true;
        this.userIdVO = UserIdVO.create(userEmail);
        this.emailAddress = new EmailAddress(userEmail);
        this.userLoginData = new UserLoginData();
        this.username = getEmail();
    }


    protected User() {
    }

    /**
     * Method to get Roles.
     *
     * @return Roles.
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * Method to set Roles
     *
     * @param roles
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Add address to user's adressList.
     *
     * @param address
     * @return boolean (true if address is added, false if it isn't).
     */
    public boolean addAddress(Address address) {

        if (!address.isValid()) {
            return false;
        }
        for (Address ad : addressList) {
            if (ad.equals(address)) {
                return false;
            }
        }
        return addressList.add(address);
    }

    /**
     * Create a new address
     *
     * @param addressId  Unique ID for address.
     * @param street     Street of the address.
     * @param postalCode Postal code number of the address.
     * @param city       City of the address.
     * @param country    Country of the address.
     * @return
     */
    public Address createAddress(String addressId, String street, String postalCode, String city, String country) {

        return new Address(addressId, street, postalCode, city, country);
    }

    /**
     * Edit user's address fields.
     *
     * @param address user's address to edit.
     * @return boolean (true for a success edit, false for fail).
     */
    public void editAddress(Address address) {
        if (address.isValid()) {
            for (Address element : addressList) {
                if (element.getAddressDescription().equals(address.getAddressDescription())) {
                    element.setCity(address.getCity());
                    element.setCountry(address.getCountry());
                    element.setPostalCode(address.getPostalCode());
                    element.setStreet(address.getStreet());
                }
            }
        }
    }

    /**
     * Get user's address by ID
     *
     * @param addressId
     * @return a instance of Address
     */
    public Address getAddressById(String addressId) {
        for (Address ad : addressList) {
            if (ad.getAddressDescription().equals(addressId)) {
                return ad;
            }
        }
        return null;
    }

    /**
     * Gets the user Address list.
     *
     * @return List<Address> user's address list.
     */
    public List<Address> getAddressList() {
        return addressList;
    }

    /**
     * Gets the user birth date.
     *
     * @return LocalDate user's birth date.
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the user birth date.
     *
     * @param birthDate user's birth Date.
     */
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets the user emailAddress.
     *
     * @return String user's emailAddress.
     */
    public EmailAddress getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the user emailAddress.
     *
     * @param emailAddress user's emailAddress.
     * @throws AddressException
     */
    public void setEmailAddress(String emailAddress) throws AddressException {
        this.emailAddress = new EmailAddress(emailAddress);
    }

    /**
     * Gets the user's name
     *
     * @return String user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the user name.
     *
     * @param name user's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the user phone number.
     *
     * @return String user's phone number.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the user phone number.
     *
     * @param phone String user's phone number.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }


    /**
     * Gets the user taxPayerId.
     *
     * @return String user's taxPayerId.
     */
    public String getTaxPayerId() {
        return taxPayerId;
    }

    /**
     * Sets taxPayerId parameter.
     *
     * @param nif user's taxPayerId.
     */
    public void setTaxPayerId(String nif) {
        taxPayerId = nif;
    }

    public UserIdVO getUserIdVO() {
        return userIdVO;
    }

    public void setUserIdVO(UserIdVO userIdVO) {
        this.userIdVO = userIdVO;
    }

    public String getEmail() {
        return userIdVO.getUserEmail();
    }

    public UserLoginData getUserLoginData() {
        return userLoginData;
    }

    public void setUserLoginData(UserLoginData userLoginData) {
        this.userLoginData = userLoginData;
    }

    /**
     * Check if user's status is active.
     *
     * @return boolean (true if it is, false if it isn't).
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Check if address parameters are correct. (if emailAddress has an '@' or name
     * is not null, e.g.).
     *
     * @return boolean (true if address is correct, false if it isn't).
     */
    public boolean isValid(String email) {
        try {
            emailAddress = new EmailAddress(email);
        } catch (AddressException ae) {
            logger.log(Level.INFO, "an exception was thrown", ae);
            return false;
        }
        return !(name == null || name.isEmpty() || phone == null || phone.isEmpty());
    }

    /**
     * Remove user's address from Address list.
     *
     * @param address user's address.
     * @return boolean (true if it is, false if it isn't).
     */
    public boolean removeAddress(Address address) {

        return addressList.remove(address);
    }

    /**
     * Set user's status active.
     */
    public void setActive() {
        isActive = true;
    }

    /**
     * Set user's status inactive.
     */
    public void setInactive() {
        isActive = false;
    }

    /**
     * Sets the user profile as ROLE_DIRECTOR.
     */
    public void setProfileCollaborator() {
        Role collaborator = new Role(RoleName.ROLE_COLLABORATOR);
        roles.add(collaborator);
    }

    /**
     * Sets the user profile as ROLE_DIRECTOR.
     */
    public void setProfileDirector() {
        Role director = new Role(RoleName.ROLE_DIRECTOR);
        roles.add(director);
    }

    /**
     * Sets the user profile as Registered User.
     */
    public void setProfileRegisteredUser() {
        Role registeredUser = new Role(RoleName.ROLE_REGISTEREDUSER);
        roles.add(registeredUser);
    }

    /**
     * Sets the user profile as ROLE_ADMINISTRATOR.
     */
    public void setProfileAdministrator() {
        Role administrator = new Role(RoleName.ROLE_ADMINISTRATOR);
        roles.add(administrator);
    }

    /**
     * Checks if user password is equal to the given parameter.
     *
     * @param password Parameter to compare user password to.
     * @return String containing user emailAddress and user profile, if successful,
     * or error message otherwise.
     */
    public LoginTokenDTO validatePassword(String password) {

        if (this.password.equals(password)) {
            return new LoginTokenDTO(name, userIdVO.getUserEmail(), roles.toString(), isActive(), getUserLoginData().getApprovedDateTime(),
                    getUserLoginData().getLastLoginDate(), getUserLoginData().getConfirmationToken(),
                    roles.toString() + " " + name + " SUCCESSFULLY LOGGED");
        }
        return new LoginTokenDTO("Invalid Email or Password");
    }

    /**
     * Method to validate password and confirmation code.
     *
     * @param password
     * @param confirmationCode
     * @return message according to success or insuccess of validation.
     */
    public LoginTokenDTO validatePasswordAndConfirmationCode(String password, String confirmationCode) {
        if (!password.equals(this.password)) {
            return new LoginTokenDTO("Invalid Data introduced!", 1);
        }
        if (!isFirstLogin()) {
            return new LoginTokenDTO("Account already activated, log in with the same credentials", 2);
        }
        if (!confirmationCode.equals(getUserLoginData().getConfirmationToken())) {
            return new LoginTokenDTO("Wrong Confirmation code", 3);
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        getUserLoginData().setLastLoginDate(localDateTime);
        getUserLoginData().setApprovedDateTime(localDateTime);
        return new LoginTokenDTO(name, userIdVO.getUserEmail(), roles.toString(), true, localDateTime, localDateTime, confirmationCode, roles.toString() + " " + name + " ACCOUNT ACTIVATED, SUCCESSFULLY LOGGED");

    }

    /**
     * Method to check if user is logon for the first time.
     *
     * @return true if yes, false otherwise.
     */
    public boolean isFirstLogin() {
        return (userLoginData.getLastLoginDate() == null);
    }

    /**
     * Method to pass user to DTO.
     *
     * @return userDTO.
     */
    public UserRestDTO toDTO() {
        UserRestDTO userDTO = new UserRestDTO();
        userDTO.setName(this.name);
        userDTO.setEmail(this.userIdVO.getUserEmail());
        userDTO.setPhone(this.phone);
        userDTO.setBirthDate(this.birthDate);
        userDTO.setTaxPayerId(this.taxPayerId);
        userDTO.setProfile(this.roles.toString());
        return userDTO;
    }

    /**
     * Checks if user password is equal to the given parameter.
     *
     * @param password Parameter to compare user password to.
     * @return String containing user emailAddress and user profile, if successful,
     * or error message otherwise.
     */
    public LoginTokenDTO validatePasswordRest(String password) {

        if (this.password.equals(password)) {
            return new LoginTokenDTO(name, userIdVO.getUserEmail(), roles.toString(), isActive(), getUserLoginData().getApprovedDateTime(),
                    getUserLoginData().getLastLoginDate(), getUserLoginData().getConfirmationToken(),
                    roles.toString() + " " + name + " SUCCESSFULLY LOGGED");
        }
        return null;
    }

    /**
     * Method to get password.
     *
     * @return password.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password with its new "value".
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Method to get username.
     *
     * @return username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method to check if user has role registered user.
     *
     * @return true if he has, false otherwise.
     */
    public boolean hasRoleRegisteredUser() {
        for (Role r : roles) {
            if (r.isRegisteredUser()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if user has role administrator user.
     *
     * @return true if he has, false otherwise.
     */
    public boolean hasRoleAdministrator() {
        for (Role r : roles) {
            if (r.isAdministrator()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if user has role collaborator user.
     *
     * @return true if he has, false otherwise.
     */
    public boolean hasRoleCollaborator() {
        for (Role r : roles) {
            if (r.isCollaborator()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if user has role director user.
     *
     * @return true if he has, false otherwise.
     */
    public boolean hasRoleDirectorr() {
        for (Role r : roles) {
            if (r.isDirector()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }

        User user = (User) o;

        return userIdVO.equals(user.userIdVO);
    }

    @Override
    public int hashCode() {
        return userIdVO.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("email='").append(userIdVO.getUserEmail()).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", profile=").append(roles);
        sb.append(", taxPayerId='").append(taxPayerId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}