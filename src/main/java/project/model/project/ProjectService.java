package project.model.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;
import project.dto.rest.AvailableCostOptionsDTO;
import project.dto.rest.ProjectCostOptionsDTO;
import project.dto.rest.ProjectDTO;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskCostRestDTO;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.ProjectRepository;
import project.model.project.Project.ProjectUnits;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Extracts simple name from a class, given its full name.
     *
     * @param classNames list of strings containing full class names.
     * @return list of strings containing classes' simple names.
     */
    private static List<String> extractSimpleClassNames(List<String> classNames) {

        List<String> simpleNames = new ArrayList<>();

        for (String className : classNames) {
            int i = className.lastIndexOf('.');
            simpleNames.add(className.substring(i + 1));
        }
        return simpleNames;
    }

    /**
     * Add one project in the database.
     *
     * @param id   project unique code & search parameter
     * @param name project name
     * @return true: project added to database; false: project id not found
     */
    public boolean addProject(String id, String name) {
        Project newProject = new Project(id, name);

        for (Project sameID : getProjectList())
            if (sameID.equals(newProject))
                return false;

        getProjectList().add(newProject);
        projectRepository.save(newProject);
        return true;
    }

    /**
     * Update one project in the database.
     *
     * @param project project object
     * @return true: project updated in database; false: project if project does not
     * exist
     */
    public boolean updateProject(Project project) {

        for (Project proj : projectRepository.findAll()) {
            if (project.getId().equals(proj.getId())) {

                projectRepository.save(project);
                return true;
            }
        }
        return false;
    }

    /**
     * Get average hours (by user) for each completed task in the last month
     *
     * @param u User from which return average hours for your completed tasks in
     *          the last month
     * @return average hours
     */
    public double calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(User u) {

        double averageHours = 0.0;
        int numProjects = 0;

        for (Project p : listUserProjects(u))
            for (ProjectCollaborator c : p.getProjectCollaboratorList()) // TOSOLVE entry point project
                if (c.getUser().equals(u))
                {
                	numProjects++;
                	averageHours += p.calculateCollaboratorsAverageHoursCompletedTasksLastMonth(c);
                }


        return (numProjects == 0 ? 0 : averageHours/numProjects);
    }

        /**
         * Get number of hours (by user) for completed tasks in the last month, in all
         * projects
         *
         * @param user - User from which return number of hours for your completed
         *          	tasks in the last month
         * @param units - project unit
         *
         * @return total of hours
         */
        public double calculateUsersHoursCompletedTasksLastMonthInAllProjects(User user, ProjectUnits units) {
            int totalHours = 0;


            for (Project p : listUserProjects(user)) {
                totalHours = getTotalHours(user, units, totalHours, p);
            }
            return totalHours;
        }

    private int getTotalHours(User user, ProjectUnits units, int totalHours, Project p) {
        if (p.getUnit().equals(units))
            {
            for (ProjectCollaborator c : p.getProjectCollaboratorList()) // TOSOLVE entry point project
                if (c.getUser().equals(user))
totalHours += p.calculateCollaboratorsHoursCompletedTasksLastMonth(c);
            }
        return totalHours;
    }

    /**
     * Get project by id in project list
     *
     * @param id ID will identify the project
     * @return project if it exist, null otherwise.
     */
    public Project getProjectByID(String id) {
        Project project;
        if (projectRepository.existsById(id)) {
            project = projectRepository.getOneByCode(id);
            return project;
        }
        return null;
    }

    /**
     * Get projectDTO by id in project list
     *
     * @param id ID will identify the project
     * @return projectDTO if it exist, null otherwise.
     */
    public ProjectDTO getProjectDTOByID(String id) {
        if (projectRepository.existsById(id))
            return projectRepository.getOneByCode(id).toDTO();
        return null;
    }

    /**
     * Get project list
     *
     * @return Project List
     */
    public List<Project> getProjectList() {

        return projectRepository.findAll();
    }

    /**
     * Get project manager projects
     *
     * @return Project manager projects List
     */
    public List<Project> projectManagerProjectsList(User user) {

        List<Project> projectManagerProjects = new ArrayList<>();

        for (Project project : getProjectList())
            if (project.getProjectManager().getUser().equals(user))
                projectManagerProjects.add(project);
        return projectManagerProjects;
    }

    /**
     * Get tasks, from a specific project, without active project collaborators assigned
     *
     * @param projectId ID of project
     * @return list of TaskRestDTO unassigned tasks
     */
    public List<TaskRestDTO> listUnassignedTasksToDTO(String projectId) {

        List<TaskRestDTO> taskDTOList = new ArrayList<>();
        Project project = getProjectByID(projectId);

        for (Task task : project.listUnassignedTasks()) {

            taskDTOList.add(task.toDTO());
        }

        return taskDTOList;
    }

    /**
     * Get project collaborators, from a specific project, without tasks assigned
     *
     * @param projectId ID of project
     * @return list of ProjectCollaborators unassigned tasks
     */
    public List<ProjectCollaborator> listUnassignedProjectCollaborators(String projectId) {

    	 Project project = getProjectByID(projectId);

        return project.listUnassignedProjectCollaborator();
    }


    /**
     * Gets all active projects.
     *
     * @return List with all active projects.
     */
    public List<Project> listAllActiveProjects() {

        List<Project> activeProjects = new ArrayList<>();

        for (Project project : getProjectList())
            if (project.isActive())
                activeProjects.add(project);
        return activeProjects;
    }

    /**
     * Gets all projects according to its active status.
     *
     * @return list of projectDTO.
     */
    public List<ProjectDTO> listProjectsByActiveStatus(boolean activeStatus) {

        List<ProjectDTO> projectList = new ArrayList<>();

        for (Project project : getProjectList()) {
            if (project.isActive() == activeStatus) {
                projectList.add(project.toDTO());
            }
        }
        return projectList;
    }

    /**
     * Get all project managers
     *
     * @return list with all project managers
     */
    public List<User> listAllProjectManager() {
        List<User> projectManagers = new ArrayList<>();

        for (Project p : getProjectList())
            if (p.getProjectManager() != null && !projectManagers.contains(p.getProjectManager().getUser()))
                projectManagers.add(p.getProjectManager().getUser());
        return projectManagers;
    }

    //
    // /**
    // * Get users completed tasks in reverse order (It already comes in reverse
    // order from listCollaboratorCompletedTasks)
    // *
    // * @param u User from which return your own completed tasks
    // * @return user completed tasks
    // */
    //
    public List<Task> listUserCompletedTasksInAllProjectsSortedByReverseOrder(User u) {

        List<Task> userCompletedTasks = new ArrayList<>();

        for (Project p : listUserProjects(u)) {
            for (ProjectCollaborator c : p.getProjectCollaboratorList())

                if (c.getUser().equals(u)) {
                    userCompletedTasks.addAll(p.listCollaboratorCompletedTasks(c));
                }
        }

        return userCompletedTasks;
    }

    /**
     * Get user completed tasks in all projects in reverse order
     *
     * @param user User from which return your own completed tasks in the last month
     * @return list of completed tasks
     */
    public List<TaskRestDTO> listUserCompletedTasksInAllProjectsSortedByReverseOrderToDTO(User user) {

        List<TaskRestDTO> taskDTOList = new ArrayList<>();

        for (Task task : listUserCompletedTasksInAllProjectsSortedByReverseOrder(user)) {

            taskDTOList.add(task.toDTO());
        }

        return taskDTOList;
    }

    /**
     * Get users completed tasks in the last month in reverse order (it comes in
     * reverse order from listCollaboratorsCompletedTasksLastMonth)
     *
     * @param u User from which return your own completed tasks in the last month
     * @return completed tasks in the last month
     */
    public List<Task> listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(User u) {

        List<Task> completedLastMonth = new ArrayList<>();

        for (Project p : listUserProjects(u))
            for (ProjectCollaborator c : p.getProjectCollaboratorList())
                if (c.getUser().equals(u))
                    completedLastMonth.addAll(p.listCollaboratorsCompletedTasksLastMonth(c));

        return completedLastMonth;
    }

    /**
     * Get users pending tasks in ascending order
     *
     * @param u User from which return your own pending tasks list
     * @return user pending tasks in ascending order
     */
    public List<Task> listUserPendentTasksInAllProjects(User u) {

        List<Task> listUserPendentTasksInAllProjects = new ArrayList<>();
        List<Task> listWithoutPredictedDateOfConclusion = new ArrayList<>();

        for (Project p : listUserProjects(u)) {

            for (ProjectCollaborator c : p.getProjectCollaboratorList()) {

                if (c.getUser().equals(u)) {
                    listUserPendentTasksInAllProjects.addAll(p.listCollaboratorTasksInReadyToStart(c));
                    listUserPendentTasksInAllProjects.addAll(p.listCollaboratorTasksInProgress(c));
                }
            }
        }

        for (Task task : listUserPendentTasksInAllProjects) {
            if (task.getPredictedDateOfConclusion() == null) {
                listWithoutPredictedDateOfConclusion.add(task);

            }
        }
        listUserPendentTasksInAllProjects.removeAll(listWithoutPredictedDateOfConclusion);

        listUserPendentTasksInAllProjects.sort(Comparator.comparing(Task::getPredictedDateOfConclusion));
        listUserPendentTasksInAllProjects.addAll(listWithoutPredictedDateOfConclusion);
        return listUserPendentTasksInAllProjects;
    }

    public List<TaskRestDTO> listUserPendingTasksInAllProjects(User u) {

        List<TaskRestDTO> taskDTOList = new ArrayList<>();

        for (Task task : listUserPendentTasksInAllProjects(u)) {

            taskDTOList.add(task.toDTO());
        }

        return taskDTOList;
    }

    public List<TaskRestDTO> listUserAllTasksInAllProjects(User u) {

        List<TaskRestDTO> taskDTOList = new ArrayList<>();

        for (Task task : listUserTasksInAllProjects(u)) {

            taskDTOList.add(task.toDTO());
        }

        return taskDTOList;
    }

    public List<ProjectDTO> listAllUserProjects(User user) {

        List<ProjectDTO> projectDTOList = new ArrayList<>();

        for (Project project : listUserProjects(user)) {

        	projectDTOList.add(project.toDTO());
        }

        return projectDTOList;
    }

    /**
     * Get users project list
     *
     * @param user User from which return your own project list
     * @return user projects
     */
    public List<Project> listUserProjects(User user) {
        // TOSOLVE verificar persitencia do projectcolllaborator list

        List<Project> userProjects = new ArrayList<>();

        for (Project p : getProjectList())
            for (ProjectCollaborator c : p.getProjectCollaboratorList())
                if (c.getUser().equals(user))
                    userProjects.add(p);
        return userProjects;
    }

    /**
     * Get users task list
     *
     * @param u User from which return your own tasks list
     * @return user tasks
     */
    public List<Task> listUserTasksInAllProjects(User u) {

        List<Task> userTasks = new ArrayList<>();

        for (Project p : listUserProjects(u))
            for (ProjectCollaborator c : p.getProjectCollaboratorList()) {
                if (c.getUser().equals(u))
                	userTasks.addAll(p.listCollaboratorTasks(c));
            }

        return userTasks;
    }




    /**
     * Get users task collaborator registrys in all projects
     *
     * @param User
     * @return list of taskCollaboratorRegistry
     */
    public List<TaskCollaboratorRegistry> listUserTaskCollabRegistryInAllProjectsOrderByLastReport(User u) {

        List<TaskCollaboratorRegistry> userTaskCollabRegistrys = new ArrayList<>();

        for (Project p : listUserProjects(u))
            for (ProjectCollaborator c : p.getProjectCollaboratorList()) {
                if (c.getUser().equals(u))
                	{
                	userTaskCollabRegistrys.addAll(p.listAllTaskCollaboratorRegistry(c));
                	}
            }

        userTaskCollabRegistrys.sort(Collections.reverseOrder());
        return userTaskCollabRegistrys;
    }

    /**
     * Lists a project's cost calculation options.
     *
     * @param projectId
     * @return
     */
    public List<String> listProjectCostCalculationOptions(String projectId) {

        Project project = getProjectByID(projectId);

        return project.getCostCalculationOptions();
    }

    /**
     * Lists a project's cost calculation options.
     *
     * @param projectId
     * @return DTO with cost calculation options
     */
    public AvailableCostOptionsDTO listProjectCostCalculationOptionsToDTO(String projectId) {

        AvailableCostOptionsDTO availableCostOptionsDTO = new AvailableCostOptionsDTO();
        for (String costOption : listProjectCostCalculationOptions(projectId)) {
            availableCostOptionsDTO.getAvailableCalculationOptions().add(costOption);
        }
        return availableCostOptionsDTO;
    }

    /**
     * Sets a project's cost calculation method.
     *
     * @param projectId and costCalculationChoice
     * @param costCalculationChoice Cost calculation choice.
     */
    public void setProjectCostCalculationMechanism(String projectId, String costCalculationChoice) {

        Project project = getProjectByID(projectId);
        project.setCostCalculatorPersistence(costCalculationChoice);
        project.getCostCalculator();
        updateProject(project);
    }

    /**
     * Sets a project's cost calculation method.
     *
     * @param projectId and costCalculationChoice
     * @param costCalculationChoice Cost calculation choice DTO.
     */
    public AvailableCostOptionsDTO setProjectCostCalculationMechanismToDTO(String projectId, String costCalculationChoice) {

        AvailableCostOptionsDTO availableCostOptionsDTO = new AvailableCostOptionsDTO();
        setProjectCostCalculationMechanism(projectId, costCalculationChoice);
        availableCostOptionsDTO.setCalculationOption(costCalculationChoice);
        return availableCostOptionsDTO;
    }

    /**
     * Lists all classes that implement the CostCalculation interface.
     *
     * @return list of string containing the simple name of all those classes.
     */
    public List<String> listAllCostCalculationOptions() {

        String costCalculatorPackageName = CostCalculator.class.getPackage().getName();
        ScanResult scanResult = new FastClasspathScanner(costCalculatorPackageName).scan();

        return extractSimpleClassNames(scanResult.getNamesOfClassesImplementing(CostCalculator.class));
    }

    /**
     * Lists all classes that implement the CostCalculation interface.
     *
     * @return DTO containing a list of all those classes.
     */
    public AvailableCostOptionsDTO listAllCostCalculationOptionsToDTO() {

        AvailableCostOptionsDTO availableCostOptionsDTO = new AvailableCostOptionsDTO();
        for(String costOption : listAllCostCalculationOptions()){
            availableCostOptionsDTO.getAvailableCalculationOptions().add(costOption);
        }
        return availableCostOptionsDTO;
    }

    /**
     * Establishes the available options to calculate the cost of a project thus
     * far, namely to resolve the ambiguities from a user submitting a report which
     * time span encompasses two or more user costs.
     *
     * @param projectId              ID of project.
     * @param costCalculationOptions List of chosen available options to calculate cost of project.
     */
    public void setListOfCostCalculationOptions(String projectId, List<String> costCalculationOptions) {

        Project project = getProjectByID(projectId);

        project.setCostCalculationOptions(costCalculationOptions);
        updateProject(project);
    }

    /**
     * Establishes the available options to calculate the cost of a project thus
     * far, namely to resolve the ambiguities from a user submitting a report which
     * time span encompasses two or more user costs.
     *
     * @param setProjectCostOptionsInDTO
     * @return SetProjectCostOptionsOutDTO
     */
    public ProjectCostOptionsDTO setListOfCostCalculationOptions(ProjectCostOptionsDTO setProjectCostOptionsInDTO) {

        Project project = getProjectByID(setProjectCostOptionsInDTO.getProjectId());

        if (setProjectCostOptionsInDTO.getCostCalculationOptions().isEmpty())
            return null;
        project.setCostCalculationOptions(setProjectCostOptionsInDTO.getCostCalculationOptions());
        updateProject(project);

        return setProjectCostOptionsInDTO;

    }

    /**
     * Method to calculate total cost so far.
     * @param projectId
     * @return total cost so far.
     */
    public double calculateTotalCostSoFar(String projectId) {

        Project project = getProjectByID(projectId);
        return project.calculateTotalCostSoFar();

    }

    /**
     * Delete all projects.
     */
    public void deleteAll() {
        projectRepository.deleteAll();

    }

    /**
     * Method to get a list of collaborators assignment requests for all tasks in a project
     * @param projectId
     * @return list of assignment requests DTOs
     */
    public List<TaskCollaboratorRegistryRESTDTO> listAssignmentRequestsToDTO(String projectId){

        List<TaskCollaboratorRegistryRESTDTO> taskCollaboratorRegistryDTOList = new ArrayList<>();
        List<TaskCollaboratorRegistry> taskcollaRegList = projectRepository.getOneByCode(projectId).listAssignmentRequests();
        for(TaskCollaboratorRegistry taskCollaboratorRegistry : taskcollaRegList){
            taskCollaboratorRegistryDTOList.add(taskCollaboratorRegistry.toDTO());
        }

        return taskCollaboratorRegistryDTOList;
    }

    /**
     * Method to get a list of collaborators removal requests for all tasks in a project
     * @param projectId
     * @return list of assignment requests DTOs
     */
    public List<TaskCollaboratorRegistryRESTDTO> listRemovalRequestsToDTO(String projectId){

        List<TaskCollaboratorRegistryRESTDTO> taskCollaboratorRegistryDTOList = new ArrayList<>();
        List<TaskCollaboratorRegistry> taskcollaRegList = projectRepository.getOneByCode(projectId).listRemovalRequests();
        for(TaskCollaboratorRegistry taskCollaboratorRegistry : taskcollaRegList){
            taskCollaboratorRegistryDTOList.add(taskCollaboratorRegistry.toDTO());
        }

        return taskCollaboratorRegistryDTOList;
    }


    /**
     * Method to get a list of costs of each task of a specific project
     *
     * @param projectId
     *
     * @return list of TaskCostRestDTO with costs of each task
     */
	public List<TaskCostRestDTO> getProjectTasksCostToDTO(String projectId) {
		List<TaskCostRestDTO> taskCosts = new ArrayList<>();

		if (projectRepository.existsById(projectId))
		{
			Project p = projectRepository.getOneByCode(projectId);
			for (Task t : p.getTasksList())
			{
				TaskCostRestDTO tc = new TaskCostRestDTO();
				tc.setTaskId(t.getId());
				tc.setTitle(t.getTitle());
				tc.setCost(t.calculateTotalCostsoFar(p.getCostCalculator()));
				taskCosts.add(tc);
			}
		}

		return taskCosts;
	}

}
