package project.model.project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class MaximumTimePeriodCost implements CostCalculator {

	@Override
	public double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {

		double maxValue=-1;
		
		
		Optional<Double> max =costAndTimePeriodList.stream()
				.map(ProjectCollaboratorCostAndTimePeriod :: getCost)
				.max(Comparator.naturalOrder());
	
		if(max.isPresent()) {
			
			maxValue=max.get();
		}
		
	  return report.getQuantity() * maxValue;
	
}
}