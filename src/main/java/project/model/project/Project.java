package project.model.project;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import project.dto.rest.ProjectDTO;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.lang.reflect.Constructor;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to create projects and manage team "projectCollaboratorList" and tasks
 * "tasksList".
 */
@Entity
@Proxy(lazy = false)
public class Project {

    @Transient
    Logger logger = Logger.getAnonymousLogger();
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "project_id")
    List<Task> tasks;
    @Size(min = 2, max = 500)
    private String description = "<no project description>";
    @Enumerated(EnumType.STRING)
    private ProjectUnits unit;
    private double globalBudget;
    @Column(nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isActive = true;
    private LocalDateTime startDate;
    private LocalDateTime finalDate;
    @Id
    private String code;
    private String name;
    @Enumerated(EnumType.STRING)
    private ProjectStatus status;
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "project_id")
    private List<ProjectCollaborator> projectCollaboratorList;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToOne(cascade = CascadeType.ALL)
    private ProjectCollaborator projectManager;
    @Transient
    private CostCalculator costCalculator;
    private String costCalculatorPersistence;
    @Embedded
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> costCalculationOptions = new ArrayList<>();

    /**
     * Constructor of a new project with id and name.
     *
     * @param id   Alpha-numerical reference of the project.
     * @param name Project name.
     */
    public Project(String id, String name) {
        code = id;
        this.name = name;
        status = ProjectStatus.START_UP;
        unit = ProjectUnits.HOURS;
        projectCollaboratorList = new ArrayList<>();
        tasks = new ArrayList<>();
        this.costCalculator = new LastTimePeriodCost();
        costCalculationOptions.add(costCalculator.getClass().getSimpleName());
        this.costCalculatorPersistence = this.costCalculator.getClass().getSimpleName();
    }

    public Project(String id, String name, List<String> costCalcOptions) {
        code = id;
        this.name = name;
        status = ProjectStatus.START_UP;
        unit = ProjectUnits.HOURS;
        projectCollaboratorList = new ArrayList<>();
        tasks = new ArrayList<>();
        setCostCalculationOptions(costCalcOptions);
    }

    protected Project() {
    }

    public Project(String projectId, String name, String description, LocalDateTime startDate, LocalDateTime finalDate) {
        code = projectId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.finalDate = finalDate;
        status = ProjectStatus.START_UP;
        unit = ProjectUnits.HOURS;
        projectCollaboratorList = new ArrayList<>();
        tasks = new ArrayList<>();
        this.costCalculator = new LastTimePeriodCost();
        costCalculationOptions.add(costCalculator.getClass().getSimpleName());
        this.costCalculatorPersistence = this.costCalculator.getClass().getSimpleName();
        this.isActive = true;

    }

    /**
     * @return the costCalculator interface's  absolute path
     */
    public String getCostCalculatorAbsolutePath() {

        return CostCalculator.class.getPackage().getName();
    }

    /**
     * Builds a DTO from project instance.
     *
     * @return ProjectDTO.
     */
    public ProjectDTO toDTO() {

        ProjectDTO projDTO = new ProjectDTO();
        projDTO.setProjectId(code);
        projDTO.setName(name);
        projDTO.setDescription(description);
        projDTO.setIsActive(isActive());
        projDTO.setUnit(unit.toString());
        projDTO.setGlobalBudget(globalBudget);
        projDTO.setUserEmail(projectManager.getUser().getEmail());
        projDTO.setStartDate(startDate);
        projDTO.setFinalDate(finalDate);
        projDTO.setCompletedTasks(listCompletedTasks().size());
        projDTO.setInitiatedButNotCompletedTasks(listInitiatedButNotCompletedTasks().size());
        projDTO.setNotInitiatedTasks(listNotInitiatedTasks().size());
        projDTO.setCanceledTasks(listCancelledTasks().size());
        projDTO.setUnassignedCollaborators(listUnassignedProjectCollaborator().size());
        projDTO.setTasks(getTasksList().size());
        projDTO.setCollaborators(listActiveProjectCollaborators().size());
        projDTO.setCurrentCost(calculateTotalCostSoFar());
        projDTO.setCostOptionSelected(costCalculatorPersistence);

        return projDTO;
    }

    /**
     * Auxiliary method to refresh task attributes in tasklist
     *
     * @param task
     * @return true if task was successfully updated in tasklist, false if tasklist
     * doesnt contains this task
     */
    public boolean refreshTaskList(Task task) {
        if (tasks.contains(task)) {
            tasks.set(tasks.indexOf(task), task);
            return true;
        }
        return false;
    }

    /**
     * Get project collaborator
     *
     * @param user
     * @return collaborator
     */
    public ProjectCollaborator findProjectCollaborator(User user) {
        for (ProjectCollaborator col : this.getProjectCollaboratorList())
            if (col.getUser().equals(user))
                return col;
        return null;
    }

    /**
     * @return the description of the project.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets a new project description.
     *
     * @param description New description of the project.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the project's final date.
     */
    public LocalDateTime getFinalDate() {
        return finalDate;
    }

    /**
     * sets a new project final date.
     *
     * @param finalDate the new project's final date.
     */
    public void setFinalDate(LocalDateTime finalDate) {
        this.finalDate = finalDate;
    }

    /**
     * Get overall project budget.
     *
     * @return global project budget.
     */
    public double getGlobalBudget() {
        return globalBudget;
    }

    /**
     * Sets overall project budget.
     *
     * @param globalBudget
     */
    public void setGlobalBudget(double globalBudget) {
        this.globalBudget = globalBudget;
    }

    /**
     * @return project ID.
     */
    public String getId() {
        return code;
    }

    /**
     * @return name of the project.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets project name.
     *
     * @param name new name of project.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return list with all the collaborators of the project.
     */
    public List<ProjectCollaborator> getProjectCollaboratorList() {
        return projectCollaboratorList;
    }

    /**
     * @return User project manager.
     */
    public ProjectCollaborator getProjectManager() {
        return projectManager;
    }

    /**
     * @return the start date of the project.
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * sets a new project start date.
     *
     * @param startDate the new start date for the project.
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the status of the project.
     */
    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    /**
     * @return list with all the project's tasks.
     */
    public List<Task> getTasksList() {

        return tasks;
    }

    /**
     * Get the project chosen unit.
     *
     * @return the chosen unit.
     */
    public ProjectUnits getUnit() {
        return unit;
    }

    /**
     * Sets the project budget unit.
     *
     * @param unit
     */
    public void setUnit(ProjectUnits unit) {
        this.unit = unit;

    }

    /**
     * Checks if collaborator has some task assigned.
     *
     * @param colab
     * @return true if collaborator has at least one task assigned, false otherwise
     */
    public boolean hasAssignedTasks(ProjectCollaborator colab) {
        for (Task t : tasks) {
            if (t.hasProjectCollaborator(colab))
                return true;
        }
        return false;
    }

    /**
     * Checks if User u is already in the project's collaborator list.
     *
     * @param colab ROLE_COLLABORATOR to be searched for.
     * @return true if collaborator is in the project's collaborator list, false
     * otherwise
     */
    public boolean hasActiveProjectCollaborator(ProjectCollaborator colab) {
        return listActiveProjectCollaborators().contains(colab);

    }

    /**
     * @return true if project is active, false if project is not active.
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * get list of all collaborators without any task assigned
     *
     * @return list of collaborators without assigned tasks
     */
    public List<ProjectCollaborator> listUnassignedProjectCollaborator() {
        List<ProjectCollaborator> unassignedColabList = new ArrayList<>();

        for (ProjectCollaborator collab : listActiveProjectCollaborators())
            if (!hasAssignedTasks(collab) && collab != getProjectManager())
                unassignedColabList.add(collab);

        return unassignedColabList;
    }

    /**
     * Sets project as active or not active.
     */
    public void setActive() {
        isActive = true;
    }

    /**
     * Sets project as active or not active.
     */
    public void setInactive() {
        isActive = false;
    }

    /**
     * If costCalculator is null(condition achieved when the object is returned from the
     * DB), this method "reconstructs" the attribute using reflection. It uses
     * String costCalculatorPersistence, which is a string (persisted) representation of costCalculator for
     * the "reconstruction".
     *
     * @return costCalculator
     */
    public CostCalculator getCostCalculator() {

        String costCalculatorClassName = "";

        if (this.costCalculator != null) {
            costCalculatorClassName = this.costCalculator.getClass().getSimpleName();
        }

        if (!costCalculatorClassName.equals(this.costCalculatorPersistence)) {

            try {
                constructInstanceOfCostCalculator();
            } catch (Exception e) {
                logger.log(Level.ALL, "Could not construct instance of costCalculator.\n" + e.getMessage(), e);
            }
        }
        return costCalculator;
    }

    public void setCostCalculator(CostCalculator costCalculator) {
        this.costCalculator = costCalculator;
        this.costCalculatorPersistence = this.costCalculator.getClass().getSimpleName();
    }

    /**
     * Constructs instance of CostCalculator Interface through reflection.
     *
     * @throws Exception
     */
    private void constructInstanceOfCostCalculator() throws Exception {

        Class<?> clazz = Class.forName(getCostCalculatorAbsolutePath() + "." + this.costCalculatorPersistence);
        Constructor<?> construct = clazz.getConstructor();
        this.costCalculator = (CostCalculator) construct.newInstance();
    }

    public String getCostCalculatorPersistence() {
        return costCalculatorPersistence;
    }

    public void setCostCalculatorPersistence(String costCalculatorPersistence) {
        this.costCalculatorPersistence = costCalculatorPersistence;
    }

    public boolean setProjectManager(ProjectCollaborator collab) {
        if (projectCollaboratorList.contains(collab)) {
            this.projectManager = collab;
            return true;
        }
        return false;
    }

    public List<String> getCostCalculationOptions() {
        return costCalculationOptions;
    }

    public void setCostCalculationOptions(List<String> costCalculationOptions) {
        if (!costCalculationOptions.contains(costCalculatorPersistence)) {
            this.costCalculatorPersistence = costCalculationOptions.get(0);
            this.costCalculator = getCostCalculator();
        }
        this.costCalculationOptions = costCalculationOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Project))
            return false;

        Project project = (Project) o;

        return code.equals(project.code);
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    /*
      String representation of a project, with project ID, Project Manager and
      current state.

      @return Project object data as a string.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ProjectCollaborator pc : projectCollaboratorList) {
            sb.append(pc.getUser().getName());
            sb.append(" (");
            sb.append(pc.getUser().getEmail());
            sb.append("); ");
        }

        String projCollab = sb.toString();

        StringBuilder sbt = new StringBuilder();

        SortedSet<Task> taskSorted = new TreeSet<>(Comparator.comparing(Task::getTitle));
        taskSorted.addAll(getTasksList());

        for (Task t : taskSorted) {
            sbt.append(t.getId());
            sbt.append("-");
            sbt.append(t.getTitle());
            sbt.append(" ");
            sbt.append(t.getStatus().getClass().getSimpleName());
            sbt.append("; ");
        }

        String projTaskList = sbt.toString();

        return "\nProject id=" + code + "\nname=" + name + "\ndescription=" + description + "\nprojectManager="
                + projectManager + "\nstatus=" + status + "\nunit=" + unit + "\nglobalBudget=" + globalBudget
                + "\nisActive=" + isActive + "\nstartDate=" + startDate + "\nfinalDate=" + finalDate
                + "\nprojectCollaboratorList=" + projCollab + "\ntasksList=" + projTaskList;
    }

    public void addProjectCollaborator(ProjectCollaborator projectCollaborator) {
        if (!getProjectCollaboratorList().contains(projectCollaborator)) {
            projectCollaboratorList.add(projectCollaborator);
        }

    }

    /**
     * Remove project collaborator from projectCollaborators Active list
     *
     * @param projectCollaborator
     */
    public void removeProjectCollaborator(ProjectCollaborator projectCollaborator) {
        if (getProjectCollaboratorList().contains(projectCollaborator) && projectCollaborator.isActiveInProject()) {
            projectCollaborator.getLastCostAndTimePeriod().setEndDate(LocalDate.now());
        }

    }

    /**
     * Get number of hours (by user) for completed tasks in the last month
     *
     * @param collab User from which return number of hours for your completed tasks in
     *               the last month
     * @return total of hours
     */
    public double calculateCollaboratorsHoursCompletedTasksLastMonth(ProjectCollaborator collab) {

        double totalHours = 0;

        List<Task> collabTasksCompletedLastMonth = listCollaboratorsCompletedTasksLastMonth(collab);

        for (Task t : collabTasksCompletedLastMonth)
            totalHours = totalHours + t.calculateProjectCollaboratorWorkedHours(collab);

        return totalHours;
    }

    /**
     * Get collaborators completed tasks in the last month
     *
     * @param collab ROLE_COLLABORATOR from which return your own completed tasks in the
     *               last month
     * @return completed tasks in the last month
     */
    public List<Task> listCollaboratorsCompletedTasksLastMonth(ProjectCollaborator collab) {

        List<Task> completedLastMonth = new ArrayList<>();

        for (Task t : listCompletedTasks()) {
            if (t.hasProjectCollaboratorInTaskCollaboratorRegistryList(collab) && t.isCompletedLastMonth())
                completedLastMonth.add(t);
        }

        completedLastMonth.sort(Collections.reverseOrder());
        return completedLastMonth;
    }

    /**
     * Gets the listOfTasks's completed tasks.
     *
     * @return List with all completed tasks.
     */
    public Set<Task> listCompletedTasks() {

        List<Task> listOfCompletedTasks = new ArrayList<>();

        for (Task task : getTasksList())
            if (task.isCompleted())
                listOfCompletedTasks.add(task);

        return new LinkedHashSet<>(listOfCompletedTasks);
    }

    /**
     * Get collaborators completed tasks in reverse order
     *
     * @param colab Collaborators from which return your own completed tasks
     * @return collaborator's completed tasks in reverse order
     */
    public List<Task> listCollaboratorCompletedTasks(ProjectCollaborator colab) {

        List<Task> collaboratorCompletedTasks = new ArrayList<>();

        for (Task t : listCompletedTasks()) {
            if (t.hasProjectCollaboratorInTaskCollaboratorRegistryList(colab))
                collaboratorCompletedTasks.add(t);
        }
        collaboratorCompletedTasks.sort(Collections.reverseOrder());

        return collaboratorCompletedTasks;
    }

    /**
     * List tasks in Cancelled State
     *
     * @return collaborators Cancelled tasks
     */
    public Set<Task> listCancelledTasks() {

        Set<Task> cancelledTasks = new HashSet<>();

        for (Task task : getTasksList())
            if (task.getTaskState().isOnCancelledState())
                cancelledTasks.add(task);

        return cancelledTasks;
    }

    /**
     * List collaborators tasks in ReadyToStart State
     *
     * @param collab ROLE_COLLABORATOR from which return your own ReadyToStart tasks list
     * @return collaborators ReadyToStart tasks
     */
    public List<Task> listCollaboratorTasksInReadyToStart(ProjectCollaborator collab) {

        List<Task> collabPendentTasks = new ArrayList<>();

        for (Task t : listCollaboratorTasks(collab))
            if (t.getTaskState().isOnReadyToStartState())
                collabPendentTasks.add(t);

        return collabPendentTasks;
    }

    /**
     * List collaborators tasks in InProgress State
     *
     * @param collab ROLE_COLLABORATOR from which return your own InProgress tasks list
     * @return collaborators InProgress tasks
     */
    public List<Task> listCollaboratorTasksInProgress(ProjectCollaborator collab) {

        List<Task> collabInProgressTasks = new ArrayList<>();

        for (Task t : listCollaboratorTasks(collab))
            if (t.getTaskState().isOnInProgressState())
                collabInProgressTasks.add(t);

        return collabInProgressTasks;
    }

    /**
     * List collaborators not initiated tasks
     *
     * @return List of not initiated tasks
     */
    public Set<Task> listNotInitiatedTasks() {

        Set<Task> notInitiatedTasks = new HashSet<>();

        for (Task t : getTasksList())
            if (t.getTaskState().isOnCreatedState() || t.getTaskState().isOnPlannedState()
                    || t.getTaskState().isOnAssignedState() || t.getTaskState().isOnReadyToStartState())
                notInitiatedTasks.add(t);

        return notInitiatedTasks;
    }

    /**
     * Checks all the tasks a user is involved with.
     *
     * @param collab User to search for in all the project's tasks.
     * @return List of all the tasks pertaining to User u.
     */
    public List<Task> listCollaboratorTasks(ProjectCollaborator collab) {

        List<Task> collaboratorTasks = new ArrayList<>();

        for (Task t : getTasksList())
            if (t.hasProjectCollaborator(collab))
                collaboratorTasks.add(t);
        return collaboratorTasks;
    }

    /**
     * Get average hours (by collaborator) for completed tasks in the last month
     *
     * @param collab ROLE_COLLABORATOR from which return average hours for your completed
     *               tasks in the last month
     * @return average hours
     */
    public double calculateCollaboratorsAverageHoursCompletedTasksLastMonth(ProjectCollaborator collab) {

        double averageHours;

        double totalHours = calculateCollaboratorsHoursCompletedTasksLastMonth(collab);

        int numberTasks = listCollaboratorsCompletedTasksLastMonth(collab).size();

        if (numberTasks == 0)
            averageHours = 0;
        else
            averageHours = totalHours / numberTasks;
        return averageHours;
    }

    /**
     * Calculates the total cost of the project to this moment.
     *
     * @return the cost of the project so far.
     */
    public double calculateTotalCostSoFar() {

        double cost = 0.0;
        try {
            for (Task t : getTasksList()) {
                cost += t.calculateTotalCostsoFar(this.getCostCalculator());
            }
        } catch (NullPointerException e) {
            logger.log(Level.ALL, "No cost calculation method defined", e);
            return 0;
        }
        return cost;
    }

    /**
     * Calculates the total cost of the project to this moment.
     *
     * @return the cost of the project so far.
     */
    public double calculateCostSoFarOfTask(Task t) {

        double cost;
        try {
            cost = t.calculateTotalCostsoFar(this.getCostCalculator());
        } catch (NullPointerException e) {
            logger.log(Level.ALL, "No cost calculation method defined", e);
            return 0;
        }
        return cost;
    }

    /**
     * Retrieves a specific task taking into account its title.
     *
     * @param title search parameter.
     * @return a specific task, if it's found, or null if it is not found.
     */
    public Task findTaskByTitle(String title) {

        for (Task task : getTasksList())
            if (task.getTitle().equals(title)) {
                return task;
            }
        return null;
    }

    /**
     * Method that searches a project's list of tasks for a task with the given ID.
     *
     * @param taskId ID of task to be searched.
     * @return the tasks with that ID or null, if it doesn't find it.
     */
    public Task findTaskByID(String taskId) {

        for (Task task : getTasksList())
            if (task.getId().equals(taskId)) {
                return task;
            }
        return null;
    }

    /**
     * Checks if project has a specific task.
     *
     * @param t Task to be searched for in the project's task list.
     * @return true if task t is in the project task list, false otherwise.
     */
    public boolean hasTask(Task t) {

        boolean result = false;

        for (Task task : getTasksList())
            if (t.equals(task))
                result = true;
        return result;
    }

    /**
     * Gets all the reports from all the project's tasks.
     *
     * @return List with all the reports.
     */
    public List<Report> listAllReports() {
        List<Report> rep = new ArrayList<>();
        for (Task task : getTasksList())
            rep.addAll(task.listAllReports());
        return rep;
    }

    /**
     * Gets the listOfTask's tasks that have been initiated but are still to be
     * completed.
     *
     * @return list with all initiated but not completed tasks.
     */
    public Set<Task> listInitiatedButNotCompletedTasks() {

        Set<Task> listOfInitiatedButNotCompletedTasks = new HashSet<>();

        for (Task task : getTasksList())
            if ((task.getStatus().isOnInProgressState() || task.getStatus().isOnSuspendedState()))
                listOfInitiatedButNotCompletedTasks.add(task);
        return listOfInitiatedButNotCompletedTasks;
    }

    /**
     * Get the list of not completed tasks and with predicted date of conclusion
     * expired.
     *
     * @return list with all not completed tasks and with predicted date of
     * conclusion expired.
     */
    public Set<Task> listNotCompletedTasksAndWithPredictedDateOfConclusionExpired() {

        Set<Task> listOfNotCompletedTasksAndWithPredictedDateOfConclusionExpired = new HashSet<>();

        for (Task task : getTasksList())
            if (!(task.isCompleted()) && (task.getPredictedDateOfConclusion() != null)
                    && (task.getPredictedDateOfConclusion().toLocalDate().isBefore(LocalDate.now())))
                listOfNotCompletedTasksAndWithPredictedDateOfConclusionExpired.add(task);
        return listOfNotCompletedTasksAndWithPredictedDateOfConclusionExpired;
    }

    /**
     * Searches all task that have no collaborator associated with.
     *
     * @return List with all tasks.
     */
    public Set<Task> listUnassignedTasks() {
        Set<Task> unassignedTaskList = new HashSet<>();

        for (Task t : getTasksList())
            if (!t.hasAssignedProjectCollaborator())
                unassignedTaskList.add(t);
        return unassignedTaskList;
    }

    /**
     * Gets the list of uninitialized tasks.
     *
     * @return list with all uninitialized tasks.
     */
    public Set<Task> listUninitializedTasks() {

        Set<Task> listOfUninitializedTasks = new LinkedHashSet<>();

        for (Task task : getTasksList())
            if (task.getEffectiveStartDate() == null)
                listOfUninitializedTasks.add(task);
        return listOfUninitializedTasks;
    }

    /**
     * Gets the list of assignment requests
     *
     * @return list with all of assignment requests
     */
    public List<TaskCollaboratorRegistry> listAssignmentRequests() {

        List<TaskCollaboratorRegistry> listAssignmentRequests = new ArrayList<>();

        for (Task task : getTasksList()) {

            listAssignmentRequests.addAll(task.listAssignmentRequests());

        }

        return listAssignmentRequests;
    }

    /**
     * Gets the list of assignment requests
     *
     * @return list with all of assignment requests
     */
    public List<TaskCollaboratorRegistry> listRemovalRequests() {

        List<TaskCollaboratorRegistry> listRemovalRequests = new ArrayList<>();

        for (Task task : getTasksList()) {

            listRemovalRequests.addAll(task.listRemovalRequests());

        }

        return listRemovalRequests;
    }


    /**
     * Gets the list of TaskCollaboratorRegistry of projectCollaborator
     *
     * @return list of askCollaboratorRegistry
     */
    public List<TaskCollaboratorRegistry> listAllTaskCollaboratorRegistry(ProjectCollaborator projectCollaborator) {
        List<TaskCollaboratorRegistry> taskCollRegList = new ArrayList<>();

        for (Task t : getTasksList())
            taskCollRegList.addAll(t.listTaskCollaboratorRegistry(projectCollaborator));

        return taskCollRegList;
    }

    /**
     * Change dependent task status.
     *
     * @param taskDependencySolved
     */
    public List<Task> changeDependentTasksStatus(Task taskDependencySolved) {

        List<Task> tasksThatChangedState = new ArrayList<>();
        List<Task> taskDependencies;

        for (Task task : getTasksList()) {

            taskDependencies = task.getTaskDependencies();
            if (taskDependencies.contains(taskDependencySolved)) {
                task.getTaskDependencies().remove(taskDependencySolved);
                task.getTaskState().changeTo();
                tasksThatChangedState.add(task);
            }
            task.setTaskStatePersisted(task.getTaskState().getClass().getSimpleName()); // NECESSARY CODE!
        }
        return tasksThatChangedState;
    }

    public void removeTask(Task task) {
        if (task.getTaskState().isOnCreatedState() || task.getTaskState().isOnPlannedState()
                || task.getTaskState().isOnAssignedState() || task.getTaskState().isOnReadyToStartState())
            tasks.remove(task);
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    /**
     * @return list with all Active collaborators of the project.
     */
    public List<ProjectCollaborator> listActiveProjectCollaborators() {
        List<ProjectCollaborator> activeProjCollaborators = new ArrayList<>();
        for (ProjectCollaborator collab : projectCollaboratorList) {
            if (collab.isActiveInProject())
                activeProjCollaborators.add(collab);
        }
        return activeProjCollaborators;
    }

    public enum ProjectStatus {
        CLOSED, DELIVERY, EXECUTION, PLANNED, START_UP, WARRANTY
    }

    public enum ProjectUnits {
        HOURS, PEOPLE_MONTH
    }

}