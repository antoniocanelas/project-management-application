package project.model.project;

import java.util.List;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public interface CostCalculator {

	double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList);
}
