package project.model.project;

import java.util.List;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class LastTimePeriodCost implements CostCalculator {

	@Override
	public double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {
		
		
		return report.getQuantity() * costAndTimePeriodList.get(costAndTimePeriodList.size() - 1).getCost();
	}

}
