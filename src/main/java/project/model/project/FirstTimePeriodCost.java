package project.model.project;

import java.util.List;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class FirstTimePeriodCost implements CostCalculator {
	
	@Override
	public double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {
		
		return report.getQuantity() * costAndTimePeriodList.get(0).getCost();
	}
}
