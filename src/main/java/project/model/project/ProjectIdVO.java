package project.model.project;

import java.util.UUID;

public class ProjectIdVO {
    private UUID projectId;

    public ProjectIdVO() {
        this.projectId = UUID.randomUUID();
    }

    public UUID getProjectId() {
        return projectId;
    }
}
