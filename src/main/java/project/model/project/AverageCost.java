package project.model.project;

import java.util.List;

import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class AverageCost implements CostCalculator {

	@Override
	public double calculateReportCost(Report report, List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList) {
		double cost;
		double sum = 0;
		double quantity = report.getQuantity();

		for (ProjectCollaboratorCostAndTimePeriod pjct : costAndTimePeriodList) {
			sum += pjct.getCost();
		}
		if (!costAndTimePeriodList.isEmpty()) {
			cost = sum / (costAndTimePeriodList.size());
		} else {
			cost = 0;
		}
		return quantity * cost;
	}
}
