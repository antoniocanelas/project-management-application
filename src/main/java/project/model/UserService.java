package project.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import project.dto.UserDTO;
import project.dto.UserRegistryDTO;
import project.dto.rest.LoginRestDTO;
import project.dto.rest.PasswordRestDTO;
import project.dto.rest.UserRegistryRestDTO;
import project.dto.rest.UserRestDTO;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.util.UserBuilder;
import project.util.UserConverter;
import project.util.UserConverterFactory;
import system.dto.LoginTokenDTO;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserService {

    UserBuilder userBuilder;
    UserConverter userConverter;
    UserRegistryDTO userRegistryDto;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepo;

    public UserService(UserRepository uRepo) {
        this.userRepo = uRepo;

    }

    public UserService() {

    }

    /**
     * Method to create an user.
     *
     * @param name
     * @param phone
     * @param email
     * @param tin
     * @param birthDate
     * @param addressId
     * @param street
     * @param postalCode
     * @param city
     * @param country
     * @param password
     * @return user.
     * @throws AddressException
     */
    private static User createUser(String name, String phone, String email, String tin, LocalDate birthDate, String addressId, String street, String postalCode, String city, String country, String password) throws AddressException {
        User user = new User(name, phone, email, tin, birthDate, addressId, street, postalCode, city, country);
        user.setPassword(password);
        return user;
    }

    /**
     * Validates and adds a new user to userRegistry.
     *
     * @param name
     * @param phone
     * @param email
     * @param tin
     * @param birthDate
     * @param id
     * @param street
     * @param postalCode
     * @param city
     * @param country
     * @return True if registration is successful, false otherwise.
     * @throws AddressException
     */
    public boolean addUser(String name, String phone, String email, String tin, LocalDate birthDate, String id,
                           String street, String postalCode, String city, String country) throws AddressException {

        if (userRepo.existsById(UserIdVO.create(email))) {
            return false;
        }
        User user = createUser(name, phone, email, tin, birthDate, id, street, postalCode, city, country);
        if (user.isValid(email)) {
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Validates and adds a new user to userRegistry.
     *
     * @param userInDTO
     * @return UserInDTO if registration is successful, null otherwise.
     * @throws AddressException
     */
    public UserRegistryRestDTO addUserDTO(UserRegistryRestDTO userInDTO) {
        UserIdVO userIdVO;
        userIdVO = UserIdVO.create(userInDTO.getEmail());
        if (userRepo.existsById(userIdVO)) {
            return null;
        }
        User user;
        try {
            user = createUser(userInDTO.getName(), userInDTO.getPhone(), userInDTO.getEmail(),
                    userInDTO.getTin(), userInDTO.getBirthDate(), userInDTO.getAddressId(), userInDTO.getStreet(),
                    userInDTO.getPostalCode(), userInDTO.getCity(), userInDTO.getCountry(), userInDTO.getPassword());
            if (user.isValid(userInDTO.getEmail())) {
                user.setProfileRegisteredUser();
                userRepo.save(user);
                return userInDTO;
            }
        } catch (AddressException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "An exception was thrown", e);
        }
        return null;
    }

    /**
     * Add a user to the company's list of users.
     *
     * @param user to be added to the list.
     * @return true if the user is successfully added, false otherwise.
     */
    public boolean addUser(User user) {

        if (userRepo.existsById(user.getUserIdVO())) {
            return false;
        }

        if (user.isValid(user.getEmail())) {
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Creates an instance of a User.
     *
     * @param name
     * @param phone
     * @param email
     * @param tin
     * @param birthDate
     * @param id
     * @param street
     * @param postalCode
     * @param city
     * @param country
     * @return a user.
     * @throws AddressException
     */
    public User createUser(String name, String phone, String email, String tin, LocalDate birthDate, String id,
                           String street, String postalCode, String city, String country) throws AddressException {

        return new User(name, phone, email, tin, birthDate, id, street, postalCode, city, country);
    }

    /**
     * Method to update user.
     *
     * @param user
     * @return true if it was updated, false otherwise.
     */
    public boolean updateUser(User user) {

        if (userRepo.existsById(user.getUserIdVO()) && user.isValid(user.getEmail())) {
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Lists all registered users.
     *
     * @return List of all registered users in company
     */
    public List<User> listAllUsers() {

        return userRepo.findAll();
    }

    /**
     * Lists all registered users.
     *
     * @return List of all registered users in company
     */
    public List<UserRestDTO> listAllUsersToUI() {

        List<UserRestDTO> userDTOList = new ArrayList<>();

        for (User user : userRepo.findAll()) {
            UserRestDTO userRestDTO = user.toDTO();
            userDTOList.add(userRestDTO);
        }
        return userDTOList;
    }

    /**
     * Searches all company registered users taking "email" as search parameter.
     *
     * @param email Search parameter.
     * @return User that matches search parameter or null if no user is found.
     */
    public User searchUserByEmail(String email) {
        UserIdVO userIdVO;
        userIdVO = UserIdVO.create(email);

        if (userRepo.existsById(userIdVO)) {
            return userRepo.getOneByUserIdVO(userIdVO);
        }
        return null;
    }

    /**
     * Searches all company registered users taking "email" as search parameter and return user converted in DTO
     *
     * @param email Search parameter.
     * @return UserDTO that matches search parameter or null if no user is found.
     */
    public UserRestDTO searchUserDTOByEmail(String email) {
        if (searchUserByEmail(email) == null)
            return null;
        return searchUserByEmail(email).toDTO();
    }

    /**
     * Searches all company registered users taking "profile" as search parameter.
     *
     * @param role Search parameter.
     * @return List of users that match the search parameter.
     */
    public List<User> searchUserByProfile(Role role) {

        List<User> profileList = new ArrayList<>();
        for (User elem : listAllUsers())
            if (elem.getRoles().contains(role)) {
                profileList.add(elem);
            }
        return profileList;
    }

    /**
     * Searches all company registered users taking "profile" as search parameter.
     *
     * @param role Search parameter.
     * @return List of userDTO that match the search parameter.
     */
    public List<UserRestDTO> searchUserDTOByProfile(Role role) {

        List<UserRestDTO> profileList = new ArrayList<>();

        for (User elem : listAllUsers())
            if (elem.getRoles().contains(role) && elem.isActive()) {
                profileList.add(elem.toDTO());
            }
        return profileList;
    }

    /**
     * Sets an user as Inactive
     *
     * @param user user to set Inactive
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setUserActive(User user) {

        if (userRepo.existsById(user.getUserIdVO()) && !user.isActive()) {
            user.setActive();
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Sets an user as inactive
     *
     * @param user user to set inactive
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setUserInactive(User user) {
        if (userRepo.existsById(user.getUserIdVO()) && user.isActive()) {
            user.setInactive();
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Sets a registered user as ROLE_DIRECTOR
     *
     * @param user User to which we will assign the director profile
     * @return True if assignment is successful, false otherwise.
     */
    public String setUserProfileDirector(User user) {

        String message = "User could not be set to ROLE_DIRECTOR profile because he does not exist in the company's user registry.";

        if (user == null || !userRepo.existsById(user.getUserIdVO())) {
            return message;
        }
        if (user.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR))) {
            message = "This user has already the ROLE_DIRECTOR profile.";
        } else {
            user.setProfileDirector();
            userRepo.save(user);
            message = "User with the email " + user.getEmail() + " has been set as ROLE_DIRECTOR profile.";
        }
        return message;
    }

    /**
     * Sets a registered user as ROLE_DIRECTOR
     *
     * @param user User to which we will assign the director profile
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setUserProfileDirectorRest(User user) {

        if (user != null && userRepo.existsById(user.getUserIdVO())) {
            if (user.getRoles().contains(roleRepository.getOneByName(RoleName.ROLE_DIRECTOR))) {
                return false;
            } else {
                Set<Role> userRoles = new LinkedHashSet<>();
                userRoles.add(roleRepository.getOneByName(RoleName.ROLE_DIRECTOR));
                user.setRoles(userRoles);
                userRepo.save(user);
            }
        }
        return true;
    }

    /**
     * Sets a registered user as ROLE_COLLABORATOR
     *
     * @param user User to which we will assign the collaborator profile
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setUserProfileCollaboratorRest(User user) {
        if (user != null && userRepo.existsById(user.getUserIdVO())) {
            if (user.getRoles().contains(roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR))) {
                return false;
            } else {
                Set<Role> userRoles = new LinkedHashSet<>();
                userRoles.add(roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR));
                user.setRoles(userRoles);
                userRepo.save(user);
            }
        }
        return true;
    }

    /**
     * Sets a registered user as ROLE_COLLABORATOR
     *
     * @param user User to which we will assign the collaborator profile
     * @return True if assignment is successful, false otherwise.
     */
    public String setUserProfileCollaborator(User user) {

        String message = "User could not be set to ROLE_COLLABORATOR profile because he does not exist in the company's user registry.";
        if (user != null && userRepo.existsById(user.getUserIdVO())) {
            if (user.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR))) {
                message = "This user has already the ROLE_COLLABORATOR profile.";
            } else {
                user.setProfileCollaborator();
                userRepo.save(user);
                message = "User with the email " + user.getEmail() + " has been set as ROLE_COLLABORATOR profile.";
            }
        }
        return message;
    }

    /**
     * Searches user by his email and, if successful, passes the password to be
     * verified and validated.
     *
     * @param email    Required login parameter.
     * @param password Parameter to be passed on to validatePassword method (in User class), if email is found.
     * @return The result of validatePassword (in User class), if successful, or an
     * error message otherwise.
     */
    public LoginTokenDTO validateData(String email, String password) {
        User userByThatEmail;
        UserIdVO userIdVO;
        userIdVO = UserIdVO.create(email);
        if (!userRepo.existsById(userIdVO)) {
            return new LoginTokenDTO("Invalid Email or Password");
        }

        userByThatEmail = userRepo.getOneByUserIdVO(userIdVO);
        return userByThatEmail.validatePassword(password);
    }

    /**
     * Method that check credentials validation.
     *
     * @param email            user email
     * @param password         user password
     * @param confirmationCode user confirmation code
     * @return DTO filled with information about validation
     */
    public LoginTokenDTO validateDataFirstLogin(String email, String password, String confirmationCode) {
        UserIdVO userIdVO;
        userIdVO = UserIdVO.create(email);
        User user = userRepo.getOneByUserIdVO(userIdVO);
        LoginTokenDTO loginTokenDTO = user.validatePasswordAndConfirmationCode(password, confirmationCode);
        updateUser(user);
        return loginTokenDTO;

    }

    /**
     * Method to change password
     *
     * @param email             user email
     * @param passwordRestInDTO password DTO
     * @return DTO filled with information about user password
     */
    public PasswordRestDTO setPassword(String email, PasswordRestDTO passwordRestInDTO) {

        User user;
        String password = passwordRestInDTO.getNewPassword();

        UserIdVO userIdVO = UserIdVO.create(email);

        if (email == null)
            return null;
        user = userRepo.getOneByUserIdVO(userIdVO);
        user.setPassword(password);
        userRepo.save(user);
        return passwordRestInDTO;
    }

    //US 103 - Verificar autenticidade dos novos utilizadores através do envio de um email

    /**
     * Method to activate an account.
     *
     * @param loginRestDTO
     * @return a LoginTokenDTO.
     */
    public LoginTokenDTO activateAccount(LoginRestDTO loginRestDTO) {
        UserIdVO userIdVO;
        String email = loginRestDTO.getEmail();
        String password = loginRestDTO.getPassword();
        String confirmationCode = loginRestDTO.getConfirmationCode();
        userIdVO = UserIdVO.create(email);
        if (email == null || password == null || confirmationCode == null) {
            return new LoginTokenDTO("Invalid Data introduced!", 1);
        }

        if (!userRepo.existsById(userIdVO)) {
            return new LoginTokenDTO("Invalid Email or Password", 1);
        }

        return validateDataFirstLogin(email, password, confirmationCode);

    }

    /**
     * Searches user by his email and, if successful, passes the password to be
     * verified and validated.
     *
     * @param loginRestDTO Required DTO login parameter.
     *                     class), if email is found.
     * @return The result of validatePassword (in User class), if successful, or an
     * error message otherwise.
     */
    public LoginTokenDTO validateDataRest(LoginRestDTO loginRestDTO) {
        User user = null;
        String email = loginRestDTO.getEmail();
        String password = loginRestDTO.getPassword();
        UserIdVO userIdVO;
        userIdVO = UserIdVO.create(email);

        if (email != null && password != null && userRepo.existsById(userIdVO)) {
            user = userRepo.getOneByUserIdVO(userIdVO);

        }
        if (user == null) {
            return null;
        }
        return user.validatePasswordRest(password);
    }

    /**
     * Sets a registered user as ROLE_DIRECTOR
     *
     * @param user User to which we will assign the director profile
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setProfileDirectorTo(User user) {

        if (userRepo.existsById(user.getUserIdVO())) {
            user.setProfileDirector();
            return true;
        }
        return false;
    }

    /**
     * Sets a registered user as ROLE_COLLABORATOR
     *
     * @param user User to which we will assign the collaborator profile
     * @return True if assignment is successful, false otherwise.
     */
    public boolean setProfileCollaboratorTo(User user) {

        if (userRepo.existsById(user.getUserIdVO())) {
            user.setProfileCollaborator();
            userRepo.save(user);
            return true;
        }
        return false;
    }

    /**
     * Method to populate users from a file.
     *
     * @param filename
     * @return users.
     * @throws Exception
     */
    public List<User> usersPopulateDatabaseFromFile(String filename) throws Exception {

        String userDirectory = System.getProperty("user.dir");
        String inputUserFile = userDirectory + "/src/main/resources/project/util/" + filename;
        userBuilder = new UserBuilder(this);

        userConverter = UserConverterFactory.createUserConverter(inputUserFile);
        userRegistryDto = userConverter.parseToUser(inputUserFile);
        for (UserDTO userDTO : userRegistryDto.getListUsers()) {
            userBuilder.createAndPersistUser(userDTO);
        }

        List<User> users = this.listAllUsers();

        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            if (i >= 2) {
                Role userRole = new Role(RoleName.ROLE_COLLABORATOR);
                user.getRoles().add(userRole);
            }
            this.updateUser(user);
        }
        return users;
    }

    public List<User> usersPopulateDatabaseFromFileRest(String filePath) throws Exception {

        userBuilder = new UserBuilder(this);

        List<User> users = new ArrayList<>();

        userConverter = UserConverterFactory.createUserConverter(filePath);
        userRegistryDto = userConverter.parseToUser(filePath);
        for (UserDTO userDTO : userRegistryDto.getListUsers()) {
            if (searchUserByEmail(userDTO.getEmail()) == null) {
                userBuilder.createAndPersistUser(userDTO);
                User user = searchUserByEmail(userDTO.getEmail());
                user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
                users.add(user);
            }
        }

        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            Set<Role> userRoles = new LinkedHashSet<>();
            userRoles.add(roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR));
            user.setRoles(userRoles);
            this.updateUser(user);
        }
        return users;
    }

    public boolean setUserProfileAdministrator(User user) {

        if (user != null && userRepo.existsById(user.getUserIdVO())) {
            if (user.getRoles().contains(roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR))) {
                return false;
            } else {
                Set<Role> userRoles = new LinkedHashSet<>();
                userRoles.add(roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR));
                user.setRoles(userRoles);
                userRepo.save(user);
            }
        }
        return true;
    }

    public boolean setUserProfileRegisteredUserRest(User user) {

        if (user == null) {
            return false;
        } else if (userRepo.existsById(user.getUserIdVO())) {
            if (user.getRoles().contains(roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER))) {
                return false;
            }
            Set<Role> userRoles = new LinkedHashSet<>();
            userRoles.add(roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER));
            user.setRoles(userRoles);
            userRepo.save(user);

        }
        return true;
    }
}
