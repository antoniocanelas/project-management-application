package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Planned extends BaseState {
    Task task;

    public Planned(Task task) {
        this.task = task;
    }

    private static boolean conditionPredictedDates(Task task) {
        return task.getPredictedDateOfStart() != null || task.getPredictedDateOfConclusion() != null;
    }

    private static boolean conditionEffectiveDates(Task task) {
        return task.getEffectiveStartDate() == null && task.getEfectiveDateOfConclusion() == null;
    }

    @Override
    public boolean isOnPlannedState() {
        return true;
    }

    @Override
    public boolean isValid() {
        return (conditionPredictedDates(task) && !task.hasAssignedProjectCollaborator()
                && conditionEffectiveDates(task));

    }

	public List<String> listAvailableActions() {

		List<String> availableActions = new ArrayList<>();
		availableActions.add(taskStateName + SET_PREDICTED_START_DATE);
		availableActions.add(taskStateName + SET_PREDICTED_END_DATE);
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_TASK_DEPENDENCY);
		availableActions.add(taskStateName + REMOVE_TASK);

		return availableActions;
	}
    
    @Override
    public void changeTo() {
        TaskState assigned = new Assigned(task);
        TaskState readyToStart = new ReadyToStart(task);
        TaskState created = new Created(task);

        List<TaskState> validTransitions = new ArrayList<>();
        validTransitions.add(assigned);
        validTransitions.add(readyToStart);
        validTransitions.add(created);
        for (TaskState ts : validTransitions) {
            if (ts.isValid()) {
                task.setTaskState(ts);
                return;
            }
        }
    }
}
