package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Completed extends BaseState {
	Task task;

	public Completed(Task task) {
		this.task = task;
	}

	@Override
	public boolean isValid() {
		return (task.getEffectiveStartDate() != null && task.getEfectiveDateOfConclusion() != null);
	}

	@Override
	public boolean isOnCompletedState() {
		return true;
	}

	@Override
	public void changeTo() {
		TaskState suspended = new Suspended(task);
		TaskState inProgress = new InProgress(task);

		List<TaskState> validTransitions = new ArrayList<>();
		validTransitions.add(suspended);
		validTransitions.add(inProgress);

		for (TaskState ts : validTransitions) {
			if (ts.isValid()) {
				task.setTaskState(ts);
				return;
			}
		}
	}

	public List<String> listAvailableActions() {

		return new ArrayList<>();
	}
}
