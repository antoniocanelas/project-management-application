package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Assigned extends BaseState {
    Task task;

    public Assigned(Task task) {
        this.task = task;
    }

    @Override
    public boolean isOnAssignedState() {
        return true;
    }

    @Override
    public boolean isValid() {

        return (
                task.hasAssignedProjectCollaborator()
                        && !task.canStart() && conditionEffectiveDates(task));
    }

    public boolean conditionEffectiveDates(Task task) {
        return task.getEffectiveStartDate() == null
                && task.getEfectiveDateOfConclusion() == null;
    }

    @Override
    public void changeTo() {
        TaskState readyToStartState = new ReadyToStart(task);
        TaskState plannedState = new Planned(task);
        TaskState createdState = new Created(task);

        List<TaskState> validTransitions = new ArrayList<>();
        validTransitions.add(readyToStartState);
        validTransitions.add(plannedState);
        validTransitions.add(createdState);

        for (TaskState ts : validTransitions) {
            if (ts.isValid()) {
                task.setTaskState(ts);
                return;
            }
        }
    }
    
    public List<String> listAvailableActions() {
    	
		List<String> availableActions = new ArrayList<>();
		availableActions.add(taskStateName + ADD_TASK_DEPENDENCY);
		availableActions.add(taskStateName + REMOVE_TASK);
		availableActions.add(taskStateName + REQUEST_REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + SET_PREDICTED_START_DATE);
		availableActions.add(taskStateName + SET_PREDICTED_END_DATE);
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		
		return availableActions;
}

}
