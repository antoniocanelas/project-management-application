package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class ReadyToStart extends BaseState {
	Task task;

	public ReadyToStart(Task task) {
		this.task = task;
	}

	private static boolean conditionEffectiveDates(Task task) {
		return task.getEffectiveStartDate() == null && task.getEfectiveDateOfConclusion() == null;
	}

	@Override
	public boolean isOnReadyToStartState() {
		return true;
	}

	@Override
	public boolean isValid() {
		return (task.hasAssignedProjectCollaborator() && task.canStart() && conditionEffectiveDates(task));

	}

	public List<String> listAvailableActions() {

		List<String> availableActions = new ArrayList<>();
		availableActions.add(taskStateName + REQUEST_REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + REMOVE_COLLABORATOR);
		availableActions.add(taskStateName + ADD_TASK_DEPENDENCY);
		availableActions.add(taskStateName + REMOVE_TASK);
		availableActions.add(taskStateName + SET_PREDICTED_START_DATE);
		availableActions.add(taskStateName + SET_PREDICTED_END_DATE);
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_REPORT);
		
		return availableActions;
	}
	
	@Override
	public void changeTo() {
		TaskState assigned = new Assigned(task);
		TaskState inProgress = new InProgress(task);
		TaskState planned = new Planned(task);

		List<TaskState> validTransitions = new ArrayList<>();
		validTransitions.add(assigned);
		validTransitions.add(inProgress);
		validTransitions.add(planned);

		for (TaskState ts : validTransitions) {
			if (ts.isValid()) {
				task.setTaskState(ts);
				return;
			}
		}
	}
}
