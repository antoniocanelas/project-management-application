package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Created extends BaseState {
    Task task;

    public Created(Task task) {
        this.task = task;
    }

    private static boolean conditionPredictedDates(Task task) {
        return task.getPredictedDateOfStart() == null && task.getPredictedDateOfConclusion() == null;
    }

    private static boolean conditionEffectiveDates(Task task) {
        return task.getEffectiveStartDate() == null && task.getEfectiveDateOfConclusion() == null;
    }

    @Override
    public boolean isOnCreatedState() {
        return true;
    }

    @Override
    public boolean isValid() {
        return (!task.hasAssignedProjectCollaborator()) && (conditionPredictedDates(task))
                && (conditionEffectiveDates(task));
    }

    @Override
    public void changeTo() {

        TaskState plannedState = new Planned(task);
        TaskState assignedState = new Assigned(task);
        TaskState readyToStart = new ReadyToStart(task);

        List<TaskState> validTransitions = new ArrayList<>();
        validTransitions.add(assignedState);
        validTransitions.add(plannedState);
        validTransitions.add(readyToStart);

        for (TaskState ts : validTransitions) {
            if (ts.isValid()) {
                task.setTaskState(ts);
                return;
            }
        }

    }
    
    public List<String> listAvailableActions() {
    	
		List<String> availableActions = new ArrayList<>();
		availableActions.add(taskStateName + SET_PREDICTED_START_DATE);
		availableActions.add(taskStateName + SET_PREDICTED_END_DATE);
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_TASK_DEPENDENCY);
		availableActions.add(taskStateName + REMOVE_TASK);
		
		return availableActions;
}
}
