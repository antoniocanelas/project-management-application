package project.model.task.taskstate;

import java.util.ArrayList;
import java.util.List;

import project.model.task.Task;

public class Suspended extends BaseState {
	Task task;

	public Suspended(Task task) {
		this.task = task;
	}

	@Override
	public boolean isOnSuspendedState() {
		return true;
	}

	@Override
	public boolean isValid() {
		return (!task.hasAssignedProjectCollaborator() && !task.listAllReports().isEmpty()
				&& task.getEffectiveStartDate() != null && task.getEfectiveDateOfConclusion() == null);
	}

	public List<String> listAvailableActions() {

		List<String> availableActions = new ArrayList<>();
		availableActions.add(taskStateName + REQUEST_ADD_COLLABORATOR);
		availableActions.add(taskStateName + ADD_COLLABORATOR);
		availableActions.add(taskStateName + CANCEL_TASK);
		availableActions.add(taskStateName + SET_TASK_COMPLETED);

		return availableActions;
	}
	
	@Override
	public void changeTo() {

		TaskState completed = new Completed(task);
		TaskState inProgress = new InProgress(task);
		TaskState cancelled = new Cancelled(task);

		List<TaskState> validTransitions = new ArrayList<>();
		validTransitions.add(completed);
		validTransitions.add(inProgress);
		validTransitions.add(cancelled);

		for (TaskState ts : validTransitions) {
			if (ts.isValid()) {
				task.setTaskState(ts);
				return;
			}
		}
	}
}
