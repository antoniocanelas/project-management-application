package project.model.task;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.model.project.CostCalculator;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Entity
@Proxy(lazy = false)
public class TaskCollaboratorRegistry implements Comparable<TaskCollaboratorRegistry>{

    private static int thisTasksReportCount = 0; // necessary persistence to avoid duplicate primary keys
    @Id
    private String taskCollaboratorRegistryID;
    @OneToOne(cascade = CascadeType.ALL)
    private ProjectCollaborator projectCollaborator;
    private LocalDateTime collaboratorAddedToTaskDate;
    private LocalDateTime collaboratorRemovedFromTaskDate;
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "taskCollabRegistry_id")
    private List<Report> reportList;
    @Enumerated(EnumType.STRING)
    private RegistryStatus registryStatus;
    private String taskId;

    /**
     * Constructor of a new task collaborator registry with id and project
     * collaborator.
     *
     * @param projectCollaborator
     * @param taskId
     */
    public TaskCollaboratorRegistry(ProjectCollaborator projectCollaborator, String taskId) {
        this.projectCollaborator = projectCollaborator;
        this.taskId = taskId;
        taskCollaboratorRegistryID = taskId + "-" + projectCollaborator.getUser().getEmail();
        reportList = new ArrayList<>();
        collaboratorAddedToTaskDate = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

    }

    protected TaskCollaboratorRegistry() {

    }

    public static int getThisTasksReportCount() {
        return thisTasksReportCount;
    }

    /**
     * Adds a report to report list.
     *
     * @param reportQuantity
     * @param startDate
     * @param endDate
     */
    public String addReport(double reportQuantity, LocalDateTime startDate, LocalDateTime endDate) {

        String statusMessage;

        if (reportQuantity > 0) {
            thisTasksReportCount++;
            Report report = new Report(taskId + "(" + thisTasksReportCount + ")", reportQuantity, startDate, endDate);
            report.setUserEmail(projectCollaborator.getUser().getEmail());
            reportList.add(report);
            statusMessage = "Report successfully added";
            return statusMessage;
        } else {
            statusMessage = "Invalid number of hours. Please try again.";
        }
        return statusMessage;
    }

    /* Adds a report to report list.
     *
     * @param reportHours
     * @param startDate
     * @param endDate
     */
    public boolean addReportRest(double reportQuantity, LocalDateTime startDate, LocalDateTime endDate) {
        if (reportQuantity > 0) {
            thisTasksReportCount++;
            Report report = new Report(taskId + "(" + thisTasksReportCount + ")", reportQuantity, startDate, endDate);
            report.setUserEmail(projectCollaborator.getUser().getEmail());
            reportList.add(report);
            return true;
        }
        return false;
    }

    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @return the collaboratorAddedToTaskDate
     */
    public LocalDateTime getCollaboratorAddedToTaskDate() {
        return collaboratorAddedToTaskDate;
    }

    /**
     * Set the date when collaborator was added to task.
     *
     * @param collaboratorAddedToTaskDate
     */
    public void setCollaboratorAddedToTaskDate(LocalDateTime collaboratorAddedToTaskDate) {
        this.collaboratorAddedToTaskDate = collaboratorAddedToTaskDate;
    }

    /**
     * @return the collaboratorRemovedFromTaskDate
     */
    public LocalDateTime getCollaboratorRemovedFromTaskDate() {
        return collaboratorRemovedFromTaskDate;
    }

    /**
     * @param collaboratorRemovedFromTaskDate the collaboratorRemovedFromTaskDate to set
     */
    public void setCollaboratorRemovedFromTaskDate(LocalDateTime collaboratorRemovedFromTaskDate) {
        this.collaboratorRemovedFromTaskDate = collaboratorRemovedFromTaskDate;
    }

    /**
     * Get the cost related to the quantity and price of the user.
     *
     * @return cost.
     */
    public double getCost(CostCalculator costCalculator) {
        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList;
        double cost = 0.0;
        for (Report rep : reportList) {
            costAndTimePeriodList = getAllCostPeriodsFromReport(rep, projectCollaborator);

            cost += costCalculator.calculateReportCost(rep, costAndTimePeriodList);
        }
        return cost;
    }

    /**
     * Finds all collaborator's time periods coinciding with report dates.
     *
     * @param report
     * @param projectCollaborator
     * @return List with all CostAndTimePeriods coinciding with report dates.
     */
    public List<ProjectCollaboratorCostAndTimePeriod> getAllCostPeriodsFromReport(Report report,
                                                                                  ProjectCollaborator projectCollaborator) {

        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();

        for (ProjectCollaboratorCostAndTimePeriod projCollabTimePeriod : orderCostAndTimePeriodList(projectCollaborator)) {

            if (isProjectCollaboratorTimePeriodBeforeReportDates(report, projCollabTimePeriod)) {
                continue;
            }
            if (isProjectCollaboratorTimePeriodCoincidingWithReportDates(report, projCollabTimePeriod)) {
                costAndTimePeriodList.add(projCollabTimePeriod);
            }
            if (isProjectCollaboratorTimePeriodAfterReportDates(report, projCollabTimePeriod)) {
                return costAndTimePeriodList;
            }
        }
        return costAndTimePeriodList;
    }

    /**
     * Orders projectCollaborator's CostAndTimePeriod list according to each start date.
     *
     * @param projCollab
     * @return ordered list.
     */
    public static List<ProjectCollaboratorCostAndTimePeriod> orderCostAndTimePeriodList(
            ProjectCollaborator projCollab) {
        List<ProjectCollaboratorCostAndTimePeriod> orderedTimePeriodList = projCollab.getCostAndTimePeriodList();

        orderedTimePeriodList.sort(Comparator.comparing(ProjectCollaboratorCostAndTimePeriod::getStartDate));

        return orderedTimePeriodList;
    }

    /**
     * Verifies if TimePeriod from project collaborator is before report dates.
     * When projeCollabTimePeriod.getEndDate is equal to report's start date we consider that the period is before the report.
     *
     * @param report
     * @param projCollabTimePeriod
     * @return true if ProjectCollaboratorCostAndTimePeriod is before report dates, false otherwise.
     */
    public boolean isProjectCollaboratorTimePeriodBeforeReportDates(Report report,
                                                                    ProjectCollaboratorCostAndTimePeriod projCollabTimePeriod) {

        LocalDate projCollabEndDate = projCollabTimePeriod.getEndDate();
        LocalDate reportStartDate = report.getStartDate().toLocalDate();

        return ((projCollabTimePeriod.getEndDate() != null) &&
                isTimePeriodDateEqualOrBeforeReportDate(projCollabEndDate, reportStartDate));
    }

    /**
     * Verifies if TimePeriod from project collaborator is within report dates. When working with boundary dates, we choose the later project collaborator's
     * cost and time period.
     *
     * @param report
     * @param projCollabTimePeriod
     * @return true if ProjectCollaboratorCostAndTimePeriod is overlapping report dates, false otherwise.
     */
    public boolean isProjectCollaboratorTimePeriodCoincidingWithReportDates(Report report, ProjectCollaboratorCostAndTimePeriod projCollabTimePeriod) {

        LocalDate projCollabStartDate = projCollabTimePeriod.getStartDate();
        LocalDate projCollabEndDate = projCollabTimePeriod.getEndDate();
        LocalDate reportStartDate = report.getStartDate().toLocalDate();
        LocalDate reportEndDate = report.getEndDate().toLocalDate();

        if ((projCollabEndDate == null) && (isTimePeriodDateEqualOrBeforeReportDate(projCollabStartDate, reportEndDate))) {
            return true;
        }
        if ((projCollabEndDate != null) && (isTimePeriodDateEqualOrBeforeReportDate(projCollabStartDate, reportStartDate) &&
                isTimePeriodDateEqualOrAfterReportDate(projCollabEndDate, reportEndDate))) {
            return true;
        }

        return (((projCollabEndDate != null) && isWhithinRangeExcludingBoundaryDates(projCollabEndDate, reportStartDate, reportEndDate))
                || isWithinRange(projCollabStartDate, reportStartDate, reportEndDate));
    }

    /**
     * Compares a given date to a date range. Excludes boundaries.
     *
     * @param dateToBeCompared
     * @param earlierDate      Range's earlier date.
     * @param laterDate        Range's later date.
     * @return true if date to be compared is within range (excluding boundary dates), false otherwise.
     */
    public static boolean isWhithinRangeExcludingBoundaryDates(LocalDate dateToBeCompared, LocalDate earlierDate, LocalDate laterDate) {
        return dateToBeCompared.isAfter(earlierDate) && dateToBeCompared.isBefore(laterDate);
    }

    /**
     * Compares a given date to a date range.
     *
     * @param dateToBeCompared
     * @param earlierDate      Range's earlier date.
     * @param laterDate        Range's later date.
     * @return true if date to be compared is within range (including boundary dates), false otherwise.
     */
    public static boolean isWithinRange(LocalDate dateToBeCompared, LocalDate earlierDate, LocalDate laterDate) {
        return isTimePeriodDateEqualOrAfterReportDate(dateToBeCompared, earlierDate)
                && isTimePeriodDateEqualOrBeforeReportDate(dateToBeCompared, laterDate);
    }

    /**
     * Verifies if a date is before of is equal to another.
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isTimePeriodDateEqualOrBeforeReportDate(LocalDate date1, LocalDate date2) {

        return (date1.isBefore(date2) || (date1.isEqual(date2)));
    }

    /**
     * Verifies if a date is equal to or after another.
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isTimePeriodDateEqualOrAfterReportDate(LocalDate date1, LocalDate date2) {
        return (date1.isAfter(date2) || (date1.isEqual(date2)));
    }

    /**
     * Verifies if TimePeriod from project collaborator is after report dates.
     *
     * @param report
     * @param projCollabTimePeriod
     * @return true if ProjectCollaboratorCostAndTimePeriod is after report dates, false otherwise.
     */
    public boolean isProjectCollaboratorTimePeriodAfterReportDates(Report report, ProjectCollaboratorCostAndTimePeriod projCollabTimePeriod) {
        return (projCollabTimePeriod.getStartDate().isAfter(report.getEndDate().toLocalDate()));
    }

    /**
     * @return the projectCollaborator
     */
    public ProjectCollaborator getProjectCollaborator() {
        return projectCollaborator;
    }

    /**
     * Get a report by its ID.
     *
     * @param id
     * @return
     */
    public Report getReportById(String id) {
        for (Report report : reportList)
            if (report.getCode().equals(id))
                return report;
        return null;
    }

    /**
     * @return the reportList
     */
    public List<Report> getReportList() {
        return reportList;
    }

    /**
     * @return the taskCollaboratorRegistryID
     */
    public String getTaskCollaboratorRegistryID() {
        return taskCollaboratorRegistryID;
    }

    /**
     * Checks if this projectCollaborator has this task assigned to.
     *
     * @return true if projectCollaborator is assigned to this task, false
     * otherwise.
     */
    public boolean hasAssignedTask() {

        return collaboratorRemovedFromTaskDate == null;
    }

    /**
     * @return the registryStatus
     */
    public RegistryStatus getRegistryStatus() {
        return registryStatus;
    }

    /**
     * @param registryStatus the registryStatus to set
     */
    public void setRegistryStatus(RegistryStatus registryStatus) {
        this.registryStatus = registryStatus;
    }

    public TaskCollaboratorRegistryRESTDTO toDTO (){
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO= new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskCollaboratorRegistryRESTDTO.setTaskId(taskId);
        taskCollaboratorRegistryRESTDTO.setProjectCollaboratorName(projectCollaborator.getUser().getName());
        taskCollaboratorRegistryRESTDTO.setProjectCollaboratorEmail(projectCollaborator.getUser().getEmail());
        taskCollaboratorRegistryRESTDTO.setCollaboratorAddedToTaskDate(collaboratorAddedToTaskDate);
        taskCollaboratorRegistryRESTDTO.setCollaboratorRemovedFromTaskDate(collaboratorRemovedFromTaskDate);
        taskCollaboratorRegistryRESTDTO.setRegistryStatus(registryStatus.toString());
        return taskCollaboratorRegistryRESTDTO;
    }

    public Report getLastReport() {
        if (reportList.isEmpty())
            return null;
        Collections.sort(reportList);
        return reportList.get(reportList.size()-1);
    }



    @Override
    public String toString() {
        return "\nTaskCollaboratorRegistryID = " + taskCollaboratorRegistryID + "\n" + projectCollaborator
                + "\nCollaboratorAddedToTaskDate = " + collaboratorAddedToTaskDate + "\nCollaboratorRemovedFromTaskDate = "
                + collaboratorRemovedFromTaskDate + "\n\nReportList = " + reportList + "\n";
    }

    public enum RegistryStatus {
        ASSIGNMENTREQUEST, REMOVALREQUEST, ASSIGNMENTAPPROVED, ASSIGNMENTCANCELLED, REMOVALAPPROVED
    }

	@Override
	public int compareTo(TaskCollaboratorRegistry tcr) {
		if (this.reportList.isEmpty() && tcr.getReportList().isEmpty())
			return 0;
		if (this.reportList.isEmpty())
			return -1;
		if (tcr.getReportList().isEmpty())
			return 1;
		if (this.getLastReport().getReportCreationDate().isAfter(tcr.getLastReport().getReportCreationDate()))
			return 1;
		if (this.getLastReport().getReportCreationDate().isBefore(tcr.getLastReport().getReportCreationDate()))
			return -1;
		return 0;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaskCollaboratorRegistry)) {
            return false;
        }
        TaskCollaboratorRegistry that = (TaskCollaboratorRegistry) o;
        return (Objects.equals(taskCollaboratorRegistryID, that.taskCollaboratorRegistryID));
    }

    @Override
    public int hashCode() {

        return Objects.hash(taskCollaboratorRegistryID);
    }


}
