package project.model.task;

import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.model.project.CostCalculator;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.task.taskstate.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.lang.reflect.Constructor;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to represent the Task object
 */
@Entity
public class Task implements Comparable<Task> {

    @Transient
    Logger logger = Logger.getAnonymousLogger();
    @Id
    @Column(name = "task_id", nullable = false) // TOSOLVE: on database make index
    private String taskId;
    private String title;
    @Size(max = 500)
    private String description;
    private double estimatedEffort;
    private LocalDateTime dateOfCreation;
    private LocalDateTime effectiveDateOfConclusion;
    private LocalDateTime effectiveStartDate;
    private LocalDateTime predictedDateOfConclusion;
    private LocalDateTime predictedDateOfStart;
    private String ganttTaskDependencies;
    private double unitCost;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
    @ManyToMany(mappedBy = "taskDependencies")
    private List<Task> tasks = new ArrayList<>();
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tasksDependencies", joinColumns = {@JoinColumn(name = "task_id")}, inverseJoinColumns = {
            @JoinColumn(name = "taskDependency_id")})
    private List<Task> taskDependencies;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "task_idDb")
    private Set<TaskCollaboratorRegistry> taskCollaboratorRegistryList;
    private String taskStatePersisted;
    private String requestCompletedByProjectCollaborator;
    @Transient
    private TaskState taskState;

    /**
     * Constructs a Task with ID and title.
     *
     * @param title
     */
    public Task(Project project, String title) {
        this.dateOfCreation = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        this.title = title;
        this.taskId = project.getId() + "-" + Integer.toString(project.getTasksList().size() + 1);
        this.taskCollaboratorRegistryList = new LinkedHashSet<>();
        this.taskDependencies = new ArrayList<>();
        this.taskState = new Created(this);
        this.taskStatePersisted = this.taskState.getClass().getSimpleName();
        this.project = project;
        this.requestCompletedByProjectCollaborator = null;

    }

    protected Task() {
    }

    /**
     * Instantiates a Task object with title, description, predicted date, predicted
     * date of conclusion, unit cost and estimated effort.
     *
     * @param project
     * @param title
     * @param description
     * @param predictedDateOfStart
     * @param predictedDateOfConclusion
     * @param unitCost
     * @param estimatedEffort
     */
    public Task(Project project, String title, String description, LocalDateTime predictedDateOfStart,
                LocalDateTime predictedDateOfConclusion, double unitCost, double estimatedEffort) {
        this.dateOfCreation = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        this.taskId = project.getId() + "-" + Integer.toString(project.getTasksList().size() + 1);
        this.title = title;
        this.description = description;
        this.predictedDateOfStart = predictedDateOfStart;
        this.predictedDateOfConclusion = predictedDateOfConclusion;
        this.unitCost = unitCost;
        this.estimatedEffort = estimatedEffort;
        this.taskCollaboratorRegistryList = new LinkedHashSet<>();
        this.taskDependencies = new ArrayList<>();
        this.taskState = new Planned(this);
        this.taskStatePersisted = this.taskState.getClass().getSimpleName();
        this.project = project;
        this.requestCompletedByProjectCollaborator = null;
    }

    /**
     * @return the taskState interface's absolute path.
     */
    private static String getTaskStateAbsolutePath() {
        return TaskState.class.getPackage().getName();
    }

    private static String createGanttTaskDependencies(List<Task> taskDependencies) {
        List<String> taskDependencyIds = new ArrayList<>();
        for (Task task : taskDependencies) {
            taskDependencyIds.add(task.getId());
        }
        return String.join(",", taskDependencyIds);
    }

    /**
     * Method to get project.
     *
     * @return project.
     */
    public Project getProject() {
        return project;
    }

    /**
     * Method to get task state.
     *
     * @return task state.
     */
    public String getState() {
        return this.taskStatePersisted;
    }

    /**
     * Method to persist task state.
     *
     * @param state
     */
    public void setTaskStatePersisted(String state) {
        this.taskStatePersisted = state;
    }

    /**
     * Verify if a task state is valid or not.
     *
     * @return true if it is valid, false otherwise.
     */
    public boolean isValid() {
        return this.getTaskState().isValid();
    }

    /**
     * Add collaborator to the task,
     *
     * @param collab
     * @return true if collaborator was successfully added to task and false if
     * ROLE_COLLABORATOR already exist in task.
     */
    public boolean addProjectCollaborator(ProjectCollaborator collab) {
        TaskCollaboratorRegistry taskCollabReg = this.getLastTaskCollaboratorRegistryOf(collab);

        if (!this.hasProjectCollaborator(collab)) {
            if (taskCollabReg == null) {
                taskCollabReg = new TaskCollaboratorRegistry(collab, this.getId());
                this.taskCollaboratorRegistryList.add(taskCollabReg);
                taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTAPPROVED);
            } else if (taskCollabReg.getCollaboratorRemovedFromTaskDate() != null) {
                taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTAPPROVED);
                taskCollabReg.setCollaboratorRemovedFromTaskDate(null);
            } else if (taskCollabReg.getRegistryStatus()
                    .equals(TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTREQUEST)) {
                taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTAPPROVED);
            }

            this.changeStateTo();

            return true;
        }
        return false;
    }

    /**
     * Method to request a project collaborator to certain task.
     *
     * @param collab
     * @return a string saying if request is denied, successfully sent, or already
     * made.
     */
    public String requestAddProjectCollaborator(ProjectCollaborator collab) {// TOCHECK retirar texto estranho

        TaskCollaboratorRegistry collRegAux = this.getLastTaskCollaboratorRegistryOf(collab);

        if (collRegAux == null || collRegAux.getRegistryStatus().equals(RegistryStatus.REMOVALAPPROVED)) {
            TaskCollaboratorRegistry taskCollabReg = new TaskCollaboratorRegistry(collab, this.getId());
            taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTREQUEST);
            this.taskCollaboratorRegistryList.add(taskCollabReg);

            return "Request successfully sent.";
        } else if (collRegAux.getRegistryStatus().equals(RegistryStatus.ASSIGNMENTREQUEST)) {
            return " Request already made.";

        }
        return "Request denied. You are already working in this task.";

    }

    /**
     * Find the last task in a specific collaborator registry.
     *
     * @param collab
     * @return the last task, or null.
     */
    public TaskCollaboratorRegistry getLastTaskCollaboratorRegistryOf(ProjectCollaborator collab) {
        List<TaskCollaboratorRegistry> taskRegAux = new ArrayList<>();
        for (TaskCollaboratorRegistry taskReg : this.taskCollaboratorRegistryList) {
            if (taskReg.getProjectCollaborator().equals(collab)) {
                taskRegAux.add(taskReg);
                return taskRegAux.get(taskRegAux.size() - 1);
            }
        }
        return null;
    }

    /**
     * Cancel assignment request based on a specific collaborator.
     *
     * @param collab
     */
    public void cancelAssignmentRequest(ProjectCollaborator collab) {
        TaskCollaboratorRegistry taskCollabReg = this.getLastTaskCollaboratorRegistryOf(collab);
        if (taskCollabReg.getRegistryStatus() == RegistryStatus.ASSIGNMENTREQUEST) {
            taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTCANCELLED);
        }
    }

    /**
     * Request to remove a certain project collaborator from associeted task.
     *
     * @param collab
     * @return
     */
    public boolean requestRemoveProjectCollaborator(ProjectCollaborator collab) {
        TaskCollaboratorRegistry taskCollabReg = this.getLastTaskCollaboratorRegistryOf(collab);

        if (!this.hasProjectCollaboratorInTaskCollaboratorRegistryList(collab))
            return false;

        if (taskCollabReg.getRegistryStatus().equals(TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED)) {
            taskCollabReg.setRegistryStatus(RegistryStatus.REMOVALREQUEST);
            return true;
        }
        return false;
    }

    /**
     * Cancel removal requests based on a certain collaborator.
     *
     * @param collab
     */
    public void cancelRemovalRequest(ProjectCollaborator collab) {
        TaskCollaboratorRegistry taskCollabReg = this.getLastTaskCollaboratorRegistryOf(collab);
        if (taskCollabReg.getRegistryStatus() == RegistryStatus.REMOVALREQUEST) {
            taskCollabReg.setRegistryStatus(RegistryStatus.ASSIGNMENTAPPROVED);
        }
    }

    /**
     * Adds report.
     *
     * @param reportStartDate collaborator, report hours, report start date and report end date.
     * @return message with success or failure.
     */
    public String addReport(ProjectCollaborator projCollab, double reportQuantity, LocalDateTime reportStartDate,
                            LocalDateTime reportEndDate) {

        String statusMessage;

        if (!this.hasProjectCollaboratorInTaskCollaboratorRegistryList(projCollab)) {
            return "This collaborator has never worked in this task.";
        }
        if (!(taskCanReceiveReports())) {
            return "This task's state doesn't permit adding reports";
        }

        if (!(isReportPeriodCoincidingWithProjectCollaboratorPeriod(projCollab, reportStartDate, reportEndDate))) {
            return "Invalid report dates. Please try again.";
        }

        statusMessage = getLastTaskCollaboratorRegistryOf(projCollab).addReport(reportQuantity, reportStartDate,
                reportEndDate);

        if ("Report successfully added".equals(statusMessage) && (this.getTaskState().isOnReadyToStartState())) {
            this.effectiveStartDate = reportStartDate;
        }
        this.changeStateTo();

        return statusMessage;
    }

    /**
     * Method to add report.
     *
     * @param projCollab
     * @param reportQuantity
     * @param reportStartDate
     * @param reportEndDate
     * @return true if report was added, false otherwise.
     */
    public boolean addReportRest(ProjectCollaborator projCollab, double reportQuantity, LocalDateTime reportStartDate,
                                 LocalDateTime reportEndDate) {

        if (!this.hasProjectCollaboratorInTaskCollaboratorRegistryList(projCollab) || !(taskCanReceiveReports())
                || !(isReportPeriodCoincidingWithProjectCollaboratorPeriod(projCollab, reportStartDate,
                reportEndDate))) {
            return false;
        } else if (getLastTaskCollaboratorRegistryOf(projCollab).addReportRest(reportQuantity, reportStartDate,
                reportEndDate)) {
            if (effectiveStartDate == null) {
                this.effectiveStartDate = reportStartDate;
            }

            return true;
        }

        return false;
    }

    /**
     * Method to check if the submitted report dates are coinciding with project collaborator period in task.
     *
     * @param projCollab
     * @param reportStartDate
     * @param reportEndDate
     * @return true if the dates are coinciding, false otherwise.
     */
    public boolean isReportPeriodCoincidingWithProjectCollaboratorPeriod(ProjectCollaborator projCollab,
                                                                         LocalDateTime reportStartDate, LocalDateTime reportEndDate) {

        for (TaskCollaboratorRegistry tcr : this.listTaskCollaboratorRegistry(projCollab)) {
            if (isReportPeriodCoincidingWithRegistryPeriod(tcr, reportStartDate, reportEndDate))
                return true;
        }
        return false;
    }

    /**
     * Method to check if the submitted report dates are coinciding with project collaborator period in task.
     *
     * @param tcr
     * @param reportStartDate
     * @param reportEndDate
     * @return true if the dates are coinciding, false otherwise.
     */
    public boolean isReportPeriodCoincidingWithRegistryPeriod(TaskCollaboratorRegistry tcr,
                                                              LocalDateTime reportStartDate, LocalDateTime reportEndDate) {

        if (tcr.getCollaboratorRemovedFromTaskDate() == null)
            return (reportStartDate.isAfter(tcr.getCollaboratorAddedToTaskDate())
                    && reportEndDate.isBefore(LocalDateTime.now()));
        else {
            return (reportStartDate.isAfter(tcr.getCollaboratorAddedToTaskDate())
                    && reportEndDate.isBefore(tcr.getCollaboratorRemovedFromTaskDate()));
        }
    }

    /**
     * Method to check if a task can receive reports.
     *
     * @return true if it can, false otherwise.
     */
    public boolean taskCanReceiveReports() {

        return (this.getTaskState().isOnReadyToStartState() || this.getTaskState().isOnInProgressState());
    }

    /**
     * Add task dependency to certain task.
     *
     * @param task
     */
    public void addTaskDependency(Task task) {
        if (project.getTasksList().contains(task) && !this.equals(task)) {
            boolean isValidToAddDependency = this.getTaskState().isOnCreatedState()
                    || this.getTaskState().isOnPlannedState() || this.getTaskState().isOnReadyToStartState()
                    || this.getTaskState().isOnAssignedState();
            boolean isValidDependency = (!task.getTaskState().isOnCancelledState())
                    && (!task.getTaskState().isOnCompletedState());
            if (isValidToAddDependency && isValidDependency && !this.taskDependencies.contains(task)) {
                this.taskDependencies.add(task);
                this.changeStateTo();
            }
        }
    }

    /**
     * Get collaborator's list of reports in this task.
     *
     * @param collab
     * @return list of all report's of collaborator in task.
     */
    public List<Report> listCollaboratorReports(ProjectCollaborator collab) {
        List<Report> collaboratorReportList = new ArrayList<>();

        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList)
            if (taskCollReg.getProjectCollaborator().equals(collab))
                collaboratorReportList.addAll(taskCollReg.getReportList());
        return collaboratorReportList;
    }

    /**
     * Get collaborator work hours in task if collaborator exist in task.
     *
     * @param collab
     * @return collaborator's worked hours in task.
     */
    public double calculateProjectCollaboratorWorkedHours(ProjectCollaborator collab) {

        double hours = 0;
        List<Report> reportList = this.listCollaboratorReports(collab);
        for (Report report : reportList)
            hours += report.getQuantity();
        return hours;
    }

    /**
     * Calculates the present total cost of this task.
     *
     * @return The total cost so far.
     */
    public double calculateTotalCostsoFar(CostCalculator costCalculator) {
        double cost = 0.0;
        for (TaskCollaboratorRegistry taskCollaboratorRegistry : this.taskCollaboratorRegistryList)
            cost += taskCollaboratorRegistry.getCost(costCalculator);
        return cost;
    }

    /**
     * Calculate the days of work between start date and conclusion date of the task.
     *
     * @return days between start and conclusion of the task.
     */
    public int calculateWorkDaysBetweenStartAndConclusion() {
        int daysToPerformTask = 0;
        LocalDateTime start = this.getEffectiveStartDate();
        while (start.compareTo(this.getEfectiveDateOfConclusion()) < 0) {
            if ((start.getDayOfWeek() != DayOfWeek.SATURDAY) && (start.getDayOfWeek() != DayOfWeek.SUNDAY))
                daysToPerformTask++;
            start = start.plusDays(1);
        }
        return daysToPerformTask;
    }

    /**
     * Searches a specific report taken its ID into account.
     *
     * @param id the search parameter.
     * @return a specific report, if the ID matches, or null, if the report is not
     * found.
     */
    public Report findReportById(String id) {
        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList)
            for (Report report : taskCollReg.getReportList())
                if (report.getCode().equals(id))
                    return report;
        return null;
    }

    /**
     * Get task date of creation.
     *
     * @return date of creation.
     */
    public LocalDateTime getDateOfCreation() {
        return this.dateOfCreation;
    }

    /**
     * Method to set date of creation of certain task.
     *
     * @param dateOfCreation
     */
    public void setDateOfCreation(LocalDateTime dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    /**
     * Get task description.
     *
     * @return string description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Set task description to the new description received.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get task effective date of conclusion.
     *
     * @return effective date of conclusion.
     */
    public LocalDateTime getEfectiveDateOfConclusion() {
        return this.effectiveDateOfConclusion;
    }

    /**
     * Get task start date.
     *
     * @return start date.
     */
    public LocalDateTime getEffectiveStartDate() {
        return this.effectiveStartDate;
    }

    /**
     * Set task start date to the new start date received.
     *
     * @param startDate
     */
    public void setEffectiveStartDate(LocalDateTime startDate) {

        this.effectiveStartDate = startDate;

        this.changeStateTo();
    }

    public String getGanttTaskDependencies() {
        return ganttTaskDependencies;
    }

    public void setGanttTaskDependencies(String ganttTaskDependencies) {
        this.ganttTaskDependencies = ganttTaskDependencies;
    }

    /**
     * Get estimated effort of Task.
     *
     * @return double.
     */
    public double getEstimatedEffort() {
        return this.estimatedEffort;
    }

    /**
     * Method to set estimated effort.
     *
     * @param estimatedEffort
     */
    public void setEstimatedEffort(double estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }

    /**
     * Get task ID.
     *
     * @return string ID.
     */
    public String getId() {
        return this.taskId;
    }

    /**
     * Method to set task ID.
     *
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * Get task predicted date of conclusion.
     *
     * @return predicted date of conclusion.
     */
    public LocalDateTime getPredictedDateOfConclusion() {
        return this.predictedDateOfConclusion;
    }

    /**
     * Set task predicted date of conclusion to the new predicted date of conclusion
     * received.
     *
     * @param predictedDateOfConclusion
     */
    public void setPredictedDateOfConclusion(LocalDateTime predictedDateOfConclusion) {
        this.predictedDateOfConclusion = predictedDateOfConclusion;
    }

    /**
     * Get predicted Date to start task
     *
     * @return LocalDateTime
     */
    public LocalDateTime getPredictedDateOfStart() {
        return this.predictedDateOfStart;
    }

    /**
     * Set predicted date of start.
     *
     * @param predictedDateOfStart
     */
    public void setPredictedDateOfStart(LocalDateTime predictedDateOfStart) {
        this.taskState = this.getTaskState();
        if (this.taskState.isOnCreatedState() || this.taskState.isOnPlannedState() || this.taskState.isOnAssignedState()
                || this.taskState.isOnReadyToStartState()) {

            this.predictedDateOfStart = predictedDateOfStart;
        }
        if (this.taskState.isOnCreatedState()) {
            this.changeStateTo();
        }
    }

    /**
     * Get task dependencies.
     *
     * @return task dependencies list.
     */
    public List<Task> getTaskDependencies() {
        return this.taskDependencies;
    }

    /**
     * Get task title.
     *
     * @return string title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Set task title to the new title received.
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get unit cost of Task.
     *
     * @return double
     */
    public double getUnitCost() {
        return this.unitCost;
    }

    /**
     * Method to set unit cost.
     *
     * @param unitCost
     */
    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    /**
     * If taskState is null(condition achieved when the object is returned from the
     * DB), this method "reconstructs" the attribute using reflection. It uses
     * String taskStatePersisted, which is a string (persisted) representation of
     * taskState for the "reconstruction".
     *
     * @return taskState
     */
    public TaskState getTaskState() {

        if (this.taskState == null) {

            try {
                constructInstanceOfTaskState();
            } catch (Exception e) {
                logger.log(Level.ALL, "Could not construct instance of taskState\n" + e.getMessage(), e);
            }
        }
        return this.taskState;
    }

    /**
     * Set a new task state, if new task state is valid.
     *
     * @param newTaskState
     */
    public void setTaskState(TaskState newTaskState) {
        if (newTaskState.isValid()) {
            this.taskState = newTaskState;
            this.taskStatePersisted = taskState.getClass().getSimpleName();
        }
    }

    /**
     * Methodo to construct instance of task state.
     *
     * @throws Exception
     */
    private void constructInstanceOfTaskState() throws Exception {

        Class<?> clazz = Class.forName(getTaskStateAbsolutePath() + "." + this.taskStatePersisted);
        Constructor<?> construct = clazz.getConstructor(Task.class);
        this.taskState = (TaskState) construct.newInstance(this);
    }

    /**
     * Check if the task has any collaborator assigned.
     *
     * @return False if active collaborators list is empty and true if not.
     */
    public boolean hasAssignedProjectCollaborator() {
        return !listActiveCollaborators().isEmpty();
    }

    /**
     * Checks if collaborator exists in active collaborators list.
     *
     * @param collab
     * @return true if collaborator exist in list and false if user doesn't exist in
     * active users list.
     */
    public boolean hasProjectCollaborator(ProjectCollaborator collab) {

        return listActiveCollaborators().contains(collab);
    }

    /**
     * Checks if collaborator exists in task's report.
     *
     * @param collab
     * @return true if collaborator exist in task's report and false if collaborator
     * not exist.
     */
    public boolean hasProjectCollaboratorInTaskCollaboratorRegistryList(ProjectCollaborator collab) {

        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList)
            if (taskCollReg.getProjectCollaborator().equals(collab))
                return true;
        return false;
    }

    /**
     * Get task status.
     *
     * @return task status (true if task is complete, false if task is pending).
     */
    public boolean isCompleted() {
        return this.getTaskState().isOnCompletedState();
    }

    /**
     * Checks if task was completed last month.
     *
     * @return true if task has completed last month and false if is not completed
     * or it's completed in other month.
     */
    public boolean isCompletedLastMonth() {

        LocalDateTime now = LocalDateTime.now();
        Month currentMonth = now.getMonth();

        return (this.isCompleted() && this.getEfectiveDateOfConclusion().getMonth() == currentMonth.minus(1));
    }

    /**
     * List of all report's in task.
     *
     * @return list of all reports.
     */
    public List<Report> listAllReports() {
        List<Report> reportList = new ArrayList<>();
        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList)
            reportList.addAll(taskCollReg.getReportList());
        return reportList;
    }

    /**
     * Return a list of all Collaborators Registry.
     *
     * @return list of TaskCollaboratorRegistry.
     */
    public Set<TaskCollaboratorRegistry> listAllTaskCollaboratorRegistry() {
        return this.taskCollaboratorRegistryList;
    }

    /**
     * Return a list of TaskCollaboratorRegistry of a Project ROLE_COLLABORATOR.
     *
     * @param collab
     * @return List of TaskCollaboratorRegistry.
     */
    public List<TaskCollaboratorRegistry> listTaskCollaboratorRegistry(ProjectCollaborator collab) {
        List<TaskCollaboratorRegistry> collRegList = new ArrayList<>();
        for (TaskCollaboratorRegistry TaskCollReg : this.taskCollaboratorRegistryList)
            if (TaskCollReg.getProjectCollaborator().equals(collab))
                collRegList.add(TaskCollReg);
        return collRegList;
    }

    /**
     * List assignment requests.
     *
     * @return ROLE_COLLABORATOR registry list (list of assignment requests).
     */
    public List<TaskCollaboratorRegistry> listAssignmentRequests() {
        List<TaskCollaboratorRegistry> collRegList = new ArrayList<>();
        for (TaskCollaboratorRegistry TaskCollReg : this.taskCollaboratorRegistryList)
            if (TaskCollReg.getRegistryStatus().equals(RegistryStatus.ASSIGNMENTREQUEST))
                collRegList.add(TaskCollReg);
        return collRegList;
    }

    /**
     * List removal requests.
     *
     * @return ROLE_COLLABORATOR registry list (list of removal requests).
     */
    public List<TaskCollaboratorRegistry> listRemovalRequests() {
        List<TaskCollaboratorRegistry> collRegList = new ArrayList<>();
        for (TaskCollaboratorRegistry TaskCollReg : this.taskCollaboratorRegistryList)
            if (TaskCollReg.getRegistryStatus().equals(RegistryStatus.REMOVALREQUEST))
                collRegList.add(TaskCollReg);
        return collRegList;
    }

    /**
     * Remove ROLE_COLLABORATOR from the task
     *
     * @param collab
     * @return true if collaborator was successfully removed, false otherwise.
     */
    public boolean removeProjectCollaborator(ProjectCollaborator collab) {

        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList) {
            if (taskCollReg.getProjectCollaborator().equals(collab)
                    && taskCollReg.getCollaboratorRemovedFromTaskDate() == null) {
                taskCollReg.setCollaboratorRemovedFromTaskDate(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES));
                taskCollReg.setRegistryStatus(RegistryStatus.REMOVALAPPROVED);
                this.changeStateTo();
                return true;
            }
        }
        return false;
    }

    /**
     * Searches a particular TaskCollaboratorRegistry tanking into account its ID.
     *
     * @param id Search parameter.
     * @return A TaskCollaboratorRegistry object or, if not found, null.
     */
    public TaskCollaboratorRegistry getTaskCollaboratorRegistryByID(String id) {

        for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList)
            if (taskCollReg.getTaskCollaboratorRegistryID().equals(id))
                return taskCollReg;
        return null;
    }

    /**
     * Sets the date of conclusion
     *
     * @param effectiveDateOfConclusion
     */
    public void setEffectiveDateOfConclusion(LocalDateTime effectiveDateOfConclusion) {
        this.effectiveDateOfConclusion = effectiveDateOfConclusion;
        this.changeStateTo();
    }

    /**
     * Set task concluded
     */
    public boolean setTaskCompleted() {
        this.effectiveDateOfConclusion = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        if ((this.getTaskState().isOnInProgressState() || this.getTaskState().isOnSuspendedState())) {
            this.changeStateTo();
            for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList) {
                taskCollReg.setCollaboratorRemovedFromTaskDate(this.effectiveDateOfConclusion);
            }
            this.getProject().changeDependentTasksStatus(this);
        }
        return this.getTaskState().isOnCompletedState();
    }

    /**
     * Mark task as complete in the collaborator tasks list
     *
     * @param collab ROLE_COLLABORATOR that completed a task in your own tasks list
     * @return True if task was successfully marked as completed, false otherwise.
     */
    public List<Task> markAsCompletedByCollaborator(ProjectCollaborator collab) {

        List<Task> tasksWithChangedState = new ArrayList<>();
        if ((hasProjectCollaboratorInTaskCollaboratorRegistryList(collab))
                && (this.getTaskState().isOnCompletedState())) {
            tasksWithChangedState = this.getProject().changeDependentTasksStatus(this);
        }
        return tasksWithChangedState;
    }

    /**
     * Set task cancelled
     *
     * @return
     */
    public boolean setTaskCancelled() {
        if (this.getTaskState().isOnInProgressState() || this.getTaskState().isOnSuspendedState()) {
            this.taskState = new Cancelled(this);
            this.taskStatePersisted = this.taskState.getClass().getSimpleName();
            for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList) {
                taskCollReg.setCollaboratorRemovedFromTaskDate(LocalDateTime.now());
            }
        }
        this.getProject().changeDependentTasksStatus(this);

        return this.taskState.isOnCancelledState();
    }

    /**
     * Set task suspended
     *
     * @return
     */
    public boolean setTaskSuspended() {
        if (this.getTaskState().isOnInProgressState()) {
            this.taskState = new Suspended(this);
            this.taskStatePersisted = this.taskState.getClass().getSimpleName();
            for (TaskCollaboratorRegistry taskCollReg : this.taskCollaboratorRegistryList) {
                taskCollReg.setCollaboratorRemovedFromTaskDate(LocalDateTime.now());
            }
        }
        this.getProject().changeDependentTasksStatus(this);

        return this.taskState.isOnSuspendedState();
    }

    /**
     * Checks for equality taking title as a parameter.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        Task other = (Task) obj;
        return (this.taskId.equals(other.taskId));
    }

    /**
     * return hashCode of class
     */
    @Override
    public int hashCode() {
        return this.taskId.hashCode();
    }

    /**
     * Get task state status.
     *
     * @return task state.
     */
    public TaskState getStatus() {
        return this.getTaskState();
    }

    /**
     * Checks if task can start.
     *
     * @return boolean - true if task can start, false otherwise
     */
    public boolean canStart() {
        if (this.taskDependencies.isEmpty())
            return true;
        for (Task aTaskDependenciesList : this.taskDependencies)
            if ((!aTaskDependenciesList.isCompleted() && !aTaskDependenciesList.getTaskState().isOnCancelledState()))
                return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Task ID: ").append(this.getId()).append("\t");
        sb.append("Task Title: ").append(this.getTitle()).append("\t");
        sb.append("Task Status: ").append(this.getStatus()).append("\n");
        sb.append("\t\tTask Predicted Start Date: ").append(this.getPredictedDateOfStart()).append("\n");
        sb.append("\t\tTask Predicted End Date: ").append(this.getPredictedDateOfConclusion()).append("\n");
        sb.append("\t\tTask Effective Start Date: ").append(this.getEffectiveStartDate()).append("\n");
        sb.append("\t\tTask Effective End Date: ").append(this.getEfectiveDateOfConclusion()).append("\n\n");

        return sb.toString();

    }

    /**
     * Method to change task state.
     */
    public void changeStateTo() {
        this.taskState = this.getTaskState();
        this.taskState.changeTo();
        this.taskStatePersisted = this.taskState.getClass().getSimpleName();
    }

    /**
     * Method to list all Assigned Project Collaborators in Task
     *
     * @return Set of ProjectCollaborator
     */
    public Set<ProjectCollaborator> listActiveCollaborators() {
        Set<ProjectCollaborator> activeCollabList = new HashSet<>();
        for (TaskCollaboratorRegistry tcr : taskCollaboratorRegistryList) {
            if ((tcr.getCollaboratorRemovedFromTaskDate() == null)
                    && (tcr.getRegistryStatus().equals(RegistryStatus.ASSIGNMENTAPPROVED)
                    || tcr.getRegistryStatus().equals(RegistryStatus.REMOVALREQUEST)))
                activeCollabList.add(tcr.getProjectCollaborator());
        }
        return activeCollabList;
    }

    /**
     * Compare task effective date of conclusion
     */
    @Override
    public int compareTo(Task o) {
        if (this.getEfectiveDateOfConclusion() == null || o.getEfectiveDateOfConclusion() == null)
            return 0;
        return this.getEfectiveDateOfConclusion().compareTo(o.getEfectiveDateOfConclusion());
    }

    /**
     * Method to get task collaborator registry list.
     *
     * @return collaborator registry list.
     */
    public Set<TaskCollaboratorRegistry> getTaskCollaboratorRegistryList() {
        return taskCollaboratorRegistryList;
    }

    /**
     * Method to set task collaborator registry list.
     *
     * @param taskCollaboratorRegistryList
     */
    public void setTaskCollaboratorRegistryList(Set<TaskCollaboratorRegistry> taskCollaboratorRegistryList) {
        this.taskCollaboratorRegistryList = taskCollaboratorRegistryList;
    }

    /**
     * Method to check if a task related to certain collaborator has a report.
     *
     * @param projectCollaborator
     * @param reportId
     * @return true if it has, false otherwise.
     */
    public boolean taskCollaboratorHasReport(ProjectCollaborator projectCollaborator, String reportId) {
        for (Report r : getLastTaskCollaboratorRegistryOf(projectCollaborator).getReportList()) {
            if (r.getCode().equals(reportId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to request to complete one task.
     *
     * @param projectCollaborator
     * @return true if request was sent, false otherwise.
     */
    public boolean requestTaskCompleted(ProjectCollaborator projectCollaborator) {

        if (this.getStatus().isOnInProgressState() && this.hasProjectCollaborator(projectCollaborator)) {

            requestCompletedByProjectCollaborator = projectCollaborator.getUser().getEmail();
            return true;
        }
        return false;
    }

    /**
     * Method to get request task completed.
     *
     * @return
     */
    public String getRequestTaskCompleted() {

        return requestCompletedByProjectCollaborator;

    }

    /**
     * Method to set request task completed.
     *
     * @return
     */
    public void setRequestTaskCompleted(String request) {

        this.requestCompletedByProjectCollaborator = request;
    }

    public List<TaskCollaboratorRegistryRESTDTO> activeTaskCollaboratorRegistryToDTO() {
        List<TaskCollaboratorRegistryRESTDTO> tcrList = new ArrayList<>();
        for (TaskCollaboratorRegistry tcr : taskCollaboratorRegistryList) {
            if (tcr.getRegistryStatus().equals(RegistryStatus.ASSIGNMENTAPPROVED)
                    || (tcr.getRegistryStatus().equals(RegistryStatus.REMOVALREQUEST))) {
                tcrList.add(tcr.toDTO());
            }
        }
        return tcrList;
    }

    public List<TaskCollaboratorRegistryRESTDTO> allTaskCollaboratorRegistryToDTO() {
        List<TaskCollaboratorRegistryRESTDTO> tcrList = new ArrayList<>();
        for (TaskCollaboratorRegistry tcr : taskCollaboratorRegistryList) {
            if (tcr.getRegistryStatus().equals(RegistryStatus.ASSIGNMENTAPPROVED)
                    || (tcr.getRegistryStatus().equals(RegistryStatus.REMOVALREQUEST)) || (tcr.getRegistryStatus().equals(RegistryStatus.REMOVALAPPROVED))) {
                tcrList.add(tcr.toDTO());
            }
        }
        return tcrList;
    }

    /**
     * Method to pass task to DTO.
     *
     * @return taskRestDTO.
     */
    public TaskRestDTO toDTO() {
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskRestDTO.setTaskId(taskId);
        taskRestDTO.setTitle(title);
        taskRestDTO.setDescription(description);
        taskRestDTO.setEstimatedEffort(estimatedEffort);
        taskRestDTO.setDateOfCreation(dateOfCreation);
        taskRestDTO.setPredictedDateOfStart(predictedDateOfStart);
        taskRestDTO.setPredictedDateOfConclusion(predictedDateOfConclusion);
        taskRestDTO.setEffectiveStartDate(effectiveStartDate);
        taskRestDTO.setEffectiveDateOfConclusion(effectiveDateOfConclusion);
        taskRestDTO.setUnitCost(unitCost);
        taskRestDTO.setProjectId(project.getId());
        taskRestDTO.setState(taskStatePersisted);
        taskRestDTO.setCompletedRequest(this.getRequestTaskCompleted());
        taskRestDTO.setGanttTaskDependencies(createGanttTaskDependencies(taskDependencies));
        return taskRestDTO;
    }
}
