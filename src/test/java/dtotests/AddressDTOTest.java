package dtotests;

import org.junit.Before;
import org.junit.Test;
import project.dto.AddressDTO;

import static org.junit.Assert.assertEquals;

public class AddressDTOTest {
    AddressDTO addressDto;

    @Before
    public void setUp() {
        addressDto = new AddressDTO("city", "country", "postalCodeCity", "postalCodeAddress", "street");

    }

    @Test
    public void test() {
        assertEquals("city", addressDto.getCity());
    }

    @Test
    public void testGetCountry() {
        assertEquals("country", addressDto.getCountry());
    }

    @Test
    public void testGetPostalCodeCit() {
        assertEquals("postalCodeCity", addressDto.getPostalCodeCity());
    }

    @Test
    public void testGetPostalCodeAddress() {
        assertEquals("postalCodeAddress", addressDto.getPostalCodeAddress());
    }

    @Test
    public void testGetStreet() {
        assertEquals("street", addressDto.getStreet());
    }

}
