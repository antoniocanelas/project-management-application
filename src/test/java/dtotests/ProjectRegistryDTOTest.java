package dtotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectDTO;
import project.dto.ProjectRegistryDTO;


public class ProjectRegistryDTOTest {

	ProjectRegistryDTO projectRegistryDTO;
	ProjectDTO projectDTOTest;
	List<ProjectDTO> listProjects=new ArrayList<>();

    @Before
    public void setUp() {
    	
    	projectRegistryDTO = new ProjectRegistryDTO();
    	projectRegistryDTO.setListProjects(listProjects);
    		
    }

    @Test
    public void testGetListProjects() {
    	
    	assertTrue(projectRegistryDTO.getListProjects().equals(listProjects));
    }
    
    @Test
    public void testToString() {
        
        String expected = "[lista_projetos=" + listProjects + "]";
        assertEquals(expected, projectRegistryDTO.toString());
    }
	
	
}
