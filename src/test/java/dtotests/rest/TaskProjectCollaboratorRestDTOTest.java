package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectCollaboratorDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;

public class TaskProjectCollaboratorRestDTOTest {

	TaskProjectCollaboratorRestDTO testDTO;
	TaskProjectCollaboratorRestDTO testDTO2;
	
	ProjectCollaboratorDTO testDTO3;
	
	
	
	@Before
	public void setUp()  {
		

		testDTO = new TaskProjectCollaboratorRestDTO();
		
		testDTO.setProjectCollaboratorId(1);
		testDTO.setTaskId("task1");
		
	}
	
	@Test
	public void testSetTaskId() {
		
		assertEquals("task1", testDTO.getTaskId());
	}
	
	@Test
	public void testgetProjectCollaboratorId() {
		
		assertEquals(1, testDTO.getProjectCollaboratorId());
	}
	
	
	@Test
	public void testEqualsTrue() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId("task1");
	;
		assertEquals(testDTO,testDTO);
		assertEquals(testDTO,testDTO2);
		
		
	}
	
	
	@Test
	public void testEqualsFalseDifferentTaskID() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId("task3");
		
		assertNotEquals(testDTO,testDTO2);
		
	}
	
	@Test
	public void testEqualsFalseDifferentCollaboratorID() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(3);
		testDTO2.setTaskId("task3");
		
		assertNotEquals(testDTO,testDTO2);
		
	}
	
	@Test
	public void testEqualsFalseDifferentClass() {

		testDTO3 = new ProjectCollaboratorDTO();
		
	   assertFalse(testDTO.getClass().equals(testDTO3.getClass()));
	   
		assertNotEquals(testDTO,testDTO3);
		
	}
	
	@Test
	public void testEqualsFalseNull() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId(null);
		
		assertNotEquals(testDTO,testDTO2);
		assertFalse(testDTO.equals(testDTO2));
		
	}
	
	@Test
	public void testEqualsFalseNullTaskID() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId("Test");
		testDTO.setTaskId(null);
		
		assertNotEquals(testDTO,testDTO2);
		assertFalse(testDTO.equals(testDTO2));
		
	}
	
	@Test
	public void testEqualsFalseNullTaskIDs() {

		testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId(null);
		testDTO.setTaskId(null);
		
		assertEquals(testDTO,testDTO2);
		assertTrue(testDTO.equals(testDTO2));
		
	}
	
	
	@Test
	public void testToString() {

		String expected="TaskProjectCollaboratorRestDTO [projectCollaboratorId=" + 1 + ", taskId=" + "task1"
				+ "]";
		
		assertEquals(expected,testDTO.toString());
	
		
	}
	
	@Test
	public void testHasCode() {

		
       testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId("task1");
	
		assertEquals(testDTO.hashCode(),testDTO.hashCode());
		assertEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}
	
	@Test
	public void testHasCodeFalse() {

		
       testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId("task5");
		
		assertNotEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}
	
	@Test
	public void testHasCodeNull() {

		
       testDTO2 = new TaskProjectCollaboratorRestDTO();
		
		testDTO2.setProjectCollaboratorId(1);
		testDTO2.setTaskId(null);
		
		assertNotEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}
}
