package dtotests.rest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.ProjectCostOptionsDTO;

public class SetProjectCostOptionsDTOTest {
	
	ProjectCostOptionsDTO projectCostOptionsDTO;
	List<String> costCalculationOptions;

	@Before
	public void setUp() {
		projectCostOptionsDTO = new ProjectCostOptionsDTO();
		costCalculationOptions = new ArrayList<>();
		costCalculationOptions.add("AverageCost");
		costCalculationOptions.add("LastTimePeriodCost");
		projectCostOptionsDTO.setCostCalculationOptions(costCalculationOptions);
		projectCostOptionsDTO.setProjectId("1");
	}

	@Test
	public void testHashCode() {
		ProjectCostOptionsDTO testDTO = new ProjectCostOptionsDTO();
		testDTO.setCostCalculationOptions(costCalculationOptions);
		testDTO.setProjectId("1");
		assertEquals(projectCostOptionsDTO.hashCode(),testDTO.hashCode());
		testDTO.setProjectId(null);
		projectCostOptionsDTO.setProjectId(null);
		assertEquals(projectCostOptionsDTO.hashCode(),testDTO.hashCode());
		testDTO.setCostCalculationOptions(null);
		projectCostOptionsDTO.setCostCalculationOptions(null);
		assertEquals(projectCostOptionsDTO.hashCode(),testDTO.hashCode());
	}

	@Test
	public void testSetAndGetCostCalculationOptions() {
		assertEquals(projectCostOptionsDTO.getCostCalculationOptions(),costCalculationOptions);
		List<String> newOptions = new ArrayList<>();
		newOptions.add("MaximumCost");
		projectCostOptionsDTO.setCostCalculationOptions(newOptions);
		assertEquals(projectCostOptionsDTO.getCostCalculationOptions(),newOptions);
	}
	
	@Test
	public void testGetAndSetProjectId() {
		assertEquals("1", projectCostOptionsDTO.getProjectId());
		projectCostOptionsDTO.setProjectId("5");
		assertEquals("5", projectCostOptionsDTO.getProjectId());
	}

	@Test
	public void testEqualsTrue() {
		ProjectCostOptionsDTO testDTO = new ProjectCostOptionsDTO();
		testDTO.setCostCalculationOptions(costCalculationOptions);
		testDTO.setProjectId("1");
		assertEquals(projectCostOptionsDTO,testDTO);
	}
	
	@Test
	public void testEqualsFalseDifferentProject() {
		ProjectCostOptionsDTO testDTO = new ProjectCostOptionsDTO();
		testDTO.setCostCalculationOptions(costCalculationOptions);
		testDTO.setProjectId("2");
		assertNotEquals(projectCostOptionsDTO,testDTO);
	}
	
	@Test
	public void testEqualsFalseDifferentCostOptions() {
		ProjectCostOptionsDTO testDTO = new ProjectCostOptionsDTO();
		List<String> newOptions = new ArrayList<>();
		newOptions.add("MaximumCost");
		testDTO.setCostCalculationOptions(newOptions);
		testDTO.setProjectId("1");
		assertNotEquals(projectCostOptionsDTO,testDTO);
	}
	
	@Test
	public void testEqualsFalseNullCase() {
		assertNotEquals(projectCostOptionsDTO,null);
	}
	
	@Test
	public void testEqualsFalseNullAttributes() {
		ProjectCostOptionsDTO testDTO = new ProjectCostOptionsDTO();
		//Null cost options
		testDTO.setProjectId("1");
		testDTO.setCostCalculationOptions(null);
		assertNotEquals(projectCostOptionsDTO,testDTO);
		assertNotEquals(testDTO,projectCostOptionsDTO);
		
		//Both costCalculations null and same projectID
		projectCostOptionsDTO.setCostCalculationOptions(null);
		assertEquals(testDTO,projectCostOptionsDTO);		
		//Null project id
		projectCostOptionsDTO.setCostCalculationOptions(costCalculationOptions);
		testDTO.setCostCalculationOptions(costCalculationOptions);
		testDTO.setProjectId(null);
		assertNotEquals(projectCostOptionsDTO,testDTO);
		assertNotEquals(testDTO,projectCostOptionsDTO);
		//Null both id's
		projectCostOptionsDTO.setProjectId(null);
		assertEquals(testDTO,projectCostOptionsDTO);
		//All attributes null
		projectCostOptionsDTO.setCostCalculationOptions(null);
		assertNotEquals(testDTO,projectCostOptionsDTO);
	}
	
	@Test
	public void testEqualsFalseDifferentObject() {
		assertNotEquals(projectCostOptionsDTO,1);
	}


	@Test
	public void testToString() {
		String expected = "SetProjectCostOptionsDTO [projectId="+projectCostOptionsDTO.getProjectId()+", costCalculationOptions=[AverageCost, LastTimePeriodCost]]";
		assertEquals(expected,projectCostOptionsDTO.toString());
	}
}
