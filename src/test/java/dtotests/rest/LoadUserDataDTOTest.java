package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.LoadUserDataDTO;

import static org.junit.Assert.assertTrue;

public class LoadUserDataDTOTest {

    LoadUserDataDTO loadUserDataDTO;

    @Before
    public void setUp()  {

        loadUserDataDTO = new LoadUserDataDTO();
        loadUserDataDTO.setFileName("firstUsers.xml");
    }

    @Test
    public void testGetFilenameSuccess() {
        boolean condition = loadUserDataDTO.getFileName().equals("firstUsers.xml");
        assertTrue(condition);
    }
}
