package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectCollaboratorDTO;
import project.dto.rest.ProjectCollaboratorRestDTO;

public class ProjectCollaboratorRestDTOTest {

	ProjectCollaboratorRestDTO projectCollaboratorRestDTO;
	
	@Before
	public void setUp()  {
		

		projectCollaboratorRestDTO = new ProjectCollaboratorRestDTO();
		
		projectCollaboratorRestDTO.setUserId("manuel@switch.com");
		projectCollaboratorRestDTO.setProjectId("project");
		projectCollaboratorRestDTO.setCost(2.5);
		projectCollaboratorRestDTO.setUserName("Manuel");
		projectCollaboratorRestDTO.setPhone("912348954");
		
	}
	
	@Test
	public void testGetUserName() {
		
		assertEquals("Manuel", projectCollaboratorRestDTO.getUserName());
	}
	
	@Test
	public void testGetPhone() {
		
		assertEquals("912348954", projectCollaboratorRestDTO.getPhone());
	}
	
	@Test
	public void testGetUserId() {
		
		assertEquals("manuel@switch.com", projectCollaboratorRestDTO.getUserId());
	}
	
	@Test
	public void testGetProjectId() {
		
		assertEquals("project", projectCollaboratorRestDTO.getProjectId());
	}
	
	@Test
	public void testGetCost() {
		
		assertEquals(2.5, projectCollaboratorRestDTO.getCost(),0.01);
	}
	
	@Test
    public void testEqualsSucess() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId("manuel@switch.com");
		projectCollaboratorRestDTOTest.setProjectId("project");
		projectCollaboratorRestDTOTest.setCost(2.5);
        
        assertEquals(projectCollaboratorRestDTOTest, projectCollaboratorRestDTOTest);
        assertEquals(projectCollaboratorRestDTOTest, projectCollaboratorRestDTO);
    }

    @Test
    public void testEqualsFail() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId("joana@switch.com");
		projectCollaboratorRestDTOTest.setProjectId("project");
		projectCollaboratorRestDTOTest.setCost(2.5);
		
		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest2 = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest2.setUserId("manuel@switch.com");
		projectCollaboratorRestDTOTest2.setProjectId("projectTest");
		projectCollaboratorRestDTOTest2.setCost(2.5);
		
		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest3 = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest3.setUserId("manuel@switch.com");
		projectCollaboratorRestDTOTest3.setProjectId("project");
		projectCollaboratorRestDTOTest3.setCost(3.5);

        assertNotEquals(projectCollaboratorRestDTOTest, projectCollaboratorRestDTO);
        assertNotEquals(projectCollaboratorRestDTOTest2, projectCollaboratorRestDTO);
        assertNotEquals(projectCollaboratorRestDTOTest3, projectCollaboratorRestDTO);
    }
    
    @Test
    public void testEqualsFailUserNull() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId(null);
		projectCollaboratorRestDTOTest.setProjectId("project");
		projectCollaboratorRestDTOTest.setCost(2.5);

        assertFalse(projectCollaboratorRestDTOTest.equals(projectCollaboratorRestDTO));
    }
    
    @Test
    public void testEqualsFailTwoUsersNull() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId(null);
		projectCollaboratorRestDTOTest.setProjectId("project");
		projectCollaboratorRestDTOTest.setCost(2.5);
		
		projectCollaboratorRestDTO.setUserId(null);

        assertTrue(projectCollaboratorRestDTOTest.equals(projectCollaboratorRestDTO));
        
        projectCollaboratorRestDTO.setUserId("manuel@switch.com");
    }
    
    @Test
    public void testEqualsFailProjectNull() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId("joana@switch.com");
		projectCollaboratorRestDTOTest.setProjectId(null);
		projectCollaboratorRestDTOTest.setCost(2.5);

        assertNotEquals(projectCollaboratorRestDTOTest, projectCollaboratorRestDTO);
        assertFalse(projectCollaboratorRestDTOTest.equals(projectCollaboratorRestDTO));
    }
    
    @Test
    public void testEqualsFailTwoProjectsNullAndDifferentUser() {

		ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
		projectCollaboratorRestDTOTest.setUserId("joana@switch.com");
		projectCollaboratorRestDTOTest.setProjectId(null);
		projectCollaboratorRestDTOTest.setCost(2.5);

		projectCollaboratorRestDTO.setProjectId(null);
		
        assertFalse(projectCollaboratorRestDTOTest.equals(projectCollaboratorRestDTO));
        
		projectCollaboratorRestDTO.setProjectId("project");
    }

    @Test
    public void testEqualsFailureDifferentClass() {

    	ProjectCollaboratorDTO projectCollaboratorDTO = new ProjectCollaboratorDTO();

        assertFalse(projectCollaboratorRestDTO.equals(projectCollaboratorDTO));
    }
	
    @Test
    public void testHashCode() {

    	ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest = new ProjectCollaboratorRestDTO();
    	projectCollaboratorRestDTOTest.setUserId(null);
		projectCollaboratorRestDTOTest.setProjectId(null);
		projectCollaboratorRestDTOTest.setCost(4.3);

        ProjectCollaboratorRestDTO projectCollaboratorRestDTOTest2 = new ProjectCollaboratorRestDTO();
        projectCollaboratorRestDTOTest2.setUserId("manuel@switch.com");
		projectCollaboratorRestDTOTest2.setProjectId("project");
		projectCollaboratorRestDTOTest2.setCost(2.5);

        assertEquals(projectCollaboratorRestDTO.hashCode(), projectCollaboratorRestDTOTest2.hashCode());
        assertNotEquals(projectCollaboratorRestDTOTest.hashCode(), projectCollaboratorRestDTOTest2.hashCode());
    }
    
}
