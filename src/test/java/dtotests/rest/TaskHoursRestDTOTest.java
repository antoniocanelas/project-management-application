package dtotests.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.TaskHoursRestDTO;

public class TaskHoursRestDTOTest {

	TaskHoursRestDTO taskHoursRestDTO;
	
	
	@Before
	public void setUp() {
		
		
		taskHoursRestDTO= new TaskHoursRestDTO();
		taskHoursRestDTO.setHours(2.0);
		taskHoursRestDTO.setPeopleMonth(0.4);
		
	}
	
	 @Test
	    public void testGetHours() {
	        assertEquals(2.0 ,taskHoursRestDTO.getHours(), 0.01);
	    }
	
	 
	 @Test
	    public void testGetPeopleMonth() {
	        assertEquals(0.4 ,taskHoursRestDTO.getPeopleMonth(), 0.01);
	    }
}
