package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.EditReportQuantityDTO;

import static org.junit.Assert.assertEquals;

public class EditReportQuantityDTOTest {
	
	EditReportQuantityDTO editReportQuantityDTO;

	@Before
	public void setUp()  {
		editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setCollabEmail("pedro@mail.com");
		editReportQuantityDTO.setQuantity(5.0);
		editReportQuantityDTO.setTaskID("task1");
		editReportQuantityDTO.setReportId("1");
	}

	@Test
	public void testGetQuantity() {
		assertEquals(5.0, editReportQuantityDTO.getQuantity(),0.01);
	}
	

	@Test
	public void testGetTaskID() {
		assertEquals("task1", editReportQuantityDTO.getTaskID());
	}
	
	@Test
	public void testGetReportId() {
		assertEquals("1", editReportQuantityDTO.getReportId());
	}

	@Test
	public void testGetCollabEmail() {
		assertEquals("pedro@mail.com", editReportQuantityDTO.getCollabEmail());
	}
}
