package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.TaskCostRestDTO;

import static org.junit.Assert.assertEquals;

public class TaskCostRestDTOTest {

    TaskCostRestDTO taskCostRestDTO;


    @Before
    public void setUp() {
        taskCostRestDTO = new TaskCostRestDTO();
        taskCostRestDTO.setCost(2.0);
        taskCostRestDTO.setTaskId("taskId");
        taskCostRestDTO.setTitle("title");
    }

    @Test
    public void testGetCost() {
        assertEquals(2.0 ,taskCostRestDTO.getCost(), 0.01);
    }

    @Test
    public void testGetTaskId() {
        assertEquals("taskId" ,taskCostRestDTO.getTaskId());
    }

    @Test
    public void testGetTitle() {
        assertEquals("title" ,taskCostRestDTO.getTitle());
    }
}
