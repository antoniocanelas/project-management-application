package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.SetProjectManagerDTO;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SetProjectManagerDTOTest {

    SetProjectManagerDTO setProjectManagerDTO;

    @Before
    public void setUp()  {

        setProjectManagerDTO = new SetProjectManagerDTO();
        setProjectManagerDTO.setEmail("pedro@mymail.com");
        setProjectManagerDTO.setProjectId("1");
    }

    @Test
    public void getEmailTestSucess() {

        boolean condition = setProjectManagerDTO.getEmail().equals("pedro@mymail.com");
        assertTrue(condition);
    }

    @Test
    public void getEmailTestFail() {

        boolean condition = setProjectManagerDTO.getEmail().equals("joao@mymail.com");
        assertFalse(condition);
    }

    @Test
    public void getProjectIdTestSucess() {

        boolean condition = setProjectManagerDTO.getProjectId().equals("1");
        assertTrue(condition);

    }

    @Test
    public void getProjectIdTestFail() {

        boolean condition = setProjectManagerDTO.getProjectId().equals("2");
        assertFalse(condition);
    }
}
