package dtotests.rest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.TaskRestDTO;

import static org.junit.Assert.*;

public class TaskRestDTOTest {

    TaskRestDTO testDTO;
    TaskRestDTO testDTO2;
    LocalDateTime now = LocalDateTime.now();
    List<String> dependenciesId=new ArrayList<>();

    @Before
    public void setUp() {
    	
        testDTO = new TaskRestDTO();

        testDTO.setDateOfCreation(now);
        testDTO.setDescription("testeDTO");
        testDTO.setEffectiveDateOfConclusion(now.plusDays(35));
        testDTO.setEffectiveStartDate(now.plusDays(5));
        testDTO.setEstimatedEffort(5.0);
        testDTO.setPredictedDateOfConclusion(now.plusDays(25));
        testDTO.setPredictedDateOfStart(now.plusDays(5));
        testDTO.setLastReportDate(now);
        testDTO.setLastReportCreationDate(now);
        testDTO.setState("InProgress");
        testDTO.setTaskId("task1");
        testDTO.setTitle("task1");
        testDTO.setUnitCost(1.2);
        testDTO.setDependenciesId(dependenciesId);
    }

    @Test
    public void testGetDependenciesId() {

        assertTrue(testDTO.getDependenciesId().equals(dependenciesId));
    }

    @Test
    public void testGetTaskId() {

        assertTrue(testDTO.getTaskId().equals("task1"));
    }

    @Test
    public void testSetTaskId() {

        testDTO.setTaskId("task234");

        assertTrue(testDTO.getTaskId().equals("task234"));
    }

    @Test
    public void testGetTitle() {

        assertTrue(testDTO.getTitle().equals("task1"));
    }

    @Test
    public void testSetTitle() {

        testDTO.setTitle("teste234");

        assertTrue(testDTO.getTitle().equals("teste234"));
    }

    @Test
    public void testGetDescription() {

        assertTrue(testDTO.getDescription().equals("testeDTO"));
    }

    @Test
    public void testSetDescription() {

        testDTO.setDescription("newDescription");

        assertTrue(testDTO.getDescription().equals("newDescription"));
    }

    @Test
    public void testGetEstimatedEffort() {

        assertEquals(5.0, testDTO.getEstimatedEffort(), 0.01);
    }

    @Test
    public void testSetEstimatedEffort() {

        testDTO.setEstimatedEffort(5.67);

        assertEquals(5.67, testDTO.getEstimatedEffort(), 0.001);
    }

    @Test
    public void testGetDateOfCreation() {

        assertEquals(testDTO.getDateOfCreation(), now);
    }

    @Test
    public void testSetDateOfCreation() {

        testDTO.setDateOfCreation(now.plusDays(1));

        assertEquals(testDTO.getDateOfCreation(), now.plusDays(1));
    }

    @Test
    public void testGetEffectiveDateOfConclusion() {

        assertEquals(testDTO.getEffectiveDateOfConclusion(), now.plusDays(35));
    }

    @Test
    public void testSetEffectiveDateOfConclusion() {

        testDTO.setEffectiveDateOfConclusion(now.plusDays(50));

        assertEquals(testDTO.getEffectiveDateOfConclusion(), now.plusDays(50));
    }

    @Test
    public void testGetEffectiveStartDate() {

        assertEquals(testDTO.getEffectiveStartDate(), now.plusDays(5));
    }

    @Test
    public void testSetEffectiveStartDate() {

        testDTO.setEffectiveStartDate(now.plusDays(10));

        assertEquals(testDTO.getEffectiveStartDate(), now.plusDays(10));
    }

    @Test
    public void testGetPredictedDateOfConclusion() {

        assertEquals(testDTO.getPredictedDateOfConclusion(), now.plusDays(25));
    }

    @Test
    public void testSetPredictedDateOfConclusion() {

        testDTO.setPredictedDateOfConclusion(now.plusDays(37));

        assertEquals(testDTO.getPredictedDateOfConclusion(), now.plusDays(37));
    }

    @Test
    public void testGetPredictedDateOfStart() {

        assertEquals(testDTO.getPredictedDateOfStart(), now.plusDays(5));
    }

    @Test
    public void testSetPredictedDateOfStart() {

        testDTO.setPredictedDateOfStart(now.plusDays(7));

        assertEquals(testDTO.getPredictedDateOfStart(), now.plusDays(7));
    }

    @Test
    public void testGetUnitCost() {

        assertEquals(1.2, testDTO.getUnitCost(), 0.01);
    }

    @Test
    public void testSetUnitCost() {

        testDTO.setUnitCost(1.6);

        assertEquals(1.6, testDTO.getUnitCost(), 0.01);
    }

    @Test
    public void testGetState() {

        assertEquals("InProgress", testDTO.getState());
    }

    @Test
    public void testSetState() {

        testDTO.setState("Suspended");

        assertEquals("Suspended", testDTO.getState());
    }

    @Test
    public void testGetDependenciesIdCsv() {

        assertEquals(null, testDTO.getGanttTaskDependencies());
    }

    @Test
    public void testSetDependenciesIdCsv() {

        testDTO.setGanttTaskDependencies("2-3");

        assertEquals("2-3", testDTO.getGanttTaskDependencies());
    }

    @Test
    public void testEquals() {
        testDTO2 = new TaskRestDTO();
        testDTO2.setTaskId("task2");
        assertTrue(testDTO.equals(testDTO));
        assertFalse(testDTO.equals(testDTO2));
        assertTrue(testDTO instanceof TaskRestDTO);
        assertFalse(testDTO.equals("email"));
    }

    @Test
    public void testHashCode() {
        assertEquals(testDTO.hashCode(), testDTO.hashCode());
    }



}
