package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.UserRegistryDTO;
import project.dto.rest.UserRegistryRestDTO;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class UserRegistryRestDTOTest {

	UserRegistryRestDTO userDTO;

    @Before
    public void setUp() {
    	
    	userDTO = new UserRegistryRestDTO();
		
    	userDTO.setName("Luis");
		userDTO.setEmail("luis@switch.com");
		userDTO.setPhone("123456789");
		userDTO.setTin("123456789");
		userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
		userDTO.setAddressId("1");
		userDTO.setStreet("Rua sem saida");
		userDTO.setPostalCode("3700");
		userDTO.setCity("Porto");
		userDTO.setCountry("Portugal");
		userDTO.setProfile("ROLE_DIRECTOR");
    }

    @Test
    public void testGetName() {
    	assertTrue(userDTO.getName().equals("Luis"));
    }

    @Test
    public void testSetName() {
    	userDTO.setName("Fabio");
    	
    	assertTrue(userDTO.getName().equals("Fabio"));
    }

    @Test
    public void testGetAndSetEmail() {
    	userDTO.setEmail("asdrubal@switch.com");
    	
    	assertTrue(userDTO.getEmail().equals("asdrubal@switch.com"));
    }
    
    @Test
    public void testGetAndSetPassword() {
    	userDTO.setPassword("qwerty");

    	assertTrue(userDTO.getPassword().equals("qwerty"));
    }

    @Test
    public void testGetAndSetPhone() {
    	userDTO.setPhone("987654321");

        assertTrue(userDTO.getPhone().equals("987654321"));
    }

    @Test
    public void testGetAndSetTin() {
    	userDTO.setTin("987654321");
    	
    	assertTrue(userDTO.getTin().equals("987654321"));
    }

    @Test
    public void testGetAndSetBirthDate() {
    	userDTO.setBirthDate(LocalDate.of(1977, 3, 6));

    	assertTrue(userDTO.getBirthDate().equals(LocalDate.of(1977, 3, 6)));
    }

    @Test
    public void testGetAndSetAddressId() {
    	userDTO.setAddressId("2");
    	
    	assertTrue(userDTO.getAddressId().equals("2"));
    }

    @Test
    public void testGetAndSetStreet() {
    	userDTO.setStreet("Rua Abril");
    	
    	assertTrue(userDTO.getStreet().equals("Rua Abril"));
    }

    @Test
    public void testGetAndSetPostalCode() {
    	userDTO.setPostalCode("2345");
    	
    	assertTrue(userDTO.getPostalCode().equals("2345"));
    }

    @Test
    public void testGetAndSetCity() {
    	userDTO.setCity("Faro");
    	
    	assertTrue(userDTO.getCity().equals("Faro"));
    }

    @Test
    public void testGetAndSetCountry() {
    	userDTO.setCountry("Espanha");
    	
    	assertTrue(userDTO.getCountry().equals("Espanha"));
    }
    
    @Test
    public void testGetAndSetProfile() {
    	userDTO.setProfile("ROLE_COLLABORATOR");
    	
    	assertTrue(userDTO.getProfile().equals("ROLE_COLLABORATOR"));
    }
    
    @Test
    public void testToString() {
    	  String result = userDTO.toString();
          String expected ="[utilizador=[nome_utilizador=Luis, email_utilizador=luis@switch.com, telefone=123456789, tin=123456789, birthdate=1977-03-05, profile=ROLE_DIRECTOR]";
          assertEquals(expected, result);
    }
    
    @Test
    public void testToStringFail() {
    	  String result = userDTO.toString();
          String expected ="[utilizador=[nome_utilizador=Fabio, email_utilizador=luis@switch.com, telefone=123456789, tin=123456789, birthdate=1977-03-05]";
          assertNotEquals(expected, result);
    }
    @Test
    public void testEqualsSucess() {
    	UserRegistryRestDTO testDTO = new UserRegistryRestDTO();
    	testDTO.setEmail("luis@switch.com");

        assertEquals(testDTO, testDTO);
        assertEquals(testDTO, userDTO);
    }

    @Test
    public void testEqualsFail() {

    	UserRegistryRestDTO testDTO = new UserRegistryRestDTO();
    	testDTO.setEmail("rita@switch.com");

        assertNotEquals(testDTO, userDTO);
    }

    @Test
    public void testEqualsFailureObjectNull() {

    	UserRegistryRestDTO trNull = new UserRegistryRestDTO();;
    	trNull.setEmail(null);
   
        assertNotEquals(userDTO, trNull);
        assertFalse(trNull.equals(userDTO));

    }
    @Test
    public void testEqualsFailureObjectNull2() {

    	UserRegistryRestDTO testNull = null;
        assertNotEquals(userDTO, testNull);
    }
    @Test
    public void testEqualsFailureObjectNullEmail() {

    	UserRegistryRestDTO trNull = new UserRegistryRestDTO();;
    	trNull.setEmail(null);
    	userDTO.setEmail(null);
   
        assertEquals(userDTO, trNull);
    }

    @Test
    public void testEqualsFailureDifferentClass() {

        UserRegistryDTO userRestDTO = new UserRegistryDTO();
     
        assertNotEquals(userRestDTO,userDTO);
    }
    @Test
    public void testHashCode() {
    	UserRegistryRestDTO userRegistryDTO1 = new UserRegistryRestDTO();
    	userRegistryDTO1.setEmail(null);
    	
    	UserRegistryRestDTO userRegistryDTO2 = new UserRegistryRestDTO();
    	userRegistryDTO2.setEmail("luis@switch.com");
    	
    	assertEquals(userDTO.hashCode(), userRegistryDTO2.hashCode());
    	assertNotEquals(userRegistryDTO1.hashCode(), userRegistryDTO2.hashCode());

    	
    }

}