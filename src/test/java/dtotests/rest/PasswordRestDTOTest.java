package dtotests.rest;

import org.junit.Before;
import org.junit.Test;
import project.dto.rest.PasswordRestDTO;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordRestDTOTest {

    PasswordRestDTO passwordRestDTO;

    @Before
    public void setUp()  {

        passwordRestDTO = new PasswordRestDTO();
        passwordRestDTO.setNewPassword("qwerty");
    }

    @Test
    public void testGetPasswordSucess() {

        boolean condition = passwordRestDTO.getNewPassword().equals("qwerty");
        assertTrue(condition);
    }

    @Test
    public void testGetPasswordInvalid() {

        boolean condition = passwordRestDTO.getNewPassword().equals("12345");
        assertFalse(condition);
    }
}
