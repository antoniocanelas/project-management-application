package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectCollaboratorDTO;
import project.dto.rest.ReportRestDTO;

public class ReportRestDTOTest {
	
	ReportRestDTO testDTO;
	ReportRestDTO testDTO2;
	
	ProjectCollaboratorDTO testDTO3;
	
	
	@Before
	public void setUp()  {
		

		testDTO = new ReportRestDTO();
		
		testDTO.setReportCreationDate(LocalDateTime.of(2018, 4, 5,0,0));
		testDTO.setCode("1");
		testDTO.setQuantity(2);
		testDTO.setUserEmail("joaquim@switch.com");
		testDTO.setStartDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO.setEndDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO.setEffort(2);	
		
	}
	
	@Test
	public void testGetReportCreationDate() {
		
		assertEquals(LocalDateTime.of(2018, 4, 5,0,0), testDTO.getReportCreationDate());
	}
	
	@Test
	public void testGetCode() {
		
		assertEquals("1", testDTO.getCode());
	}
	
	@Test
	public void testGetQuantity() {
		
		assertEquals(2, testDTO.getQuantity(), 0.01);
	}
	
	@Test
	public void testGetUserEmail() {
		
		assertEquals("joaquim@switch.com", testDTO.getUserEmail());
	}
	@Test
	public void testGetStartDate() {
		
		assertEquals(LocalDateTime.of(2018, 3, 5,0,0), testDTO.getStartDate());
	}
	
	@Test
	public void testGetEndDate() {
		
		assertEquals(LocalDateTime.of(2018, 3, 5,0,0), testDTO.getEndDate());
	}
	
	@Test
	public void testGetEffort() {
		
		assertEquals(2, testDTO.getEffort(),0.01);
	}
	
	
	@Test
	public void testEqualsTrue() {

		testDTO2 = new ReportRestDTO();
		
		testDTO2.setReportCreationDate(LocalDateTime.of(2018, 4, 5,0,0));
		testDTO2.setCode("1");
		testDTO2.setQuantity(2);
		testDTO2.setUserEmail("joaquim@switch.com");
		testDTO2.setStartDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO2.setEndDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO2.setEffort(2);
		
		assertEquals(testDTO,testDTO);
		assertEquals(testDTO,testDTO2);
		
		
	}
	
	@Test
	public void testEqualsFalseDifferentClass() {

		testDTO3 = new ProjectCollaboratorDTO();
		
	   assertFalse(testDTO.getClass().equals(testDTO3.getClass()));
	   
		assertNotEquals(testDTO,testDTO3);
		
	}
	
	@Test
	public void testEqualsFalseNull() {

		testDTO2 = new ReportRestDTO();
		
		assertNotEquals(testDTO,testDTO2);
		assertFalse(testDTO.equals(testDTO2));
		
	}
	
	@Test
	public void testHasCode() {

		
		testDTO2 = new ReportRestDTO();
		
		testDTO2.setReportCreationDate(LocalDateTime.of(2018, 4, 5,0,0));
		testDTO2.setCode("1");
		testDTO2.setQuantity(2);
		testDTO2.setUserEmail("joaquim@switch.com");
		testDTO2.setStartDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO2.setEndDate(LocalDateTime.of(2018, 3, 5,0,0));
		testDTO2.setEffort(2);
	
		assertEquals(testDTO.hashCode(),testDTO.hashCode());
		assertEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}
	
	@Test
	public void testHasCodeFalse() {

		
		testDTO2 = new ReportRestDTO();
		
		testDTO2.setReportCreationDate(LocalDateTime.of(2018, 5, 5,0,0));
		testDTO2.setCode("2");
		testDTO2.setQuantity(3);
		testDTO2.setUserEmail("joana@switch.com");
		testDTO2.setStartDate(LocalDateTime.of(2018, 1, 5,0,0));
		testDTO2.setEndDate(LocalDateTime.of(2018, 6, 5,0,0));
		testDTO2.setEffort(3);
		
		assertNotEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}
	
	@Test
	public void testHasCodeNull() {

		testDTO2 = new ReportRestDTO();

		assertNotEquals(testDTO.hashCode(),testDTO2.hashCode());
		
	}

}
