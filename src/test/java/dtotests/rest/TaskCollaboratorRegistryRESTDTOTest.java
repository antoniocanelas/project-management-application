package dtotests.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import project.dto.rest.TaskCollaboratorRegistryRESTDTO;

public class TaskCollaboratorRegistryRESTDTOTest {

    TaskCollaboratorRegistryRESTDTO testDTO;
    TaskCollaboratorRegistryRESTDTO testDTO2;
    TaskCollaboratorRegistryRESTDTO testDTO3;
    TaskCollaboratorRegistryRESTDTO testDTO4;

    @Before
    public void setUp() {


        testDTO = new TaskCollaboratorRegistryRESTDTO();

        testDTO.setTaskId("task1");
        testDTO.setProjectCollaboratorId(1);
        testDTO.setProjectCollaboratorName("col1");
        testDTO.setProjectCollaboratorEmail("col1@email.com");
        testDTO.setCollaboratorAddedToTaskDate(LocalDateTime.of(2018, 3, 5, 0, 0));
        testDTO.setCollaboratorRemovedFromTaskDate(LocalDateTime.of(2018, 5, 5, 0, 0));
        testDTO.setRegistryStatus("ASSIGNMENTAPPROVED");

    }

    @Test
    public void testGetTaskId() {

        assertEquals("task1", testDTO.getTaskId());
    }

    @Test
    public void testGetProjectCollaboratorId() {

        assertEquals(1, testDTO.getProjectCollaboratorId());
    }

    @Test
    public void testGetProjectCollaboratorName() {

        assertEquals("col1", testDTO.getProjectCollaboratorName());
    }

    @Test
    public void testGetProjectCollaboratorEmail() {

        assertEquals("col1@email.com", testDTO.getProjectCollaboratorEmail());
    }

    @Test
    public void testGetCollaboratorAddedToTaskDate() {

        assertEquals(LocalDateTime.of(2018, 3, 5, 0, 0), testDTO.getCollaboratorAddedToTaskDate());
    }

    @Test
    public void testGetCollaboratorRemovedFromTaskDate() {

        assertEquals(LocalDateTime.of(2018, 5, 5, 0, 0), testDTO.getCollaboratorRemovedFromTaskDate());
    }

    @Test
    public void testGetRegistryStatus() {

        assertEquals("ASSIGNMENTAPPROVED", testDTO.getRegistryStatus());
    }

    @Test
    public void testToString() {

        String expected = "TaskCollaboratorRegistryRESTDTO [taskId=" + "task1" + ", projectCollaboratorId=" + "1"
                + ", projectCollaboratorName=" + "col1" + ", projectCollaboratorEmail="
                + "col1@email.com" + ", collaboratorAddedToTaskDate=" + LocalDateTime.of(2018, 3, 5, 0, 0)
                + ", collaboratorRemovedFromTaskDate=" + LocalDateTime.of(2018, 5, 5, 0, 0) + ", registryStatus="
                + "ASSIGNMENTAPPROVED" + "]";

        assertEquals(expected, testDTO.toString());
    }

    @Test
    public void testEquals() {
        testDTO3 = new TaskCollaboratorRegistryRESTDTO();
        testDTO3.setTaskId("task3");
        testDTO3.setProjectCollaboratorId(3);
        testDTO3.setProjectCollaboratorName("col3");
        testDTO3.setProjectCollaboratorEmail("col3@email.com");
        testDTO3.setCollaboratorAddedToTaskDate(LocalDateTime.of(2018, 3, 5, 0, 0));
        testDTO3.setCollaboratorRemovedFromTaskDate(LocalDateTime.of(2018, 5, 5, 0, 0));
        testDTO3.setRegistryStatus("ASSIGNMENTAPPROVED");

        assertTrue(testDTO.equals(testDTO));
        assertFalse(testDTO.equals(testDTO2));
        assertFalse(testDTO.equals(testDTO3));

        testDTO4 = new TaskCollaboratorRegistryRESTDTO();

        testDTO4.setTaskId("task1");
        testDTO4.setProjectCollaboratorId(1);

        assertEquals(testDTO, testDTO4);
    }

    @Test
    public void testEquals2() {
        testDTO3 = new TaskCollaboratorRegistryRESTDTO();
        testDTO3.setTaskId("task1");
        testDTO3.setProjectCollaboratorId(3);
        testDTO3.setProjectCollaboratorName("col3");
        testDTO3.setProjectCollaboratorEmail("col3@email.com");
        testDTO3.setCollaboratorAddedToTaskDate(LocalDateTime.of(2018, 3, 5, 0, 0));
        testDTO3.setCollaboratorRemovedFromTaskDate(LocalDateTime.of(2018, 5, 5, 0, 0));
        testDTO3.setRegistryStatus("ASSIGNMENTAPPROVED");

        assertTrue(testDTO.equals(testDTO));
        assertFalse(testDTO.equals(testDTO2));
        assertFalse(testDTO.equals(testDTO3));

        testDTO4 = new TaskCollaboratorRegistryRESTDTO();

        testDTO4.setTaskId("task1");
        testDTO4.setProjectCollaboratorId(1);

        assertEquals(testDTO, testDTO4);
    }

    @Test
    public void testHashCode() {
        //then

        testDTO3 = new TaskCollaboratorRegistryRESTDTO();
        testDTO3.setTaskId("task1");
        testDTO3.setProjectCollaboratorId(1);
        testDTO3.setProjectCollaboratorName("col1");
        testDTO3.setProjectCollaboratorEmail("col1@email.com");
        testDTO3.setCollaboratorAddedToTaskDate(LocalDateTime.of(2018, 3, 5, 0, 0));
        testDTO3.setCollaboratorRemovedFromTaskDate(LocalDateTime.of(2018, 5, 5, 0, 0));
        testDTO3.setRegistryStatus("ASSIGNMENTAPPROVED");

        assertEquals(testDTO.hashCode(), testDTO3.hashCode());

        assertEquals(-880843179, testDTO3.hashCode());
    }

}
