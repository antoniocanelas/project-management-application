package project.security;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import static org.junit.Assert.assertFalse;

public class CustomPermissionEvaluatorTest {

    CustomPermissionEvaluator customPermissionEvaluator;
    @Autowired
    Authentication authentication;
    Object targetDomainObject;
    Object permission;

    @Before
    public void setUp() throws Exception {
        customPermissionEvaluator = new CustomPermissionEvaluator();
    }

    @Test
    public void hasPermissionFalse() {
        assertFalse(customPermissionEvaluator.hasPermission(null, null, null));

    }

    @Test
    public void hasPermission1False() {
        assertFalse(customPermissionEvaluator.hasPermission(null, null, null, null));
    }
}
