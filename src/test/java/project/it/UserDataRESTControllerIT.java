package project.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cache.support.NullValue;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.dto.rest.PasswordRestDTO;
import project.dto.rest.ProfileRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class UserDataRESTControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	UserService userService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;
	private JwtAuthenticationResponse jwt;

	private User user;
	private User admin;
	private String getOriginalPassword;
	private String password;
	private PasswordRestDTO passwordRestDTO;

	@Before
	public void setUp() throws AddressException {

		userService.addUser("Pedro", "123456789", "pedro@mymail.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "112222", LocalDate.of(1977, 3, 5), "1",
				"Rua do Amial", "4250-444", "Porto", "Portugal");

		user = userService.searchUserByEmail("pedro@mymail.com");
		admin = userService.searchUserByEmail("manuel@gmail.com");
		password = passwordEncoder.encode("12345");

		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);
		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}

		user.setPassword(password);
		admin.setPassword(password);

		user.setRoles(Collections.singleton(rolesCollaborator));
		user.setActive();
		admin.setRoles(Collections.singleton(rolesAdmin));
		admin.setActive();

		passwordRestDTO = new PasswordRestDTO();

		passwordRestDTO.setNewPassword(password);
		getOriginalPassword = passwordRestDTO.getNewPassword();
		userService.setPassword(user.getEmail(), passwordRestDTO);

		userService.updateUser(user);
		userService.updateUser(admin);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("pedro@mymail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	/**
	 * Given: an original password, When: we setup a new password in PasswordRestDTO
	 * and fill the request body , Then: we get a http status of CREATED.
	 */
	@Test
	public void setPasswordTestSuccess() {

		// Given
		String originalPassword = "12345";
		assertTrue(passwordEncoder.matches(originalPassword, getOriginalPassword));

		// When
		String userId = user.getEmail();
		String password = "qwerty";
		passwordRestDTO.setNewPassword(password);
		String getFinalPassword = passwordRestDTO.getNewPassword();

		String url = "http://localhost:" + port + "/users/" + userId + "/password";
		String requestBody = "{\"password\":\"" + getFinalPassword + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		String finalPassword = "qwerty";
		String message = "New password submitted";
		assertEquals(finalPassword, getFinalPassword);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
		assertEquals(message, response.getBody());
	}

	/**
	 * Given: a original password, When: we setup a new password in PasswordRestDTO,
	 * and give a wrong URL, Then: we get a http status of NOT_FOUND.
	 */
	@Test
	public void editUserDataSetProfileCollaboratorSuccess() {

		// Given
		String originalPassword = "12345";
		assertTrue(passwordEncoder.matches(originalPassword, getOriginalPassword));

		// When
		NullValue userId = null;
		String password = "qwerty";
		passwordRestDTO.setNewPassword(password);
		String getFinalPassword = passwordRestDTO.getNewPassword();

		String url = "http://localhost:" + port + "/users//password";
		String requestBody = "{\"password\":\"" + getFinalPassword + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertNull(response.getBody());
	}

	@Test
	public void editUserDataSetProfileBadRequest() {

		String userId = user.getEmail();
		// FIXME profile = user.getProfile();
		String inputProfile = "collab";

		String url = "http://localhost:" + port + "/users/" + userId;
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void editUserDataUserNotFound() {
		String userId = "xafain@gmail.com";
		// FIXME profile = user.getProfile();
		String inputProfile = "collaborator";
		String url = "http://localhost:" + port + "/users/" + userId;
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Director, Then we get a response status OK and the user's profile is updated
	 * successfully.
	 */
	@Test
	public void editUserDataSetProfileDirectorRestSuccess() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);
		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		String inputProfile = "director";
		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_DIRECTOR);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Director (him being already Director), Then we get a response status OK and
	 * the user's profile stays the same.
	 */
	@Test
	public void editUserDataSetProfileDirectorRestAlreadyDirector() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();

		String inputProfile = "Director";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		
		// Set user profile Director
		restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_DIRECTOR);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Administrator, Then we get a response status OK and the user's profile is
	 * updated successfully.
	 */
	@Test
	public void editUserDataSetProfileAdministratorRestSuccess() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);
		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		String inputProfile = "administrator";
		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_ADMINISTRATOR);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Director (him being already Administrator), Then we get a response status OK and
	 * the user's profile stays the same.
	 */
	@Test
	public void editUserDataSetProfileAdministratorRestAlreadyAdministrator() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		
		String inputProfile = "Administrator";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		
		// Set user profile Administrator
		restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_ADMINISTRATOR);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Collaborator, Then we get a response status OK and the user's profile is
	 * updated successfully.
	 */
	@Test
	public void editUserDataSetProfileCollaboratorRestSuccess() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		
		// Set user profile Administrator
		Set<Role> userRoles = new LinkedHashSet<>();
		userRoles.add(roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR));
		user.setRoles(userRoles);
		userService.updateUser(user);

		String inputProfile = "Collaborator";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_COLLABORATOR);
	}
	
	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Collaborator, Then we get a response status OK and the user's profile stays
	 * the same.
	 */
	@Test
	public void editUserDataSetProfileCollaboratorRestAlreadyCollaborator() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		String inputProfile = "collaborator";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_COLLABORATOR);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * RegisteredUser, Then we get a response status OK and the user's profile is
	 * updated successfully.
	 */
	@Test
	public void editUserDataSetProfileRegisteredUserRestSuccess() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();

		String inputProfile = "registereduser";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_REGISTEREDUSER);
	}

	/**
	 * Given a logged in administrator, When he tries to change a user's profile to
	 * Director (him being already Administrator), Then we get a response status OK and
	 * the user's profile stays the same.
	 */
	@Test
	public void editUserDataSetProfileRegisteredUserRestAlreadyRegisteredUser() {

		// Given
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();

		String inputProfile = "RegisteredUser";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		// Set user profile RegisteredUser
		restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);
		
		// When
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		// Then
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(userService.searchUserByEmail(userId).getRoles().iterator().next().getName(),
				RoleName.ROLE_REGISTEREDUSER);
	}
	/**
	 * Given a logged in administrator, When the user's profile to be changed to is
	 * null, Then we get a response status OK and the user's profile stays the same.
	 */
	@Test
	public void editUserDataSetProfileFailureProfileNull() {

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);
		jwt = responseEntity.getBody();

		ProfileRestDTO profileDTO = new ProfileRestDTO();
		String userId = user.getEmail();
		RoleName expectedRole = userService.searchUserByEmail(userId).getRoles().iterator().next().getName();

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<ProfileRestDTO> entity = new HttpEntity<>(profileDTO, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(expectedRole, userService.searchUserByEmail(userId).getRoles().iterator().next().getName());
	}

	/**
	 * Given a logged in administrator, When the user's profile to be changed to is
	 * not recognized, Then we get a response status OK and the user's profile stays
	 * the same.
	 */
	@Test
	public void editUserDataSetProfileFailureProfileNotRecognized() {

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		String userId = user.getEmail();
		RoleName expectedRole = userService.searchUserByEmail(userId).getRoles().iterator().next().getName();

		String inputProfile = "invalidProfile";

		String url = "http://localhost:" + port + "/users/" + userId + "/profile";
		String requestBody = "{\"profile\":\"" + inputProfile + "\"}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, userId);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(expectedRole, userService.searchUserByEmail(userId).getRoles().iterator().next().getName());

	}

	/**
	 * Given a user that gives a correct old password to verify himself When he
	 * tries to set a new password Then we get a response with ACCEPTED status
	 */
	@Test
	public void setPasswordRestTestSuccess() {

		// Given
		passwordRestDTO.setOldPassword("12345");
		passwordRestDTO.setNewPassword(passwordEncoder.encode("67890"));

		String url = "http://localhost:" + port + "/users/changePassword";

		// When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<PasswordRestDTO> entity = new HttpEntity<>(passwordRestDTO, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);

		// Then
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
	}

	/**
	 * Given a user that gives an incorrect old password to verify himself When he
	 * tries to set a new password Then we get a response with BAD_REQUEST status
	 */
	@Test
	public void setPasswordRestTestFailureOldPsswordWrong() {

		// Given
		passwordRestDTO.setOldPassword(passwordEncoder.encode("OldPasswordWrong"));
		passwordRestDTO.setNewPassword(passwordEncoder.encode("67890"));

		String url = "http://localhost:" + port + "/users/changePassword";

		// When
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<PasswordRestDTO> entity = new HttpEntity<>(passwordRestDTO, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);

		// Then
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
}
