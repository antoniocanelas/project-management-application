package project.it;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.controllers.rest.UserRegistryRESTController;
import project.dto.rest.UserRegistryRestDTO;
import project.dto.rest.UserRestDTO;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class UserRegistryRESTControllerIT {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    private User user1;
    private User user2;
    private User user3;
    private UserIdVO userIdVO;
    private JwtAuthenticationResponse jwt;

    @Before
    public void setUp() throws AddressException {

        Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
        Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
        Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
        Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

        if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
            rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
            roleRepository.save(rolesAdmin);
        }
        if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
            rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
            roleRepository.save(rolesUser);
        }

        if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
            rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
            roleRepository.save(rolesDirector);
        }
        if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
            rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
            roleRepository.save(rolesCollaborator);
        }

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
                "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
                "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
                "Rua sem saída", "4250-357", "Porto", "Portugal");

        String pass = passwordEncoder.encode("12345");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("manel@jj.com");
        user3 = userService.searchUserByEmail("joaquim@jj.com");

        user1.setPassword(pass);
        user2.setPassword(pass);
        user3.setPassword(pass);

        user1.setRoles(Collections.singleton(rolesCollaborator));
        user2.setRoles(Collections.singleton(rolesAdmin));
        user3.setRoles(Collections.singleton(rolesCollaborator));

        user1.setActive();
        user2.setActive();
        user3.setInactive();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("manel@jj.com");
        loginRequest.setPassword("12345");
        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);

        jwt = responseEntity.getBody();
    }

    @Test
    public void testUserRegistryRESTController() {

        UserRegistryRESTController ur = new UserRegistryRESTController(userService);

        assertTrue(ur instanceof UserRegistryRESTController);
    }

    /**
     * US130
     * <p>
     * GIVEN a list simulating all of the company's users WHEN we ask for all users
     * in the company, using the appropriate URI THEN we get an HTTP status of OK
     * and with the correct data.
     */
    @Test
    public void testListAllUsers() {

        // Given
        List<UserRestDTO> expectedList = new ArrayList<>();
        expectedList.add(user1.toDTO());
        expectedList.add(user2.toDTO());
        expectedList.add(user3.toDTO());
        UserRegistryRESTController.addSelfLinkToEachElement(expectedList);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<List<UserRestDTO>> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange("http://localhost:" + port + "/users",
                HttpMethod.GET, entity, new ParameterizedTypeReference<List<UserRestDTO>>() {
                });

        // Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    /**
     * US136
     * <p>
     * GIVEN all of the company's users, WHEN we ask for all users in the company
     * with profile COLLABORATOR, using the appropriate URI THEN we get an HTTP
     * status of OK.
     */
    @Test
    public void testListUsersByProfileSuccess() {

        // Given
        String profile = "ROLE_COLLABORATOR";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<ParameterizedTypeReference<List<UserRestDTO>>> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<UserRegistryRestDTO>> result = restTemplate.exchange(
                "http://localhost:" + port + "/users?profile=" + profile, HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<UserRegistryRestDTO>>() {
                });

        // Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    /**
     * US136
     * <p>
     * GIVEN a list simulating all of the company's users WHEN we ask for all users
     * in the company with profile DIRECTOR, using the appropriate URI THEN we get
     * an HTTP status of OK.
     */
    @Test
    public void testListUsersByProfileNotFound() {

        // Given
        String profile = "ROLE_DIRECTOR";

        List<UserRestDTO> expectedList = new ArrayList<>();
        expectedList.add(user1.toDTO());
        expectedList.add(user3.toDTO());
        UserRegistryRESTController.addSelfLinkToEachElement(expectedList);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange(
                "http://localhost:" + port + "/users?profile=" + profile, HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<UserRestDTO>>() {
                });

        // Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    /**
     * US135
     * <p>
     * GIVEN a user's that's registered in the company, WHEN we use the URI with the
     * parameter corresponding to its email, THEN we get a valid HTTP request along
     * with the correct user.
     */
    @Test
    public void testListUsersByEmailSuccess() {

        // Given
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("manel@jj.com");
        loginRequest.setPassword("12345");
        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);

        jwt = responseEntity.getBody();

        String email = user1.getEmail();
        UserRestDTO expected = user1.toDTO();

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<UserRestDTO> response = restTemplate.exchange(
                "http://localhost:" + port + "/users?email=" + email, HttpMethod.GET, entity, UserRestDTO.class);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expected.getEmail(), response.getBody().getEmail());
    }

    /**
     * US135
     * <p>
     * GIVEN an email not on the company's records WHEN we try the URI with that
     * email, THEN we get a HTTP request with 404 NOT FOUND and with an empty body.
     */
    @Test
    public void testListUsersByEmailFailureNotFound() {

        // Given
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsernameOrEmail("manel@jj.com");
        loginRequest.setPassword("12345");
        ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
                loginRequest, JwtAuthenticationResponse.class);

        jwt = responseEntity.getBody();
        String email = "jorgeandre@swww.portf";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<UserRegistryRestDTO> response = restTemplate.exchange(
                "http://localhost:" + port + "/users?email=" + email, HttpMethod.GET, entity,
                UserRegistryRestDTO.class);

        // Then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    /**
     * US101
     * <p>
     * GIVEN An user that not exist on application WHEN Add data to user above to do
     * registration and add user on application THEN The new user was added with
     * success
     */
    @Test
    public void testAddUserSuccess() throws AddressException {
        // GIVEN
        userIdVO = UserIdVO.create("luis@switch.com");
        assertFalse(userRepository.existsById(userIdVO));

        // WHEN

        UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
        userDTO.setName("Luis");
        userDTO.setEmail("luis@switch.com");
        userDTO.setPhone("123456789");
        userDTO.setTin("123456789");
        userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
        userDTO.setAddressId("1");
        userDTO.setStreet("Rua sem saida");
        userDTO.setPostalCode("3700");
        userDTO.setCity("Porto");
        userDTO.setCountry("Portugal");
        userDTO.setPassword("12345");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<?> entity = new HttpEntity<Object>(userDTO, headers);

        ResponseEntity<UserRegistryRestDTO> response = restTemplate.exchange("/users", HttpMethod.POST, entity,
                UserRegistryRestDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody().equals(userDTO));
        assertTrue(userRepository.existsById(userIdVO));

    }

    /**
     * US101
     * <p>
     * GIVEN An user that exist on application WHEN Add data to user above to do
     * registration and add user on application THEN The new user wasn't added
     * because already exist on application
     */
    @Test
    public void testAddUserFail() throws AddressException {
        // GIVEN
        userIdVO = UserIdVO.create("asdrubal@switch.com");
        assertTrue(userRepository.existsById(userIdVO));

        // WHEN

        UserRegistryRestDTO userDTO = new UserRegistryRestDTO();
        userDTO.setName("Asdrubal");
        userDTO.setEmail("asdrubal@switch.com");
        userDTO.setPhone("123456789");
        userDTO.setTin("123456789");
        userDTO.setBirthDate(LocalDate.of(1977, 3, 5));
        userDTO.setAddressId("1");
        userDTO.setStreet("Rua sem saida");
        userDTO.setPostalCode("3700");
        userDTO.setCity("Porto");
        userDTO.setCountry("Portugal");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + jwt.getAccessToken());
        HttpEntity<?> entity = new HttpEntity<Object>(userDTO, headers);

        ResponseEntity<UserRegistryRestDTO> response = restTemplate.exchange("/users", HttpMethod.POST, entity,
                UserRegistryRestDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertNull(response.getBody());
    }
}
