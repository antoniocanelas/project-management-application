package project.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.mail.internet.AddressException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import project.dto.rest.LoadUserDataDTO;
import project.dto.rest.UserRestDTO;
import project.jparepositories.RoleRepository;
import project.jparepositories.UserRepository;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.ApiAuthResponse;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class LoadDataRESTControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	UserRepository userRepository;
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	UserService userService;

	User user1;
	private JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}

		userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");

		String pass = passwordEncoder.encode("12345");

		user1 = userService.searchUserByEmail("manel@jj.com");
		user1.setPassword(pass);
		user1.setRoles(Collections.singleton(rolesAdmin));

		user1.setActive();

		userService.updateUser(user1);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manel@jj.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	/**
	 * Given : a filename with JSON input, we extract the value of inputFile field,
	 * When : load the loadUserDataFromXMLFile method from this URL, Then : we
	 * receive a list of users from that file that match expected List and a CREATED
	 * message.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadUsersFromFileSuccess() throws JSONException {

		// Given
		UserRestDTO cortezDTO = new UserRestDTO();
		cortezDTO.setEmail("cortez@mymail.com");
		UserRestDTO canelasDTO = new UserRestDTO();
		canelasDTO.setEmail("canelas@mymail.com");

		List<UserRestDTO> expected = new ArrayList<>();
		expected.add(canelasDTO);
		expected.add(cortezDTO);

		String filename = "{\n " + "\t\"filename\" : \"Utilizador.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/f718f15ca87b15b1c8041878528e9cb1/raw/86d749aad69494a0973af8e88653be81f415d6f0/testData.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange("http://localhost:" + port + "/users/populate",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<UserRestDTO>>() {
				});
		// Then
		assertEquals(HttpStatus.CREATED, result.getStatusCode());
		List<String> usersEmail = new ArrayList<>();
		;
		for (UserRestDTO user : result.getBody()) {
			usersEmail.add(user.getEmail());
		}
		for (UserRestDTO userD : expected) {
			assertTrue(usersEmail.contains(userD.getEmail()));
		}
	}

	/**
	 * Given : a filename with JSON input, we extract the value of inputFile field,
	 * When : we try to load a file with a wrong name
	 * Then: we get a status of Bad_request and a null body
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadUsersFromFileFailureWrongName() throws JSONException {

		// Given
		UserRestDTO cortezDTO = new UserRestDTO();
		cortezDTO.setEmail("cortez@mymail.com");
		UserRestDTO canelasDTO = new UserRestDTO();
		canelasDTO.setEmail("canelas@mymail.com");

		List<UserRestDTO> expected = new ArrayList<>();
		expected.add(canelasDTO);
		expected.add(cortezDTO);

		String filename = "{\n " + "\t\"filename\" : \"ProjetoTeste.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/111a9b8e845aceceb253b460cdb2f379/raw/292c40f9bffc746e1e085b324d262ea195ebfd88/ProjetoTeste.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange("http://localhost:" + port + "/users/populate",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<UserRestDTO>>() {
				});
		// Then
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
		assertNull(result.getBody());
	}

	/**
	 * Given : a filename with JSON input, we extract the value of inputFile field,
	 * When : load the loadProjectDataFromXMLFile method from this URL, Then : we
	 * receive a project from that file and a CREATED Status
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadProjectsFromFileSuccess() throws JSONException {

		// Given
		// Loading users file first
		String filename = "{\n " + "\t\"filename\" : \"UtilizadorTeste.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/35a845f6b524dda3f104636e0ef37a9f/raw/315155051618b656ebc44f5a78057ce56710849c/UtilizadorTeste.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		restTemplate.exchange("http://localhost:" + port + "/users/populate", HttpMethod.POST, requestEntity,
				new ParameterizedTypeReference<List<UserRestDTO>>() {
				});

		// loading of projects file.
		filename = "{\n " + "\t\"filename\" : \"ProjetoTests.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/111a9b8e845aceceb253b460cdb2f379/raw/292c40f9bffc746e1e085b324d262ea195ebfd88/ProjetoTeste.xml\"\n"
				+ "}";
		obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		requestEntity = new HttpEntity<>(filename, headers);

		// When
		ResponseEntity<ApiAuthResponse> result = restTemplate.exchange(
				"http://localhost:" + port + "/projects/populate", HttpMethod.POST, requestEntity,
				ApiAuthResponse.class);
		// Then
		assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}

	/**
	 * Given : a filename with JSON input, we extract the value of inputFile field,
	 * When : load the loadProjectDataFromXMLFile method from this URL and not all users are in the DB,
	 *  Then :we get a Status of Bad_Request.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadProjectsFromFileFailureNoUsersUploadedFirst() throws Exception {

		// Given
		String filename = "{\n " + "\t\"filename\" : \"ProjetoTesteNoUsers.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/cf63735b2c9c5c6ae515a55ac36803f3/raw/ddda06a77e68f433f01ffd34d0f50ada6853fad7/ProjetoTesteNoUsers.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		// When
		ResponseEntity<ApiAuthResponse> result = restTemplate.exchange(
				"http://localhost:" + port + "/projects/populate", HttpMethod.POST, requestEntity,
				ApiAuthResponse.class);
		// Then
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}

	/**
	 * Given : a filename with JSON input, we extract the value of inputFile field,
	 * When : we try to load a file with a wrong name
	 * Then: we get a status of Bad_request and a null body
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadProjectsFromFileFailureWrongName() throws JSONException {

		// Given
		String filename = "{\n " + "\t\"filename\" : \"WrongName.xml\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/111a9b8e845aceceb253b460cdb2f379/raw/292c40f9bffc746e1e085b324d262ea195ebfd88/ProjetoTeste.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		// When
		ResponseEntity<ApiAuthResponse> result = restTemplate.exchange(
				"http://localhost:" + port + "/projects/populate", HttpMethod.POST, requestEntity,
				ApiAuthResponse.class);
		// Then
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}

	/**
	 * Given : a filename with wrong JSON input, we extract the value of inputFile
	 * field, which is null, When : load the loadUserDataFromXMLFile method from
	 * this URL, Then : receive a NOT_FOUND message and an empty body.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadUsersFromFileFailureFilenameNotFound() throws JSONException {

		// Given
		String filename = "{\n " + "\t\"filename\" : \"\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/35a845f6b524dda3f104636e0ef37a9f/raw/315155051618b656ebc44f5a78057ce56710849c/UtilizadorTeste.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange("http://localhost:" + port + "/users/populate",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<UserRestDTO>>() {
				});
		// Then
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
	}
	
	/**
	 * Given : a filename with wrong JSON input, we extract the value of inputFile
	 * field, which is null, When : load the loadProjectDataFromXMLFile method from
	 * this URL, Then : receive a NOT_FOUND message and an empty body.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLoadProjectsFromFileFailureFilenameNotFound() throws JSONException {

		// Given
		String filename = "{\n " + "\t\"filename\" : \"\"\n" + ", "
				+ "\t\"path\" : \"https://gist.githubusercontent.com/SergioValente/35a845f6b524dda3f104636e0ef37a9f/raw/315155051618b656ebc44f5a78057ce56710849c/UtilizadorTeste.xml\"\n"
				+ "}";
		LoadUserDataDTO loadUserDataDTO = new LoadUserDataDTO();
		JSONObject obj = new JSONObject(filename);
		loadUserDataDTO.setFileName(obj.getString("filename"));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Object> requestEntity = new HttpEntity<>(filename, headers);

		ResponseEntity<List<UserRestDTO>> result = restTemplate.exchange("http://localhost:" + port + "/projects/populate",
				HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<UserRestDTO>>() {
				});
		// Then
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
	}

}
