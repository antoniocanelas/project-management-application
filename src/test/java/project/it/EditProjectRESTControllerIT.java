package project.it;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import project.dto.rest.SetProjectManagerDTO;
import project.model.UserService;
import project.jparepositories.ProjectRepository;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectUserService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.jparepositories.RoleRepository;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class EditProjectRESTControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	UserService userService;

	@Autowired
	ProjectService projectService;

	@Autowired
	ProjectUserService projectUserService;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	RoleRepository roleRepository;

	private User user1, user2, director;
	private ProjectCollaborator projectCollaborator2, projectCollaborator1;
	private Project project;
	private LocalDate birth;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		birth = LocalDate.of(1999, 11, 11);
		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		director = userService.searchUserByEmail("manuel@gmail.com");
			
		String pass = passwordEncoder.encode("12345");

		user1.setPassword(pass);
		user2.setPassword(pass);
		director.setPassword(pass);

		user1.setRoles(Collections.singleton(rolesCollaborator));
		user2.setRoles(Collections.singleton(rolesCollaborator));
		director.setRoles(Collections.singleton(rolesDirector));

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(director);

		projectService.addProject("1", "Project 1");
		project = projectService.getProjectByID("1");

		projectCollaborator1 = project.findProjectCollaborator(user1);
		projectCollaborator2 = project.findProjectCollaborator(user2);

		SetProjectManagerDTO originalProjectManager = new SetProjectManagerDTO();
		String firstProjectManagerEmail = user1.getEmail();
		String projectId = project.getId();
		originalProjectManager.setEmail(firstProjectManagerEmail);
		originalProjectManager.setProjectId(projectId);

		projectUserService.setProjectManager(originalProjectManager);
		projectRepository.getOneByCode(projectId).addProjectCollaborator(projectCollaborator2);
		projectRepository.getOneByCode(projectId).addProjectCollaborator(projectCollaborator1);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("manuel@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	/**
	 * Given : Set a new project manager from certain email included in project. At
	 * this point, we have Asdrubal as PM, When : run SetProjectManager method
	 * filled with one project and the new project manager email, Then : receive a
	 * httpStatus message of ACCEPTED and Joaquim is now the new PM.
	 */
	@Test
	public void setProjectManagerTestSuccess() {
		// Given
		assertEquals(user1.getEmail(), projectRepository.getOneByCode("1").getProjectManager().getUser().getEmail());

		// When
		SetProjectManagerDTO setProjectManagerDTO = new SetProjectManagerDTO();
		setProjectManagerDTO.setEmail(user2.getEmail());
		setProjectManagerDTO.setProjectId(project.getId());
		String projectId = setProjectManagerDTO.getProjectId();

		String url = "http://localhost:" + port + "/projects/" + projectId + "/project-manager";
		String requestBody = "{\"email\":\"" + setProjectManagerDTO.getEmail() + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, projectId);
		String responseExpected = "{\"projectId\":\"" + projectId + "\"" + ",\"email\":\""
				+ setProjectManagerDTO.getEmail() + "\"}";

		// Then
		assertEquals(user2.getEmail(), projectRepository.getOneByCode("1").getProjectManager().getUser().getEmail());
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
		assertEquals(responseExpected, response.getBody());
	}

	/**
	 * Given : Set a new project manager from certain email included in project. At
	 * this point, we have Asdrubal as PM, When : run SetProjectManager method
	 * filled with one project and the new project manager email (not included in
	 * project), Then : receive a httpStatus message of BAD_REQUEST and Asdrubal
	 * remains the PM.
	 */
	@Test
	public void setProjectManagerTestFail() {

		// Given
		assertEquals(user1.getEmail(), projectRepository.getOneByCode("1").getProjectManager().getUser().getEmail());

		// When
		SetProjectManagerDTO setProjectManagerDTO = new SetProjectManagerDTO();
		String wrongMail = "wrong@mymail.com";
		setProjectManagerDTO.setEmail(wrongMail);
		setProjectManagerDTO.setProjectId(project.getId());
		String projectId = setProjectManagerDTO.getProjectId();

		String url = "http://localhost:" + port + "/projects/" + projectId + "/project-manager";
		String requestBody = "{\"email\":\"" + setProjectManagerDTO.getEmail() + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, projectId);

		// Then
		assertEquals(user1.getEmail(), projectRepository.getOneByCode("1").getProjectManager().getUser().getEmail());
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertNull(response.getBody());
	}

	@Test
	public void testSetProjectCostOptionsSuccess() {

		String projectId = project.getId();
		List<String> newCostOptions = new ArrayList<>();
		newCostOptions.add("FirstTimePeriodCost");
		newCostOptions.add("AverageCost");

		String url = "http://localhost:" + port + "/projects/" + projectId + "/costOptions";
		String requestBody = "{\"costCalculationOptions\":[\"" + newCostOptions.get(0) + "\"" + ",\""
				+ newCostOptions.get(1) + "\"]}";
		String responseExpected = "{\n" + "  \"projectId\" : \"" + project.getId() + "\""
				+ ",\n  \"costCalculationOptions\" : [ \"" + newCostOptions.get(0) + "\"" + ", \""
				+ newCostOptions.get(1) + "\" ]\n}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, projectId);

		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
//		assertEquals(responseExpected, response.getBody());
	}

	@Test
	public void testSetProjectCostOptionsBadRequest() {

		String projectId = project.getId();
		List<String> newCostOptions = new ArrayList<>();
		newCostOptions.add("FirstTimePeriodCost");
		newCostOptions.add("AverageCost");

		String url = "http://localhost:" + port + "/projects/" + projectId + "/costOptions";
		String requestBody = "{\"cost\":[\"" + newCostOptions.get(0) + "\"]}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, projectId);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

	}

	@Test
	public void testSetProjectCostOptionsNotFound() {

		String projectId = "333";
		List<String> newCostOptions = new ArrayList<>();
		newCostOptions.add("FirstTimePeriodCost");
		newCostOptions.add("AverageCost");

		String url = "http://localhost:" + port + "/projects/" + projectId + "/costOptions";
		String requestBody = "{\"cost\":[\"" + newCostOptions.get(0) + "\"]}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, projectId);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

	}

	@Test
	public void testGetProjectCostOptionsNotFound() {

		String projectId = "333";

		String url = "http://localhost:" + port + "/projects/" + projectId + "/costOptions";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, projectId);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

	}

	@Test
	public void testGetProjectCostOptionsSuccess() {

		String projectId = project.getId();

		String url = "http://localhost:" + port + "/projects/" + projectId + "/costOptions";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, projectId);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
