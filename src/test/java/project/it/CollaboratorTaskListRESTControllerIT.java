package project.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import project.controllers.rest.CollaboratorTaskListRESTController;
import project.dto.rest.TaskRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CollaboratorTaskListRESTControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	CollaboratorTaskListRESTController collabTaskListRESTController;

	@LocalServerPort
	int port;

	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	UserService userService;

	@Autowired
	ProjectService projectService;

	@Autowired
	ProjectCollaboratorService projectCollaboratorService;

	@Autowired
	TaskService taskService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	List<User> expectedUser;

	LocalDate birth;
	User user1;
	User user2;
	User user3;
	User user4;
	ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

	Project project1;
	Task task1, task2, task3, task4, task5, task6;

	LocalDateTime d1;
	LocalDateTime d2;
	LocalDateTime d3;
	LocalDateTime d4;
	int y1;
	Month m1;

	private JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		collabTaskListRESTController = new CollaboratorTaskListRESTController(userService, projectService, taskService);

		birth = LocalDate.of(1999, 11, 11);
		m1 = LocalDateTime.now().getMonth();
		y1 = LocalDateTime.now().getYear();

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		String pass = passwordEncoder.encode("12345");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		user1.setPassword(pass);
		user2.setPassword(pass);
		user3.setPassword(pass);
		user4.setPassword(pass);

		user1.setRoles(Collections.singleton(rolesCollaborator));
		user2.setRoles(Collections.singleton(rolesCollaborator));
		user3.setRoles(Collections.singleton(rolesCollaborator));
		user4.setRoles(Collections.singleton(rolesCollaborator));

		// user2.setProfileCollaborator();
		// user3.setProfileCollaborator();
		// user4.setProfileCollaborator();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(user3);
		userService.updateUser(user4);

		projectService.addProject("1", "Project 1");
		project1 = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project1, user1);
		projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
		projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
		projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

		projectManager = project1.findProjectCollaborator(user1);
		projectCollaborator2 = project1.findProjectCollaborator(user2);
		projectCollaborator3 = project1.findProjectCollaborator(user3);
		projectCollaborator4 = project1.findProjectCollaborator(user4);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
		taskService.addTask(project1, "task2", "task2", d1, d2.minusDays(2), 1.2, 1.3);
		taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);

		task1 = project1.findTaskByTitle("task1");
		task2 = project1.findTaskByTitle("task2");
		task3 = project1.findTaskByTitle("task3");
		task4 = project1.findTaskByTitle("task4");
		task5 = project1.findTaskByTitle("task5");
		task6 = project1.findTaskByTitle("task6");

		task1.addProjectCollaborator(projectCollaborator2);
		task2.addProjectCollaborator(projectCollaborator2);
		task3.addProjectCollaborator(projectCollaborator2);
		task3.addProjectCollaborator(projectCollaborator4);
		task5.addProjectCollaborator(projectCollaborator2);
		task5.addProjectCollaborator(projectCollaborator4);

		task5.setEffectiveStartDate(d1.plusDays(20));
		task5.setEffectiveDateOfConclusion(d2.minusMonths(4));
		task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
		task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

		projectService.updateProject(project1);
		taskService.updateTask(task1);
		taskService.updateTask(task2);
		taskService.updateTask(task3);
		taskService.updateTask(task4);
		taskService.updateTask(task5);
		taskService.updateTask(task6);

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("joaquim@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	@After
	public void tearDown() {
		taskService.deleteAll();

	}

	/**
	 * Given a collaborator with pending tasks, 
	 * When he asks for his pending tasks,
	 * Then he receives status ok and a list with his pending tasks.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testURI() throws Exception {

		// Given
		// Login with user4
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("liliana@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();

		// build taskDTO object from task5
		TaskRestDTO task3DTO = task3.toDTO();

		// convert it to json and get a string representation of it
		String representationOfTaskDTOObject = convertJsonToString(task3DTO);

		// When
		// perform get method from URI and get response as string
		String testURI = "/users/" + user4.getEmail() + "/tasks/pending";
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(testURI);
		builder.header("Authorization", "Bearer " + jwt.getAccessToken());

		MvcResult result = this.mockMvc.perform(builder).andExpect(status().isOk()).andReturn();
		String getResult = result.getResponse().getContentAsString();

		// Then
		String[] resultWithoutLinks = getResult.split("links");
		String[] representationOfTaskDTOObjectWithoutLinks = representationOfTaskDTOObject.split("links");

		assertEquals(representationOfTaskDTOObjectWithoutLinks[0], resultWithoutLinks[0]);
	}

	private String convertJsonToString(TaskRestDTO taskDTO) {

		String convertedObject = "";
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		try {
			convertedObject = "[" + mapper.writeValueAsString(taskDTO) + "]";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertedObject;

	}

	/**
	 * GIVEN: that the application context loads successfully, it is creating the
	 * authcontroller, that project has the given project collaborator and that a
	 * task is completed WHEN: a http GET request is made to
	 * http://localhost:port/users/{id}/tasks/completed THEN: return a http status
	 * code of 200 (OK) and the expected completed tasks
	 */
	@Test
	public void testGetCollaboratorCompletedTasksSuccess() {

		// GIVEN
		assertThat(collabTaskListRESTController).isNotNull();
		assertTrue(project1.hasActiveProjectCollaborator(projectCollaborator2));
		String userEmail = "joaquim@gmail.com";

		// WHEN
		TaskRestDTO taskDTO1 = task5.toDTO();
		List<TaskRestDTO> expectedList = new ArrayList<>();
		expectedList.add(taskDTO1);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<List<TaskRestDTO>> result = restTemplate.exchange(
				"http://localhost:" + port + "/users/" + userEmail + "/tasks/completed", HttpMethod.GET, entity,
				new ParameterizedTypeReference<List<TaskRestDTO>>() {
				});
		// THEN
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(expectedList.get(0).getTaskId(), result.getBody().get(0).getTaskId());
		assertFalse(result.getBody().get(0).getLinks().isEmpty());
	}

	/**
	 * GIVEN: that the application context loads successfully and that it is
	 * creating the authcontroller WHEN: a http GET request is made to
	 * http://localhost:port/users/1/tasks/completed THEN: return the expected
	 * content-type header field
	 */
	@Test
	public void testGetCollaboratorCompletedTasksContentTypeSuccess() {

		// GIVEN
		assertThat(collabTaskListRESTController).isNotNull();
		String userEmail = "joaquim@gmail.com";

		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<Double> entity = new HttpEntity<>(null, headers);

		ResponseEntity<String> responseEntity = restTemplate.exchange(
				"http://localhost:" + port + "/users/" + userEmail + "/tasks/completed", HttpMethod.GET, entity,
				String.class);
		String result = responseEntity.getHeaders().getContentType().toString();

		// THEN
		String expected = "application/json;charset=UTF-8";
		assertThat(result).isEqualTo(expected);
	}

	/**
	 * GIVEN: that the application context loads successfully and that it is
	 * creating the authcontroller WHEN: a http GET request is made to
	 * http://localhost:port/users/{id}/tasks/completed with an empty path parameter
	 * id THEN: return a http status code 404 (NOT_FOUND) because spring application
	 * could not map the url to any method
	 */
	@Test
	public void testGetCollaboratorCompletedTasksStatusCodeFail() {

		// GIVEN
		assertThat(collabTaskListRESTController).isNotNull();

		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		String userEmail = "";
		ResponseEntity<String> responseEntity = restTemplate.exchange(
				"http://localhost:" + port + "/users/" + userEmail + "/tasks/completed", HttpMethod.GET, entity,
				String.class);

		// THEN
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	}
}
