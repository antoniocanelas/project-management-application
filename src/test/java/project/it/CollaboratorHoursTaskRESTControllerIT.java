package project.it;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;

import javax.mail.internet.AddressException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import project.controllers.rest.CollaboratorHoursTaskControllerREST;
import project.dto.rest.TaskHoursRestDTO;
import project.jparepositories.RoleRepository;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.payload.JwtAuthenticationResponse;
import project.payload.LoginRequest;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CollaboratorHoursTaskRESTControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	CollaboratorHoursTaskControllerREST collaboratorHoursTaskControllerREST;
	@Autowired
	UserService userService;
	@Autowired
	ProjectService projectService;
	@Autowired
	TaskService taskService;
	@Autowired
	ProjectCollaboratorService projectCollaboratorService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	User userProjectManager;
	User userProjectCollaborator;
	User userProjectCollaborator2;
	String userId;
	String userId2;
	ProjectCollaborator projectCollaborator, projectManager;
	ProjectCollaborator projectCollaborator2;
	Project project;
	Project project2;
	Task t1;
	Task t2;
	LocalDate birth;
	LocalDateTime d1;
	LocalDateTime d2;
	LocalDateTime d9;
	LocalDateTime d10;
	int y1;
	Month m1;
	String expectedMessage, message, taskId;

	private JwtAuthenticationResponse jwt;

	@Before
	public void setUp() throws AddressException {

		Role rolesAdmin = roleRepository.getOneByName(RoleName.ROLE_ADMINISTRATOR);
		Role rolesUser = roleRepository.getOneByName(RoleName.ROLE_REGISTEREDUSER);
		Role rolesDirector = roleRepository.getOneByName(RoleName.ROLE_DIRECTOR);
		Role rolesCollaborator = roleRepository.getOneByName(RoleName.ROLE_COLLABORATOR);

		if (!roleRepository.existsByName(RoleName.ROLE_ADMINISTRATOR)) {
			rolesAdmin = new Role(RoleName.ROLE_ADMINISTRATOR);
			roleRepository.save(rolesAdmin);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_REGISTEREDUSER)) {
			rolesUser = new Role(RoleName.ROLE_REGISTEREDUSER);
			roleRepository.save(rolesUser);
		}

		if (!roleRepository.existsByName(RoleName.ROLE_DIRECTOR)) {
			rolesDirector = new Role(RoleName.ROLE_DIRECTOR);
			roleRepository.save(rolesDirector);
		}
		if (!roleRepository.existsByName(RoleName.ROLE_COLLABORATOR)) {
			rolesCollaborator = new Role(RoleName.ROLE_COLLABORATOR);
			roleRepository.save(rolesCollaborator);
		}

		collaboratorHoursTaskControllerREST = new CollaboratorHoursTaskControllerREST(userService, projectService);
		birth = LocalDate.of(1999, 11, 11);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		String pass = passwordEncoder.encode("12345");

		userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
		userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
		userProjectCollaborator2 = userService.searchUserByEmail("manuel@gmail.com");

		userProjectManager.setPassword(pass);
		userProjectCollaborator.setPassword(pass);
		userProjectCollaborator2.setPassword(pass);

		userProjectManager.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator.setRoles(Collections.singleton(rolesCollaborator));
		userProjectCollaborator2.setRoles(Collections.singleton(rolesCollaborator));

		userId = userProjectCollaborator.getEmail();
		userId2 = userProjectCollaborator2.getEmail();

		userService.updateUser(userProjectManager);
		userService.updateUser(userProjectCollaborator);
		userService.updateUser(userProjectCollaborator2);

		projectService.addProject("1", "Project 1");
		project = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project, userProjectManager);
		projectService.updateProject(project);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator2, 2);

		projectService.addProject("2", "Project 2");
		project2 = projectService.getProjectByID("2");
		projectCollaboratorService.setProjectManager(project2, userProjectManager);
		projectService.updateProject(project2);
		projectCollaboratorService.addProjectCollaborator(project2, userProjectCollaborator2, 2);

		projectManager = project.findProjectCollaborator(userProjectManager);
		projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
		projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);
		y1 = LocalDateTime.now().getYear();
		m1 = LocalDateTime.now().getMonth();
		d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
		d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);

		taskService.addTask(project, "task1", "task", d1, d2, 1.2, 1.3);
		taskService.addTask(project, "task2", "task", d1, d2, 1.2, 1.3);
		t1 = taskService.findTaskByID("1-1");
		t2 = taskService.findTaskByID("1-2");

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsernameOrEmail("joaquim@gmail.com");
		loginRequest.setPassword("12345");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = restTemplate.postForEntity("/api/auth/signin",
				loginRequest, JwtAuthenticationResponse.class);

		jwt = responseEntity.getBody();
	}

	@After
	public void tearDown() {
		taskService.deleteAll();

	}

	/**
	 * US215
	 * 
	 * GIVEN A project collaborator associated to two tasks on project 1. This tasks
	 * have reports made by this project collaborator and are completed. WHEN We ask
	 * for the number of hours spent by this project collaborator on completed tasks
	 * last month. THEN We get an HTTP status of OK and the number of hours .
	 */
	@Test
	public void testGetUsersHoursCompletedTasksLastMonth() {
		// GIVEN

		t1.addProjectCollaborator(projectCollaborator);
		t2.addProjectCollaborator(projectCollaborator);

		TaskCollaboratorRegistry col1TaskRegistry = t1
				.getTaskCollaboratorRegistryByID(t1.getId() + "-" + projectCollaborator.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		TaskCollaboratorRegistry col2TaskRegistry = t2
				.getTaskCollaboratorRegistryByID(t2.getId() + "-" + projectCollaborator.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		t1.addReport(projectCollaborator, 5, d9, d9.plusDays(5));
		t2.addReport(projectCollaborator, 2, d9, d9.plusDays(5));
		t2.setTaskCompleted();
		t2.setEffectiveDateOfConclusion(d10);
		t1.setTaskCompleted();
		t1.setEffectiveDateOfConclusion(d9);
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		
		TaskHoursRestDTO expected = new TaskHoursRestDTO();
		expected.setHours(7.0);
		
		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<TaskHoursRestDTO> responseGet = restTemplate.exchange("/users/" + userId + "/hours", HttpMethod.GET,
				entity, new ParameterizedTypeReference<TaskHoursRestDTO>() {
				});
		// THEN
		assertEquals(HttpStatus.OK, responseGet.getStatusCode());
		assertEquals(expected.getHours(), responseGet.getBody().getHours(), 0.001);
	}

	@Test
	/**
	 * US215
	 * 
	 * GIVEN A project collaborator not associated to two tasks on project 2. WHEN
	 * We ask for the number of hours spent by this project collaborator on
	 * completed tasks last month. THEN We get an HTTP status of OK and the number
	 * of hours .
	 */
	public void testGetUsersHoursCompletedTasksLastMonth2() {
		// GIVEN

		project.getProjectCollaboratorList().add(projectCollaborator2);
		project2.getProjectCollaboratorList().add(projectCollaborator2);

		t1.addReport(projectCollaborator, 5, d9, d9.plusDays(5));
		t2.addReport(projectCollaborator, 2, d9, d9.plusDays(5));
		t2.setTaskCompleted();
		t2.setEffectiveDateOfConclusion(d10);
		t1.setTaskCompleted();
		t1.setEffectiveDateOfConclusion(d9);
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		
		TaskHoursRestDTO expected = new TaskHoursRestDTO();
		expected.setHours(0.0);

		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<TaskHoursRestDTO> responseGet = restTemplate.exchange("/users/" + userId2 + "/hours", HttpMethod.GET,
				entity, new ParameterizedTypeReference<TaskHoursRestDTO>() {
				});

		// THEN
		assertEquals(HttpStatus.OK, responseGet.getStatusCode());
		assertEquals(expected.getHours(), responseGet.getBody().getHours(), 0.001);
	}

	@Test
	/**
	 * US215
	 * 
	 * GIVEN A project collaborator null. WHEN We ask for the number of hours spent
	 * by this project collaborator on completed tasks last month. THEN We get an
	 * HTTP status of BAD REQUEST.
	 */
	public void testGetUsersHoursCompletedTasksLastMonthUserNull() {
		// WHEN
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Bearer " + jwt.getAccessToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<TaskHoursRestDTO> responseGet = restTemplate.exchange("/users/" + null + "/hours", HttpMethod.GET, entity,
				new ParameterizedTypeReference<TaskHoursRestDTO>() {
				});

		// THEN

		assertThat(responseGet.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(responseGet.getBody() == null);
	}
}
