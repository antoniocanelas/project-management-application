package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.ProjectDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;

public class GetProjectManagerProjectsRESTControllerTest {

	
	
	
	UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskProjectCollaboratorService taskProjectCollaboratorService;
    

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    GetProjectManagerProjectsRESTController getProjectManagerProjectsRESTController;
 
    

    private LocalDate birth;
    private User user1;
    private User projectManager;
    private ProjectCollaborator projectCollaborator1;
    private Project project1;
    private Project project2;



    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        getProjectManagerProjectsRESTController = new GetProjectManagerProjectsRESTController(userService,projectService,taskProjectCollaboratorService);

        birth = LocalDate.of(1999, 11, 11);
        userService.addUser("Diogo", "91739812379", "diogo@switch.com", "132456765", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        userService.addUser("Lisa", "666 555 5556", "lisa@switch.com", "5656546456", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        
        user1 = userService.searchUserByEmail("diogo@switch.com");
        user1.setProfileCollaborator();

        projectManager = userService.searchUserByEmail("lisa@switch.com");
        projectManager.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(projectManager);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, projectManager);
        projectCollaboratorService.addProjectCollaborator(project1, user1, 3);
        
        projectService.addProject("2", "Project 2");
        project2 = projectService.getProjectByID("2");
        projectCollaboratorService.setProjectManager(project2, projectManager);
        projectCollaboratorService.addProjectCollaborator(project2, user1, 3);
        
        
        projectCollaborator1 = projectCollaboratorService.getProjectCollaborator(project1.getId(), "diogo@switch.com");
        projectCollaborator1 = projectCollaboratorService.getProjectCollaborator(project2.getId(), "diogo@switch.com");
        projectCollaborator1.setId(10);

        projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);

        projectService.updateProject(project1);
        projectService.updateProject(project2);

    }

    @Test
    public void getProjectManagerProjectsTest() {

        //given
        List<ProjectDTO> expected = new ArrayList<>();
        expected.add(project1.toDTO());
        expected.add(project2.toDTO());

        //when
       List<ProjectDTO> result = getProjectManagerProjectsRESTController.getProjectManagerProjects("lisa@switch.com");

        //then
        assertEquals(expected, result);

    }
    
    
    @Test
    public void getProjectManagerProjectsTestNoProjects() {

        //given
        List<ProjectDTO> expected = new ArrayList<>();
       

        //when
       List<ProjectDTO> result = getProjectManagerProjectsRESTController.getProjectManagerProjects("diogo@switch.com");

        //then
        assertEquals(expected, result);

    }
    
    
    @Test
    public void getProjectManagerProjectsAfterSetTest() {

        //given
    	projectCollaboratorService.setProjectManager(project1,user1);
        List<ProjectDTO> expected = new ArrayList<>();
        expected.add(project1.toDTO());

        //when
       List<ProjectDTO> result = getProjectManagerProjectsRESTController.getProjectManagerProjects("diogo@switch.com");

        //then
        assertEquals(expected, result);

    }
	

}
