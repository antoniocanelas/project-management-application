package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import project.dto.rest.ProjectCostOptionsDTO;
import project.dto.rest.SetProjectManagerDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectUserService;
import project.services.ProjectCollaboratorService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EditProjectControllerRESTTest {

    EditProjectControllerREST editProjectControllerREST;
    ProjectUserService projectUserService;
    UserService userService;
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;

    SetProjectManagerDTO setProjectManagerDTO;
    ProjectCostOptionsDTO setProjectCostOptionsDTO;
    User user;
    Project project;
    String name, phone, email, tin, id, street, postalCode, city, country, projectId, projectName;
    LocalDate birthDate;

    @Before
    public void setUp() throws AddressException {

        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass(); 
        projectService = new ProjectService(projectRepositoryClass);
        projectUserService = new ProjectUserService(projectRepositoryClass, userRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);
        userService = new UserService(userRepositoryClass);
        editProjectControllerREST = new EditProjectControllerREST(projectUserService, projectService);
        

        name = "Pedro";
        phone = "919999999";
        email = "pedro@mymail.com";
        tin = "123456";
        id = "1";
        street = "Rua";
        postalCode = "4444-444";
        city = "Porto";
        country = "Portugal";
        birthDate = LocalDate.of(1999, 11, 11);


        userService.addUser(name, phone, email, tin, birthDate, id, street, postalCode, city, country);
        user = userService.searchUserByEmail(email);
        user.setProfileCollaborator();
        userService.updateUser(user);

        projectId = "1";
        projectName = "Project 1";
        projectService.addProject(projectId, projectName);
        project = projectService.getProjectByID("1");
        projectService.updateProject(project);
        projectCollaboratorService.setProjectManager(project, user);
    }

    /**
     * Test method to change project manager.
     * Given : Set a new project manager from certain email included in project,
     * When : run SetProjectManager method filled with one project and the new project manager email,
     * Then : receive a httpStatus message of ACCEPTED.
     */
    @Test
    public void testSetProjectManagerSuccess() {

        // Given
        setProjectManagerDTO = new SetProjectManagerDTO();
        setProjectManagerDTO.setEmail(user.getEmail());

        // When
        HttpEntity<SetProjectManagerDTO> result = editProjectControllerREST.setProjectManager(project.getId(), setProjectManagerDTO);
        ResponseEntity<SetProjectManagerDTO> expected = new ResponseEntity<>(setProjectManagerDTO, HttpStatus.ACCEPTED);

        // Then
        assertEquals (expected, result);
    }

    /**
     * Test method to change project manager.
     * Given :  Set a new project manager from certain email not included in project,
     * When : run SetProjectManager method filled with one project and the new project manager email (not in project),
     * Then : receive a httpStatus message of BAD_REQUEST.
     */
    @Test
    public void testSetProjectManagerFail() {

        // Given
        setProjectManagerDTO = new SetProjectManagerDTO();
        String wrongEmail = "joaquim@mymail.com";
        setProjectManagerDTO.setEmail(wrongEmail);

        // When
        HttpEntity<SetProjectManagerDTO> result = editProjectControllerREST.setProjectManager(project.getId(), setProjectManagerDTO);
        ResponseEntity<SetProjectManagerDTO> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Then
        assertEquals(expected, result);
    }
    

    @Test
    public void testSetProjectCostOptionSuccess() {

        setProjectCostOptionsDTO = new ProjectCostOptionsDTO();
        List<String> costOptions = new ArrayList<>();
        costOptions.add("AverageCost");
        costOptions.add("FirstTimePeriodCost");
        costOptions.add("MaximumCost");
        setProjectCostOptionsDTO.setCostCalculationOptions(costOptions);

        HttpEntity<ProjectCostOptionsDTO> result = editProjectControllerREST.setCostOptions(project.getId(), setProjectCostOptionsDTO);
        setProjectCostOptionsDTO.setProjectId(project.getId());
        HttpEntity<ProjectCostOptionsDTO> expected = new ResponseEntity<>(setProjectCostOptionsDTO,HttpStatus.ACCEPTED);

        assertEquals(expected, result);
        assertEquals(expected.getBody(),setProjectCostOptionsDTO);
    }
    
    
    @Test
    public void testeSetProjectCostOptionsProjectNotFound() {

    	setProjectCostOptionsDTO = new ProjectCostOptionsDTO();

        HttpEntity<ProjectCostOptionsDTO> result = editProjectControllerREST.setCostOptions("1321", setProjectCostOptionsDTO);
        setProjectCostOptionsDTO.setProjectId("1321");
        HttpEntity<ProjectCostOptionsDTO> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        assertEquals(expected, result);
    }

    @Test
    public void testSetProjectCostOptionsFail() {

    	setProjectCostOptionsDTO = new ProjectCostOptionsDTO();

        HttpEntity<ProjectCostOptionsDTO> result = editProjectControllerREST.setCostOptions(project.getId(), setProjectCostOptionsDTO);
        setProjectCostOptionsDTO.setProjectId(project.getId());
        HttpEntity<ProjectCostOptionsDTO> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        assertEquals(expected, result);
    }
    
    @Test
    public void testListAvailableProjectCostOptionsStatusOk() {

    	setProjectCostOptionsDTO = new ProjectCostOptionsDTO();

        HttpEntity<ProjectCostOptionsDTO> result = editProjectControllerREST.listAvailableCostOptions(project.getId());
        setProjectCostOptionsDTO.setProjectId(project.getId());
        setProjectCostOptionsDTO.setCostCalculationOptions(project.getCostCalculationOptions());
        HttpEntity<ProjectCostOptionsDTO> expected = new ResponseEntity<>(setProjectCostOptionsDTO,HttpStatus.OK);

        assertEquals(expected, result);
    }
    
    @Test
    public void testListAvailableProjectCostOptionsStatusNotFound() {

    	setProjectCostOptionsDTO = new ProjectCostOptionsDTO();

        HttpEntity<ProjectCostOptionsDTO> result = editProjectControllerREST.listAvailableCostOptions("222");
        setProjectCostOptionsDTO.setProjectId("222");
        setProjectCostOptionsDTO.setCostCalculationOptions(project.getCostCalculationOptions());
        HttpEntity<ProjectCostOptionsDTO> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        assertEquals(expected, result);
    }
    
    
}
