package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.ReportRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CollaboratorReportsRestTest {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    TaskProjectCollaboratorService taskProjCollabReg;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    CollaboratorReportsRestController collaboratorReportsRest;

    private LocalDate birth;
    private User user1;
    private User user2;
    private User projectManager;
    private ProjectCollaborator projectCollaborator1;
    private Project project1;
    private LocalDateTime d1;
    private Task t1;
    private Task t2;
    private ProjectCollaboratorCostAndTimePeriod costAndPeriod;
    private Report report;
    private Report report2;


    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjCollabReg = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);

        collaboratorReportsRest = new CollaboratorReportsRestController(taskProjCollabReg);

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        birth = LocalDate.of(1999, 11, 11);
        userService.addUser("Diogo", "91739812379", "diogo@switch.com", "132456765", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        userService.addUser("Lisa", "666 555 5556", "lisa@switch.com", "5656546456", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        userService.addUser("Tiago", "666 555 5556", "tiago@switch.com", "5656546456", birth, "2", "Rua ", "4433",
                "cidade", "pais");
        user1 = userService.searchUserByEmail("diogo@switch.com");
        user1.setProfileCollaborator();
        user2 = userService.searchUserByEmail("tiago@switch.com");
        user2.setProfileCollaborator();

        projectManager = userService.searchUserByEmail("lisa@switch.com");
        projectManager.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(projectManager);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, projectManager);
        projectCollaboratorService.addProjectCollaborator(project1, user1, 3);
        projectCollaborator1 = projectCollaboratorService.getProjectCollaborator(project1.getId(), "diogo@switch.com");
        projectCollaborator1.setId(10);
        costAndPeriod = new ProjectCollaboratorCostAndTimePeriod(10);
        costAndPeriod.setStartDate(birth);
        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
        costAndTimePeriodList.add(costAndPeriod);
        projectCollaborator1.setCostAndTimePeriodList(costAndTimePeriodList);
        projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);
        taskService.addTask(project1, "Test 1");
        taskService.addTask(project1, "Test 2");
        t1 = project1.findTaskByTitle("Test 1");
        t2 = project1.findTaskByTitle("Test 2");

        t1.setPredictedDateOfStart(d1.plusDays(20));
        t1.setPredictedDateOfConclusion(d1.plusDays(40));
        t2.setPredictedDateOfStart(d1.plusDays(20));
        t2.setPredictedDateOfConclusion(d1.plusDays(40));

        t1.addProjectCollaborator(projectCollaborator1);
        t1.getLastTaskCollaboratorRegistryOf(projectCollaborator1)
                .setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(20));
        t2.addProjectCollaborator(projectCollaborator1);
        t2.getLastTaskCollaboratorRegistryOf(projectCollaborator1)
                .setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(20));



        t1.setEffectiveStartDate(LocalDateTime.now().minusMonths(6));

        t1.addReportRest(projectCollaborator1, 9, d1, d1.plusDays(5));
        report = t1.listAllReports().get(0);

        t2.addReportRest(projectCollaborator1, 10, d1, d1.plusDays(5));
        report2 = t2.listAllReports().get(0);

        taskService.updateTask(t1);
        taskService.updateTask(t2);

        projectService.updateProject(project1);

    }

    @Test
    public void getReportByTaskAndCollab() {

        //given
        String taskId = t1.getId();
        String collabEmail = projectCollaborator1.getUser().getEmail();
        List<ReportRestDTO> expected = new ArrayList<>();


        //when
        ResponseEntity<List<ReportRestDTO>> result = collaboratorReportsRest.getReportByTaskAndCollab(collabEmail,taskId);

        //then
        expected.add(report.toDTO());
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());

    }

    @Test
    public void getReportByCollab() {


        //given
        String collabEmail = projectCollaborator1.getUser().getEmail();
        List<ReportRestDTO> expected = new ArrayList<>();


        //when
        ResponseEntity<List<ReportRestDTO>> result = collaboratorReportsRest.getReportByCollab(collabEmail);
        //then
        expected.add(report.toDTO());
        expected.add(report2.toDTO());
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());
    }

}