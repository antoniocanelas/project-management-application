package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

public class ProjectManagerTaskRequestsControllerRESTTest {
	
	ProjectManagerTaskRequestsControllerREST projectManagerTaskRequestsControllerREST;
    TaskProjectCollaboratorService taskProjectCollaboratorService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    LocalDate birth;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    User userProjectManager;
    User userProjectCollaborator;
    ProjectCollaborator projectCollaborator, projectManager;
    Project project;
    Task t1;
    Task t2;
    LocalDateTime d1;
    LocalDateTime d2;
    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO;
    String expectedMessage, message, taskId1,taskId2;

	@Before
	public void setUp() throws AddressException {
		
		userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        ProjectCollaborator.setStartIdGenerator(1);
        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
        projectManagerTaskRequestsControllerREST = new ProjectManagerTaskRequestsControllerREST(projectService,taskProjectCollaboratorService,userService,taskService,projectCollaboratorService);
        birth = LocalDate.of(1999, 11, 11);
        taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");

        userProjectManager.setProfileCollaborator();
        userProjectCollaborator.setProfileCollaborator();

        userService.updateUser(userProjectManager);
        userService.updateUser(userProjectCollaborator);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

        projectManager = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectManager.getEmail());
        projectCollaborator = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectCollaborator.getEmail());

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project, "task2", "task2", d1.plusDays(2), d2.plusDays(5), 1.2, 1.3);
        t1 = taskService.findTaskByID("1-1");
        t2 = taskService.findTaskByID("1-2");
    
        taskId1 = t1.getId();
        taskId2 = t2.getId();
        
        
       
	}

	@Test
	public void testListAssignmentRequests() {
		//GIVEN 2 request from collaborator to task1 and task2
		List<TaskCollaboratorRegistryRESTDTO> assignmentList = new ArrayList<>();
		taskProjectCollaboratorRestDTO.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        taskProjectCollaboratorRestDTO.setTaskId(taskId2);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
		//WHEN
        HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> result = projectManagerTaskRequestsControllerREST.listAssignmentRequests("1");
        //THEN
        assignmentList = projectService.listAssignmentRequestsToDTO("1");
        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> expected = new ResponseEntity<>(assignmentList, HttpStatus.OK);
        assertEquals(result.getBody().size(), expected.getBody().size());
        assertEquals(result.getBody().get(0).getRegistryStatus(),expected.getBody().get(0).getRegistryStatus());
        assertEquals(result.getBody().get(1).getRegistryStatus(),expected.getBody().get(1).getRegistryStatus());
        
	}

	@Test
	public void testListRemovalRequests() {
		
		//GIVEN 2 request from collaborator to remove from task1 and task2
		List<TaskCollaboratorRegistryRESTDTO> removalList = new ArrayList<>();
		t1.addProjectCollaborator(projectCollaborator);
		t2.addProjectCollaborator(projectCollaborator);
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		assertEquals(t1.listActiveCollaborators().size(),1);
		assertEquals(t2.listActiveCollaborators().size(),1);
		//WHEN
		taskProjectCollaboratorRestDTO.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);
        taskProjectCollaboratorRestDTO.setTaskId(taskId2);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);
        HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> result = projectManagerTaskRequestsControllerREST.listRemovalRequests("1");
        //THEN
        removalList = projectService.listRemovalRequestsToDTO("1");
        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> expected = new ResponseEntity<>(removalList, HttpStatus.OK);
        assertEquals(result.getBody().size(), expected.getBody().size());
        assertEquals(result.getBody().get(0).getRegistryStatus(),expected.getBody().get(0).getRegistryStatus());
        assertEquals(result.getBody().get(1).getRegistryStatus(),expected.getBody().get(1).getRegistryStatus());	
			

	}

	@Test
	public void testListCompletedRequests() {
		//GIVEN 2 request from collaborator to task1 and task2
		t1.addProjectCollaborator(projectCollaborator);
		t2.addProjectCollaborator(projectCollaborator);
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		assertEquals(t1.listActiveCollaborators().size(),1);
		assertEquals(t2.listActiveCollaborators().size(),1);
		t1.getLastTaskCollaboratorRegistryOf(projectCollaborator).setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(2));
		t1.addReport(projectCollaborator, 2,LocalDateTime.now().minusDays(1), LocalDateTime.now().minusDays(1));
		assertEquals(t1.listAllReports().size(),1);
		t1.requestTaskCompleted(projectCollaborator);
		
		//WHEN
        HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> result = projectManagerTaskRequestsControllerREST.listCompletedRequests("1");
        //THEN
        assertEquals(result.getBody().size(),1);
//        assignmentList = projectService.listAssignmentRequestsToDTO("1");
//        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> expected = new ResponseEntity<>(assignmentList, HttpStatus.OK);
//        assertEquals(result.getBody().size(), expected.getBody().size());
//        assertEquals(result.getBody().get(0).getRegistryStatus(),expected.getBody().get(0).getRegistryStatus());
//        assertEquals(result.getBody().get(1).getRegistryStatus(),expected.getBody().get(1).getRegistryStatus());		
	}

	@Test
	public void testCancelAssignmentRequest() {
		//GIVEN 2 request from collaborator to task1 and task2
		List<TaskCollaboratorRegistryRESTDTO> assignmentList = new ArrayList<>();
		taskProjectCollaboratorRestDTO.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        taskProjectCollaboratorRestDTO.setTaskId(taskId2);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskCollaboratorRegistryRESTDTO.setTaskId(taskId1);
        assertEquals(projectManagerTaskRequestsControllerREST.listAssignmentRequests("1").getBody().size(),2);
		//WHEN
        HttpEntity<TaskCollaboratorRegistryRESTDTO> result = projectManagerTaskRequestsControllerREST.cancelAssignmentRequest(taskId1, taskCollaboratorRegistryRESTDTO);
        
        //THEN
        assignmentList = projectService.listAssignmentRequestsToDTO("1");
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryDTOOUT = new TaskCollaboratorRegistryRESTDTO();
        ProjectCollaborator colab = projectCollaboratorService.getProjectCollaborator("1",projectCollaborator.getUser().getEmail());
        Task t1 = taskService.findTaskByID("1-1");
        taskCollaboratorRegistryDTOOUT.setRegistryStatus(t1.getLastTaskCollaboratorRegistryOf(colab).getRegistryStatus().toString());
        ResponseEntity<TaskCollaboratorRegistryRESTDTO> expected = new ResponseEntity<>(taskCollaboratorRegistryDTOOUT, HttpStatus.OK);
        assertEquals(result.getBody().getRegistryStatus(),expected.getBody().getRegistryStatus());
        assertEquals(assignmentList.size(),1);
    			
	}

	@Test
	public void testCancelRemovalRequest() {
		//GIVEN 2 request from collaborator to remove from task1 and task2
		t1.addProjectCollaborator(projectCollaborator);
		t2.addProjectCollaborator(projectCollaborator);
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		assertEquals(t1.listActiveCollaborators().size(),1);
		assertEquals(t2.listActiveCollaborators().size(),1);
		
		taskProjectCollaboratorRestDTO.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);
        taskProjectCollaboratorRestDTO.setTaskId(taskId2);
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO);
        HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> removalList = projectManagerTaskRequestsControllerREST.listRemovalRequests("1");
        assertEquals(removalList.getBody().size(),2);
        //WHEN
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskCollaboratorRegistryRESTDTO.setTaskId(taskId1);
        HttpEntity<TaskCollaboratorRegistryRESTDTO> result = projectManagerTaskRequestsControllerREST.cancelRemovalRequest("1", taskCollaboratorRegistryRESTDTO);
        //THEN
        removalList = projectManagerTaskRequestsControllerREST.listRemovalRequests("1");
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryDTOOUT = new TaskCollaboratorRegistryRESTDTO();
        ProjectCollaborator colab = projectCollaboratorService.getProjectCollaborator("1",projectCollaborator.getUser().getEmail());
        Task t1 = taskService.findTaskByID("1-1");
        taskCollaboratorRegistryDTOOUT.setRegistryStatus(t1.getLastTaskCollaboratorRegistryOf(colab).getRegistryStatus().toString());
        ResponseEntity<TaskCollaboratorRegistryRESTDTO> expected = new ResponseEntity<>(taskCollaboratorRegistryDTOOUT, HttpStatus.OK);
        assertEquals(result.getBody().getRegistryStatus(),expected.getBody().getRegistryStatus());
        assertEquals(removalList.getBody().size(),1);
        			
	}

	
	@Test
	public void testApproveCompletedRequest() {
		
		ResponseEntity<TaskRestDTO> result = projectManagerTaskRequestsControllerREST.approveCompletedRequest(taskId1);
		ResponseEntity<TaskRestDTO> expected = new ResponseEntity<>(taskService.markTaskCompleted(t1.getId()), HttpStatus.OK);
		
		assertEquals(expected.toString(), result.toString());
	}
	

	
	
	
	@Test
	public void testCancelCompletedRequest() {
		
		ResponseEntity<TaskRestDTO> result = projectManagerTaskRequestsControllerREST.cancelCompletedRequest(taskId1);
		ResponseEntity<TaskRestDTO> expected = new ResponseEntity<>(taskService.cancelMarkTaskCompleted(t1.getId()), HttpStatus.OK);
		
		assertEquals(expected.toString(), result.toString());
	}
	

}
