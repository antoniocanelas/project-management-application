package project.controllers.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectRepositoryClass;
import project.dto.rest.AvailableCostOptionsDTO;
import project.model.project.Project;
import project.model.project.ProjectService;

public class ChooseCostCalculationRESTControllerTest {

	ProjectService projectService;

	ChooseCostCalculationRESTController chooseCostCalculationRESTController;

	ProjectRepositoryClass projectRepositoryClass;
	Project project;

	@Before
	public void setUp() {

		projectRepositoryClass = new ProjectRepositoryClass();
		projectService = new ProjectService(projectRepositoryClass);
		chooseCostCalculationRESTController = new ChooseCostCalculationRESTController(projectService);
	}

	@Test
	@Transactional
	public void testChooseCostCalculationController() {

		chooseCostCalculationRESTController = new ChooseCostCalculationRESTController(projectService);

		assertTrue(chooseCostCalculationRESTController instanceof ChooseCostCalculationRESTController);
	}

	@Test
	@Transactional
	public void testListProjectCostCalculationOptions() {

		// Given
		String choice1 = "AverageCost";
		String choice2 = "MinimumCost";
		String choice3 = "MaximumCost";

		projectService.addProject("1001", "SETI");
		Project project = projectService.getProjectByID("1001");

		// When
		AvailableCostOptionsDTO expectedDTO = new AvailableCostOptionsDTO();

		List<String> expected = project.getCostCalculationOptions();
		expected.add(choice1);
		expected.add(choice2);
		expected.add(choice3);

		expectedDTO.setAvailableCalculationOptions(expected);
		project.setCostCalculationOptions(expected);
		projectService.updateProject(project);

		ResponseEntity<AvailableCostOptionsDTO> result = chooseCostCalculationRESTController
				.listProjectCostCalculationOptions(project.getId());

		// Then
		assertThat(expectedDTO.getAvailableCalculationOptions(),
				containsInAnyOrder(result.getBody().getAvailableCalculationOptions().toArray()));
	}

	@Test
	@Transactional
	public void testSetProjectCostCalculationMechanism() {

		// Given
		String choice = "AverageCost";
		AvailableCostOptionsDTO expectedDTO = new AvailableCostOptionsDTO();
		expectedDTO.setCalculationOption(choice);
		
		projectService.addProject("1001", "SETI");
		Project project = projectService.getProjectByID("1001");

		// When
		ResponseEntity<AvailableCostOptionsDTO> result = chooseCostCalculationRESTController.setProjectCostCalculationMechanism(project.getId(), expectedDTO);
		// Then
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertEquals(expectedDTO.getCalculationOption(), result.getBody().getCalculationOption());
		assertTrue(choice.equals(project.getCostCalculatorPersistence()));
		assertTrue(choice.equals(project.getCostCalculator().getClass().getSimpleName()));
	}
}
