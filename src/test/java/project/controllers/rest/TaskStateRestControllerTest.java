package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectService;
import project.services.TaskService;

public class TaskStateRestControllerTest {

	TaskStateRestController taskStateRestController;
	UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskProjectService taskProjectService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1, project2;
    Task task1, task2, task3, task4, task5, task6, task7, task8;
    
    TaskRestDTO taskRestDTO;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;

	@Before
	public void setUp() throws AddressException {
		
		 userRepositoryClass = new UserRepositoryClass();
	     projectRepositoryClass = new ProjectRepositoryClass();
	     projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
	     taskRepositoryClass = new TaskRepositoryClass();

	        userService = new UserService(userRepositoryClass);
	        projectService = new ProjectService(projectRepositoryClass);
	        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
	        taskService = new TaskService(taskRepositoryClass);
	        taskProjectService= new TaskProjectService(projectRepositoryClass, taskRepositoryClass);
        
        taskStateRestController = new TaskStateRestController(taskProjectService);
        
        birth = LocalDate.of(1999, 11, 11);
        
        taskRestDTO = new TaskRestDTO();

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
   
        user1.setProfileCollaborator();
        user2.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);
        d3 = LocalDateTime.of(2018, 05, 15, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, user1);

        project1.setStartDate(d1);

        projectCollaboratorService.addProjectCollaborator(project1, user1, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 4);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

        projectService.updateProject(project1);

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        
        task1.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(d1);;
        
        task1.addReport(projectCollaborator2, 3, d2, d3);
        taskService.updateTask(task1);
        
        task3.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(d1);;
        
        task3.addReport(projectCollaborator2, 3, d2, d3);
        taskService.updateTask(task3);
        
        task3.setEffectiveDateOfConclusion(d2);
        taskService.updateTask(task3);
    }
        
    @Test
    public void testSetTaskCanceled() {
            	
        taskRestDTO = new TaskRestDTO();   
        taskRestDTO.setMessage("Canceled"+ task1.getId());
        
        taskProjectService.setTaskCanceles(task1.getId());
        
        ResponseEntity<TaskRestDTO> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

        HttpEntity<TaskRestDTO> result = taskStateRestController.setTaskCanceled(task1.getId());

        assertEquals(expected.toString(), result.toString());
        
    }
    
    @Test
    public void testSetTaskCanceledFail() {
    	

    	
        taskRestDTO = new TaskRestDTO();   
        taskRestDTO.setMessage("Not Canceled"+ task3.getId());
        
        ResponseEntity<TaskRestDTO> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.BAD_REQUEST);

        HttpEntity<TaskRestDTO> result = taskStateRestController.setTaskCanceled(task3.getId());

        assertEquals(expected.toString(), result.toString());
        
        
    }
	
}
