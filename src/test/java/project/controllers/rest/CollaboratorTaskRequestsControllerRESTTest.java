package project.controllers.rest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

public class CollaboratorTaskRequestsControllerRESTTest {

	

	CollaboratorTaskRequestsControllerREST collaboratorTaskRequestsControllerREST;
    TaskProjectCollaboratorService taskProjectCollaboratorService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    LocalDate birth;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    User userProjectManager;
    User userProjectCollaborator;
    User userProjectCollaborator2;
    ProjectCollaborator projectCollaborator, projectCollaborator2, projectManager;
    Project project;
    Task t1;
    Task t2;
    LocalDateTime d1;
    LocalDateTime d2;
    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO1, taskProjectCollaboratorRestDTO2 ,taskProjectCollaboratorRestDTO3;
    TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO1,taskCollaboratorRegistryRESTDTO2, taskCollaboratorRegistryRESTDTO3;
    String expectedMessage, message, taskId1,taskId2;
	
    @Before
	public void setUp() throws AddressException {
		
		userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        ProjectCollaborator.setStartIdGenerator(1);
        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
        collaboratorTaskRequestsControllerREST = new CollaboratorTaskRequestsControllerREST(projectService,taskProjectCollaboratorService,userService,taskService,projectCollaboratorService);
        birth = LocalDate.of(1999, 11, 11);
        taskProjectCollaboratorRestDTO1 = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO2 = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO3 = new TaskProjectCollaboratorRestDTO();
        
        taskCollaboratorRegistryRESTDTO1 = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO2 = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO3 = new TaskCollaboratorRegistryRESTDTO();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        
        userService.addUser("Lisa", "666 555 5556", "lisa@switch.com", "5656546456", birth, "2", "Rua ", "4433",
                "cidade", "pais");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
        userProjectCollaborator2=userService.searchUserByEmail("lisa@switch.com");

        userProjectManager.setProfileCollaborator();
        userProjectCollaborator.setProfileCollaborator();
        userProjectCollaborator2.setProfileCollaborator();

        userService.updateUser(userProjectManager);
        userService.updateUser(userProjectCollaborator);
        userService.updateUser(userProjectCollaborator2);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator2, 2);

        projectManager = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectManager.getEmail());
        projectCollaborator = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectCollaborator.getEmail());
        projectCollaborator2 = projectCollaboratorService.getProjectCollaborator(project.getId(), userProjectCollaborator2.getEmail());

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project, "task2", "task2", d1.plusDays(2), d2.plusDays(5), 1.2, 1.3);
        t1 = taskService.findTaskByID("1-1");
        t2 = taskService.findTaskByID("1-2");
    
        taskId1 = t1.getId();
        taskId2 = t2.getId();
        
    }
	
	@Test
	public void testListCollabtRequests() {
		//GIVEN 2 request from collaborator to task1 and task2
		List<TaskCollaboratorRegistryRESTDTO> assignmentList = new ArrayList<>();
		
		taskProjectCollaboratorRestDTO1.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO1.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO1);
        
        
        taskProjectCollaboratorRestDTO1.setTaskId(taskId1);
        taskProjectCollaboratorRestDTO1.setProjectCollaboratorId(projectCollaborator2.getId());
        taskProjectCollaboratorService.requestAddProjectCollaboratorToTask(taskProjectCollaboratorRestDTO1);
        
        t2.addProjectCollaborator(projectCollaborator);	
  		taskService.updateTask(t2);
    
        taskProjectCollaboratorRestDTO2.setTaskId(taskId2);
        taskProjectCollaboratorRestDTO2.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorService.requestRemoveProjectCollaboratorFromTask(taskProjectCollaboratorRestDTO2);
       
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorEmail(projectCollaborator.getUser().getEmail());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorId(projectCollaborator.getId());
        taskCollaboratorRegistryRESTDTO1.setTaskId(taskId1);
        taskCollaboratorRegistryRESTDTO1.setRegistryStatus(RegistryStatus.ASSIGNMENTREQUEST.toString());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorName(projectCollaborator.getUser().getName());
        taskCollaboratorRegistryRESTDTO1.setCollaboratorAddedToTaskDate(t1.getLastTaskCollaboratorRegistryOf(projectCollaborator).getCollaboratorAddedToTaskDate());
        
        taskCollaboratorRegistryRESTDTO2.setProjectCollaboratorEmail(projectCollaborator.getUser().getEmail());
        taskCollaboratorRegistryRESTDTO2.setProjectCollaboratorId(projectCollaborator.getId());
        taskCollaboratorRegistryRESTDTO2.setTaskId(taskId2);
        taskCollaboratorRegistryRESTDTO2.setRegistryStatus(RegistryStatus.REMOVALREQUEST.toString());
        taskCollaboratorRegistryRESTDTO2.setProjectCollaboratorName(projectCollaborator.getUser().getName());
        taskCollaboratorRegistryRESTDTO2.setCollaboratorAddedToTaskDate(t1.getLastTaskCollaboratorRegistryOf(projectCollaborator).getCollaboratorAddedToTaskDate());

        
        
		//WHEN
        HttpEntity<List<TaskCollaboratorRegistryRESTDTO>> result = collaboratorTaskRequestsControllerREST.listColaboratorAssignmentRequests(projectCollaborator.getUser().getEmail());
        //THEN
        assignmentList.add(taskCollaboratorRegistryRESTDTO1);
        assignmentList.add(taskCollaboratorRegistryRESTDTO2);
        ResponseEntity<List<TaskCollaboratorRegistryRESTDTO>> expected = new ResponseEntity<>(assignmentList, HttpStatus.OK);
        assertEquals(expected.toString(),result.toString());
        
	}
	
	
	@Test
	public void testMakeAssignementRequest() {
		//Given 1 project with 2 tasks and 1 collaborator
		TaskCollaboratorRegistryRESTDTO taskCollRegRestDTO = new TaskCollaboratorRegistryRESTDTO();
		taskCollRegRestDTO.setProjectCollaboratorEmail(projectCollaborator.getUser().getEmail());
		//When: collaborator make assignment request to task 1
		HttpEntity<TaskCollaboratorRegistryRESTDTO> result = collaboratorTaskRequestsControllerREST.makeAssignementRequest(taskId1, taskCollRegRestDTO);
		//Then:
		ProjectCollaborator colab = projectCollaboratorService.getProjectCollaborator("1",projectCollaborator.getUser().getEmail());
		Task t1 = taskService.findTaskByID("1-1");
		taskCollRegRestDTO.setRegistryStatus(t1.getLastTaskCollaboratorRegistryOf(colab).getRegistryStatus().toString());
		ResponseEntity<TaskCollaboratorRegistryRESTDTO> expected = new ResponseEntity<>(taskCollRegRestDTO,HttpStatus.OK);
		assertEquals(result, expected);
		assertEquals(expected.getBody().getRegistryStatus(),result.getBody().getRegistryStatus());
		
	}
	
	
	@Test
	public void testMakeRemovalRequest() {
		t2.addProjectCollaborator(projectCollaborator);	
  		taskService.updateTask(t2);
		TaskCollaboratorRegistryRESTDTO taskCollRegRestDTO = new TaskCollaboratorRegistryRESTDTO();
		taskCollRegRestDTO.setProjectCollaboratorEmail(projectCollaborator.getUser().getEmail());
		//When: collaborator make assignment request to task 1
		HttpEntity<TaskCollaboratorRegistryRESTDTO> result = collaboratorTaskRequestsControllerREST.makeRemovalRequest(taskId2, taskCollRegRestDTO);
		//Then:
		
		taskCollRegRestDTO.setRegistryStatus(t2.getLastTaskCollaboratorRegistryOf(projectCollaborator).getRegistryStatus().toString());
		ResponseEntity<TaskCollaboratorRegistryRESTDTO> expected = new ResponseEntity<>(taskCollRegRestDTO,HttpStatus.OK);
		assertEquals(result, expected);
		assertEquals(expected.getBody().getRegistryStatus(),result.getBody().getRegistryStatus());
		
	}
}
