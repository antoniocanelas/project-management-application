package project.controllers.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.security.UserPrincipal;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

public class CollaboratorTaskListRESTControllerTest {

	CollaboratorTaskListRESTController collabTaskListRESTController;

	UserService userService;

	ProjectService projectService;

	TaskService taskService;

	ProjectCollaboratorService projCollabReg;

	List<User> expectedUser;

	LocalDate birth;
	User user1;
	User user2;
	User user3;
	User user4;
	ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

	Project project1;
	Task task1, task2, task3, task4, task5, task6, task7;

	LocalDateTime d1;
	LocalDateTime d2;
	LocalDateTime d3;
	LocalDateTime d4;
	int y1;
	Month m1;

	UserRepositoryClass userRepositoryClass;
	ProjectRepositoryClass projectRepositoryClass;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	TaskRepositoryClass taskRepositoryClass;
	UserPrincipal userPrincipal;

	@Before
	public void setUp() throws AddressException {

		userRepositoryClass = new UserRepositoryClass();
		projectRepositoryClass = new ProjectRepositoryClass();
		taskRepositoryClass = new TaskRepositoryClass();
		projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

		userService = new UserService(userRepositoryClass);
		projectService = new ProjectService(projectRepositoryClass);
		projCollabReg = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
		taskService = new TaskService(taskRepositoryClass);

		collabTaskListRESTController = new CollaboratorTaskListRESTController(userService, projectService, taskService);

		birth = LocalDate.of(1999, 11, 11);
		y1 = LocalDateTime.now().getYear();
		m1 = LocalDateTime.now().getMonth();

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		user2.setProfileCollaborator();
		user3.setProfileCollaborator();
		user4.setProfileCollaborator();

		userService.updateUser(user2);
		userService.updateUser(user3);
		userService.updateUser(user4);
		
        List<String>listProjectWhereProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsProjectManager = new ArrayList<>();
        List<String>listTasksWhereIsTaskCollaborator = new ArrayList<>();
        userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator, listTasksWhereIsProjectManager);


		/*
		 * Project 1 id= "1"
		 *
		 * user1 is project manager users 2, 3 and 4 are added to the project and become
		 * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
		 * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
		 *
		 * project 1 has 6 tasks
		 *
		 * tasks 1 and 2 have reports tasks 1 and 2 are pending
		 *
		 * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
		 * and no end date
		 *
		 * tasks 5 and 6 have reports and are concluded
		 *
		 *
		 * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
		 * in task 2 has 4 reports of 4,8,8,4 hours
		 *
		 * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
		 * report in task5 1 report in task6
		 *
		 * projectCollaborator4 has no assigned tasks
		 *
		 *
		 */

		projectService.addProject("1", "Project 1");
		project1 = projectService.getProjectByID("1");
		projCollabReg.setProjectManager(project1, user1);
		projCollabReg.addProjectCollaborator(project1, user2, 2);
		projCollabReg.addProjectCollaborator(project1, user3, 4);
		projCollabReg.addProjectCollaborator(project1, user4, 5);

		projectManager = project1.findProjectCollaborator(user1);
		projectCollaborator2 = project1.findProjectCollaborator(user2);
		projectCollaborator3 = project1.findProjectCollaborator(user3);
		projectCollaborator4 = project1.findProjectCollaborator(user4);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
		taskService.addTask(project1, "task2", "task2", d1, d2.minusDays(2), 1.2, 1.3);
		taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task7", "task7", d1, d2, 1.2, 1.2);

		task1 = project1.findTaskByTitle("task1");
		task2 = project1.findTaskByTitle("task2");
		task3 = project1.findTaskByTitle("task3");
		task4 = project1.findTaskByTitle("task4");
		task5 = project1.findTaskByTitle("task5");
		task6 = project1.findTaskByTitle("task6");
		task7 = project1.findTaskByTitle("task7");

		task1.addProjectCollaborator(projectCollaborator2);
		task2.addProjectCollaborator(projectCollaborator2);
		task3.addProjectCollaborator(projectCollaborator2);
		task3.addProjectCollaborator(projectCollaborator4);
		task5.addProjectCollaborator(projectCollaborator2);
		
		task5.addProjectCollaborator(projectCollaborator4);
		task7.addProjectCollaborator(projectCollaborator2);

		task5.setEffectiveStartDate(d1.plusDays(20));
		task5.setEffectiveDateOfConclusion(d2.minusMonths(4));
		task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
		task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));
		
		task7.getLastTaskCollaboratorRegistryOf(projectCollaborator2).setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(41));		
		task7.addReport(projectCollaborator2, 7, LocalDateTime.now().minusDays(40),LocalDateTime.now().minusDays(35) );
		
		projectService.updateProject(project1);
		taskService.updateTask(task1);
		taskService.updateTask(task2);
		taskService.updateTask(task3);
		taskService.updateTask(task4);
		taskService.updateTask(task5);
		taskService.updateTask(task6);
		taskService.updateTask(task7);

	}

	@Test
	@Transactional
	public void testCollaboratorTaskListRESTController() {

		CollaboratorTaskListRESTController collabTaskListRestController = new CollaboratorTaskListRESTController(
				userService, projectService, taskService);

		assertTrue(collabTaskListRestController instanceof CollaboratorTaskListRESTController);

	}


	@Test
	@Transactional
	public void testGetCollaboratorTasksSuccess() {

		List<TaskRestDTO> expectedList = new ArrayList<>();
		expectedList.add(task1.toDTO());
		expectedList.add(task2.toDTO());
		expectedList.add(task3.toDTO());
		expectedList.add(task5.toDTO());
		
		expectedList.add(task7.toDTO());
		expectedList.add(task5.toDTO());

		 collabTaskListRESTController.getCollaboratorTasks(user2.getEmail(), userPrincipal);

		
		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorTasks(user2.getEmail(),userPrincipal);

		
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(expectedList, HttpStatus.OK);

		assertEquals(expected.toString(),result.toString());
		
		

	}

	@Test
	@Transactional
	public void testGetCollaboratorPendingTasksSuccess() {

		List<TaskRestDTO> expectedList = new ArrayList<>();

		TaskRestDTO task2DTO = task3.toDTO();
		expectedList.add(task2DTO);

		ResponseEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorPendingTasks(user4.getEmail(), userPrincipal);

		
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(expectedList, HttpStatus.OK);

		assertEquals(expected.toString(),result.toString());

	}

	@Test
	@Transactional
	public void testGetUserPendentTasksNoTasks() {

		List<TaskRestDTO> expectedList = new ArrayList<>();


		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorPendingTasks(user3.getEmail(), userPrincipal);

		
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(expectedList, HttpStatus.OK);

		
		assertEquals(expected, result);
	}

	/**
	 * Test with success the getCollaboratorCompletedTasks method
	 * 
	 * GIVEN: a user exists in the database and task5 is completed WHEN: ask for a
	 * list of completed tasks on all projects the user was involved as project
	 * collaborator THEN: return an object with list of completed tasks by the user
	 */
	@Test
	@Transactional
	public void testGetCollaboratorCompletedTasksSuccess() {

		// GIVEN
		assertNotNull(userService.searchUserByEmail("joaquim@gmail.com"));
		assertTrue(task5.isCompleted());

		// WHEN
		TaskRestDTO taskDTO = task5.toDTO();
		List<TaskRestDTO> completedTasks = new ArrayList<>();
		completedTasks.add(taskDTO);

		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController
				.getCollaboratorCompletedTasks("joaquim@gmail.com");

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(completedTasks, HttpStatus.OK);
		assertEquals(expected.toString(), result.toString());
	}

	/**
	 * Test failure the getCollaboratorCompletedTasks method
	 * 
	 * GIVEN: a request with an empty string passed as user's email WHEN: try to get
	 * the list of tasks completed THEN: return a HttpStatus bad request
	 */
	@Test
	@Transactional
	public void testGetCollaboratorCompletedTasksEmptyFail() {

		// GIVEN
		String userEmail = "";
		assertTrue(userEmail.isEmpty());

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorCompletedTasks(userEmail);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected, result);
	}

	/**
	 * Test failure the getCollaboratorCompletedTasks method
	 * 
	 * GIVEN: a request with a null object passed as user's email WHEN: try to get
	 * the list of tasks completed THEN: return a HttpStatus bad request
	 */
	@Test
	@Transactional
	public void testGetCollaboratorCompletedTasksNullFail() {

		// GIVEN
		String userEmail = null;
		assertNull(userEmail);

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorCompletedTasks(userEmail);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected, result);
	}
	
	/**
	 * Test with success the getCollaboratorCompletedTasksLastMonth method
	 * 
	 * GIVEN: a user exists in the database and task7 is completed WHEN: ask for a
	 * list of completed tasks on all projects the user was involved as project
	 * collaborator THEN: return an object with list of completed tasks last month by the user
	 */
	@Test
	@Transactional
	public void testGetCollaboratorCompletedTasksLastMonthSuccess() {

		// GIVEN
		assertNotNull(userService.searchUserByEmail("joaquim@gmail.com"));
	    task7.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(1));
		assertTrue(task7.isCompleted());

		// WHEN
	
		
		TaskRestDTO taskDTO = task7.toDTO();
		List<TaskRestDTO> completedTasks = new ArrayList<>();
		completedTasks.add(taskDTO);

		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController
				.getCollaboratorCompletedTasksLastMonth("joaquim@gmail.com");

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(completedTasks, HttpStatus.OK);
		assertEquals(expected.toString(), result.toString());
	}
	
	
	/**
	 * Test failure the getCollaboratorCompletedTasks method
	 * 
	 * GIVEN: a request with an empty string passed as user's email WHEN: try to get
	 * the list of tasks completed last month THEN: return a HttpStatus bad request
	 */
	@Test
	@Transactional
	public void testGetCollaboratorCompletedTasksLastMonthEmptyFail() {

		// GIVEN
		String userEmail = "";
		assertTrue(userEmail.isEmpty());

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController.getCollaboratorCompletedTasksLastMonth(userEmail);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected, result);
	}
	
	/**
	 * Test with success the listCollaboratorTasksOrderByLastReport
	 * 
	 */
	@Test
	@Transactional
	public void testListCollaboratorTasksOrderByLastReport() {

		// WHEN
    	
		TaskRestDTO taskDTO1 = task1.toDTO();
		TaskRestDTO taskDTO2 = task2.toDTO();
		TaskRestDTO taskDTO3 = task3.toDTO();
		TaskRestDTO taskDTO5 = task5.toDTO();
		TaskRestDTO taskDTO7 = task7.toDTO();
		
		List<TaskRestDTO> recentAtivityList = new ArrayList<>();
		recentAtivityList.add(taskDTO7);
		recentAtivityList.add(taskDTO1);
		recentAtivityList.add(taskDTO2);
		recentAtivityList.add(taskDTO3);
		recentAtivityList.add(taskDTO5);

		HttpEntity<List<TaskRestDTO>> result = collabTaskListRESTController
				.listCollaboratorTasksOrderByLastReport("joaquim@gmail.com");

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(recentAtivityList,HttpStatus.OK);
		
		assertEquals(expected.toString(), result.toString());
	}

}
