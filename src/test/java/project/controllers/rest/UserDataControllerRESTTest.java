package project.controllers.rest;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.PasswordRestDTO;
import project.dto.rest.UserRestDTO;
import project.model.UserService;
import project.model.user.User;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserDataControllerRESTTest {

	UserService userService;
	UserRepositoryClass userRepositoryClass;
	
	UserDataControllerREST udcRest;
	User user1,user2,user3;
	UserRestDTO userInDTO;
	UserRestDTO userOutDTO;
	PasswordRestDTO passwordRestDTO;
	HttpEntity<UserRestDTO> expected;
	HttpEntity<UserRestDTO> result;
	String userId;

	

	@Before
	public void setUp() throws AddressException {
		
		userRepositoryClass = new UserRepositoryClass();
		userService = new UserService(userRepositoryClass);
		udcRest = new UserDataControllerREST(userService);
		userInDTO = new UserRestDTO();
		userOutDTO = new UserRestDTO();
		
		userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789", LocalDate.of(1977, 3, 5), "1",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");
		userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789", LocalDate.of(1978, 11, 12), "2",
				"Rua sem saída", "4250-357", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user1.setProfileCollaborator();
		user1.setActive();

		user2 = userService.searchUserByEmail("manel@jj.com");
		user2.setProfileDirector();
		user2.setActive();

		user3 = userService.searchUserByEmail("joaquim@jj.com");
		user3.setProfileCollaborator();
		user3.setActive();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(user3);	
		
	}


	/**
	 * <p>GIVEN: user with profile as collaborator</p>
	 * <p>WHEN: set user profile to director</p>
	 * <p>THEN: return true operation</p>
	 */
	@Test
	public void testSetUserProfileTrueCase() {
		//GIVEN
		String userId = user1.getUserIdVO().getUserEmail();
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
		//WHEN
		boolean result = udcRest.setUserProfile(userId, "director");
		//THEN
		assertEquals(true,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
	}

	@Test
	public void testSetUserProfileTrueCaseSwitchAlreadyUpperCase() {
		//GIVEN
		String id = user1.getUserIdVO().getUserEmail();
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
		//WHEN
		boolean result = udcRest.setUserProfile(id, "DIRECTOR");
		//THEN
		assertEquals(true,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
	}
	
	/**
	 * <p>GIVEN: user with profile as collaborator</p>
	 * <p>WHEN: set user profile to an unknown roll</p>
	 * <p>THEN: return false operation and user still got his previous profile</p>
	 */
	@Test
	public void testSetUserProfileFalseCaseWrongProfileInputed() {
		//GIVEN
		String userId = user1.getUserIdVO().getUserEmail();
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
		//WHEN
		boolean result = udcRest.setUserProfile(userId, "admin");
		//THEN
		assertEquals(false,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
	}
	
	/**
	 * <p>GIVEN: user with profile as collaborator</p>
	 * <p>WHEN: set user profile to null roll</p>
	 * <p>THEN: return true operation but user still got his previous profile</p>
	 */
	@Test
	public void testSetUserProfileTrueCaseNullProfile() {
		//GIVEN
		String userId = user1.getUserIdVO().getUserEmail();
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
		//WHEN
		boolean result = udcRest.setUserProfile(userId, null);
		//THEN
		assertEquals(true,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
	}
	
	
	/**
	 * Test editUserData() to change profile of an user to ROLE_DIRECTOR
	 * 
	 * <p>GIVEN: user with profile as collaborator</p>
	 * <p>WHEN: set user profile to ROLE_DIRECTOR</p>
	 * <p>THEN: user has his profile changed to ROLE_DIRECTOR, and return a httpsatus = accepted</p>
	 */
	@Test
	public void testEditUserDataSetProfileDirector() {	
		//GIVEN
		userId = user1.getUserIdVO().getUserEmail();
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));

		//WHEN
		userInDTO.setProfile("director");
		result = udcRest.editUserData(userId, userInDTO);
		//THEN
		userOutDTO = userService.searchUserDTOByEmail(userId);
		expected =  new ResponseEntity<>(userOutDTO,HttpStatus.ACCEPTED);
		assertEquals(expected,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
	}
	
	
	/**
	 * Test editUserData() to change profile of an user to ROLE_COLLABORATOR
	 * 
	 * <p>GIVEN: user with profile as director</p>
	 * <p>WHEN: set user profile to collaborator</p>
	 * <p>THEN: user has his profile changed to ROLE_DIRECTOR, and return a httpsatus = accepted</p>
	 */
	@Test
	public void testEditUserDataSetProfileCollaborator() {	
		//GIVEN
		userId = user2.getUserIdVO().getUserEmail();

		assertTrue(user2.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
		//WHEN
		userInDTO.setProfile("collaborator");
		result = udcRest.editUserData(userId, userInDTO);
		//THEN
		userOutDTO = userService.searchUserDTOByEmail(userId);
		expected =  new ResponseEntity<>(userOutDTO,HttpStatus.ACCEPTED);
		assertEquals(expected,result);
		assertTrue(user1.getRoles().contains(new Role(RoleName.ROLE_COLLABORATOR)));
	}
	
	/**
	 * Test editUserData() to change profile of an user to an invalid role, i.e., when profile's field has invalid name
	 * 
	 * <p>GIVEN: user with profile as director</p>
	 * <p>WHEN: set user profile to "collab" - invalid inputed profile's name</p>
	 * <p>THEN: user still has profile as ROLE_DIRECTOR, and return a httpsatus = BAD_REQUEST</p>
	 */
	@Test
	public void testEditUserDataSetProfileBadRequest() {	
		//GIVEN
		userId = user2.getUserIdVO().getUserEmail();
		assertTrue(user2.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));

		//WHEN
		userInDTO.setProfile("collab");
		result = udcRest.editUserData(userId, userInDTO);
		//THEN
		expected =  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected,result);
		assertTrue(user2.getRoles().contains(new Role(RoleName.ROLE_DIRECTOR)));
	}
	
	/**
	 * Test editUserData() to edit personal data of an invalid user
	 * 
	 * <p>GIVEN: invalid userId </p>
	 * <p>WHEN: try to edit his personal data - edit profile and edit name</p>
	 * <p>THEN: return a httpsatus = NOT_FOUND</p>
	 */
	@Test
	public void testEditUserDataInvalidUser() {	
		//GIVEN
		userId = "invalid@gmail.com";
		//WHEN
		userInDTO.setProfile("collaborator");
		userInDTO.setName("joaquim");
		result = udcRest.editUserData(userId, userInDTO);
		//THEN
		expected =  new ResponseEntity<>(HttpStatus.NOT_FOUND);
		assertEquals(expected,result);
	}

	/**
	 * Test to set password.
	 * Given : a password,
	 * When : try to set a new password,
	 * Then : return a http status = ACCEPTED.
	 */
	@Test
	public void setPasswordTestSuccess() {

		// Given
	    passwordRestDTO = new PasswordRestDTO();
	    String password = "qwerty";
	    passwordRestDTO.setNewPassword(password);
	    String message = "New password submitted";

	    // When
        HttpEntity<?> result = udcRest.setPassword(user1.getEmail(), passwordRestDTO);
        ResponseEntity<?> expected = new ResponseEntity<>(message, HttpStatus.ACCEPTED);

        // Then
        assertEquals (expected, result);
    }

	/**
	 * Test to set password.
	 * Given : a password,
	 * When : try to set a new password,
	 * Then : return a http status = BAD_REQUEST.
	 */
	@Test
    public void setPasswordTestFail() {

		// Given
        passwordRestDTO = new PasswordRestDTO();
        String password = "qwerty";
        passwordRestDTO.setNewPassword(password);

        // When
        HttpEntity<?> result = udcRest.setPassword(null, passwordRestDTO);
        expected =  new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Then
        assertEquals (expected, result);
    }
}
