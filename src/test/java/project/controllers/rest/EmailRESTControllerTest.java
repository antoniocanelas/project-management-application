package project.controllers.rest;

import jparepositoriestest.JavaMailSenderClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.EmailMessageDTO;
import project.services.EmailServiceImpl;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class EmailRESTControllerTest {

    EmailServiceImpl emailServiceImpl;
    JavaMailSenderClass javaMailSenderClass;

    EmailMessageDTO emailMessageDTO;
    EmailControllerREST erc;

    @Before
    public void setUp() {
        javaMailSenderClass = new JavaMailSenderClass();
        emailServiceImpl = new EmailServiceImpl(javaMailSenderClass);

        emailMessageDTO = new EmailMessageDTO();

        emailMessageDTO.setTo("1960031@isep.ipp.pt");
        emailMessageDTO.setSubject("Hello World");
        emailMessageDTO.setMessage("Hello World from Group 1 fabulous App");

        erc = new EmailControllerREST(emailServiceImpl);

    }

    @Test
    @Transactional
    public void testSendEmailNotNull() {

        assertThat(erc).isNotNull();
    }

    @Test
    @Transactional
    public void testSendEmail() {

        String expected = "Email sent";
        String result = erc.sendEmail();

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSendEmail2() {

        String expected = "Email sent";
        String result = erc.sendEmail(emailMessageDTO.toString());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testSendEmail3() {

        String expected = "Email sent";
        String result = erc.sendEmail(emailMessageDTO);

        assertEquals(expected, result);
    }

}
