package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.CreateReportRestDTO;
import project.dto.rest.EditReportQuantityDTO;
import project.dto.rest.ReportRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;
import project.model.task.Task;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ReportRESTControllerTest {

	UserService userService;
	ProjectService projectService;
	TaskService taskService;
	TaskProjectCollaboratorService taskProjCollabReg;
	ProjectCollaboratorService projectCollaboratorService;

	UserRepositoryClass userRepositoryClass;
	ProjectRepositoryClass projectRepositoryClass;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	TaskRepositoryClass taskRepositoryClass;

	private LocalDate birth;
	private User user1;
	private User user2;
	private User projectManager;
	private ProjectCollaborator projectCollaborator1;
	private Project project1;
	private Project project2;
	private LocalDateTime d1;
	private Task t1;
	private Task t2;
	private ReportRESTController reportRESTController;
	private ProjectCollaborator projectCollaborator2;
	private ProjectCollaboratorCostAndTimePeriod costAndPeriod;

	@Before
	public void setUp() throws AddressException {
		userRepositoryClass = new UserRepositoryClass();
		projectRepositoryClass = new ProjectRepositoryClass();
		projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
		taskRepositoryClass = new TaskRepositoryClass();

		userService = new UserService(userRepositoryClass);
		projectService = new ProjectService(projectRepositoryClass);
		projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
		taskService = new TaskService(taskRepositoryClass);
		taskProjCollabReg = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);
		reportRESTController = new ReportRESTController(taskProjCollabReg);

		d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
		birth = LocalDate.of(1999, 11, 11);
		userService.addUser("Diogo", "91739812379", "diogo@switch.com", "132456765", birth, "2", "Rua ", "4433",
				"cidade", "pais");
		userService.addUser("Lisa", "666 555 5556", "lisa@switch.com", "5656546456", birth, "2", "Rua ", "4433",
				"cidade", "pais");
		userService.addUser("Tiago", "666 555 5556", "tiago@switch.com", "5656546456", birth, "2", "Rua ", "4433",
				"cidade", "pais");
		user1 = userService.searchUserByEmail("diogo@switch.com");
		user1.setProfileCollaborator();
		user2 = userService.searchUserByEmail("tiago@switch.com");
		user2.setProfileCollaborator();

		projectManager = userService.searchUserByEmail("lisa@switch.com");
		projectManager.setProfileCollaborator();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(projectManager);

		projectService.addProject("1", "Project 1");
		project1 = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project1, projectManager);
		projectCollaboratorService.addProjectCollaborator(project1, user1, 3);
		projectCollaborator1 = projectCollaboratorService.getProjectCollaborator(project1.getId(), "diogo@switch.com");
		projectCollaborator1.setId(10);
		costAndPeriod = new ProjectCollaboratorCostAndTimePeriod(10);
		costAndPeriod.setStartDate(birth);
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		costAndTimePeriodList.add(costAndPeriod);
		projectCollaborator1.setCostAndTimePeriodList(costAndTimePeriodList);
		projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);
		taskService.addTask(project1, "Test 1");
		taskService.addTask(project1, "Test 2");
		t1 = project1.findTaskByTitle("Test 1");
		t2 = project1.findTaskByTitle("Test 2");

		t1.setPredictedDateOfStart(d1.plusDays(20));
		t1.setPredictedDateOfConclusion(d1.plusDays(40));
		t2.setPredictedDateOfStart(d1.plusDays(20));
		t2.setPredictedDateOfConclusion(d1.plusDays(40));

		t1.addProjectCollaborator(projectCollaborator1);
		t1.getLastTaskCollaboratorRegistryOf(projectCollaborator1)
				.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusMonths(20));
		t2.addProjectCollaborator(projectCollaborator1);

		t1.addReport(projectCollaborator1, 9, d1, d1.plusDays(5));
		t1.setEffectiveStartDate(LocalDateTime.now().minusMonths(6));

		taskService.updateTask(t1);
		taskService.updateTask(t2);

		projectService.updateProject(project1);
	}

	@Test
	public void addReportTest() {
		// given
		CreateReportRestDTO reportRestDTO = new CreateReportRestDTO();
		reportRestDTO.setProjectCollaboratorID(projectCollaborator1.getId());
		reportRestDTO.setTaskID(t1.getId());

		reportRestDTO.setStartDate(LocalDateTime.now().minusDays(10));
		reportRestDTO.setEndDate(LocalDateTime.now().minusDays(1));
		reportRestDTO.setQuantity(10);
		reportRestDTO.setProjectCollaboratorEmail(projectCollaborator1.getUser().getEmail());

		ResponseEntity<CreateReportRestDTO> message = reportRESTController.createReport2(t1.getId(), reportRestDTO);

		assertEquals(reportRestDTO, message.getBody());
		assertEquals(HttpStatus.OK, message.getStatusCode());
	}


	@Test
	public void addReportTest2() {
		// given
		CreateReportRestDTO reportRestDTO = new CreateReportRestDTO();
		reportRestDTO.setProjectCollaboratorEmail(projectCollaborator1.getUser().getEmail());
		reportRestDTO.setTaskID(t1.getId());

		reportRestDTO.setStartDate(LocalDateTime.now().minusDays(10));
		reportRestDTO.setEndDate(LocalDateTime.now().minusDays(1));
		reportRestDTO.setQuantity(10);
		ResponseEntity<CreateReportRestDTO> message = reportRESTController.createReport2(t1.getId(), reportRestDTO);
		String expectedMessage = "Report added";
		assertEquals(reportRestDTO, message.getBody());
		assertEquals(HttpStatus.OK, message.getStatusCode());
	}

	@Test
	public void addReportTest2Fail() {
		// given
		CreateReportRestDTO reportRestDTO = new CreateReportRestDTO();
		reportRestDTO.setProjectCollaboratorEmail(projectCollaborator1.getUser().getEmail());
		reportRestDTO.setTaskID(t1.getId());
		t1.removeProjectCollaborator(projectCollaborator1);

		ResponseEntity<CreateReportRestDTO> message = reportRESTController.createReport2(t1.getId(), reportRestDTO);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}

	@Test
	public void addReportTestDiferentProjects() {

		projectService.addProject("2", "Project 2");
		project2 = projectService.getProjectByID("2");
		projectCollaboratorService.setProjectManager(project2, projectManager);
		projectCollaboratorService.addProjectCollaborator(project2, user2, 10);
		projectCollaborator2 = projectCollaboratorService.getProjectCollaborator(project2.getId(), "tiago@switch.com");

		projectService.updateProject(project2);

		projectCollaboratorService.updateProjectCollaborator(projectCollaborator1);
		
		
		CreateReportRestDTO reportRestDTO = new CreateReportRestDTO();
		reportRestDTO.setProjectCollaboratorID(projectCollaborator2.getId());
		reportRestDTO.setTaskID(t1.getId());
		reportRestDTO.setStartDate(LocalDateTime.now().minusDays(15));
		reportRestDTO.setEndDate(LocalDateTime.now().minusDays(1));
		reportRestDTO.setQuantity(10);
		reportRestDTO.setProjectCollaboratorEmail(projectCollaborator2.getUser().getEmail());

		ResponseEntity<CreateReportRestDTO> message = reportRESTController.createReport2(t1.getId(), reportRestDTO);
		String expectedMessage = "Report not added";
//		assertEquals(reportRestDTO, message.getBody());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}

	
	@Test
	public void listTaskReportsTest() {
		// given
		List<ReportRestDTO> reportListExpected =taskProjCollabReg.reportsListToDTO(t1.listAllReports());
		CreateReportRestDTO reportRestDTO = new CreateReportRestDTO();
		reportRestDTO.setTaskID(t1.getId());
		ResponseEntity<List<ReportRestDTO>> reportListResult = reportRESTController.listTaskReportsDTO(t1.getId());
		assertEquals(reportListExpected, reportListResult.getBody());
	}
	
	


	@Test
	public void updateReportFailReporiDNotFoundTest() {
		
		Report report = t1.listAllReports().get(0);
		EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
		editReportQuantityDTO.setTaskID(t1.getId());
		editReportQuantityDTO.setReportId(report.getCode()+"2");
		editReportQuantityDTO.setQuantity(10);
		ResponseEntity<EditReportQuantityDTO>  message = reportRESTController.updateReport2(
				editReportQuantityDTO.getReportId(), editReportQuantityDTO.getTaskID(), editReportQuantityDTO);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}
	
	@Test
	public void updateReportFailInvalidEffortTest() {
		
		Report report = t1.listAllReports().get(0);
		EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setTaskID(t1.getId());
		editReportQuantityDTO.setReportId(report.getCode());
		editReportQuantityDTO.setQuantity(-1);
		editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
		ResponseEntity<EditReportQuantityDTO> message = reportRESTController.updateReport2(
				editReportQuantityDTO.getReportId(), editReportQuantityDTO.getTaskID(), editReportQuantityDTO);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}

	@Test
	public void testUpdateReport2() {
		Report report = t1.listAllReports().get(0);
		EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setTaskID(t1.getId());
		editReportQuantityDTO.setReportId(report.getCode());
		editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());

		ResponseEntity<EditReportQuantityDTO> message = reportRESTController.updateReport2(
				editReportQuantityDTO.getReportId(), editReportQuantityDTO.getTaskID(), editReportQuantityDTO);
		assertEquals(HttpStatus.OK, message.getStatusCode());
		assertEquals(editReportQuantityDTO, message.getBody());
	}

	@Test
	public void testUpdateReport2FailInvalidQuantity() {
		Report report = t1.listAllReports().get(0);
		EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setTaskID(t1.getId());
		editReportQuantityDTO.setReportId(report.getCode());
		editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());
		editReportQuantityDTO.setQuantity(-5);

		ResponseEntity<EditReportQuantityDTO> message = reportRESTController.updateReport2(
				editReportQuantityDTO.getReportId(), editReportQuantityDTO.getTaskID(), editReportQuantityDTO);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}

	@Test
	public void testUpdateReport2Fail() {

		EditReportQuantityDTO editReportQuantityDTO = new EditReportQuantityDTO();
		editReportQuantityDTO.setTaskID(t1.getId());
		editReportQuantityDTO.setCollabEmail(projectCollaborator1.getUser().getEmail());

		ResponseEntity<EditReportQuantityDTO> message = reportRESTController.updateReport2(
				editReportQuantityDTO.getReportId(), editReportQuantityDTO.getTaskID(), editReportQuantityDTO);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, message.getStatusCode());
	}


}
