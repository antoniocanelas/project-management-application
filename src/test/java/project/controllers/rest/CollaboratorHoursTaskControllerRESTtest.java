package project.controllers.rest;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import javax.mail.internet.AddressException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

public class CollaboratorHoursTaskControllerRESTtest {

	CollaboratorHoursTaskControllerREST collaboratorHoursTaskControllerREST;
	UserService userService;
	ProjectService projectService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;
	LocalDate birth;

	UserRepositoryClass userRepositoryClass;
	ProjectRepositoryClass projectRepositoryClass;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	TaskRepositoryClass taskRepositoryClass;

	User userProjectManager;
	User userProjectCollaborator;
	User userProjectCollaborator2;

	String userId;
	String userId2;
	ProjectCollaborator projectCollaborator, projectManager;
	ProjectCollaborator projectCollaborator2;
	Project project;
	Project project2;
	Task t1;
	Task t2;
	LocalDateTime d1;
	LocalDateTime d2;
	String expectedMessage, message, taskId;

	@Before
	public void setUp() throws AddressException {
		userRepositoryClass = new UserRepositoryClass();
		projectRepositoryClass = new ProjectRepositoryClass();
		taskRepositoryClass = new TaskRepositoryClass();
		projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

		userService = new UserService(userRepositoryClass);
		projectService = new ProjectService(projectRepositoryClass);
		projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
		taskService = new TaskService(taskRepositoryClass);

		collaboratorHoursTaskControllerREST = new CollaboratorHoursTaskControllerREST(userService, projectService);
		birth = LocalDate.of(1999, 11, 11);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Manuel", "96 452 56 56", "manuel@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
		userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");
		userProjectCollaborator2 = userService.searchUserByEmail("manuel@gmail.com");

		userProjectManager.setProfileCollaborator();
		userProjectCollaborator.setProfileCollaborator();
		userProjectCollaborator2.setProfileCollaborator();
		userId = userProjectCollaborator.getEmail();
		userId2 = userProjectCollaborator2.getEmail();

		userService.updateUser(userProjectManager);
		userService.updateUser(userProjectCollaborator);
		userService.updateUser(userProjectCollaborator2);

		projectService.addProject("1", "Project 1");
		project = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project, userProjectManager);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);
		projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator2, 2);

		projectService.addProject("2", "Project 2");
		project2 = projectService.getProjectByID("2");
		projectCollaboratorService.setProjectManager(project2, userProjectManager);
		projectCollaboratorService.addProjectCollaborator(project2, userProjectCollaborator2, 2);

		projectManager = project.findProjectCollaborator(userProjectManager);
		projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);
		projectCollaborator2 = project.findProjectCollaborator(userProjectCollaborator2);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		taskService.addTask(project, "task1", "task", d1, d2, 1.2, 1.3);
		taskService.addTask(project, "task2", "task", d1, d2, 1.2, 1.3);
		t1 = taskService.findTaskByID("1-1");
		t2 = taskService.findTaskByID("1-2");
	}

	@Test
	public void testGetUsersHoursCompletedTasksLastMonth() {

		// GIVEN A project collaborator associated to two tasks on project 1.
		// This tasks have reports made by this project collaborator and are completed.

		int y1 = LocalDateTime.now().getYear();
		Month m1 = LocalDateTime.now().getMonth();
		LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
		LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
		t1.addProjectCollaborator(projectCollaborator);
		t2.addProjectCollaborator(projectCollaborator);

		TaskCollaboratorRegistry col1TaskRegistry = t1
				.getTaskCollaboratorRegistryByID(t1.getId() + "-" + projectCollaborator.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		TaskCollaboratorRegistry col2TaskRegistry = t2
				.getTaskCollaboratorRegistryByID(t2.getId() + "-" + projectCollaborator.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		t1.addReport(projectCollaborator, 5, d9, d9.plusDays(5));
		t2.addReport(projectCollaborator, 2, d9, d9.plusDays(5));
		t2.setTaskCompleted();
		t2.setEffectiveDateOfConclusion(d10);
		t1.setTaskCompleted();
		t1.setEffectiveDateOfConclusion(d9);

		// WHEN We ask for the number of hours spent by this project collaborator on
		// completed tasks last month.

//		ResponseEntity<Double> result = collaboratorHoursTaskControllerREST
//				.collaboratorHoursInCompletedTasksLastMonth(userId);

		// THEN We get an HTTP status of OK and the number of hours .

//		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void testGetUsersHoursCompletedTasksLastMonth2() {

		// GIVEN A project collaborator not associated to two tasks on project 2.

		int y1 = LocalDateTime.now().getYear();
		Month m1 = LocalDateTime.now().getMonth();
		LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
		LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(2), 18, 0, 0);
		project.getProjectCollaboratorList().add(projectCollaborator2);
		project2.getProjectCollaboratorList().add(projectCollaborator2);
		t1.addReport(projectCollaborator, 5, d9, d9.plusDays(5));
		t2.addReport(projectCollaborator, 2, d9, d9.plusDays(5));
		t2.setTaskCompleted();
		t2.setEffectiveDateOfConclusion(d10);
		t1.setTaskCompleted();
		t1.setEffectiveDateOfConclusion(d9);

		// WHEN We ask for the number of hours spent by this project collaborator on
		// completed tasks last month.

//		ResponseEntity<Double> result = collaboratorHoursTaskControllerREST
//				.collaboratorHoursInCompletedTasksLastMonth(userId2);

		// THEN We get an HTTP status of OK and the number of hours .
//		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void testGetUsersHoursCompletedTasksLastMonthUserNull() {

		// GIVEN A project collaborator null.

		// WHEN We ask for the number of hours spent by this project collaborator on
		// completed tasks last month.

//		ResponseEntity<Double> result = collaboratorHoursTaskControllerREST
//				.collaboratorHoursInCompletedTasksLastMonth(null);

		// THEN We get an HTTP status of BAD REQUEST.

//		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}
}
