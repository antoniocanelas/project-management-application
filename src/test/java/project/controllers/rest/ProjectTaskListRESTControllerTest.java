package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.user.User;
import project.security.UserPrincipal;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectTaskListRESTControllerTest {

	ProjectTaskListRESTController projectTaskListRESTController;

	UserService userService;
	ProjectService projectService;
	TaskService taskService;
	ProjectCollaboratorService projectCollaboratorService;

	UserRepositoryClass userRepositoryClass;
	ProjectRepositoryClass projectRepositoryClass;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
	TaskRepositoryClass taskRepositoryClass;

	LocalDate birth;
	User user1;
	User user2;
	User user3;
	User user4;
	ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

	Project project1, project2;
	Task task1, task2, task3, task4, task5, task6, task7, task8;

	LocalDateTime d1;
	LocalDateTime d2;
	LocalDateTime d3;
	LocalDateTime d4;

	UserPrincipal userPrincipal;

	@Before
	public void setUp() throws AddressException {
		userRepositoryClass = new UserRepositoryClass();
		projectRepositoryClass = new ProjectRepositoryClass();
		projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
		taskRepositoryClass = new TaskRepositoryClass();

		userService = new UserService(userRepositoryClass);
		projectService = new ProjectService(projectRepositoryClass);
		projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
		taskService = new TaskService(taskRepositoryClass);

		projectTaskListRESTController = new ProjectTaskListRESTController(projectService, taskService);

		birth = LocalDate.of(1999, 11, 11);

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		List<String> listProjectWhereProjectManager = new ArrayList<>();
		List<String> listTasksWhereIsProjectManager = new ArrayList<>();
		List<String> listTasksWhereIsTaskCollaborator = new ArrayList<>();
		userPrincipal = UserPrincipal.create(user1, listProjectWhereProjectManager, listTasksWhereIsTaskCollaborator,
				listTasksWhereIsProjectManager);

		user1.setProfileCollaborator();
		user2.setProfileCollaborator();
		user3.setProfileCollaborator();
		user4.setProfileCollaborator();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(user3);
		userService.updateUser(user4);

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		/*
		 * Project 1 id= "1"
		 *
		 * user1 is project manager users 2, 3 and 4 are added to the project and become
		 * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
		 * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
		 *
		 * project 1 has 6 tasks
		 *
		 * tasks 1 and 2 have reports tasks 1 and 2 are pending
		 *
		 * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
		 * and no end date
		 *
		 * tasks 5 and 6 have reports and are concluded
		 *
		 *
		 * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
		 * in task 2 has 4 reports of 4,8,8,4 hours
		 *
		 * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
		 * report in task5 1 report in task6
		 *
		 * projectCollaborator4 has no assigned tasks
		 *
		 *
		 */
		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

		projectService.addProject("1", "Project 1");
		projectService.addProject("2", "Project 2");
		project1 = projectService.getProjectByID("1");
		project2 = projectService.getProjectByID("2");
		projectCollaboratorService.setProjectManager(project1, user1);
		projectCollaboratorService.setProjectManager(project2, user1);

		project1.setStartDate(d1);
		project2.setStartDate(d1);

		projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
		projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
		projectCollaboratorService.addProjectCollaborator(project1, user4, 5);
		projectCollaboratorService.addProjectCollaborator(project2, user2, 5);

		projectManager = project1.findProjectCollaborator(user1);
		projectCollaborator2 = project1.findProjectCollaborator(user2);
		projectCollaborator3 = project1.findProjectCollaborator(user3);
		projectCollaborator4 = project1.findProjectCollaborator(user4);

		taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
		taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
		taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
		taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);
		taskService.addTask(project2, "task7", "task7", d1, d2, 1.2, 1.2);
		taskService.addTask(project2, "task8", "task8", d1, d2, 1.2, 1.2);

		task1 = project1.findTaskByTitle("task1");
		task2 = project1.findTaskByTitle("task2");
		task3 = project1.findTaskByTitle("task3");
		task4 = project1.findTaskByTitle("task4");
		task5 = project1.findTaskByTitle("task5");
		task6 = project1.findTaskByTitle("task6");
		task7 = project2.findTaskByTitle("task7");
		task8 = project2.findTaskByTitle("task8");

		task1.addProjectCollaborator(projectCollaborator2);
		task2.addProjectCollaborator(projectCollaborator2);
		task3.addProjectCollaborator(projectCollaborator2);
		task5.addProjectCollaborator(projectCollaborator2);
		task7.addProjectCollaborator(projectCollaborator2);

		projectService.updateProject(project1);
		project1 = projectService.getProjectByID("1");

		projectService.updateProject(project2);
		project2 = projectService.getProjectByID("2");

		projectService.updateProject(project1);
		projectService.updateProject(project2);
		taskService.updateTask(task1);
		taskService.updateTask(task2);
		taskService.updateTask(task3);
		taskService.updateTask(task4);
		taskService.updateTask(task5);
		taskService.updateTask(task6);
		taskService.updateTask(task7);

	}

	@Test
	public void testListProjectCompletedTasks() {
		d3 = LocalDateTime.of(2018, 05, 15, 0, 0);
		d4 = LocalDateTime.of(2018, 07, 11, 0, 0);
		task5.setEffectiveStartDate(d3.minusMonths(2));
		task5.setTaskCompleted();
		task5.setEffectiveDateOfConclusion(d3);

		task6.addProjectCollaborator(projectCollaborator4);
		task6.setEffectiveStartDate(d4.minusMonths(2));
		task6.setTaskCompleted();
		task6.setEffectiveDateOfConclusion(d4);

		taskService.updateTask(task5);
		taskService.updateTask(task6);

		List<TaskRestDTO> taskRestDTO = new ArrayList<>();

		taskRestDTO.add(task5.toDTO());
		taskRestDTO.add(task6.toDTO());

		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.listProjectCompletedTasks("1",
				userPrincipal);

		assertEquals(expected.toString(), result.toString());

		task6.removeProjectCollaborator(projectCollaborator4);
	}

	@Test
	public void testListProjectCompletedTasksLastMonth() {
		d3 = LocalDateTime.of(2018, 05, 15, 0, 0);
		d4 = LocalDateTime.of(2018, 07, 11, 0, 0);

		task5.setEffectiveStartDate(d3.minusMonths(2));
		task5.setTaskCompleted();
		task5.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(1));

		task6.addProjectCollaborator(projectCollaborator4);
		task6.setEffectiveStartDate(d4.minusMonths(2));
		task6.setTaskCompleted();
		task6.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(3));

		taskService.updateTask(task5);
		taskService.updateTask(task6);

		List<TaskRestDTO> taskRestDTO = new ArrayList<>();
		taskRestDTO.add(task5.toDTO());

		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectCompletedTasksLastMonth("1",
				userPrincipal);

		assertEquals(expected.toString(), result.toString());

		task6.removeProjectCollaborator(projectCollaborator4);
	}

	@Test
	public void testGetProjectInitiatedButNotCompletedTasks() {

		task1.setEffectiveStartDate(d1);
		task2.setEffectiveStartDate(d1);
		task3.setEffectiveStartDate(d1);
		task2.setTaskCompleted();

		taskService.updateTask(task1);
		taskService.updateTask(task3);

		List<TaskRestDTO> taskRestDTO = new ArrayList<>();
		taskRestDTO.add(task1.toDTO());
		taskRestDTO.add(task3.toDTO());

		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController
				.getProjectInitiatedButNotCompletedTasks("1", userPrincipal);

		assertEquals(expected.toString(), result.toString());

	}

	@Test
	public void testGetProjectCancelledTasks() {

		task1.setEffectiveStartDate(d1);
		task2.setEffectiveStartDate(d1);
		task3.setEffectiveStartDate(d1);
		task2.setTaskCompleted();
		task1.setTaskCancelled();
		task3.setTaskCancelled();

		taskService.updateTask(task1);
		taskService.updateTask(task3);

		List<TaskRestDTO> taskRestDTO = new ArrayList<>();
		taskRestDTO.add(task1.toDTO());
		taskRestDTO.add(task3.toDTO());

		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectCancelledTasks("1",
				userPrincipal);

		assertEquals(expected.toString(), result.toString());

	}

	@Test
	public void testGetProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks() {

		task1.setEffectiveStartDate(d1);
		task1.setEffectiveDateOfConclusion(d2);
		task2.setEffectiveStartDate(d1);
		task3.setEffectiveStartDate(d1);
		task3.setEffectiveDateOfConclusion(d2);
		task2.setTaskCompleted();

		taskService.updateTask(task1);
		taskService.updateTask(task3);

		List<TaskRestDTO> taskRestDTO = new ArrayList<>();
		taskRestDTO.add(task4.toDTO());
		taskRestDTO.add(task5.toDTO());
		taskRestDTO.add(task6.toDTO());

		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(taskRestDTO, HttpStatus.OK);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController
				.getProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks("1", userPrincipal);

		assertEquals(expected.toString(), result.toString());

	}

	/**
	 * Test with success the getProjectUnassignedTasks method
	 * 
	 * GIVEN: a project that exists in the database and that task (task4) and task
	 * (task6) have no assigned project collaborators WHEN: get the list of
	 * unassigned tasks THEN: assert that the project has two unassigned task in the
	 * returned list
	 */
	@Test
	public void testGetProjectUnassignedTasksSuccess() {

		// GIVEN
		assertNotNull(projectService.getProjectByID(project1.getId()));
		assertFalse(task4.hasAssignedProjectCollaborator());
		assertFalse(task6.hasAssignedProjectCollaborator());

		TaskRestDTO task4RestDTO = task4.toDTO();
		TaskRestDTO task6RestDTO = task6.toDTO();
		List<TaskRestDTO> unassigned = new ArrayList<>();
		unassigned.add(task4RestDTO);
		unassigned.add(task6RestDTO);

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectUnassignedTasks(project1.getId(),
				userPrincipal);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(unassigned, HttpStatus.OK);
		assertEquals(expected.toString(), result.toString());
	}

	/**
	 * Test failure the getProjectUnassignedTasks method
	 * 
	 * GIVEN: a request with an empty string passed as project's id WHEN: try to get
	 * the list of unassigned tasks THEN: return a HttpStatus bad request
	 */
	@Test
	public void testGetProjectUnassignedTasksEmptyFail() {

		// GIVEN
		String projectId = "";
		assertTrue(projectId.isEmpty());

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectUnassignedTasks(projectId,
				userPrincipal);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected, result);
	}

	/**
	 * Test failure the getProjectUnassignedTasks method
	 * 
	 * GIVEN: a request with a null object passed as project's id WHEN: try to get
	 * the list of unassigned tasks THEN: return a HttpStatus bad request
	 */
	@Test
	public void testGetProjectUnassignedTasksNullFail() {

		// GIVEN
		String projectId = null;
		assertTrue(projectId == null);

		// WHEN
		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectUnassignedTasks(projectId,
				userPrincipal);

		// THEN
		ResponseEntity<List<TaskRestDTO>> expected = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		assertEquals(expected, result);
	}

	@Test
	public void testGetProjectTasks() {

		TaskRestDTO task1RestDTO = task1.toDTO();
		TaskRestDTO task2RestDTO = task2.toDTO();
		TaskRestDTO task3RestDTO = task3.toDTO();
		TaskRestDTO task4RestDTO = task4.toDTO();
		TaskRestDTO task5RestDTO = task5.toDTO();
		TaskRestDTO task6RestDTO = task6.toDTO();

		List<TaskRestDTO> expected = new ArrayList<>();
		expected.add(task1RestDTO);
		expected.add(task2RestDTO);
		expected.add(task3RestDTO);
		expected.add(task4RestDTO);
		expected.add(task5RestDTO);
		expected.add(task6RestDTO);

		HttpEntity<List<TaskRestDTO>> result = projectTaskListRESTController.getProjectTasks("1", userPrincipal);

		ResponseEntity<List<TaskRestDTO>> expected1 = new ResponseEntity<>(expected, HttpStatus.OK);
		assertEquals(expected1.toString(), result.toString());

	}
}
