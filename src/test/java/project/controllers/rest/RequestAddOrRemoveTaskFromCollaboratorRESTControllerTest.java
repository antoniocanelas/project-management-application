package project.controllers.rest;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import project.dto.rest.TaskProjectCollaboratorRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;

public class RequestAddOrRemoveTaskFromCollaboratorRESTControllerTest {

    RequestAddOrRemoveTaskFromCollaboratorRESTController requestAddOrRemoveTaskFromCollaboratorRESTController;
    TaskProjectCollaboratorService taskProjectCollaboratorService;
    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    TaskCollaboratorRegistry taskCollaboratorRegistry;

    LocalDate birth;
    User userProjectManager;
    User userProjectCollaborator;
    ProjectCollaborator projectCollaborator, projectManager;
    Project project;
    Task task;
    LocalDateTime d1;
    LocalDateTime d2;
    TaskProjectCollaboratorRestDTO taskProjectCollaboratorRestDTO;
    String expectedMessage, message, taskId;

    @Before
    public void setUp() throws AddressException {

        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        taskProjectCollaboratorService = new TaskProjectCollaboratorService(taskRepositoryClass, projectCollaboratorRepositoryClass, projectRepositoryClass);

        requestAddOrRemoveTaskFromCollaboratorRESTController = new RequestAddOrRemoveTaskFromCollaboratorRESTController(taskProjectCollaboratorService);
        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        userProjectManager = userService.searchUserByEmail("asdrubal@jj.com");
        userProjectCollaborator = userService.searchUserByEmail("joaquim@gmail.com");

        userProjectManager.setProfileCollaborator();
        userProjectCollaborator.setProfileCollaborator();

        userService.updateUser(userProjectManager);
        userService.updateUser(userProjectCollaborator);

        projectService.addProject("1", "Project 1");
        project = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project, userProjectManager);
        projectCollaboratorService.addProjectCollaborator(project, userProjectCollaborator, 2);

        projectManager = project.findProjectCollaborator(userProjectManager);
        projectCollaborator = project.findProjectCollaborator(userProjectCollaborator);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project, "task", "task", d1, d2, 1.2, 1.3);
        task = taskService.findTaskByID("1-1");
        taskProjectCollaboratorRestDTO = new TaskProjectCollaboratorRestDTO();
        taskProjectCollaboratorRestDTO.setProjectCollaboratorId(projectCollaborator.getId());
        taskProjectCollaboratorRestDTO.setEmail(projectCollaborator.getUser().getEmail());

        taskId = task.getId();
        taskProjectCollaboratorRestDTO.setTaskId(taskId);
        
    }

    @Test
    @Transactional
    public void requestAddProjectCollaboratorToTaskSucess() {

        //Given:
        //When:
        ResponseEntity<TaskProjectCollaboratorRestDTO> add = requestAddOrRemoveTaskFromCollaboratorRESTController.requestAddTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        //Then:
        assertEquals(HttpStatus.OK, add.getStatusCode());
    }

    @Test
    @Transactional
    public void addProjectCollaboratorToTaskFail() {

        //Given:
        requestAddOrRemoveTaskFromCollaboratorRESTController.requestAddTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        //When:
        ResponseEntity<TaskProjectCollaboratorRestDTO> add = requestAddOrRemoveTaskFromCollaboratorRESTController.requestAddTaskToCollaborator(taskProjectCollaboratorRestDTO, taskId);
        //Then:
        assertEquals(HttpStatus.BAD_REQUEST, add.getStatusCode());
    }
    
    

	/**
	 * GIVEN: A task with a project collaborator
	 * WHEN: A request to remove project collaborator from task is made
	 * THEN: We get an HTTP status of OK and the request was send to project manager to remove project collaborator from the task active collaborator list
	 */
    
    @Test
    @Transactional
    public void requestRemoveTaskFromCollaboratorSucess() {
    	
    	//Given
    	taskService.addProjectCollaboratorToTask(projectCollaborator, task);
		taskProjectCollaboratorService.addProjectCollaboratorToTask(taskProjectCollaboratorRestDTO);
    	
        //When
		ResponseEntity<TaskProjectCollaboratorRestDTO> result= requestAddOrRemoveTaskFromCollaboratorRESTController.requestRemoveTaskFromCollaborator(taskProjectCollaboratorRestDTO, taskId);
		
		//Then       
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

    }
    
    
    /**
	 * GIVEN: 
	 * WHEN: A request to remove project collaborator from task is made
	 * THEN: We get an HTTP status of BAD_REQUEST and the request wasn't send to project manager to remove project collaborator from the task active collaborator list
	 */

    @Test
    @Transactional
    public void removeTaskFromCollaboratorFail() {

    	//Given

    	//When
    	ResponseEntity<TaskProjectCollaboratorRestDTO> result=requestAddOrRemoveTaskFromCollaboratorRESTController.requestRemoveTaskFromCollaborator(taskProjectCollaboratorRestDTO, taskId);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}