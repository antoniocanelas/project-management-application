package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.controllers.consolecontrollers.AddOrRemoveTaskFromCollaboratorController;
import project.controllers.consolecontrollers.TaskStateController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

public class AddOrRemoveTaskFromCollaboratorControllerTest {

    TaskStateController tsc;

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Task task1, task2, task3, task4, task5, task6;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;

    AddOrRemoveTaskFromCollaboratorController artfc;

    @Before
    public void initialize() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        artfc = new AddOrRemoveTaskFromCollaboratorController(userService, projectService, taskService, projectCollaboratorService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator4 = project1.findProjectCollaborator(user4);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);

        task1 = taskService.findTaskByTitle("task1");
        task2 = taskService.findTaskByTitle("task2");
        task3 = taskService.findTaskByTitle("task3");
        task4 = taskService.findTaskByTitle("task4");
        task5 = taskService.findTaskByTitle("task5");
        task6 = taskService.findTaskByTitle("task6");

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);
        task5.addProjectCollaborator(projectCollaborator2);

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

    }

    @Test
    @Transactional
    public void testAddTaskToCollaboratorSuccess() {
        String expected = "ROLE_COLLABORATOR added to Task " + task4.getId();
        String result = artfc.addTaskToCollaborator("1", task4.getId(), "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testAddTaskToCollaboratorFail() {
        String expected = "Can not add Colaborator to this Task";
        String result = artfc.addTaskToCollaborator("1", task5.getId(), "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testAddTaskToCollaboratorNullEmail() {
        String expected = "Can not add Colaborator to this Task";
        String result = artfc.addTaskToCollaborator("1", task5.getId(), null);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testAddTaskToCollaboratorUserNull() {
        String expected = "Can not add Colaborator to this Task";
        String result = artfc.addTaskToCollaborator("1", task5.getId(), "aaa@jj.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testAddTaskToCollaboratorNullTask() {
        String expected = "Can not add Colaborator to this Task";
        String result = artfc.addTaskToCollaborator("1", null, "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRemoveTaskFromCollaboratorSuccess() {
        String expected = "ROLE_COLLABORATOR was removed to Task " + task5.getId();
        String result = artfc.removeTasktoCollaborator("1", task5.getId(), "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRemoveTaskFromCollaboratorFail() {
        String expected = "Can not remove Colaborator to this Task";
        String result = artfc.removeTasktoCollaborator("1", task6.getId(), "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRemoveTaskFromCollaboratorEmailNull() {
        String expected = "Can not remove Colaborator to this Task";
        String result = artfc.removeTasktoCollaborator("1", task6.getId(), null);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRemoveTaskFromCollaboratorNull() {
        String expected = "Can not remove Colaborator to this Task";
        String result = artfc.removeTasktoCollaborator("1", task6.getId(), "aaa@jj.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testRemoveTaskFromCollaboratorNullTask() {
        String expected = "Can not remove Colaborator to this Task";
        String result = artfc.removeTasktoCollaborator("1", null, "joaquim@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorsNotInTask() {

        StringBuilder sb = new StringBuilder();
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator3.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator3.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator3.getCurrentCost() + "\n");
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator4.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator4.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator4.getCurrentCost() + "\n");

        String expected = sb.toString();
        String result = artfc.listCollaboratorsNotInTask("1", task1.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorsNotInTaskEmpty() {

        task1.addProjectCollaborator(projectCollaborator3);
        task1.addProjectCollaborator(projectCollaborator4);

        String expected = "There are no collaborators to add to this task";
        String result = artfc.listCollaboratorsNotInTask("1", task1.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorsNotInTaskNoPM() {

        task1.addProjectCollaborator(projectManager);
        StringBuilder sb = new StringBuilder();
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator3.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator3.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator3.getCurrentCost() + "\n");
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator4.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator4.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator4.getCurrentCost() + "\n");
        String expected = sb.toString();
        String result = artfc.listCollaboratorsNotInTask("1", task1.getId());

        assertEquals(expected, result);
    }
    
    @Test
    @Transactional
    public void testListCollaboratorsInTask_PM() {

    	projectCollaboratorService.setProjectManager(project1, user2);
        task1.addProjectCollaborator(projectCollaborator2);

        String expected = "There are no active colaborators associated to this task";
        String result = artfc.listActiveCollaboratorsInTask("1", task1.getId());

        assertEquals(expected, result);
    }
    
    @Test
    @Transactional
    public void testListCollaboratorsInTask() {
        task1.addProjectCollaborator(projectCollaborator4);
        StringBuilder sb = new StringBuilder();
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator2.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator2.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator2.getCurrentCost() + "\n");
        sb.append("\nROLE_COLLABORATOR name " + projectCollaborator4.getUser().getName());
        sb.append("\nROLE_COLLABORATOR email" + ": " + projectCollaborator4.getUser().getEmail());
        sb.append("\nROLE_COLLABORATOR cost" + ": " + projectCollaborator4.getCurrentCost() + "\n");

        String expected = sb.toString();
        String result = artfc.listActiveCollaboratorsInTask("1", task1.getId());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorsInTaskEmpty() {

        task1.removeProjectCollaborator(projectCollaborator2);

        String expected = "There are no active colaborators associated to this task";
        String result = artfc.listActiveCollaboratorsInTask("1", task1.getId());

        assertEquals(expected, result);
    }

}
