package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.CollaboratorHoursTaskController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CollaboratorHoursControllerTest {

    CollaboratorHoursTaskController chtc;

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    TaskService taskService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Task task1, task2, task3, task4, task5, task6;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        chtc = new CollaboratorHoursTaskController(userService, projectService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);
        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");

        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator4 = project1.findProjectCollaborator(user4);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");

        task1.addProjectCollaborator(projectCollaborator2);
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col1TaskRegistry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        task5.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col2TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");
    }

    @Test
    @Transactional
    public void testcalculateCollaboratorsHoursCompletedTasksLastMonthInAllProjectsSuccess() {

        String email = "joaquim@gmail.com";
        task5.setTaskCompleted();
        task5.setEffectiveDateOfConclusion(LocalDateTime.of(y1, m1.minus(1), 22, 0, 0));
        taskService.updateTask(task5);

        String expected = "Total hours spent in all projects: 5.0";
        String result = chtc.calculateCollaboratorsHoursCompletedTasksLastMonthInAllProjects(email);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testcalculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjectsSuccess() {

        String email = "joaquim@gmail.com";
        task3.setTaskCompleted();
        task3.setEffectiveDateOfConclusion(LocalDateTime.of(y1, m1.minus(1), 22, 0, 0));
        task5.setTaskCompleted();
        task5.setEffectiveDateOfConclusion(LocalDateTime.of(y1, m1.minus(1), 22, 0, 0));
        taskService.updateTask(task3);
        taskService.updateTask(task5);

        String expected = "Average hours per completed task was: 6.0";
        String result = chtc.calculateCollaboratorsAverageHoursCompletedTasksLastMonthInAllProjects(email);

        assertEquals(expected, result);
    }

}
