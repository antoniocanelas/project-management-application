package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.controllers.consolecontrollers.AccountController;
import project.model.UserService;
import jparepositoriestest.UserRepositoryClass;
import project.model.user.User;
import system.dto.LoginTokenDTO;


public class AccountControllerTest {

    private UserService userService;
    private UserRepositoryClass userRepoClass;
    private AccountController ac;
    LocalDate birth = LocalDate.of(1999, 11, 11);
    private User user1;

    @Before
    public void setUp() throws AddressException {
    	userRepoClass = new UserRepositoryClass();
    	userService = new UserService(userRepoClass);

        LocalDate birth = LocalDate.of(1999, 11, 11);
        
        userService.addUser("Asdrubal", "99 555 666 28", "asdrubal@jj.com", "06555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        User user1 = userService.searchUserByEmail("asdrubal@jj.com");

        user1.setPassword("12345");
        user1.setProfileDirector();
        userService.updateUser(user1);
        user1 = userService.searchUserByEmail("asdrubal@jj.com");
    }

    @Test
    public void testAccountControllerCompany() {
        ac = new AccountController(userService);
        assertTrue(ac instanceof AccountController);
    }

    @Test
    public void testLoginSuccess() {
        ac = new AccountController(userService);

        LoginTokenDTO result = ac.login("asdrubal@jj.com", "12345");

        assertTrue(result.getMessage().equals(result.getProfile().toString() +
                " " + result.getName() + " SUCCESSFULLY LOGGED"));

    }

    @Test
    public void testLoginFailureNoUserByThatEmail() {
        ac = new AccountController(userService);

        LoginTokenDTO result = ac.login("asdrubaaal@jj.com", "12345");

        assertTrue(result.getMessage().equals("Invalid Email or Password"));
    }

    @Test
    public void testLoginFailureInvalidPassword() {
        ac = new AccountController(userService);

        LoginTokenDTO result = ac.login("asdrubal@jj.com", "SWitCH");

        assertTrue(result.getMessage().equals("Invalid Email or Password"));
    }
    
    @Test
    public void testApproveAccount()
    {
    	ac = new AccountController(userService);
    	user1 = userService.searchUserByEmail("asdrubal@jj.com");
    	user1.setInactive();
    	assertEquals(null, user1.getUserLoginData().getApprovedDateTime());
    	
    	ac.approveAccount("asdrubal@jj.com");
    	assertTrue(user1.isActive());
    	assertTrue(user1.getUserLoginData().getApprovedDateTime()!=null);
    }
    
    @Test
    public void testUpdateLastLoginDate()
    {
    	ac = new AccountController(userService);
    	user1 = userService.searchUserByEmail("asdrubal@jj.com");
    	assertNull(user1.getUserLoginData().getLastLoginDate());
    	
    	ac.updateLastLoginDate("asdrubal@jj.com");
    	assertTrue(user1.getUserLoginData().getLastLoginDate()!=null);
    }
}
