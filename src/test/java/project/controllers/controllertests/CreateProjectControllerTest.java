package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;

import project.controllers.consolecontrollers.CreateProjectController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.services.ProjectCollaboratorService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class CreateProjectControllerTest {

    UserService userService;
    ProjectService projectService;
    ProjectCollaboratorService projectCollaboratorService;
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    CreateProjectController createProjectController;
    User user1, user2;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        createProjectController = new CreateProjectController(userService, projectService, projectCollaboratorService);

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789",
                LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12),
                "2", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789",
                LocalDate.of(1978, 11, 12), "2", "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user1.setProfileCollaborator();
        user2 = userService.searchUserByEmail("manel@jj.com");
        user2.setProfileDirector();
        projectService.addProject("55", "Project5");
        Project project = projectService.getProjectByID("55");
        userService.updateUser(user1);
        userService.updateUser(user2);
        projectService.updateProject(project);

    }

    @Test
    @Transactional
    public void testSuccess() {
        createProjectController.addProject("10", "teste 10", "asdrubal@jj.com");

        List<User> expectedUser = new ArrayList<>();
        expectedUser.add(user1);
        List<Project> expectedProject = new ArrayList<>();
        expectedProject.add(projectService.getProjectByID("55"));
        expectedProject.add(projectService.getProjectByID("10"));

        assertThat(expectedProject, containsInAnyOrder(projectService.getProjectList().toArray()));
        assertThat(expectedUser, containsInAnyOrder(projectService.listAllProjectManager().toArray()));
    }

    @Test
    @Transactional
    public void testFail() {
        createProjectController.addProject("11", "teste fail", "asdrubal@jj.com");

        List<User> expectedInvalidUser = new ArrayList<>();
        expectedInvalidUser.add(user2);

        assertNotEquals(expectedInvalidUser, projectService.listAllProjectManager());
    }

    @Test
    @Transactional
    public void testFailUserNull() {
        String expected = "\nProject was not created because one project already exists with the same id "
                + "or project manager's email isn't valid";
        assertEquals(expected, createProjectController.addProject("11", "teste fail", "userNull@jj.com"));
    }

    @Test
    @Transactional
    public void testProjectManagerEmailInvalidFail() {
        user2 = userService.searchUserByEmail("joaquim@jj.com");
        user2.setProfileRegisteredUser();
        createProjectController.addProject("11", "teste fail", "joaquim@jj.co");
        List<User> expectedInvalidUser = new ArrayList<>();
        assertEquals(expectedInvalidUser, projectService.listAllProjectManager());
    }

    @Test
    @Transactional
    public void testProjectIdAlreadexistsFail() {
        createProjectController.addProject("55", "teste fail", "asdrubal@jj.com");
        assertTrue(projectService.getProjectList()
                .contains(projectService.getProjectByID("55")));
    }
}