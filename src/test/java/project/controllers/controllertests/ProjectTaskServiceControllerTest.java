package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.ProjectTaskListController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProjectTaskServiceControllerTest {

    ProjectTaskListController tlc;

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1, project2;
    Task task1, task2, task3, task4, task5, task6, task7, task8;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    LocalDateTime d5;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        tlc = new ProjectTaskListController(projectService,taskService);

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */
        int y1 = LocalDateTime.now().plusYears(1).getYear();

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(y1, 9, 11, 0, 0);
        d5 = LocalDateTime.of(2017, 10, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        projectService.addProject("2", "Project 2");
        project1 = projectService.getProjectByID("1");
        project2 = projectService.getProjectByID("2");
        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.setProjectManager(project2, user1);

        project1.setStartDate(d1);
        project2.setStartDate(d1);

        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);
        projectCollaboratorService.addProjectCollaborator(project2, user2, 5);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator2.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator3.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());
        projectCollaborator4 = project1.findProjectCollaborator(user4);
        projectCollaborator4.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);
        taskService.addTask(project2, "task7", "task7", d1, d2, 1.2, 1.2);
        taskService.addTask(project2, "task8", "task8", d1, d2, 1.2, 1.2);

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");
        task7 = project2.findTaskByTitle("task7");
        task8 = project2.findTaskByTitle("task8");

        task1.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col1TaskRegistry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        task2.addProjectCollaborator(projectCollaborator2);
        task3.addProjectCollaborator(projectCollaborator2);
        task5.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col2TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        task7.addProjectCollaborator(projectCollaborator2);

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

        projectService.updateProject(project2);
        project2 = projectService.getProjectByID("2");

        projectService.updateProject(project1);
        projectService.updateProject(project2);

        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);
        taskService.updateTask(task6);
        taskService.updateTask(task7);

    }

    @Test
    @Transactional
    public void testGetCompletedTasksSuccess() {
        d3 = LocalDateTime.of(2018, 05, 15, 0, 0);
        d4 = LocalDateTime.of(2018, 07, 11, 0, 0);
        task5.setEffectiveStartDate(d3.minusMonths(2));
        task5.setTaskCompleted();
        task5.setEffectiveDateOfConclusion(d3);

        task6.addProjectCollaborator(projectCollaborator4);
        task6.setEffectiveStartDate(d4.minusMonths(2));
        task6.setTaskCompleted();
        task6.setEffectiveDateOfConclusion(d4);

        List<Task> expected = new ArrayList<>();

        expected.add(task5);
        expected.add(task6);
        String taskListOf = "               LIST OF COMPLETED TASKS FOR PROJECT ";
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        sb.append("\n\n" + taskListOf + project1.getId() + "\n" + "_____________________________________________________________________" + "\n\n");
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.listProjectManagerCompletedTasks("1").toString());

        task6.removeProjectCollaborator(projectCollaborator4);
    }

    @Test
    @Transactional
    public void testGetProjectManagerTasksNoCollaboratorSuccess() {

        List<Task> expected = new ArrayList<>();
        expected.add(task4);
        expected.add(task6);
        String taskListOf = "\n\n     LIST OF TASKS WITHOUT ASSIGNED COLLABORATORS FOR PROJECT 1" + "\n_____________________________________________________________________" + "\n\n";
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        sb.append(taskListOf);
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.lisProjectManagerTasksNoCollaborator("1"));

    }

    @Test
    @Transactional
    public void testGetProjectManagerGetInitiatedButNotCompletedTasks() {

        task1.setEffectiveStartDate(d1);
        task2.setEffectiveStartDate(d1);
        task3.setEffectiveStartDate(d1);
        task2.setTaskCompleted();

        List<Task> expected = new ArrayList<>();
        expected.add(task1);
        expected.add(task3);

        String taskListOf = "\n\n          LIST OF TASKS THAT ARE IN PROGRESS FOR PROJECT 1" + "\n_____________________________________________________________________" + "\n\n";
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        sb.append(taskListOf);

        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.listProjectManagerGetInitiatedButNotCompletedTasks("1").toString());

    }

    @Test
    @Transactional
    public void testGetProjectManagerGetNotInitiatedTasks() {

        task1.setEffectiveStartDate(d1);
        task3.setEffectiveStartDate(d1);

        List<Task> expected = new ArrayList<>();
        expected.add(task2);
        expected.add(task4);
        expected.add(task5);
        expected.add(task6);
        String taskListOf = "\n\n\n\n_____________________________________________________________________" + "\n\n       LIST OF NOT INITIATED TASKS NOT COMPLETED FOR PROJECT 1" + "\n_____________________________________________________________________" + "\n\n";
        StringBuilder sb = new StringBuilder();
        sb.append(taskListOf);
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.listProjectManagerGetNotInitiatedTasks("1").toString());

    }

    @Test
    @Transactional
    public void testGetProjectManagerNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks() {

        task1.setPredictedDateOfConclusion(d1);
        task3.setPredictedDateOfConclusion(d1);

        List<Task> expected = new ArrayList<>();
        expected.add(task1);
        expected.add(task3);
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        String taskListOf = "\n\nLIST OF TASKS NOT COMPLETED BUT WITH CONCLUSION EXPIRED FOR PROJECT 1\n" + "_____________________________________________________________________" + "\n\n";
        sb.append(taskListOf);
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(),
                tlc.listProjectManagerNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks("1").toString());
    }

    @Test
    @Transactional
    public void testGetProjectTasksList() {

        List<Task> expected = new ArrayList<>();
        expected.add(task1);
        expected.add(task2);
        expected.add(task3);
        expected.add(task4);
        expected.add(task5);
        expected.add(task6);

        String taskListOf = "\n\n\n\n" + "_____________________________________________________________________" + "\n\n                LIST TASKS FOR PROJECT 1" + "\n_____________________________________________________________________" + "\n\n";
        StringBuilder sb = new StringBuilder();
        sb.append(taskListOf);
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.listProjectTasksList("1").toString());
    }

    @Test
    @Transactional
    public void testListProjectManagerCancelledTasks() {

        List<Task> cancelledTasks = new ArrayList<>();

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        assertTrue(task2.getTaskState().isOnReadyToStartState());
        assertTrue(task4.getTaskState().isOnPlannedState());
        task1.addReport(projectCollaborator2, 2, d1, d1.plusDays(5));
        assertTrue(task1.getTaskState().isOnInProgressState());
        task1.setTaskCancelled();
        assertTrue(task1.getTaskState().isOnCancelledState());
        cancelledTasks.add(task1);
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        String taskListOf = "\n\n                LIST OF CANCELLED TASKS FOR PROJECT 1\n" + "_____________________________________________________________________" + "\n\n";
        sb.append(taskListOf);
        for (Task t : cancelledTasks) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
            sb.append("_____________________________________________________________________");
        }

        assertEquals(sb.toString(), tlc.listProjectManagerCancelledTasks("1").toString());
    }

    @Test
    @Transactional
    public void testListNotInitiatedTask() {
        List<Task> expected = new ArrayList<>();
        assertTrue(task1.getTaskState().isOnReadyToStartState());
        assertTrue(task2.getTaskState().isOnReadyToStartState());
        assertTrue(task3.getTaskState().isOnReadyToStartState());
        assertTrue(task4.getTaskState().isOnPlannedState());
        assertTrue(task5.getTaskState().isOnReadyToStartState());
        assertTrue(task6.getTaskState().isOnPlannedState());
        task5.addReport(projectCollaborator2, 4, d5, d5.plusDays(5));
        task5.setTaskCompleted();
        assertTrue(task5.getTaskState().isOnCompletedState());

        expected.add(task1);
        expected.add(task2);
        expected.add(task3);
        expected.add(task4);
        expected.add(task6);

        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("_____________________________________________________________________");
        sb.append("\n\n          LIST OF NOT INITIATED TASKS FOR PROJECT 1\n" + "_____________________________________________________________________" + "\n\n");
        for (Task t : expected) {
            sb.append("Task ID: " + t.getId() + "\t");
            sb.append("Task Title: " + t.getTitle() + "\t");
            sb.append("Task Status: " + t.getStatus() + "\n");
            sb.append("\t\tTask Predicted Start Date: " + t.getPredictedDateOfStart() + "\n");
            sb.append("\t\tTask Predicted End Date: " + t.getPredictedDateOfConclusion() + "\n");
            sb.append("\t\tTask Effective Start Date: " + t.getEffectiveStartDate() + "\n");
            sb.append("\t\tTask Effective End Date: " + t.getEfectiveDateOfConclusion() + "\n\n");
        }
        sb.append("_____________________________________________________________________");

        assertEquals(sb.toString(), tlc.listNotInitiatedTask("1").toString());
    }

    @Test
    @Transactional
    public void testRemoveNotInitiatedTaskValid() {
        assertTrue(task5.getTaskState().isOnReadyToStartState());
        String expected = "TASK WITH ID " + task5.getId() + " SUCCESSFULLY REMOVED FROM PROJECT 1.";

        assertEquals(expected, tlc.removeNotInitiatedTask(task5.getId(), project1.getId()));
    }

    @Test
    @Transactional
    public void testRemoveNotInitiatedTaskInvalid() {
        assertTrue(task5.getTaskState().isOnReadyToStartState());
        task5.addReport(projectCollaborator2, 4, d5, d5.plusDays(5));
        task5.setTaskCompleted();
        String expected = "TASK WAS NOT REMOVED.";

        assertEquals(expected, tlc.removeNotInitiatedTask(task5.getId(), project1.getId()));
    }

    @Test
    @Transactional
    public void testChangeCompletedTaskToInProgressStateSucess() {
        assertTrue(task5.getTaskState().isOnReadyToStartState());
        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));

        assertTrue(task5.getTaskState().isOnInProgressState());
        task5.setTaskCompleted();
        assertTrue(task5.getTaskState().isOnCompletedState());
        task5.setEffectiveDateOfConclusion(null);
        String expected = "TASK WITH ID " + task5.getId() + " , IN PROJECT 1 WAS SUCCESSFULLY REVERTED TO SUSPENDED STATE.";

        assertEquals(expected, tlc.changeCompletedTaskToSuspendedState(task5.getId(), project1.getId()));
        assertTrue(task5.getTaskState().isOnSuspendedState());
    }

    @Test
    @Transactional
    public void testChangeCompletedTaskToInProgressStateFail() {
        assertTrue(task5.getTaskState().isOnReadyToStartState());
        String expected = "TASK WAS UNABLE TO REVERT TO SUSPENDED STATE.";
        assertEquals(expected, tlc.changeCompletedTaskToSuspendedState(task5.getId(), project1.getId()));
        assertTrue(task5.getTaskState().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void testChangeTaskCollabStatusToRemovalRequest() {
        // add Colaborator Joana to Task3
        task3.addProjectCollaborator(projectCollaborator3);
        // Check if Colaborator Joaquim is in Task 2 and Task3 and Joana in Task3
        assertTrue(task2.hasProjectCollaborator(projectCollaborator2));
        assertTrue(task3.hasProjectCollaborator(projectCollaborator2));
        assertTrue(task3.hasProjectCollaborator(projectCollaborator3));
        // Check Status of Task-ROLE_COLLABORATOR Relation
        TaskCollaboratorRegistry Collab2inTask2 = task2.listTaskCollaboratorRegistry(projectCollaborator2).get(0);
        TaskCollaboratorRegistry Collab2inTask3 = task3.listTaskCollaboratorRegistry(projectCollaborator2).get(0);
        TaskCollaboratorRegistry Collab3inTask3 = task3.listTaskCollaboratorRegistry(projectCollaborator3).get(0);

        RegistryStatus expected1 = Collab2inTask2.getRegistryStatus();
        RegistryStatus result1 = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED;

        RegistryStatus expected2 = Collab2inTask3.getRegistryStatus();
        RegistryStatus result2 = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED;

        RegistryStatus expected3 = Collab3inTask3.getRegistryStatus();
        RegistryStatus result3 = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED;

        assertEquals(expected1, result1);
        assertEquals(expected2, result2);
        assertEquals(expected3, result3);

        // Send Removal Requests by Joaquim on Task 2, and Joana on Task3
        task2.requestRemoveProjectCollaborator(projectCollaborator2);
        task3.requestRemoveProjectCollaborator(projectCollaborator3);

        RegistryStatus expected4 = Collab2inTask2.getRegistryStatus();
        RegistryStatus result4 = TaskCollaboratorRegistry.RegistryStatus.REMOVALREQUEST;

        RegistryStatus expected5 = Collab2inTask3.getRegistryStatus();
        RegistryStatus result5 = TaskCollaboratorRegistry.RegistryStatus.ASSIGNMENTAPPROVED;

        RegistryStatus expected6 = Collab3inTask3.getRegistryStatus();
        RegistryStatus result6 = TaskCollaboratorRegistry.RegistryStatus.REMOVALREQUEST;

        // Check if Status have Changed correctly
        assertEquals(expected4, result4);
        assertEquals(expected5, result5);
        assertEquals(expected6, result6);

    }

    @Test
    @Transactional
    public void testListProjectManagerRemovalRequests() {
        // add Colaborator Joana to Task3
        task3.addProjectCollaborator(projectCollaborator3);
        // Check if Colaborator Joaquim is in Task 2 and Task3 and Joana in Task3
        assertTrue(task2.hasProjectCollaborator(projectCollaborator2));
        assertTrue(task3.hasProjectCollaborator(projectCollaborator2));
        assertTrue(task3.hasProjectCollaborator(projectCollaborator3));

        // Send Removal Requests by Joaquim on Task 2, and Joana on Task3
        task2.requestRemoveProjectCollaborator(projectCollaborator2);
        task3.requestRemoveProjectCollaborator(projectCollaborator3);

        // Test list Removal Requests

        StringBuilder expected = new StringBuilder();
        String result = tlc.listProjectManagerRemovalRequests(project1.getId());

        expected.append("Removal Request ID: " + 1 + "\n");
        expected.append("Task id: ");
        expected.append(task2.getId() + "\n");
        expected.append("Task tittle: " + task2.getTitle() + "\n");
        expected.append("ROLE_COLLABORATOR: " + projectCollaborator2 + "\n");
        expected.append("Removal Request ID: " + 2 + "\n");
        expected.append("Task id: ");
        expected.append(task3.getId() + "\n");
        expected.append("Task tittle: " + task3.getTitle() + "\n");
        expected.append("ROLE_COLLABORATOR: " + projectCollaborator3 + "\n");

        assertEquals(expected.toString(), result);
    }

    @Test
    @Transactional
    public void testListProjectTasksListFull() {
        task7.addTaskDependency(task8);

        StringBuilder sb = new StringBuilder();
        sb.append("\n\n\n\n");
        sb.append("______________________________________________________________________________________________________");
        sb.append("\n\n                               TASK LIST WITH FULL INFORMATION FOR PROJECT 2" + "\n");
        sb.append("______________________________________________________________________________________________________");
        sb.append("\n\n");
        sb.append("Task ID: " + task7.getId() + "\t");
        sb.append("Task Status: " + task7.getStatus() + "\n");
        sb.append("\t\tTask Title: " + task7.getTitle() + "\t");
        sb.append("Task Description: " + task7.getDescription() + "\n");
        sb.append("\t\tTask Date of Creation: " + task7.getDateOfCreation() + "\n");
        sb.append("\t\tTask Predicted Start Date: " + task7.getPredictedDateOfStart() + "\n");
        sb.append("\t\tTask Predicted End Date: " + task7.getPredictedDateOfConclusion() + "\n");
        sb.append("\t\tTask Effective Start Date: " + task7.getEffectiveStartDate() + "\n");
        sb.append("\t\tTask Effective End Date: " + task7.getEfectiveDateOfConclusion() + "\n");
        sb.append("\t\tActive Collaborators in Task\n");
        sb.append(
                "\t\t\tROLE_COLLABORATOR Name: " + project2.getProjectCollaboratorList().get(1).getUser().getName() + ", ");
        sb.append("ROLE_COLLABORATOR Email: " + project2.getProjectCollaboratorList().get(1).getUser().getEmail()
                + ", ");
        sb.append("ROLE_COLLABORATOR Cost: " + projectCollaborator2.getCurrentCost() + " Euros\n");
        sb.append("\t\tDependency tasks\n");
        sb.append("\t\t\tTask ID: " + task8.getId() + "\t");
        sb.append("\t\t\tTask Title: " + task8.getTitle() + "\t");
        sb.append("\t\tTask Status: " + task8.getStatus() + "\n");
        sb.append("\t\tTask Predicted End Date: " + task8.getPredictedDateOfConclusion() + "\n");
        sb.append("\t\tTask Effective End Date: " + task8.getEfectiveDateOfConclusion() + "\n");
        sb.append("\t\tUnit Cost: " + task7.getUnitCost() + "Euros \t");
        sb.append("Estimated effort: " + task7.getUnitCost() + "\n\n");

        sb.append("Task ID: " + task8.getId() + "\t");
        sb.append("Task Status: " + task8.getStatus() + "\n");
        sb.append("\t\tTask Title: " + task8.getTitle() + "\t");
        sb.append("Task Description: " + task8.getDescription() + "\n");
        sb.append("\t\tTask Date of Creation: " + task8.getDateOfCreation() + "\n");
        sb.append("\t\tTask Predicted Start Date: " + task8.getPredictedDateOfStart() + "\n");
        sb.append("\t\tTask Predicted End Date: " + task8.getPredictedDateOfConclusion() + "\n");
        sb.append("\t\tTask Effective Start Date: " + task8.getEffectiveStartDate() + "\n");
        sb.append("\t\tTask Effective End Date: " + task8.getEfectiveDateOfConclusion() + "\n");
        sb.append("\t\tActive Collaborators in Task\n");
        sb.append("\t\tDependency tasks\n");
        sb.append("\t\tUnit Cost: " + task8.getUnitCost() + "Euros \t");
        sb.append("Estimated effort: " + task8.getUnitCost() + "\n\n");
        sb.append("______________________________________________________________________________________________________");

        assertEquals(sb.toString(), tlc.listProjectTasksListFull("2").toString());
    }

}
