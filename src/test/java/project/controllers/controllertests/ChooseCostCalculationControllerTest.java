package project.controllers.controllertests;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectRepositoryClass;
import project.controllers.consolecontrollers.ChooseCostCalculationController;
import project.model.project.Project;
import project.model.project.ProjectService;

public class ChooseCostCalculationControllerTest {

	ProjectService projectService;
	ProjectRepositoryClass projectRepositoryClass;
	Project project;
	ChooseCostCalculationController costCalculationController;

	@Before
	public void setUp() {

		projectRepositoryClass = new ProjectRepositoryClass();
		projectService = new ProjectService(projectRepositoryClass);
		costCalculationController = new ChooseCostCalculationController(projectService);
	}

	@Test
	@Transactional
	public void testChooseCostCalculationController() {

		costCalculationController = new ChooseCostCalculationController(projectService);

		assertTrue(costCalculationController instanceof ChooseCostCalculationController);
	}

	@Test
	@Transactional
	public void testListProjectCostCalculationOptions() {

		// Given
		String choice1 = "MaximumCost";
		String choice2 = "MinimumCost";
		String choice3 = "AverageCost";

		projectService.addProject("1001", "SETI");
		Project project = projectService.getProjectByID("1001");

		// When
		List<String> expected = project.getCostCalculationOptions();
		expected.add(choice1);
		expected.add(choice2);
		expected.add(choice3);
		project.setCostCalculationOptions(expected);
		projectService.updateProject(project);

		List<String> result = costCalculationController.listProjectCostCalculationOptions(project.getId());

		// Then
		assertThat(expected, containsInAnyOrder(result.toArray()));
	}

	@Test
	@Transactional
	public void testSetProjectCostCalculationMechanism() {

		// Given
		String choice = "MaximumTimePeriodCost";
		projectService.addProject("1001", "SETI");
		Project project = projectService.getProjectByID("1001");

		// When
		costCalculationController.setProjectCostCalculationMechanism(project.getId(), choice);
		// Then
		assertTrue(choice.equals(project.getCostCalculatorPersistence()));
		assertTrue(choice.equals(project.getCostCalculator().getClass().getSimpleName()));
	}

	@Test
	@Transactional
	public void testGetSelectedProjectCostCalculationMechanism() {

		// Given
		String choice = "MaximumTimePeriodCost";
		projectService.addProject("1001", "SETI");
		Project project = projectService.getProjectByID("1001");

		// When
		costCalculationController.setProjectCostCalculationMechanism(project.getId(), choice);
		// Then

		assertTrue(
				choice.equals(costCalculationController.getSelectedProjectCostCalculationMechanism(project.getId())));
	}
}
