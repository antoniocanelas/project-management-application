package project.controllers.controllertests;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.UserAddAddressController;
import project.model.UserService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UserAddAddressControllerTest {

    UserService userService;
    UserRepositoryClass userRepo;

    User user1;
    UserAddAddressController uaec;

    @Before
    public void setUp() throws AddressException {
        userRepo = new UserRepositoryClass();
        userService = new UserService(userRepo);

        uaec = new UserAddAddressController(userService);

        LocalDate birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 28", "asdrubal@jj.com", "06555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        user1 = userService.searchUserByEmail("asdrubal@jj.com");

        user1.setPassword("12345");
        userService.updateUser(user1);
        user1 = userService.searchUserByEmail("asdrubal@jj.com");
    }

    @Test
    @Transactional
    public void testAddAdressSuccess() {

        assertEquals(1, user1.getAddressList().size());

        String expected = "ADDRESS SUCESSFULLY ADDED.";
        String result = uaec.addAdress("asdrubal@jj.com", "2", "Beco do SWitCH", "4000-072", "Porto", "Portugal");

        assertEquals(2, user1.getAddressList().size());
        assertEquals(expected, result);

        user1.getAddressList().remove(1); //remove added test address from user's address list.
    }

    @Test
    @Transactional
    public void testAddAdressFailureEmailDoesNotExist() {

        assertEquals(1, user1.getAddressList().size());

        String expected = "No address added. Try again.";
        String result = uaec.addAdress("asd@jj.com", "2", "Beco do SWitCH", "4000-072", "Porto", "Portugal");

        assertEquals(1, user1.getAddressList().size());
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testAddAdressFailureInvalidAddressCityNotSpecified() {

        assertEquals(1, user1.getAddressList().size());

        String expected = "NO ADDRESS ADDED. TRY AGAIN.";
        String result = uaec.addAdress("asdrubal@jj.com", "2", "Beco do SWitCH", "4000-072", "", "Portugal");

        assertEquals(1, user1.getAddressList().size());
        assertEquals(expected, result);
    }
}
