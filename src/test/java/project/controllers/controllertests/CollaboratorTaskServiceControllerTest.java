package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.CollaboratorTaskListController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CollaboratorTaskServiceControllerTest {

    CollaboratorTaskListController ctlc;

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Task task1, task2, task3, task4, task5, task6;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    int y1;
    Month m1;

    @Before
    public void setUp() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        ctlc = new CollaboratorTaskListController(userService, projectService, taskService);

        birth = LocalDate.of(1999, 11, 11);
        y1 = LocalDateTime.now().getYear();
        m1 = LocalDateTime.now().getMonth();

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");
        projectCollaboratorService.setProjectManager(project1, user1);
        projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
        projectCollaboratorService.addProjectCollaborator(project1, user3, 4);
        projectCollaboratorService.addProjectCollaborator(project1, user4, 5);

        projectManager = project1.findProjectCollaborator(user1);
        projectCollaborator2 = project1.findProjectCollaborator(user2);
        projectCollaborator3 = project1.findProjectCollaborator(user3);
        projectCollaborator4 = project1.findProjectCollaborator(user4);

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2.minusDays(2), 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task6", "task6", d1, d2, 1.2, 1.2);

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");

        task1.addProjectCollaborator(projectCollaborator2);
        TaskCollaboratorRegistry col2TaskRegistry = task1.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));

        task2.addProjectCollaborator(projectCollaborator2);
        col2TaskRegistry = task2.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));

        task3.addProjectCollaborator(projectCollaborator2);
        col2TaskRegistry = task3.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));

        task5.addProjectCollaborator(projectCollaborator2);
        col2TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));

        task5.addProjectCollaborator(projectCollaborator4);
        TaskCollaboratorRegistry col4TaskRegistry = task5.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
        col4TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(20));

        task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
        task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

        projectService.updateProject(project1);
        taskService.updateTask(task1);
        taskService.updateTask(task2);
        taskService.updateTask(task3);
        taskService.updateTask(task4);
        taskService.updateTask(task5);

    }

    @Test
    @Transactional
    public void testGetUserPendentTasksNoTasks() {

        String result = ctlc.getCollaboratorPendentTasks(user3.getEmail().toString());
        String expected = "YOU HAVE NO PENDENT TASKS.\n\n";

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetCollaboratorPendentTasksSuccess() {

        String expected = task5.toString();
        String result = ctlc.getCollaboratorPendentTasks(user4.getEmail().toString());

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void getUserCompletedTasksSuccess() {

        task5.setTaskCompleted();
        task2.setEffectiveStartDate(d1);
        task2.setTaskCompleted();

        task2.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(1));
        task1.setPredictedDateOfConclusion(LocalDateTime.of(2017, 12, 18, 0, 0));
        task3.setPredictedDateOfConclusion(LocalDateTime.of(2017, 12, 19, 0, 0));

        String result = ctlc.getCollaboratorCompletedTasks(user2.getEmail().toString());
        List<Task> expected = new ArrayList<>();

        expected.add(task5);
        expected.add(task2);

        StringBuilder sb = new StringBuilder();
        for (Task t : expected) {
            sb.append(t.toString());

        }

        assertEquals(sb.toString(), result);
    }

    @Test
    @Transactional
    public void getUserCompletedTasksNoTasks() {

        String result = ctlc.getCollaboratorCompletedTasks(user3.getEmail().toString());
        String expected = "YOU HAVE NO TASKS COMPLETED AT THIS MOMENT.\n\n";

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void getUserCompletedTasksLastMonthSuccess() {

        task1.setTaskCompleted();
        task2.setEffectiveStartDate(d1);
        task2.setTaskCompleted();
        task3.setTaskCompleted();
        task5.setTaskCompleted();
        task2.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(1).minusSeconds(50));
        task3.setEffectiveDateOfConclusion(LocalDateTime.now().minusMonths(1));
        String result = ctlc.getCollaboratorConcludedTasksLastMonth(user2.getEmail().toString());
        List<Task> expected = new ArrayList<>();

        expected.add(task3);
        expected.add(task2);
        StringBuilder sb = new StringBuilder();
        for (Task t : expected) {
            sb.append(t.toString());

        }

        assertEquals(sb.toString(), result);
    }

    @Test
    @Transactional
    public void getUserCompletedTasksLastMonthNoTasks() {

        String result = ctlc.getCollaboratorConcludedTasksLastMonth(user3.getEmail().toString());
        String expected = "YOU HAVE NO TASKS COMPLETED IN LAST 30 DAYS.\n\n";

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetCollabaratorsTasksInAllProjectsSuccess() {

        String expected = task5.toString();
        String result = ctlc.getCollabaratorsTasksInAllProjects("liliana@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetCollabaratorsTasksInAllProjectsNoTasks() {

        String result = ctlc.getCollabaratorsTasksInAllProjects(user3.getEmail().toString());
        String expected = "YOU HAVE NO TASKS\n\n";

        assertEquals(expected, result);
    }

}
