package project.controllers.controllertests;

import jparepositoriestest.JavaMailSenderClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.RegistryController;
import project.services.EmailServiceImpl;
import project.model.UserService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class RegistryControllerTest {

    RegistryController rc;
    UserService userService;
    EmailServiceImpl emailService;
    JavaMailSenderClass javaMailSenderClass;

    UserRepositoryClass userRepositoryClass;

    LocalDate birth;

    @Before
    public void setUp() {
        userRepositoryClass = new UserRepositoryClass();
        userService = new UserService(userRepositoryClass);
        javaMailSenderClass = new JavaMailSenderClass();
        emailService = new EmailServiceImpl(javaMailSenderClass);

        rc = new RegistryController(userService, emailService);
        birth = LocalDate.of(1999, 11, 11);
    }

    @Test
    @Transactional
    public void registerUserTest() throws AddressException {

        assertEquals(0, userService.listAllUsers().size());

        rc.registerUser("Asdrubal", "99 555 666 22", "filipasdrubal@jj.com", "55555", birth, "1", "Rua do Amial", "4250-444",
                "Porto", "Portugal", "password");

        assertEquals(1, userService.listAllUsers().size());

    }

    @Test
    @Transactional
    public void isValidEmail() {
        assertFalse(rc.isValidEmail("pedro@gmai.commmmmmmmmmmm"));
        assertFalse(rc.isValidEmail("pedrogmail.com"));
        assertFalse(rc.isValidEmail("pedro@@@@com"));
        assertTrue(rc.isValidEmail("pedro@gmail.com"));

    }

    @Test
    @Transactional
    public void isValidDate() {
        assertFalse(rc.isValidDate("01-01-1990"));
        assertFalse(rc.isValidDate("50/50/1990"));
        assertFalse(rc.isValidDate("20-03/1990"));
        assertFalse(rc.isValidDate("20/03/90"));
        assertTrue(rc.isValidDate("02/02/1990"));
    }

    @Test
    @Transactional
    public void isValidNumber() {
        assertFalse(rc.isValidNumber("djsakld"));
        assertFalse(rc.isValidNumber("7d89sadsa"));
        assertFalse(rc.isValidNumber("@@@@@@"));
        assertFalse(rc.isValidNumber("^^^^^999"));
        assertTrue(rc.isValidNumber("919999999"));
    }

    @Test
    @Transactional
    public void isValidString() {
        assertFalse(rc.isValidString("999ds89"));
        assertFalse(rc.isValidString("(((dsad"));
        assertFalse(rc.isValidString("port:b"));
        assertTrue(rc.isValidString("PORTUGAL"));
        assertTrue(rc.isValidString("Portugal"));
    }

    @Test
    @Transactional
    public void isValidProfile() {
        assertFalse(rc.isValidProfile("teste"));
        assertFalse(rc.isValidProfile("321432"));
        assertFalse(rc.isValidProfile("teste teste"));
        assertTrue(rc.isValidProfile("COLLABORATOR"));
        assertTrue(rc.isValidProfile("DIRECTOR"));
        assertTrue(rc.isValidProfile("ADMINISTRATOR"));
        assertTrue(rc.isValidProfile("REGISTEREDUSER"));

    }
}
