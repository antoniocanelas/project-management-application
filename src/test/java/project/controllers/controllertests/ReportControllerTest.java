package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.controllers.consolecontrollers.ReportController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

public class ReportControllerTest {

	TaskService taskService;
	UserService userService;
	ProjectService projectService;
	ProjectCollaboratorService projectCollaboratorService;

	UserRepositoryClass userRepository;
	ProjectRepositoryClass projectRepository;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
	TaskRepositoryClass taskRepository;
	List<User> expectedUser;

	LocalDate birth;
	User user1;
	User user2;
	User user3;
	User user4;
	ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

	Project project1;
	Project project2;

	Task task1, task2, task3, task4, task5, task6;

	LocalDateTime d1;
	LocalDateTime d2;
	LocalDateTime d3;
	LocalDateTime d4;
	int y1;
	Month m1;

	ReportController rc;

	@Before
	public void setUp() throws AddressException {
		userRepository = new UserRepositoryClass();
		taskRepository = new TaskRepositoryClass();
		projectRepository = new ProjectRepositoryClass();
		projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass();

		userService = new UserService(userRepository);
		taskService = new TaskService(taskRepository);
		projectService = new ProjectService(projectRepository);
		projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);
		rc = new ReportController(userService, projectService, taskService);

		birth = LocalDate.of(1999, 11, 11);
		y1 = LocalDateTime.now().getYear();
		m1 = LocalDateTime.now().getMonth();

		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");
		userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
				"4250-444", "Porto", "Portugal");

		user1 = userService.searchUserByEmail("asdrubal@jj.com");
		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		user2.setProfileCollaborator();
		user3.setProfileCollaborator();
		user4.setProfileCollaborator();

		userService.updateUser(user2);
		userService.updateUser(user3);
		userService.updateUser(user4);

		user2 = userService.searchUserByEmail("joaquim@gmail.com");
		user3 = userService.searchUserByEmail("joana@gmail.com");
		user4 = userService.searchUserByEmail("liliana@gmail.com");

		/*
		 * Project 1 id= "1"
		 *
		 * user1 is project manager users 2, 3 and 4 are added to the project and become
		 * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
		 * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
		 *
		 * project 1 has 6 tasks
		 *
		 * tasks 1 and 2 have reports tasks 1 and 2 are pending
		 *
		 * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
		 * and no end date
		 *
		 * tasks 5 and 6 have reports and are concluded
		 *
		 *
		 * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
		 * in task 2 has 4 reports of 4,8,8,4 hours
		 *
		 * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
		 * report in task5 1 report in task6
		 *
		 * projectCollaborator4 has no assigned tasks
		 *
		 *
		 */

		projectService.addProject("1", "Project 1");
		projectService.addProject("2", "Project 1");

		project1 = projectService.getProjectByID("1");

		projectCollaboratorService.setProjectManager(project1, user1);
		projectCollaboratorService.addProjectCollaborator(project1, user2, 2);
		projectCollaboratorService.addProjectCollaborator(project1, user3, 4);

		project2 = projectService.getProjectByID("2");
		projectCollaboratorService.setProjectManager(project2, user1);
		projectCollaboratorService.addProjectCollaborator(project2, user2, 2);

		projectManager = project1.findProjectCollaborator(user1);
		projectCollaborator2 = project1.findProjectCollaborator(user2);
		projectCollaborator3 = project1.findProjectCollaborator(user3);
		projectCollaborator4 = project1.findProjectCollaborator(user4);

		d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
		d2 = LocalDateTime.of(2018, 03, 11, 0, 0);

		taskService.addTask(project1, "task1");
		taskService.addTask(project1, "task2");
		taskService.addTask(project1, "task3");
		taskService.addTask(project1, "task4");
		taskService.addTask(project1, "task5");
		taskService.addTask(project1, "task6");

		task1 = project1.findTaskByTitle("task1");
		task2 = project1.findTaskByTitle("task2");
		task3 = project1.findTaskByTitle("task3");
		task4 = project1.findTaskByTitle("task4");
		task5 = project1.findTaskByTitle("task5");
		task6 = project1.findTaskByTitle("task6");

		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task2.setPredictedDateOfStart(d1);
		task2.setPredictedDateOfConclusion(d2);
		task3.setPredictedDateOfStart(d1);
		task3.setPredictedDateOfConclusion(d2);
		task4.setPredictedDateOfStart(d1);
		task4.setPredictedDateOfConclusion(d2);
		task5.setPredictedDateOfStart(d1);
		task5.setPredictedDateOfConclusion(d2);
		task6.setPredictedDateOfStart(d1);
		task6.setPredictedDateOfConclusion(d2);

		task1.addProjectCollaborator(projectCollaborator2);
		TaskCollaboratorRegistry col2TaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task2.addProjectCollaborator(projectCollaborator2);
		col2TaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task3.addProjectCollaborator(projectCollaborator2);
		col2TaskRegistry = task3
				.getTaskCollaboratorRegistryByID(task3.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task5.addProjectCollaborator(projectCollaborator2);
		col2TaskRegistry = task5
				.getTaskCollaboratorRegistryByID(task5.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task5.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
		task3.addReport(projectCollaborator2, 7, d1, d1.plusDays(5));

		projectService.updateProject(project1);
		projectService.updateProject(project2);

		project1 = projectService.getProjectByID("1");
		project2 = projectService.getProjectByID("2");

		taskService.updateTask(task1);
		taskService.updateTask(task2);
		taskService.updateTask(task3);
		taskService.updateTask(task4);
		taskService.updateTask(task5);
	}

	@Test
	@Transactional
	public void testAddReportFailureCollaboratorNotAssignedToThisTask() {

		String expected = "This collaborator has never worked in this task.";
		String result = rc.addReport(user2.getEmail().toString(), task6.getId(), 6, d1, d1.plusDays(5));

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testAddReportSuccess() {

		task6.addProjectCollaborator(projectCollaborator2);
		TaskCollaboratorRegistry col2TaskRegistry = task6
				.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		taskService.updateTask(task6);
		String expected = "Report successfully added";
		String result = rc.addReport(user2.getEmail().toString(), task6.getId(), 6, d1, d1.plusDays(5));

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testAddReportTaskNull() {

		task6.addProjectCollaborator(projectCollaborator2);

		String expected = "Invalid task ID";
		String result = rc.addReport(user2.getEmail().toString(), null, 6, d1, d1.plusDays(5));

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testAddReportNoCollaboratorByThatEmail() {

		String expected = "Invalid email.";
		String result = rc.addReport("manuel@sdfg.com", task1.getId(), 6, d1, d1.plusDays(5));

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testAddReportFailureNegativeHours() {

		task6.addProjectCollaborator(projectCollaborator2);
		TaskCollaboratorRegistry col2TaskRegistry = task6
				.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		taskService.updateTask(task6);
		String expected = "Invalid number of hours. Please try again.";
		String result = rc.addReport(user2.getEmail().toString(), task6.getId(), -6, d1, d1.plusDays(5));

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testFindReportByIDSuccess() {

		int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();

		task6.addProjectCollaborator(projectCollaborator2);
		TaskCollaboratorRegistry col2TaskRegistry = task6
				.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task6.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
		taskService.updateTask(task6);
		double result = rc.findReportByID(user2.getEmail().toString(), task6.getId(),
				task6.getId() + "(" + (reportCount + 1) + ")");

		assertEquals(5.0, result, 0.001);
	}

	@Test
	@Transactional
	public void testFindReportByIDFailureNoTaskFound() {

		double result = rc.findReportByID(user2.getEmail().toString(), "ere", "1-7(1)");

		assertEquals(-1.0, result, 0.001);
	}

	@Test
	@Transactional
	public void testFindReportByIDFailureNoReportFound() {

		double result = rc.findReportByID(user2.getEmail().toString(), task2.getId(), task2.getId() + "(1)");

		assertEquals(-1.0, result, 0.01);
	}

	@Test
	@Transactional
	public void testListAllReportsByThisCollaborator() {

		int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();

		List<Report> expected = new ArrayList<>();
		expected.add(task5.findReportById(task5.getId() + "(" + (reportCount - 1) + ")"));
		String result = rc.listAllReportsByThisCollaborator("joaquim@gmail.com", task5.getId());

		assertEquals(expected.toString(), result);
	}

	@Test
	@Transactional
	public void testListAllReportsByThisCollaboratorFailureNoTaskFound() {

		String expected = "";
		String result = rc.listAllReportsByThisCollaborator("joaquim@gmail.com", "34");

		assertEquals(expected, result);
	}

	@Test
	@Transactional
	public void testUs208EditReport() {

		int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();

		TaskCollaboratorRegistry col2TaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		task2.addReport(projectCollaborator2, 23, d2, d2.plusDays(5));

		assertEquals("\nReport successfully edited.\n", rc.us208EditReport("joaquim@gmail.com", task2.getId(),
				task2.getId() + "(" + (reportCount + 1) + ")", 10));
	}

	@Test
	@Transactional
	public void testFindHowManyReportsDoesACollaboratorHasInTaskSuccess() {

		task6.addProjectCollaborator(projectCollaborator2);
		TaskCollaboratorRegistry col2TaskRegistry = task6
				.getTaskCollaboratorRegistryByID(task6.getId() + "-" + projectCollaborator2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task6.addReport(projectCollaborator2, 5, d1, d1.plusDays(5));
		taskService.updateTask(task6);

		int result = rc.findHowManyReportsDoesACollaboratorHasInTask(user2.getEmail().toString(), task6.getId());

		assertEquals(1, result);
	}

	@Test
	@Transactional
	public void testFindHowManyReportsDoesACollaboratorHasInTaskFailureNoTaskFound() {

		int result = rc.findHowManyReportsDoesACollaboratorHasInTask(user2.getEmail().toString(), "falseTask");

		assertEquals(-1, result);
	}
}
