package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.controllers.consolecontrollers.ReportCostController;
import project.controllers.consolecontrollers.TaskStateController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.services.TaskService;
import project.model.user.User;

public class ReportCostControllerTest {

	ReportCostController reportCostController;
	TaskStateController tsc;
	TaskService taskService;
	UserService userService;
	ProjectService projectService;
	ProjectCollaboratorService projectCollaboratorService;

	UserRepositoryClass userRepository;
	ProjectRepositoryClass projectRepository;
	ProjectCollaboratorRepositoryClass projectCollaboratorRepository;
	TaskRepositoryClass taskRepository;
	List<User> expectedUser;
	LocalDate birth;

	User user1;
	User user2;
	User user3;
	User projectManager;

	ProjectCollaborator projManager;
	ProjectCollaborator collaborator1;
	ProjectCollaborator collaborat2;
	ProjectCollaborator colab3;

	Project project1;

	LocalDateTime d1;

	Task t1;
	Task t2;
	Task t3;
	Task t4;
	Task t5;

	Report report1;

	List<Report> reportList;

	@Before
	public void setUp() throws AddressException {
		userRepository = new UserRepositoryClass();
    	taskRepository = new TaskRepositoryClass();
    	projectRepository = new ProjectRepositoryClass();
    	projectCollaboratorRepository = new ProjectCollaboratorRepositoryClass();
    	
    	userService = new UserService(userRepository);
    	taskService = new TaskService(taskRepository);
    	projectService = new ProjectService(projectRepository);
    	projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepository);


		reportCostController = new ReportCostController(projectService, taskService);

		d1 = LocalDateTime.of(2017, 12, 18, 0, 0);

		birth = LocalDate.of(1999, 11, 11);
		userService.addUser("Joaquim", "91739812379", "jj@jj.com", "132456765", birth, "2", "Rua ", "4433", "cidade",
				"pais");
		userService.addUser("Manel", "666 555 5556", "manel@hotmail.com", "5656546456", birth, "2", "Rua ", "4433",
				"cidade", "pais");
		userService.addUser("Crisostomo", "666 555 5556", "crisostomo@gmail.com", "5656546456", birth, "2", "Rua ",
				"4433", "cidade", "pais");
		userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "2", "Rua ", "4433",
				"cidade", "pais");

		user1 = userService.searchUserByEmail("jj@jj.com");
		user2 = userService.searchUserByEmail("manel@hotmail.com");
		user3 = userService.searchUserByEmail("crisostomo@gmail.com");
		projectManager = userService.searchUserByEmail("asdrubal@jj.com");

		user1.setProfileCollaborator();
		user2.setProfileCollaborator();
		user3.setProfileCollaborator();

		userService.updateUser(user1);
		userService.updateUser(user2);
		userService.updateUser(user3);

		user1 = userService.searchUserByEmail("jj@jj.com");
		user2 = userService.searchUserByEmail("manel@hotmail.com");
		user3 = userService.searchUserByEmail("crisostomo@gmail.com");

		collaborator1 = new ProjectCollaborator(project1, user1, 3);
		collaborat2 = new ProjectCollaborator(project1, user2, 3);
		collaborat2.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());

		colab3 = new ProjectCollaborator(project1, user3, 3);

		projectService.addProject("1", "Project 1");
		project1 = projectService.getProjectByID("1");
		projectCollaboratorService.setProjectManager(project1, projectManager);

		projectCollaboratorService.addProjectCollaborator(project1, user1, 3);
		projectCollaboratorService.addProjectCollaborator(project1, user2, 3);
		projectCollaboratorService.addProjectCollaborator(project1, user3, 3);
		projectCollaboratorService.addProjectCollaborator(project1, user2, 5);

		taskService.addTask(project1, "Test 1");
		taskService.addTask(project1, "Test 2");
		taskService.addTask(project1, "test 3");
		taskService.addTask(project1, "Test 4");
		taskService.addTask(project1, "test 5");

		t1 = project1.findTaskByTitle("Test 1");
		t2 = project1.findTaskByTitle("Test 2");
		t3 = project1.findTaskByTitle("test 3");
		t4 = project1.findTaskByTitle("Test 4");
		t5 = project1.findTaskByTitle("test 5");

		t1.setPredictedDateOfStart(d1.plusDays(20));
		t1.setPredictedDateOfConclusion(d1.plusDays(40));
		t2.setPredictedDateOfStart(d1.plusDays(20));
		t2.setPredictedDateOfConclusion(d1.plusDays(40));
		t4.setPredictedDateOfStart(d1.plusDays(20));
		t4.setPredictedDateOfConclusion(d1.plusDays(30));
		t5.setPredictedDateOfStart(d1.plusDays(20));
		t5.setPredictedDateOfConclusion(d1.plusDays(40));

		t1.addProjectCollaborator(collaborat2);
		t2.addProjectCollaborator(collaborat2);
		t4.addProjectCollaborator(collaborat2);
		t5.addProjectCollaborator(collaborat2);

		reportList = new ArrayList<>();

		reportList.add(report1);

		TaskCollaboratorRegistry col1TaskRegistry = t1
				.getTaskCollaboratorRegistryByID(t1.getId() + "-" + collaborat2.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		TaskCollaboratorRegistry col2TaskRegistry = t2
				.getTaskCollaboratorRegistryByID(t2.getId() + "-" + collaborat2.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		TaskCollaboratorRegistry col3TaskRegistry = t4
				.getTaskCollaboratorRegistryByID(t4.getId() + "-" + collaborat2.getUser().getEmail());
		col3TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		TaskCollaboratorRegistry col4TaskRegistry = t5
				.getTaskCollaboratorRegistryByID(t5.getId() + "-" + collaborat2.getUser().getEmail());
		col4TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

		t1.addReport(collaborat2, 9, d1, d1.plusDays(5));
		t2.addReport(collaborat2, 9, d1, d1.plusDays(5));
		t4.addReport(collaborat2, 9, d1, d1.plusDays(5));
		t5.addReport(collaborat2, 9, d1, d1.plusDays(5));

		projectService.updateProject(project1);
		project1 = projectService.getProjectByID("1");
		taskService.updateTask(t1);
		taskService.updateTask(t2);
		taskService.updateTask(t5);
		taskService.updateTask(t4);
	}

	@Test
	@Transactional
	public void testGetCostSoFar() {
		assertEquals(108.0, reportCostController.getProjectCostSoFar("1"), 0.01);
	}

	@Test
	@Transactional
	public void testGetUnit() {

		assertEquals(project1.getUnit().toString(), reportCostController.getUnit("1"));
	}

	@Test
	@Transactional
	public void testListReportCostDetails() {
		StringBuilder sb = new StringBuilder();
		String separator = "_________________________________";
		String cost = "Cost: ";
		String task = "Task ID: ";
		sb.append("\n\n\n\n");
		sb.append(separator + "\n");
		sb.append("\nPROJECT " + project1.getId() + " COST DETAILS\n");
		sb.append(separator + "\n\n");
		sb.append(task + t1.getId() + "\n");
		sb.append(cost + project1.calculateCostSoFarOfTask(t1) + "\n");
		sb.append("\n");
		sb.append(task + t2.getId() + "\n");
		sb.append(cost + project1.calculateCostSoFarOfTask(t2) + "\n");
		sb.append("\n");
		sb.append(task + t3.getId() + "\n");
		sb.append(cost + project1.calculateCostSoFarOfTask(t3) + "\n");
		sb.append("\n");
		sb.append(task + t4.getId() + "\n");
		sb.append(cost + project1.calculateCostSoFarOfTask(t4) + "\n");
		sb.append("\n");
		sb.append(task + t5.getId() + "\n");
		sb.append(cost + project1.calculateCostSoFarOfTask(t5) + "\n");
		sb.append("\n");
		sb.append(separator);
		sb.append("\n\n");

		assertEquals(sb.toString(), reportCostController.listCostSoFarDetails(project1.getId()));
	}

}
