package project.controllers.controllertests;

import jparepositoriestest.ProjectRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.DirectorSetProjectCostCalculationOptions;
import project.model.project.ProjectService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class DirectorSetProjectCostCalculationOptionsTest {

    ProjectRepositoryClass projectRepositoryClass;
    ProjectService projectService;
    DirectorSetProjectCostCalculationOptions setProjCostOptions;

    @Before
    public void setUp() {
        projectRepositoryClass = new ProjectRepositoryClass();
        projectService = new ProjectService(projectRepositoryClass);
        setProjCostOptions = new DirectorSetProjectCostCalculationOptions(projectService);
    }

    @Test
    @Transactional
    public void testDirectorSetProjectCostCalculationOptions() {

        setProjCostOptions = new DirectorSetProjectCostCalculationOptions(projectService);

        assertTrue(setProjCostOptions instanceof DirectorSetProjectCostCalculationOptions);
    }

    @Test
    @Transactional
    public void testListAllCostCalculationOptions() {

        // Given
        List<String> expected = new ArrayList<>();
        expected.add("AverageCost");
        expected.add("FirstTimePeriodCost");
        expected.add("LastTimePeriodCost");
        expected.add("MinimumTimePeriodCost");
        expected.add("MaximumTimePeriodCost");

        // When
        List<String> result = setProjCostOptions.listAllCostCalculationOptions();

        // Then
        assertThat(expected, containsInAnyOrder(result.toArray()));
    }

    @Test
    @Transactional
    public void testSetProjectListOfCostCalculationOptions() {

        /**
         * GIVEN: A project with a default Cost Calculation option [Last Time Period Cost]
         */
        List<String> result = new ArrayList<>();
        List<String> defaultCostOptions = new ArrayList<>();
        defaultCostOptions.add("LastTimePeriodCost");

        projectService.addProject("1002", "SOLID to STUPID");
        result = projectService.getProjectByID("1002").getCostCalculationOptions();
        assertEquals(1, result.size());
        assertEquals(defaultCostOptions, result);

        /**
         * WHEN: Add a list of Cost Calculation Options
         *
         */
        List<String> newCostOptions = new ArrayList<>();
        newCostOptions.add("AverageCost");
        newCostOptions.add("FirstTimePeriodCost");
        newCostOptions.add("LastTimePeriodCost");
        setProjCostOptions.setProjectListOfCostCalculationOptions("1002", newCostOptions);

        /**
         * THEN: The project contains a list with 3 cost calculations options
         * [Last Time Period Cost]
         * [First Time Period Cost]
         * [Last Time Period Cost]
         */
        result = setProjCostOptions.listProjectCalculationOptions("1002");
        assertThat(newCostOptions, containsInAnyOrder(result.toArray()));
    }
}
