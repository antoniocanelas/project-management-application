package project.controllers.controllertests;

import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.UserActivationController;
import project.model.UserService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserActivationControllerTest {

    UserService userService;
    UserRepositoryClass userRepo;
    UserActivationController userActivationController;
    User user1, user2;

    @Before
    public void setUp() throws AddressException {
        userRepo = new UserRepositoryClass();
        userService = new UserService(userRepo);
        userActivationController = new UserActivationController(userService);

        userService.addUser("Asdrubal", "123456789", "asdrubal@jj.com", "123456789",
                LocalDate.of(1977, 3, 5), "1", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Manel", "123456789", "manel@jj.com", "123456789", LocalDate.of(1978, 11, 12),
                "2", "Rua sem saída", "4250-357", "Porto", "Portugal");
        userService.addUser("Joaquim", "123456789", "joaquim@jj.com", "123456789",
                LocalDate.of(1978, 11, 12), "2", "Rua sem saída", "4250-357", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user1.setActive();

        user2 = userService.searchUserByEmail("manel@jj.com");
        user2.setInactive();

        userService.updateUser(user1);
        userService.updateUser(user2);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("manel@jj.com");
    }

    @Test
    public void testInativateUserSucess2() {
        assertEquals("User was sucessfully inactivated.", userActivationController.inactivateUser("asdrubal@jj.com"));
    }

    @Test
    public void testInativateUserFail() {
        assertEquals("User activation has failed because there is not registration in the company's user registry.",
                userActivationController.inactivateUser("pedro@gmail.com"));
    }

    @Test
    public void testInativateUserFail2() {
        assertEquals("User is already inactive.", userActivationController.inactivateUser("manel@jj.com"));
    }

    @Test
    public void testReativateUserSucess() {

        userActivationController.reactivateUser("manel@jj.com");
        assertTrue(user2.isActive());
    }

    @Test
    public void testReativateUserSucess2() {
        assertEquals("User was sucessfully activated.", userActivationController.reactivateUser("manel@jj.com"));
    }

    @Test
    public void testReativateUserFail() {
        assertEquals("User activation has failed because there is not registration in the company's user registry.",
                userActivationController.reactivateUser("pedro@gmail.com"));
    }

    @Test
    public void testReativateUserFail2() {
        assertEquals("User is already active.", userActivationController.reactivateUser("asdrubal@jj.com"));
    }

}
