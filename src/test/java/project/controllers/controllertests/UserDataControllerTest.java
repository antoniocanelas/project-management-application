package project.controllers.controllertests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.controllers.consolecontrollers.UserDataController;
import project.model.UserService;
import jparepositoriestest.UserRepositoryClass;
import project.model.user.Address;
import project.model.user.User;

public class UserDataControllerTest {

    UserDataController udc;
    UserService userService;
    UserRepositoryClass userRepo;

    LocalDate birth1 = LocalDate.of(1999, 11, 11);
    User user1;

    @Before
    public void setup() throws AddressException {
    	userRepo = new UserRepositoryClass();
    	userService = new UserService(userRepo);
        udc = new UserDataController(userService);

        userService.addUser("Pedro", "9199999", "pedro@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "país");
        user1 = userService.searchUserByEmail("pedro@gmail.com");
        user1.setPassword("HELLo");
        Address newAddress = user1.createAddress("1", "street", "postalCode", "city", "country");
        user1.addAddress(newAddress);
        userService.updateUser(user1);
        user1 = userService.searchUserByEmail("pedro@gmail.com");
        String userEmail = user1.getEmail();
        String password = "qwerty";
        udc.setNewPassword(userEmail, password);

    }

    @Test
    public void testEditPersonalData_editNameSuccess() {
        assertEquals("Pedro", udc.getName("pedro@gmail.com"));
        udc.editName("Lisa", "pedro@gmail.com");
        assertEquals("Lisa", udc.getName("pedro@gmail.com"));
    }

    @Test
    public void testEditPersonalData_editAddressId() {
        assertEquals("1", udc.getAddressId("pedro@gmail.com", "1"));
        udc.editAddressId("pedro@gmail.com", "1", "2");
        assertEquals("2", udc.getAddressId("pedro@gmail.com", "2"));
    }

    @Test
    public void testEditPersonalData_editAddressIdNullCase() {
        assertNull(udc.getAddressId("pedro@gmail.com", null));
    }

    @Test
    public void testEditPersonalData_editAddressStreet() {

        String expectedStreet = udc.getAddressStreet("pedro@gmail.com", "1");

        assertEquals("street", expectedStreet);

        udc.editAddressStreet("pedro@gmail.com", "1", "newStreet");
        expectedStreet = udc.getAddressStreet("pedro@gmail.com", "1");

        assertEquals("newStreet", expectedStreet);
    }

    @Test
    public void testEditPersonalData_editAddressCity() {

        String expectedCity = udc.getAddressCity("pedro@gmail.com", "1");

        assertEquals("city", expectedCity);

        udc.editAddressCity("pedro@gmail.com", "1", "newCity");
        expectedCity = udc.getAddressCity("pedro@gmail.com", "1");

        assertEquals("newCity", expectedCity);
    }

    @Test
    public void testEditPersonalData_editAddressPostalCode() {

        String expectedPostalCode = udc.getAddressPostalCode("pedro@gmail.com", "1");

        assertEquals("postalCode", expectedPostalCode);

        udc.editAddressPostalCode("pedro@gmail.com", "1", "newCode");
        expectedPostalCode = udc.getAddressPostalCode("pedro@gmail.com", "1");

        assertEquals("newCode", expectedPostalCode);
    }

    @Test
    public void testEditPersonalData_editAddressCountry() {
        String expectedCountry = udc.getAddressCountry("pedro@gmail.com", "1");

        assertEquals("country", expectedCountry);

        udc.editAddressCountry("pedro@gmail.com", "1", "newCountry");

        expectedCountry = udc.getAddressCountry("pedro@gmail.com", "1");

        assertEquals("newCountry", expectedCountry);

    }

    @Test
    public void testGetBirth() {
        assertEquals("1999-11-11", udc.getBirthDate("pedro@gmail.com"));
    }

    @Test
    public void testGetPhone() {
        assertEquals("9199999", udc.getPhone("pedro@gmail.com"));
    }

    @Test
    public void testGetTpi() {
        assertEquals("332432", udc.getTpi("pedro@gmail.com"));
    }

    @Test
    public void testGetAddressList() {

        assertEquals(user1.getAddressList().toString(), udc.getAddressList("pedro@gmail.com"));
    }
    
    @Test
    public void testSetActive() {
    	user1.setInactive();
    	assertEquals(user1.isActive(),false);
    	udc.setActive("pedro@gmail.com");
    	assertEquals(user1.isActive(),true);
    }
}
