package project.controllers.controllertests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.controllers.consolecontrollers.CreateTaskController;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.model.task.Task;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class CreateTaskControllerTest {

    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    CreateTaskController tc;

    List<User> expectedUser;

    LocalDate birth;
    User user1;
    User user2;
    User user3;
    User user4;
    ProjectCollaborator projectCollaborator2, projectCollaborator3, projectCollaborator4, projectManager;

    Project project1;
    Task task1, task2, task3, task4, task5, task6;

    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;

    List<Task> taskDependencies;

    @Before
    public void setUp() throws AddressException {

        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        tc = new CreateTaskController(userService, projectService, taskService);

        birth = LocalDate.of(1999, 11, 11);

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@jj.com", "55555", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joaquim", "96 452 56 56", "joaquim@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Joana", "93 333 33 33", "joana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");
        userService.addUser("Lilina", "93 333 33 33", "liliana@gmail.com", "112222", birth, "1", "Rua do Amial",
                "4250-444", "Porto", "Portugal");

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileCollaborator();

        userService.updateUser(user1);
        userService.updateUser(user2);
        userService.updateUser(user3);
        userService.updateUser(user4);

        user1 = userService.searchUserByEmail("asdrubal@jj.com");
        user2 = userService.searchUserByEmail("joaquim@gmail.com");
        user3 = userService.searchUserByEmail("joana@gmail.com");
        user4 = userService.searchUserByEmail("liliana@gmail.com");

        //projectRegistry.updateProject(project1);
        //project1 = projectRegistry.getProjectByID("1");

        /*
         * Project 1 id= "1"
         *
         * user1 is project manager users 2, 3 and 4 are added to the project and become
         * ProjectCollaborators user2 = projectCollaborator2 and costs 2; user3 =
         * projectCollaborator3 and costs 4; user4 = projectCollaborator3 and costs 5;
         *
         * project 1 has 6 tasks
         *
         * tasks 1 and 2 have reports tasks 1 and 2 are pending
         *
         * tasks 3 and 4 have no collaborators assigned, so they don't have a start date
         * and no end date
         *
         * tasks 5 and 6 have reports and are concluded
         *
         *
         * projectCollaborator2 is in task 1 and 2 in task 1 has one report on 4 hours
         * in task 2 has 4 reports of 4,8,8,4 hours
         *
         * projectCollaborator3 is in task 1, 2, 5 and 6 has reports in task 5 and 6 1
         * report in task5 1 report in task6
         *
         * projectCollaborator4 has no assigned tasks
         *
         *
         */
        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        projectService.addProject("1", "Project 1");
        project1 = projectService.getProjectByID("1");

        projectCollaboratorService.setProjectManager(projectService.getProjectByID("1"), user1);
        project1.setStartDate(d1);
        projectManager = project1.findProjectCollaborator(user1);

        taskService.addTask(project1, "task1", "task1", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task2", "task2", d1, d2, 1.2, 1.3);
        taskService.addTask(project1, "task3", "task3", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task4", "task4", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task5", "task5", d1, d2, 1.2, 1.2);
        taskService.addTask(project1, "task6");

        task1 = project1.findTaskByTitle("task1");
        task2 = project1.findTaskByTitle("task2");
        task3 = project1.findTaskByTitle("task3");
        task4 = project1.findTaskByTitle("task4");
        task5 = project1.findTaskByTitle("task5");
        task6 = project1.findTaskByTitle("task6");

        projectService.updateProject(project1);
        project1 = projectService.getProjectByID("1");

    }

    @Test
    @Transactional
    public void testCreateTaskWithDateRelatedToProjectStartDate() {
        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!\nThe task will start 15 days after the project start date (2017-11-11T00:00) and will have the duration of 30 days.\n\n";
        assertEquals(expected,
                tc.createTaskWithDateRelatedToProjectStartDate("1", "task6", "clean the room", 15, 30, 5, 7));

    }

    @Test
    @Transactional
    public void testCreateTaskWithDateRelatedToProjectStartDate2() {

        assertNull(taskService.findTaskByTitle("task10"));
        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!\nThe task will start 15 days after the project start date (2017-11-11T00:00) and will have the duration of 30 days.\n\n";
        assertEquals(expected,
                tc.createTaskWithDateRelatedToProjectStartDate("1", "task10", "clean the room", 15, 30, 5, 7));
        assertNotNull(taskService.findTaskByTitle("task3"));
    }

    @Test
    @Transactional
    public void testCreateTaskWithOutDates() {

        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!" + "\n\n";
        assertEquals(expected, tc.createTaskWithOutDates("1", "task4"));

    }

    @Test
    @Transactional
    public void testCreateTaskWithOutDates2() {

        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!" + "\n\n";
        assertEquals(expected, tc.createTaskWithOutDates("1", "task40"));

    }

    @Test
    @Transactional
    public void testCreateTaskWithDateEffectiveDate() {

        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!" + "\n\n";
        assertEquals(expected, tc.createTaskWithDateEffectiveDate("1", "task4", "clean the room ", d1, d2, 3, 5));

    }

    @Test
    @Transactional
    public void testCreateTaskWithDateEffectiveDate2() {

        String expected = "\n" +
                "TASK SUCESSFULLY CREATED!" + "\n\n";
        assertEquals(expected, tc.createTaskWithDateEffectiveDate("1", "task40", "clean the room ", d1, d2, 3, 5));

    }

}
