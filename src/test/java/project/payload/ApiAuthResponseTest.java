package project.payload;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApiAuthResponseTest {

    ApiAuthResponse apiAuthResponse;


    @Before
    public void setUp() throws Exception {

        apiAuthResponse = new ApiAuthResponse();
        apiAuthResponse.setMessage("hurray");
        apiAuthResponse.setSuccess(true);
    }

    @Test
    public void getAndSetSuccess() {

        assertTrue(apiAuthResponse.getSuccess());
        apiAuthResponse.setSuccess(false);
        assertFalse(apiAuthResponse.getSuccess());
    }

    @Test
    public void getMessage() {
        assertEquals("hurray", apiAuthResponse.getMessage());
        apiAuthResponse.setMessage("Not hurray");
        assertEquals("Not hurray", apiAuthResponse.getMessage());

    }


}