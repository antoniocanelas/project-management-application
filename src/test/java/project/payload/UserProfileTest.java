package project.payload;

import org.junit.Before;
import org.junit.Test;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UserProfileTest {

    UserProfile userProfile;

    User user = new User("Pedr", "919999999", "pedro@mail.com", "123456", LocalDate.now().minusYears(20),
            "2", "Rua ", "4433", "cidade", "país");

    public UserProfileTest() throws AddressException {
    }


    @Before
    public void setUp() {
        userProfile = new UserProfile(user);
    }

    @Test
    public void getEmail() {
        assertEquals(user.getEmail(), userProfile.getEmail());
        userProfile.setEmail("p@mail.com");
        assertEquals("p@mail.com", userProfile.getEmail());
    }

    @Test
    public void getName() {
        assertEquals(user.getName(), userProfile.getName());
        userProfile.setName("paulo");
        assertEquals("paulo", userProfile.getName());
    }

    @Test
    public void getBirth() {
        assertEquals(user.getBirthDate(), userProfile.getBirth());
        userProfile.setBirth(LocalDate.now().minusYears(15));
        assertEquals(LocalDate.now().minusYears(15), userProfile.getBirth());
    }

    @Test
    public void getTaxPayer() {
        assertEquals(user.getTaxPayerId(), userProfile.getTaxPayer());
        userProfile.setTaxPayer("654321");
        assertEquals("654321", userProfile.getTaxPayer());
    }

    @Test
    public void getPhone() {
        assertEquals(user.getPhone(), userProfile.getPhone());
        userProfile.setPhone("918888888");
        assertEquals("918888888", userProfile.getPhone());
    }
}