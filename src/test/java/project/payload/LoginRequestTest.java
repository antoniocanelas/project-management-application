package project.payload;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LoginRequestTest {

    LoginRequest loginRequest;

    @Before
    public void setUp()  {
        loginRequest = new LoginRequest();

    }

    @Test
    public void getUsernameOrEmail() {
        loginRequest.setPassword("12345");
        loginRequest.setUsernameOrEmail("switch@it.com");
        assertEquals("switch@it.com", loginRequest.getUsernameOrEmail());
        assertEquals("12345", loginRequest.getPassword());

    }

}