package project.payload;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JwtAuthenticationResponseTest {

    JwtAuthenticationResponse jwtAuthenticationResponse;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testConstructor() {
        jwtAuthenticationResponse = new JwtAuthenticationResponse("teste");
        assertEquals("teste", jwtAuthenticationResponse.getAccessToken());
    }
}
