package project.model.unittests;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import project.dto.TaskDTO;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class TaskTest {

    // Dates
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    LocalDateTime d5;
    LocalDateTime d6;
    LocalDateTime d7;
    LocalDateTime d8;
    LocalDateTime d9;
    LocalDateTime d10;

    // Tasks
    Task t1;
    Task t2;
    Task t3;
    Task t4;
    Task t5;
    Task t6;
    Task t7;
    Task t8;

    // Users
    User user1;
    User user2;
    User user3;

    // Project Collaborators
    ProjectCollaborator col1;
    ProjectCollaborator col2;
    ProjectCollaborator col3;

    // Projects
    private Project project;

    @Before
    public void initialize() throws AddressException {

        project = new Project("1", "SWitCH");

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d2 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d3 = LocalDateTime.of(2020, 12, 18, 0, 0);
        d4 = LocalDateTime.of(2017, 12, 1, 0, 0);
        d5 = LocalDateTime.of(2018, 5, 18, 0, 0);
        d6 = LocalDateTime.of(2022, 11, 29, 0, 0);
        d7 = LocalDateTime.of(2017, 11, 18, 10, 3);
        d8 = LocalDateTime.of(2017, 11, 1, 11, 40);
        d9 = LocalDateTime.of(2017, 11, 18, 15, 20);
        d10 = LocalDateTime.of(2017, 11, 29, 17, 10);

        t1 = new Task(project, "Test 1");
        t2 = new Task(project, "Test 2");
        t3 = new Task(project, "Test 3");
        t4 = new Task(project, "Test 4");
        t5 = new Task(project, "Test 5");
        t6 = new Task(project, "Test 4");
        t7 = new Task(project, "Test 7");
        t8 = new Task(project, "Test 8");

        t1.setTaskId("1-1");
        t2.setTaskId("1-2");
        t3.setTaskId("1-3");
        t4.setTaskId("1-4");
        t5.setTaskId("1-5");
        t6.setTaskId("1-4");
        t7.setTaskId("1-7");
        t8.setTaskId("1-8");

        t1.setPredictedDateOfStart(d1);
        t1.setPredictedDateOfConclusion(d3);
        t3.setPredictedDateOfStart(d1);
        t3.setPredictedDateOfConclusion(d3);
        t4.setPredictedDateOfStart(d1);
        t4.setPredictedDateOfConclusion(d4);
        t6.setPredictedDateOfStart(d1);
        t6.setPredictedDateOfConclusion(d6);
        t8.setPredictedDateOfStart(d1);
        t8.setPredictedDateOfConclusion(d3);

        project.addTask(t1);
        project.addTask(t2);
        project.addTask(t3);
        project.addTask(t4);
        project.addTask(t5);
        project.addTask(t6);
        project.addTask(t7);
        project.addTask(t8);

        // t1 = new Task("1", "Test 1", "make switch", d1, d3, 9, 11);
        // t2 = new Task("2", "Test 2");
        // t3 = new Task("3", "test 1", "wash the dishes", d1, d3, 9, 11);
        // t4 = new Task("4", "Test 4", "clean the car", d1, d4, 5, 7);
        // t5 = new Task("1", "test 1 ");
        // t6 = new Task("4", "Test 4", "clean the car", d1, d6, 3, 2);
        // t7 = new Task("7", "Create State");
        // t8 = new Task("8", "Assigned State", "wash the dishes", d1, d3, 9, 11);

        LocalDate dn1 = LocalDate.of(2000, 04, 8);
        user1 = new User("Ana", "123", "ana@gmail.com", "2345", dn1, "2", "Rua ", "4433", "cidade", "país");
        user2 = new User("Bebiana", "1243", "bebiana@hotmail.com", "23455", dn1, "2", "Rua ", "4433", "cidade", "país");
        user3 = new User("TIago", "1243", "tiago@hotmail.com", "23455", dn1, "2", "Rua ", "4433", "cidade", "país");

        col1 = new ProjectCollaborator(project, user1, 5);
        col1.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());
        col2 = new ProjectCollaborator(project, user2, 5);
        col2.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());
        col3 = new ProjectCollaborator(project, user3, 5);
        col3.getCostAndTimePeriodList().get(0).setStartDate(d1.minusYears(3).toLocalDate());

        t1.addProjectCollaborator(col1);
        TaskCollaboratorRegistry col1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t1.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t1.addReport(col1, 6, d8, d8.plusDays(5));
        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t1.addReport(col1, 5, d10, d10.plusDays(5));

        t1.addReport(col2, 5, d10, d10.plusDays(5));
        t1.addReport(col2, 5, d9, d9.plusDays(5));

        t8.addTaskDependency(t5);
        t8.addProjectCollaborator(col3);
    }

    @Test

    public void efectiveDateOfConclusion() {
        // Given
        // When
        t2.setPredictedDateOfStart(d1);
        t2.addProjectCollaborator(col2);
        t2.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t2.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d1);
        t2.setEffectiveDateOfConclusion(d4);
        t3.setEffectiveDateOfConclusion(null);
        // Then
        assertEquals(0, t3.compareTo(t3));
        assertEquals(0, t1.compareTo(t3));
        assertEquals(0, t3.compareTo(t1));
        assertEquals(17, t1.compareTo(t2));
        assertEquals(-17, t2.compareTo(t1));
        assertEquals(0, t1.compareTo(t1));
        t2.removeProjectCollaborator(col2);
    }

    // Test Equals
    @Test

    public void testCheckifTwoTasksAreNotEqual_onlyTitleConstructor() {
        // Given
        // Then
        assertNotEquals(t1, t2);
    }

    @Test

    public void testCheckifTwoTasksAreNotEqual_fullTitleConstructor() {
        // Given
        // Then
        assertNotEquals(t3, t4);
    }

    @Test

    public void testCheckifTwoTasksAreEqual_fullTitleConstructor() {
        // Given
        // Then
        assertEquals(t4, t6);
    }

    @Test
    public void testCheckStatusOfNewTask() {
        // Given
        // Then
        boolean condition = t1.isCompleted();
        assertFalse(condition);
    }

    @Test

    public void testCheckChangeStatus() {
        // Given
        // Then
        boolean condition1 = t1.isCompleted();
        boolean condition2 = t1.setTaskCompleted();
        assertFalse(condition1);
        assertTrue(condition2);
    }

    @Test

    public void testCheckChangeCancelled() {
        // Given
        // When
        t1.setTaskCancelled();
        // Then
        boolean condition1 = t1.getTaskState().isOnCancelledState();
        boolean condition2 = t1.setTaskCompleted();
        assertTrue(condition1);
        assertFalse(condition2);
    }

    @Test

    public void testCheckChangeStatus_ConcludedDate() {
        // Given
        // When
        t1.setTaskCompleted();
        // Then
        LocalDateTime ld1 = t1.getEfectiveDateOfConclusion();
        assertEquals(LocalDateTime.now().toLocalDate(), ld1.toLocalDate());
    }

    @Test

    public void testHasAssignedUserTrue() {
        // Given
        // When
        t2.addProjectCollaborator(col1);
        // Then
        boolean condition = t2.hasAssignedProjectCollaborator();
        assertTrue(condition);
    }

    @Test

    public void testHasAssignedUserFalse() {
        // Given
        // Then
        boolean condition = t4.hasAssignedProjectCollaborator();
        assertFalse(condition);
    }

    @Test

    public void testSetEffectiveDateOfStart() {
        // Given
        // When
        t2.setPredictedDateOfStart(d1);
        t3.setPredictedDateOfStart(d1);
        t8.setPredictedDateOfStart(d1);
        // Then
        assertEquals(d1, t2.getPredictedDateOfStart());
        assertEquals(d1, t3.getPredictedDateOfStart());
        assertEquals(d1, t8.getPredictedDateOfStart());
        boolean condition = t2.getTaskState().isOnPlannedState();
        assertTrue(condition);
    }

    @Test

    public void testSetGantTaskDependencies() {
        t1.setGanttTaskDependencies("1-1");
        String result = t1.getGanttTaskDependencies();
        assertEquals("1-1", result);

    }

    @Test

    public void testCheckSetTitle_titleonlyTitleConstructor() {
        // Given
        // When
        String title = "titulo";
        t1.setTitle(title);
        // Then
        assertEquals(title, t1.getTitle());
    }

    @Test

    public void testCheckDescription_onlyTitleConstructor() {
        // Given
        // When
        String description = "descri��o";
        t1.setDescription(description);
        // Then
        assertEquals(description, t1.getDescription());
    }

    @Test

    public void testCheckSetStartDate_startDateonlyTitleConstructor() {
        // Given
        // When
        t1.setEffectiveStartDate(d1);
        // Then
        assertEquals(d1, t1.getEffectiveStartDate());
    }

    @Test

    public void testCheckConclusionDate_onlyTitleConstructor() {
        // Given
        // When
        t1.setPredictedDateOfConclusion(d4);
        // Then
        assertEquals(d4, t1.getPredictedDateOfConclusion());
    }

    @Test

    public void testCheckNullDateOfConclusion_nullDates() {
        // Given
        // Then
        assertNull(t1.getEfectiveDateOfConclusion());
    }

    @Test

    public void testCheckEmptyStarttDate_nullDates() {
        // Given
        // Then
        assertNull(t4.getEffectiveStartDate());
        // When
        t4.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t4
                .getTaskCollaboratorRegistryByID(t4.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        t4.addReport(col2, 2, d1, d1.plusDays(5));
        // Then
        assertEquals(d1, t4.getEffectiveStartDate());
        t4.removeProjectCollaborator(col2);
    }

    @Test

    public void testCheckGetUnitCost_titleonlyTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7");
        // Then
        assertEquals(0.0, t7.getUnitCost(), 0.01);
    }

    @Test

    public void testCheckGetUnitCost_fullTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7", "Test 7 Task", d1, d2, 3, 50);
        // Then
        assertEquals(3.0, t7.getUnitCost(), 0.01);
    }

    @Test

    public void testCheckGetEstimatedEffort_titleonlyTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7");
        // Then
        assertEquals(0.0, t7.getEstimatedEffort(), 0.01);
    }

    @Test

    public void testCheckgetEstimatedEffort_fullTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7", "Test 7 Task", d1, d2, 3, 50);
        // Then
        assertEquals(50.0, t7.getEstimatedEffort(), 0.01);
    }

    @Test

    public void testCheckGetPredictedDateOfStart_titleonlyTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7");
        // Then
        assertNull(t7.getPredictedDateOfStart());
    }

    @Test

    public void testCheckGetPredictedDateOfStart_fullTitleConstructor() {
        // Given
        // When
        t7 = new Task(project, "Test 7", "Test 7 Task", d1, d2, 3, 50);
        // Then
        assertEquals(d1, t7.getPredictedDateOfStart());
    }

    @Test

    public void testCheckSetPredictedDateOfStartCreatedState() {
        // Given
        // When
        t7 = new Task(project, "Test 7");
        // Then
        boolean condition1 = t7.getTaskState().isOnCreatedState();
        assertTrue(condition1);
        // When
        t7.setPredictedDateOfStart(d1);
        // Then
        boolean condition2 = t7.getTaskState().isOnPlannedState();
        assertTrue(condition2);
    }

    @Test

    public void testCheckSetPredictedDateOfStartPlannedState() {
        // Given
        // When
        t7 = new Task(project, "Test 7", "Test 7 Task", d1, d2, 3, 50);
        // Then
        boolean condition = t7.getTaskState().isOnPlannedState();
        assertTrue(condition);
        // When
        t7.setPredictedDateOfStart(d1.plusDays(20));
        // Then
        assertEquals(d1.plusDays(20), t7.getPredictedDateOfStart());
    }

    @Test

    public void testCheckSetPredictedDateOfStartAssignedState() {
        // Given
        // When
        t7 = new Task(project, "Test 7");

        t7.setPredictedDateOfStart(d1);
        t7.setPredictedDateOfConclusion(d2);
        t7.addTaskDependency(t1);
        t7.addProjectCollaborator(col1);
        // Then
        boolean condition = t7.getTaskState().isOnAssignedState();
        assertTrue(condition);
        // When
        t7.setPredictedDateOfStart(d1.plusDays(20));
        // Then
        assertEquals(d1.plusDays(20), t7.getPredictedDateOfStart());
    }

    @Test

    public void testCheckSetPredictedDateOfStartReadyToStartState() {
        // Given
        // When
        t7 = new Task(project, "Test 7", "Test 7 Task", d1, d2, 3, 50);
        t7.addProjectCollaborator(col1);
        // Then
        boolean condition = t7.getTaskState().isOnReadyToStartState();
        assertTrue(condition);
        // When
        t7.setPredictedDateOfStart(d1.plusDays(25));
        // Then
        assertEquals(d1.plusDays(25), t7.getPredictedDateOfStart());
    }

    @Test

    public void testGetColaboratedReport() {
        // Given
        // Then
        int reportCount = TaskCollaboratorRegistry.getThisTasksReportCount();
        List<Report> expected = new ArrayList<>();
        expected.add(t1.findReportById(t1.getId() + "(" + (reportCount - 2) + ")"));
        expected.add(t1.findReportById(t1.getId() + "(" + (reportCount - 1) + ")"));
        expected.add(t1.findReportById(t1.getId() + "(" + (reportCount) + ")"));
        assertEquals(expected, t1.listCollaboratorReports(col1));
    }

    @Test

    public void testGetColaboratedReportEmptyList() {
        // Given
        // Then
        List<Report> expected = new ArrayList<>();
        assertEquals(expected, t4.listCollaboratorReports(col1));
    }

    @Test

    public void testCheckcalculateWorkDaysBetweenStartAndConclusion() {
        // Given
        // When
        t1.setEffectiveStartDate(d4);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d1);
        // Then
        assertEquals(11, t1.calculateWorkDaysBetweenStartAndConclusion());
    }

    @Test

    public void testCheckcalculateWorkDaysBetweenStartAndConclusion_difYears() {
        // Given
        // When
        t1.setEffectiveStartDate(d4);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d3);
        // Then
        assertEquals(795, t1.calculateWorkDaysBetweenStartAndConclusion());
    }

    @Test

    public void testGetActiveUsersListEmptyList() {
        // Given
        // Then
        Set<ProjectCollaborator> Expected = new HashSet<>();
        assertEquals(Expected, t4.listActiveCollaborators());
    }

    @Test

    public void testGetActiveUsersList() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.addProjectCollaborator(col2);
        // Then
        Set<ProjectCollaborator> Expected = new HashSet<>();
        Expected.add(col1);
        Expected.add(col2);
        assertEquals(Expected, t1.listActiveCollaborators());
    }

    @Test

    public void testAddUserToTask() {
        // Given
        // Then
        boolean condition = t2.addProjectCollaborator(col1);
        assertTrue(condition);
        Set<ProjectCollaborator> expected = new HashSet<>();
        expected.add(col1);
        expected.add(col2);
        assertEquals(expected, t1.listActiveCollaborators());
    }

    @Test

    public void testAddUserToTaskFail() {
        // Given
        // Then
        boolean condition = t1.addProjectCollaborator(col2);
        assertFalse(condition);
        Set<ProjectCollaborator> expected = new HashSet<>();
        expected.add(col1);
        expected.add(col2);
        assertEquals(expected, t1.listActiveCollaborators());
    }

    @Test

    public void removeProjectCollaboratorFromTaskSucess() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.removeProjectCollaborator(col1);
        t1.removeProjectCollaborator(col2);
        // Then
        Set<ProjectCollaborator> Expected = new HashSet<>();
        int reportSize = t1.listTaskCollaboratorRegistry(col1).size();
        assertNotNull(t1.listTaskCollaboratorRegistry(col1).get(reportSize - 1).getCollaboratorRemovedFromTaskDate());
        assertEquals(Expected, t1.listActiveCollaborators());
        t1.listTaskCollaboratorRegistry(col1).get(0).setCollaboratorRemovedFromTaskDate(null);
        assertNull(t1.listTaskCollaboratorRegistry(col1).get(0).getCollaboratorRemovedFromTaskDate());
        assertEquals(RegistryStatus.REMOVALAPPROVED, t1.listTaskCollaboratorRegistry(col1).get(0).getRegistryStatus());
    }

    @Test

    public void testGetTaskCollaboratorList() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.addProjectCollaborator(col2);
        Set<TaskCollaboratorRegistry> expected = new HashSet<>(t1.listAllTaskCollaboratorRegistry());
        // Then
        assertEquals(expected, t1.listAllTaskCollaboratorRegistry());
    }

    @Test

    public void testHasCollaboratorInReportList() {
        // Given
        // Then
        boolean condition1 = t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col1);
        boolean condition2 = t4.hasProjectCollaboratorInTaskCollaboratorRegistryList(col2);
        assertTrue(condition1);
        assertFalse(condition2);
    }

    @Test

    public void testHasCollaboratorTask() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        // Then
        boolean condition1 = t1.hasProjectCollaborator(col1);
        boolean condition2 = t4.hasProjectCollaborator(col2);
        assertTrue(condition1);
        assertFalse(condition2);
    }

    @Test

    public void testIsCompleted() {
        // Given
        // When
        t2.setTaskCompleted();
        t3.addProjectCollaborator(col2);
        t3.setEffectiveStartDate(d1);
        t3.setTaskCompleted();
        t5.setTaskCompleted();
        t6.setTaskCompleted();
        // Then
        boolean condition1 = t3.isCompleted();
        boolean condition2 = t1.isCompleted();
        assertTrue(condition1);
        assertFalse(condition2);
        t3.removeProjectCollaborator(col2);
    }

    @Test

    public void testSetTaskCompletedSuccess() {
        // Given
        // Then
        boolean condition1 = t1.setTaskCompleted();
        boolean condition2 = t1.getTaskState().isOnCompletedState();
        assertTrue(condition1);
        assertTrue(condition2);

        for (TaskCollaboratorRegistry taskCollReg : t1.listAllTaskCollaboratorRegistry()) {
            boolean condition3 = taskCollReg.getCollaboratorRemovedFromTaskDate() != null;
            assertTrue(condition3);
        }
        boolean condition4 = t1.getEfectiveDateOfConclusion() != null;
        boolean condition5 = t1.isCompleted();
        assertTrue(condition4);
        assertTrue(condition5);
    }

    @Test

    public void testIsCompletedLastMonth() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        // When
        t3.addProjectCollaborator(col2);
        t3.setEffectiveStartDate(d1);
        t3.setTaskCompleted();
        t5.setTaskCompleted();
        t3.setEffectiveDateOfConclusion(d9);
        // Then
        boolean condition1 = t3.isCompletedLastMonth();
        boolean condition2 = t5.isCompletedLastMonth();
        assertTrue(condition1);
        assertFalse(condition2);
    }

    @Test

    public void testUserHoursinTask() {
        // Given
        List<ProjectCollaborator> expected = new ArrayList<>();
        expected.add(col1);
        // When
        ProjectCollaborator col3;
        col3 = new ProjectCollaborator(project, user3, 5);
        t1.addProjectCollaborator(col3);
        // Then
        boolean condition = expected.contains(col1);
        assertTrue(condition);
        assertEquals(16.0, t1.calculateProjectCollaboratorWorkedHours(col1), 0.01);
        assertEquals(0.0, t1.calculateProjectCollaboratorWorkedHours(col3), 0.01);
    }

    @Test

    public void testEqualsFalseDifgetClass() {

        TaskDTO taskdto = new TaskDTO();

        boolean condition = t2.equals(taskdto);
        assertFalse(condition);
    }

    @Test

    public void testEqualsEqualsNullObject() {
        // Given
        // When
        t1 = null;
        // Then
        boolean condition = t2.equals(t1);
        assertFalse(condition);
    }

    @Test

    public void testcompareToFalseNull1() {
        // Given
        // When
        t1.setEffectiveDateOfConclusion(null);
        // Then
        assertEquals(0, t2.compareTo(t1));
        assertEquals(0, t1.compareTo(t2));
    }

    @Test

    public void testcompareToFalseNull3() {
        // Given
        // When
        t1.setEffectiveDateOfConclusion(null);
        t2.setEffectiveDateOfConclusion(d1);
        // Then
        assertEquals(0, t2.compareTo(t1));
        assertEquals(0, t1.compareTo(t2));
    }

    @Test

    public void testcompareToFalseNull2() {
        // Given
        // When
        t1 = null;
        // Then
        assertEquals(0, t2.compareTo(t1));
    }

    @Test

    public void testAddHoursToUserToTask() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.addProjectCollaborator(col2);
        t1.addReport(col1, 5, d7, d7.plusDays(5));
        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t1.addReport(col2, 6, d8, d8.plusDays(5));
        // Then
        assertEquals(26.0, t1.calculateProjectCollaboratorWorkedHours(col1), 0.1);
    }

    @Test

    public void testAddHoursNullEffectiveStartDate() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t6
                .getTaskCollaboratorRegistryByID(t6.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        t6.setEffectiveStartDate(null);
        t6.addReport(col2, 5, d1, d1.plusDays(5));
        // Then
        assertEquals(d1, t6.getEffectiveStartDate());
    }

    @Test

    public void testAddHourstTaskDependenciesCanStartFail() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.addTaskDependency(t2);
        t1.addReport(col1, 5, d1, d1.plusDays(5));
        // Then
        assertNotEquals(d1, t1.getEffectiveStartDate());
    }

    @Test

    public void testAddHourstTaskDependenciesCanStartFailAndEffectiveStartDateNull() {
        // Given
        // When
        t1.addProjectCollaborator(col1);
        t1.setEffectiveStartDate(null);
        t1.addProjectCollaborator(col1);
        t1.addTaskDependency(t2);
        t1.addReport(col1, 5, d1, d1.plusDays(5));
        // Then
        assertNotEquals(d1, t1.getEffectiveStartDate());
    }

    @Test

    public void testSearchTaskCollaboratorRegistryByIDSuccess() {
        // Given
        // Then
        boolean condition = t1.getTaskCollaboratorRegistryByID(t1.getId() + "-ana@gmail.com")
                .getTaskCollaboratorRegistryID().equals(t1.getId() + "-ana@gmail.com");
        assertTrue(condition);
    }

    @Test

    public void testSearchTaskCollaboratorRegistryByIDNullValue() {
        // Given
        // Then
        assertNull(t1.getTaskCollaboratorRegistryByID("2-ana@gmail.com"));
    }

    @Test

    public void testGetTaskDependenciesCreatedStateWithDependencyCancelled() {
        // Given
        // When
        t8.setTaskCancelled();
        // Then
        boolean condition1 = t8.getTaskState().isOnCancelledState();
        boolean condition2 = t2.getTaskState().isOnCreatedState();
        assertFalse(condition1);
        assertTrue(condition2);
        // When
        t2.addTaskDependency(t8);
        List<Task> taskDependenciesList = new ArrayList<>();
        taskDependenciesList.add(t8);
        // Then
        assertEquals(taskDependenciesList, t2.getTaskDependencies());
    }

    @Test
    @Transactional
    public void testSetTaskCancelled() {
        // Given
        TaskCollaboratorRegistry anaRegistry = t1.getLastTaskCollaboratorRegistryOf(col1);
        TaskCollaboratorRegistry bebianaRegistry = t1.getLastTaskCollaboratorRegistryOf(col2);
        boolean condition1 = t1.getTaskState().isOnCancelledState();
        assertFalse(condition1);

        //When
        boolean condition2 = t1.setTaskCancelled();
        // Then
        boolean condition3 = t1.getTaskState().isOnCancelledState();
        assertTrue(condition2);
        assertTrue(condition3);

        assertNotNull(anaRegistry.getCollaboratorRemovedFromTaskDate());
        assertNotNull(bebianaRegistry.getCollaboratorRemovedFromTaskDate());

    }

    @Test
    @Transactional
    public void testSetTaskSuspended() {
        // Given
        TaskCollaboratorRegistry anaRegistry = t1.getLastTaskCollaboratorRegistryOf(col1);
        TaskCollaboratorRegistry bebianaRegistry = t1.getLastTaskCollaboratorRegistryOf(col2);
        boolean condition1 = t1.getTaskState().isOnInProgressState();
        assertTrue(condition1);

        //When
        boolean condition2 = t1.setTaskSuspended();
        // Then
        boolean condition3 = t1.getTaskState().isOnSuspendedState();
        assertTrue(condition2);
        assertTrue(condition3);

        assertNotNull(anaRegistry.getCollaboratorRemovedFromTaskDate());
        assertNotNull(bebianaRegistry.getCollaboratorRemovedFromTaskDate());

    }

    @Test
    @Transactional
    public void testSetTaskSuspendedNotInState() {
        // Given
        TaskCollaboratorRegistry anaRegistry = t1.getLastTaskCollaboratorRegistryOf(col1);
        TaskCollaboratorRegistry bebianaRegistry = t1.getLastTaskCollaboratorRegistryOf(col2);
        boolean condition1 = t1.getTaskState().isOnInProgressState();
        assertTrue(condition1);
        //When
        t1.setTaskCancelled();
        boolean condition2 = t1.getTaskState().isOnCancelledState();
        assertTrue(condition2);
        boolean condition3 = t1.setTaskSuspended();
        // Then
        boolean condition4 = t1.getTaskState().isOnSuspendedState();
        assertFalse(condition3);
        assertFalse(condition4);

    }

    @Test

    public void testGetTaskDependenciesWithCancelledDependency() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        boolean condition1 = t1.getTaskState().isOnCancelledState();
        assertFalse(condition1);
        // When
        t1.setTaskCancelled();
        // Then
        boolean condition2 = t1.getTaskState().isOnCancelledState();
        boolean condition3 = t2.getTaskState().isOnCreatedState();
        assertTrue(condition2);
        assertTrue(condition3);
        // When
        t2.addTaskDependency(t1);
        // then
        assertEquals(taskDependenciesList, t2.getTaskDependencies());
    }

    @Test

    public void testAddReportFailureCollaboratorNotAssignedToThisTask() {
        // Given
        // Then
        String expected = "This collaborator has never worked in this task.";
        String result = t6.addReport(col2, 6, d1, d1.plusDays(5));
        assertEquals(expected, result);
    }

    @Test

    public void testAddReportAuxFailureCollaboratorCurrentlyNotWorkingInThisTask() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        t6.addReport(col2, 6, d1, d1.plusDays(5));
        t6.removeProjectCollaborator(col2);
        // Then
        String expected = "This task's state doesn't permit adding reports";
        String result = t6.addReport(col2, 15, d1, d1.plusDays(5));
        assertEquals(expected, result);
    }

    @Test

    public void testAddReportFailureInvalidReportDates() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        t6.addReport(col2, 6, d1, d1.plusDays(10));
        assertFalse(t6.isReportPeriodCoincidingWithProjectCollaboratorPeriod(col2, d1, d2));
        // Then
        String expected = "Invalid report dates. Please try again.";
        String result = t6.addReport(col2, 6, d1, d1.plusDays(10));
        assertEquals(expected, result);
    }

    @Test

    public void testAddReportSuccess() {
        // Given
        t6.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t6
                .getTaskCollaboratorRegistryByID(t6.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        // Then
        String expected = "Report successfully added";
        String result = t6.addReport(col2, 6, d1, d1.plusDays(5));
        assertEquals(expected, result);
    }

    @Test

    public void testAddReportFailureNegativeHours() {
        // Given
        // Then
        t6.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t6
                .getTaskCollaboratorRegistryByID(t6.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        // Then
        String expected = "Invalid number of hours. Please try again.";
        String result = t6.addReport(col2, -6, d1, d1.plusDays(5));
        assertEquals(expected, result);
    }


    @Test

    public void testAddReportRestFailureCollaboratorNotAssignedToThisTask() {
        // Given
        // Then
        boolean result = t6.addReportRest(col2, 6, d1, d1.plusDays(5));
        assertFalse(result);
    }

    @Test

    public void testAddReportRestAuxFailureCollaboratorCurrentlyNotWorkingInThisTask() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        t6.addReport(col2, 6, d1, d1.plusDays(5));
        t6.removeProjectCollaborator(col2);
        // Then
        boolean result = t6.addReportRest(col2, 15, d1, d1.plusDays(5));
        assertFalse(result);
    }

    @Test

    public void testAddReportRestSuccess() {
        // Given
        t6.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t6
                .getTaskCollaboratorRegistryByID(t6.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        // Then
        assertNull(t6.getEffectiveStartDate());
        boolean result = t6.addReportRest(col2, 6, d1, d1.plusDays(5));
        assertEquals(t6.getEffectiveStartDate(), t6.listAllReports().get(0).getStartDate());

        assertTrue(result);
    }

    @Test

    public void testAddReportRestFailureNegativeHours() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        TaskCollaboratorRegistry col2TaskRegistry = t6
                .getTaskCollaboratorRegistryByID(t6.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        // Then
        boolean result = t6.addReportRest(col2, -6, d1, d1.plusDays(5));
        assertFalse(result);
    }

    @Test

    public void testHashCode() {
        // Given
        t6.setTaskId("1-4");
        // Then
        assertEquals(t4.hashCode(), t6.hashCode());
        assertNotEquals(t1.hashCode(), t2.hashCode());
    }

    @Test

    public void testIsTaskValid() {
        // Given
        // Then
        boolean condition = t1.isValid();
        assertTrue(condition);

    }

    @Test

    public void testToString() {
        // Given
        // Then
        StringBuilder sb = new StringBuilder();
        sb.append("Task ID: " + t3.getId() + "\t");
        sb.append("Task Title: " + t3.getTitle() + "\t");
        sb.append("Task Status: " + t3.getStatus() + "\n");
        sb.append("\t\tTask Predicted Start Date: " + t3.getPredictedDateOfStart() + "\n");
        sb.append("\t\tTask Predicted End Date: " + t3.getPredictedDateOfConclusion() + "\n");
        sb.append("\t\tTask Effective Start Date: " + t3.getEffectiveStartDate() + "\n");
        sb.append("\t\tTask Effective End Date: " + t3.getEfectiveDateOfConclusion() + "\n\n");
        String expectedResult = sb.toString();
        assertEquals(expectedResult, t3.toString());
    }

    @Test

    public void cancelAssignmentRequestTest_success() {
        // Given
        // When
        t1.requestAddProjectCollaborator(col3);
        t1.cancelAssignmentRequest(col3);
        // Then
        assertEquals(RegistryStatus.ASSIGNMENTCANCELLED,
                t1.getLastTaskCollaboratorRegistryOf(col3).getRegistryStatus());
    }

    @Test

    public void requestRemoveProjectCollaboratorFromTask_success() {
        // Given
        // When
        assertTrue(t1.requestRemoveProjectCollaborator(col1));
        // Then
        assertEquals(RegistryStatus.REMOVALREQUEST, t1.getLastTaskCollaboratorRegistryOf(col1).getRegistryStatus());
    }

    @Test

    public void requestRemoveProjectCollaboratorFromTask_failNotInAssignementAprovedStatus() {
        // Given
        // When
        t1.requestAddProjectCollaborator(col3);
        t1.requestRemoveProjectCollaborator(col3);
        // Then
        assertNotEquals(RegistryStatus.REMOVALREQUEST, t1.getLastTaskCollaboratorRegistryOf(col3).getRegistryStatus());
    }

    @Test

    public void cancelRemovalRequestTest() {
        // Given
        // When
        t1.requestRemoveProjectCollaborator(col1);
        t1.cancelRemovalRequest(col1);
        // Then
        assertEquals(RegistryStatus.ASSIGNMENTAPPROVED, t1.getLastTaskCollaboratorRegistryOf(col1).getRegistryStatus());
    }

    @Test

    public void cancelRemovalRequestTestNotInRemovalRequest() {
        // Given
        // When
        t1.requestTaskCompleted(col2);
        t1.cancelRemovalRequest(col2);
        // Then
        assertEquals(RegistryStatus.ASSIGNMENTAPPROVED, t1.getLastTaskCollaboratorRegistryOf(col1).getRegistryStatus());
    }

    @Test

    public void listAssignmentRequestTest() {
        // Given
        // When
        t2.requestAddProjectCollaborator(col3);
        t2.requestAddProjectCollaborator(col2);
        TaskCollaboratorRegistry tcr1 = t2.listTaskCollaboratorRegistry(col3).get(0);
        TaskCollaboratorRegistry tcr2 = t2.listTaskCollaboratorRegistry(col2).get(0);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        expected.add(tcr1);
        expected.add(tcr2);
        // Then
        assertThat(t2.listAssignmentRequests(), containsInAnyOrder(expected.toArray()));
    }

    @Test

    public void listRemovalRequestTest() {
        // Given
        // When
        t1.requestRemoveProjectCollaborator(col1);
        t1.requestRemoveProjectCollaborator(col2);
        TaskCollaboratorRegistry tcr1 = t1.listTaskCollaboratorRegistry(col1).get(0);
        TaskCollaboratorRegistry tcr2 = t1.listTaskCollaboratorRegistry(col2).get(0);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        expected.add(tcr1);
        expected.add(tcr2);
        // Then
        assertThat(t1.listRemovalRequests(), containsInAnyOrder(expected.toArray()));
    }

    @Test

    public void setTaskCancelledTest_successOnProgressState() {
        // Given
        // When
        t1.setTaskCancelled();
        // Then
        boolean condition = t1.getTaskState().isOnCancelledState();
        assertTrue(condition);
    }

    @Test

    public void setTaskCancelledTest_successOnSuspendedState() {
        // Given
        // When
        t1.removeProjectCollaborator(col1);
        t1.removeProjectCollaborator(col2);
        t1.removeProjectCollaborator(col3);
        t1.setTaskCancelled();
        // Then
        boolean condition = t1.getTaskState().isOnCancelledState();
        assertTrue(condition);
    }

    @Test

    public void calculateTotalCostsoFarTest() {
        // Given
        // Then
        assertEquals(130.0, t1.calculateTotalCostsoFar(t1.getProject().getCostCalculator()), 0.1);
    }

    @Test

    public void addTaskDependencySucessCreatedState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        taskDependenciesList.add(t3);
        assertTrue(t2.getTaskState().isOnCreatedState());
        assertFalse(t2.getTaskState().isOnPlannedState());
        assertFalse(t2.getTaskState().isOnAssignedState());
        assertFalse(t2.getTaskState().isOnReadyToStartState());
        // When
        t2.addTaskDependency(t3);
        // Then
        assertEquals(taskDependenciesList, t2.getTaskDependencies());
        assertTrue(t2.getTaskState().isOnCreatedState());
        assertFalse(t2.getTaskState().isOnPlannedState());
        assertFalse(t2.getTaskState().isOnAssignedState());
        assertFalse(t2.getTaskState().isOnReadyToStartState());

    }

    @Test

    public void addTaskDependencySucessReadyToStartState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        taskDependenciesList.add(t3);
        // When
        t4.addProjectCollaborator(col1);
        // Then
        assertFalse(t4.getTaskState().isOnCreatedState());
        assertFalse(t4.getTaskState().isOnPlannedState());
        assertFalse(t4.getTaskState().isOnAssignedState());
        assertTrue(t4.getTaskState().isOnReadyToStartState());
        // When
        t4.addTaskDependency(t3);
        // Then
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
        assertFalse(t4.getTaskState().isOnCreatedState());
        assertFalse(t4.getTaskState().isOnPlannedState());
        assertTrue(t4.getTaskState().isOnAssignedState());
        assertFalse(t2.getTaskState().isOnReadyToStartState());
    }

    @Test

    public void addTaskDependencySucessPlannedState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        taskDependenciesList.add(t3);
        assertFalse(t4.getTaskState().isOnCreatedState());
        assertTrue(t4.getTaskState().isOnPlannedState());
        assertFalse(t4.getTaskState().isOnAssignedState());
        assertFalse(t4.getTaskState().isOnReadyToStartState());
        // When
        t4.addTaskDependency(t3);
        // Then
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
        assertFalse(t4.getTaskState().isOnCreatedState());
        assertTrue(t4.getTaskState().isOnPlannedState());
        assertFalse(t4.getTaskState().isOnAssignedState());
        assertFalse(t4.getTaskState().isOnReadyToStartState());

    }

    @Test

    public void addTaskDependencySucessAssignedState() {
        // Given
        // When
        List<Task> taskDependenciesList = new ArrayList<>();
        t3.addProjectCollaborator(col1);
        // Then
        assertFalse(t3.getTaskState().isOnCreatedState());
        assertFalse(t3.getTaskState().isOnPlannedState());
        // When
        t3.addTaskDependency(t7);
        // Then
        assertTrue(t3.getTaskState().isOnAssignedState());
        assertFalse(t3.getTaskState().isOnReadyToStartState());
        taskDependenciesList.add(t7);
        assertEquals(taskDependenciesList, t3.getTaskDependencies());
        assertFalse(t3.getTaskState().isOnCreatedState());
        assertFalse(t3.getTaskState().isOnPlannedState());
        assertTrue(t3.getTaskState().isOnAssignedState());
        assertFalse(t3.getTaskState().isOnReadyToStartState());
    }

    @Test

    public void addTaskDependencyFailInvalidState() {
        // Given
        // When
        List<Task> taskDependenciesList = new ArrayList<>();
        t1.setTaskCompleted();
        // Then
        assertFalse(t1.getTaskState().isOnCreatedState());
        assertFalse(t1.getTaskState().isOnPlannedState());
        assertFalse(t1.getTaskState().isOnAssignedState());
        assertFalse(t1.getTaskState().isOnReadyToStartState());
        // When
        t1.addTaskDependency(t3);
        // Then
        assertFalse(t1.getTaskState().isOnCreatedState());
        assertFalse(t1.getTaskState().isOnPlannedState());
        assertFalse(t1.getTaskState().isOnAssignedState());
        assertFalse(t1.getTaskState().isOnReadyToStartState());
        assertEquals(taskDependenciesList, t1.getTaskDependencies());
    }

    @Test

    public void addTaskDependencySuccessTaskNotInCancelledState() {
        List<Task> taskDependenciesList = new ArrayList<>();
        // Given
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
        // Then
        assertTrue(t4.getTaskState().isOnPlannedState());
        // When
        t3.changeStateTo();
        // Then
        assertFalse(t3.getTaskState().isOnCancelledState());
        // When
        t4.addTaskDependency(t3);
        taskDependenciesList.add(t3);
        // Then
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test

    public void addTaskDependencyFailCompletedState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        assertTrue(t4.getTaskState().isOnPlannedState());
        // When
        t1.setEffectiveStartDate(d1);
        t1.changeStateTo();
        t1.setTaskCompleted();
        // Then
        assertTrue(t1.getTaskState().isOnCompletedState());
        // When
        t4.addTaskDependency(t1);
        // Then

        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test
    public void addTaskDependencyFailCancelledState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        assertTrue(t4.getTaskState().isOnPlannedState());
        // When
        t1.setTaskCancelled();
        // Then
        assertTrue(t1.getTaskState().isOnCancelledState());
        // When
        t4.addTaskDependency(t1);
        // Then

        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test

    public void addTaskDependencySuspendedState() {
        // Given
        // Then
        List<Task> taskDependenciesList = new ArrayList<>();
        assertTrue(t4.getTaskState().isOnPlannedState());
        // When
        t1.setEffectiveStartDate(d1);
        t1.changeStateTo();
        t1.removeProjectCollaborator(col1);
        t1.removeProjectCollaborator(col2);
        // Then
        assertTrue(t1.getTaskState().isOnSuspendedState());
        // When
        t4.addTaskDependency(t1);
        // Then
        taskDependenciesList.add(t1);
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test

    public void addTaskDependencyFailSameTask() {
        // Given
        List<Task> taskDependenciesList = new ArrayList<>();
        // When
        assertTrue(t4.getTaskState().isOnPlannedState());

        t4.addTaskDependency(t4);
        // Then

        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test
    public void addTaskDependencyFailTaskisNotInProject() {
        // Given
        List<Task> taskDependenciesList = new ArrayList<>();
        assertTrue(t4.getTaskState().isOnPlannedState());
        // When
        Task testTask = new Task(project, "taskTast");
        t4.addTaskDependency(testTask);
        // Then
        assertEquals(taskDependenciesList, t4.getTaskDependencies());
    }

    @Test

    public void addTaskDependencyFailTaskAlreadyInDependenciesList() {
        // Given
        // When
        List<Task> taskDependenciesList = new ArrayList<>();
        t3.addProjectCollaborator(col1);
        // Then
        assertFalse(t3.getTaskState().isOnCreatedState());
        assertFalse(t3.getTaskState().isOnPlannedState());
        // When
        t3.addTaskDependency(t7);
        taskDependenciesList.add(t7);
        // Then
        assertTrue(t3.getTaskState().isOnAssignedState());
        assertFalse(t3.getTaskState().isOnReadyToStartState());
        assertEquals(taskDependenciesList, t3.getTaskDependencies());
        t3.addTaskDependency(t7);
        assertEquals(taskDependenciesList, t3.getTaskDependencies());

    }

    @Test

    public void testRequestAddProjectCollaboratorToTaskSuccess() {
        // Given
        // When
        t6.addProjectCollaborator(col2);
        t6.addReport(col2, 6, d1, d1.plusDays(5));
        t6.removeProjectCollaborator(col2);
        // Then
        assertTrue(
                t6.getLastTaskCollaboratorRegistryOf(col2).getRegistryStatus().equals(RegistryStatus.REMOVALAPPROVED));
        String expected = "Request successfully sent.";
        String result = t6.requestAddProjectCollaborator(col2);
        assertEquals(expected, result);
        assertEquals(expected, result);
    }

    @Test

    public void testAddProjectCollaboratorToTask() {
        // Given
        // When
        t6.requestAddProjectCollaborator(col2);
        // Then
        assertTrue(t6.getLastTaskCollaboratorRegistryOf(col2).getRegistryStatus()
                .equals(RegistryStatus.ASSIGNMENTREQUEST));
        // When
        t6.addProjectCollaborator(col2);
        // Then
        assertTrue(t6.getLastTaskCollaboratorRegistryOf(col2).getRegistryStatus()
                .equals(RegistryStatus.ASSIGNMENTAPPROVED));
        // When
        t6.getLastTaskCollaboratorRegistryOf(col2).setRegistryStatus(RegistryStatus.REMOVALAPPROVED);
        t6.addProjectCollaborator(col2);
        // Then
        assertFalse(t6.getLastTaskCollaboratorRegistryOf(col2).getRegistryStatus()
                .equals(RegistryStatus.ASSIGNMENTAPPROVED));
    }

    @Test

    public void testRequestRemoveProjectCollaboratorFromTask() {
        // Given
        // When
        t6.requestAddProjectCollaborator(col2);
        // Then
        assertTrue(t6.getLastTaskCollaboratorRegistryOf(col2).getRegistryStatus()
                .equals(RegistryStatus.ASSIGNMENTREQUEST));
        assertFalse(t6.requestRemoveProjectCollaborator(col2));
    }

    @Test

    public void testCanStart() {
        // Given
        // When
        t3.addProjectCollaborator(col1);
        t2.addProjectCollaborator(col1);
        TaskCollaboratorRegistry col1TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        // Then
        assertTrue(t3.getTaskState().isOnReadyToStartState());
        // When
        t3.addTaskDependency(t2);
        t2.addReport(col1, 2, d1, d1.plusDays(5));
        // Then
        assertTrue(t3.getTaskState().isOnAssignedState());
        // When
        t2.setTaskCompleted();
        // Then
        assertTrue(t2.getTaskState().isOnCompletedState());
        assertTrue(t3.canStart());
    }

    @Test

    public void testCanStartFalse() {
        // Given
        // When
        t3.addProjectCollaborator(col1);
        // Then
        assertTrue(t3.getTaskState().isOnReadyToStartState());
        // When
        t3.addTaskDependency(t2);
        // Then
        assertTrue(t3.getTaskState().isOnAssignedState());
        assertFalse(t2.getTaskState().isOnCompletedState());
        assertFalse(t3.canStart());

    }

    @Test

    public void testCanStartTrueTaskCancelled() {
        // Given
        // When
        t7.addProjectCollaborator(col1);
        TaskCollaboratorRegistry col1TaskRegistry = t7
                .getTaskCollaboratorRegistryByID(t7.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t7.addReport(col1, 5, d8, d8.plusDays(5));
        t3.addProjectCollaborator(col1);
        // Then
        assertTrue(t3.getTaskState().isOnReadyToStartState());
        // When
        t3.addTaskDependency(t7);
        // Then
        assertTrue(t3.getTaskState().isOnAssignedState());
        // When
        t7.setTaskCancelled();
        // Then
        assertTrue(t7.getTaskState().isOnCancelledState());
        assertTrue(t3.canStart());

    }

    @Test

    public void testCanStartEmptyList() {
        // Given
        // Then
        assertTrue(t4.canStart());
    }

    @Test
    public void testListActiveCollaborators() {
        // Given
        // Then
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col1));
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col2));
        Set<ProjectCollaborator> expectedList = t1.listActiveCollaborators();
        assertEquals(2, expectedList.size());
        assertEquals(true, expectedList.contains(col1));
        assertEquals(true, expectedList.contains(col2));
    }

    @Test

    public void testListActiveCollaboratorsAfterRemoveCollaborator() {
        // Given
        // Then
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col1));
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col2));
        // When
        t1.removeProjectCollaborator(col1);
        // Then
        Set<ProjectCollaborator> expectedList = t1.listActiveCollaborators();
        assertEquals(1, expectedList.size());
        assertEquals(false, expectedList.contains(col1));
        assertEquals(true, expectedList.contains(col2));
    }

    @Test

    public void testListActiveCollaboratorsAfterCompletedTask() {
        // Given
        // Then
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col1));
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col2));
        // When
        t1.setTaskCompleted();
        Set<ProjectCollaborator> expectedList = t1.listActiveCollaborators();
        // Then
        assertEquals(0, expectedList.size());
        assertEquals(false, expectedList.contains(col1));
        assertEquals(false, expectedList.contains(col2));
    }

    @Test

    public void testListActiveCollaboratorsAfterRemovalRequest() {
        // Given
        // Then
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col1));
        assertEquals(true, t1.hasProjectCollaboratorInTaskCollaboratorRegistryList(col2));
        // When
        t1.requestRemoveProjectCollaborator(col2);
        Set<ProjectCollaborator> expectedList = t1.listActiveCollaborators();
        // Then
        assertEquals(2, expectedList.size());
        assertEquals(true, expectedList.contains(col1));
        assertEquals(true, expectedList.contains(col2));
    }

    @Test

    public void testGetState() {
        // Given
        // Then
        String state = "InProgress";
        assertEquals(state, t1.getState());
    }

    @Test

    public void testSetState() {
        // Given
        // When
        String state = "InProgress";
        t1.setTaskStatePersisted(state);
        // Then
        assertEquals(state, t1.getState());
    }

    @Test

    public void testSetEstimatedEffort() {
        // Given
        // When
        t1.setEstimatedEffort(2.0);
        // Then
        assertEquals(2.0, t1.getEstimatedEffort(), 0.01);
    }

    @Test

    public void testSetUnitCost() {
        // Given
        // When
        t1.setUnitCost(2.0);
        // Then
        assertEquals(2.0, t1.getUnitCost(), 0.01);
    }

    @Test(expected = Exception.class)
    public void testGetTaskStateFailureCouldNotConstructObject() {

        // When
        t1.setTaskState(null);
        t1.setTaskStatePersisted(null);

        // Then
        t1.getTaskState(); // throws an exception
    }

    @Test

    public void testIsReportPeriodCoincidingWithRegistryNullDateSucess() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);
        TaskCollaboratorRegistry trc = t1.getLastTaskCollaboratorRegistryOf(col1);

        assertTrue(t1.isReportPeriodCoincidingWithRegistryPeriod(trc, reportStartDate, reportEndDate));

    }

    @Test

    public void testIsReportPeriodCoincidingWithRegistryNullDateFail() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = LocalDateTime.now().plusDays(5);
        TaskCollaboratorRegistry trc = t1.getLastTaskCollaboratorRegistryOf(col1);

        assertFalse(t1.isReportPeriodCoincidingWithRegistryPeriod(trc, reportStartDate, reportEndDate));

    }

    @Test

    public void testIsReportPeriodCoincidingWithRegistryPeriodNotNollDateSuccess() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);
        TaskCollaboratorRegistry trc = t1.getLastTaskCollaboratorRegistryOf(col1);
        trc.setCollaboratorRemovedFromTaskDate(LocalDateTime.now());

        assertTrue(t1.isReportPeriodCoincidingWithRegistryPeriod(trc, reportStartDate, reportEndDate));

    }

    @Test

    public void testIsReportPeriodCoincidingWithRegistryPeriodNotNollDateFail() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = LocalDateTime.now().plusDays(5);
        TaskCollaboratorRegistry trc = t1.getLastTaskCollaboratorRegistryOf(col1);
        trc.setCollaboratorRemovedFromTaskDate(LocalDateTime.now());

        assertFalse(t1.isReportPeriodCoincidingWithRegistryPeriod(trc, reportStartDate, reportEndDate));

    }

    @Test

    public void testIsReportPeriodCoincidingWithRegistryPeriodNotNollDateFailStartDate() {

        LocalDateTime reportStartDate = LocalDateTime.of(2014, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);
        TaskCollaboratorRegistry trc = t1.getLastTaskCollaboratorRegistryOf(col1);
        trc.setCollaboratorRemovedFromTaskDate(LocalDateTime.now());

        assertFalse(t1.isReportPeriodCoincidingWithRegistryPeriod(trc, reportStartDate, reportEndDate));

    }

    @Test

    public void testIsReportPeriodCoincidingWithProjectCollaboratorPeriod() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);

        assertTrue(t1.isReportPeriodCoincidingWithProjectCollaboratorPeriod(col1, reportStartDate, reportEndDate));
    }

    @Test

    public void testIsReportPeriodCoincidingWithProjectCollaboratorPeriodFalse() {

        LocalDateTime reportStartDate = LocalDateTime.of(2014, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);
        assertFalse(t1.isReportPeriodCoincidingWithProjectCollaboratorPeriod(col1, reportStartDate, reportEndDate));
    }

    @Test

    public void testIsReportPeriodCoincidingWithProjectCollaboratorPeriodCollaboratorRemoved() {

        LocalDateTime removalDate = LocalDateTime.of(2017, 11, 3, 0, 0);
        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = reportStartDate.plusDays(5);

        t1.removeProjectCollaborator(col1);
        t1.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorRemovedFromTaskDate(removalDate);
        assertFalse(t1.isReportPeriodCoincidingWithProjectCollaboratorPeriod(col1, reportStartDate, reportEndDate));
    }

    @Test

    public void testIsReportPeriodCoincidingWithProjectCollaboratorPeriodReportDateBeforeNow() {

        LocalDateTime reportStartDate = LocalDateTime.of(2017, 11, 1, 0, 0);
        LocalDateTime reportEndDate = LocalDateTime.of(2022, 11, 1, 0, 0);
        assertFalse(t1.isReportPeriodCoincidingWithProjectCollaboratorPeriod(col1, reportStartDate, reportEndDate));
    }

    @Test
    public void testMarkAsCompletedByCollaborator() {
        // Given
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnPlannedState());
        List<Task> expected = new ArrayList<>();

        // When
        t4.addProjectCollaborator(col1);
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnReadyToStartState());
        t4.addTaskDependency(t1);
        assertTrue(t4.getTaskState().isOnAssignedState());
        t1.setTaskCompleted();
        assertTrue(t1.getTaskState().isOnCompletedState());
        // Then
        assertEquals(expected, t1.markAsCompletedByCollaborator(col1));
        assertTrue(t4.getTaskState().isOnReadyToStartState());

    }

    @Test
    public void testMarkAsCompletedByCollaboratorTaskIsNotCompleted() {
        // Given
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnPlannedState());
        List<Task> expected = new ArrayList<>();

        // When
        t4.addProjectCollaborator(col1);
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnReadyToStartState());
        t4.addTaskDependency(t1);
        assertTrue(t4.getTaskState().isOnAssignedState());

        assertFalse(t1.getTaskState().isOnCompletedState());
        // Then

        assertEquals(expected, t1.markAsCompletedByCollaborator(col1));
    }

    @Test
    public void testMarkAsCompletedByCollaboratorNotinTask() {
        // Given
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnPlannedState());
        List<Task> expected = new ArrayList<>();
        // When
        t4.addProjectCollaborator(col1);
        assertTrue(t1.getTaskState().isOnInProgressState());
        assertTrue(t4.getTaskState().isOnReadyToStartState());
        t4.addTaskDependency(t1);
        assertTrue(t4.getTaskState().isOnAssignedState());
        t1.setTaskCompleted();
        assertTrue(t1.getTaskState().isOnCompletedState());
        // Then

        assertEquals(expected, t1.markAsCompletedByCollaborator(col3));
    }

    @Test
    public void taskCollaboratorHasReportTest() {
        assertTrue(t1.taskCollaboratorHasReport(col1, t1.listAllReports().get(0).getCode()));
    }

    @Test
    public void taskCollaboratorHasReportTest_fail() {
        assertFalse(t1.taskCollaboratorHasReport(col2, t1.listAllReports().get(0).getCode()));
    }

    @Test
    public void taskCollaboratorHasReportTest_fail2() {

        assertFalse(t1.taskCollaboratorHasReport(col1, t1.listAllReports().get(4).getCode()));
    }

    @Test
    public void requestTaskCompletedTest() {

        // Given an initiated task with a project collaborator associated
        assertTrue(t1.getStatus().isOnInProgressState());
        assertTrue(t1.hasProjectCollaborator(col1));

        // When the collaborator send a request to mark task completed

        t1.requestTaskCompleted(col1);

        // Then the task receive the request and save the project collaborator email

        String result = t1.getRequestTaskCompleted();
        String expected = col1.getUser().getEmail();

        assertEquals(expected, result);
    }

    @Test
    public void requestTaskCompletedTestFailTaskIsNotInProgress() {

        // Given an not initiated task with a project collaborator associated
        t2.addProjectCollaborator(col1);
        assertFalse(t2.getStatus().isOnInProgressState());
        assertTrue(t2.hasProjectCollaborator(col1));

        // When the collaborator send a request to mark task completed

        t2.requestTaskCompleted(col1);

        // Then the task cant't receive the request because is not on progress

        String result = t2.getRequestTaskCompleted();
        String expected = null;

        assertEquals(expected, result);
    }

    @Test
    public void requestTaskCompletedTestFailCollaboratorNotInTask() {

        // Given an initiated task and a project collaborator that is not associated with that task

        assertTrue(t1.getStatus().isOnInProgressState());
        assertFalse(t1.hasProjectCollaborator(col3));

        // When the collaborator send a request to mark task completed

        t1.requestTaskCompleted(col3);

        // Then the task cant't receive the request because is not on progress

        String result = t1.getRequestTaskCompleted();
        String expected = null;

        assertEquals(expected, result);
    }

    @Test
    public void taskDTO() {

        // Given
        TaskRestDTO taskDTO = t1.toDTO();

        // When
        String result = taskDTO.toString();

        // Then
        String expected = "TaskRestDTO [projectId=1, taskId=1-1, dateOfCreation=" + t1.getDateOfCreation() + ", effectiveDateOfConclusion=null, effectiveStartDate=2017-11-01T11:40, predictedDateOfConclusion=2020-12-18T00:00, predictedDateOfStart=2017-12-18T00:00, title=Test 1, description=null, estimatedEffort=0.0, completedRequest=null, unitCost=0.0, state=InProgress]";

        assertEquals(expected, result);

    }

    @Test
    public void taskDTOParameter() {

        // Given
        TaskRestDTO taskDTOExpected = new TaskRestDTO();
        TaskRestDTO taskDTOResult;

        t1.setDateOfCreation(LocalDateTime.of(2016, 12, 18, 0, 0));
        t1.setEffectiveStartDate(LocalDateTime.of(2017, 12, 18, 0, 0));
        t1.setTitle("Title");
        t1.setDescription("description");
        t1.setEstimatedEffort(90.0);
        t1.setUnitCost(2.5);

        ProjectCollaborator projectCollaboratorTest = new ProjectCollaborator(project, user1, 9.0);
        t1.addProjectCollaborator(projectCollaboratorTest);
        t1.requestTaskCompleted(projectCollaboratorTest);

        taskDTOResult = t1.toDTO();

        taskDTOExpected.setDateOfCreation(LocalDateTime.of(2016, 12, 18, 0, 0));
        taskDTOExpected.setPredictedDateOfStart(LocalDateTime.of(2017, 12, 18, 0, 0));
        taskDTOExpected.setPredictedDateOfConclusion(LocalDateTime.of(2020, 12, 18, 0, 0));
        taskDTOExpected.setEffectiveStartDate(LocalDateTime.of(2017, 12, 18, 0, 0));
        taskDTOExpected.setEffectiveDateOfConclusion(null);
        taskDTOExpected.setProjectId("1");
        taskDTOExpected.setTaskId("1-1");
        taskDTOExpected.setTitle("Test 1");
        taskDTOExpected.setTitle("Title");
        taskDTOExpected.setDescription("description");
        taskDTOExpected.setEstimatedEffort(90.0);
        taskDTOExpected.setUnitCost(2.5);
        taskDTOExpected.setState("InProgress");
        taskDTOExpected.setCompletedRequest("ana@gmail.com");

        // When
        // Then

        assertEquals(taskDTOExpected.toString(), taskDTOResult.toString());

        //When we set an effective date of conclusion and call toDTO
        t1.setEffectiveDateOfConclusion(LocalDateTime.of(2018, 12, 18, 0, 0));
        taskDTOResult = t1.toDTO();

        //Then we expect DTO to have an effective date of conclusion and a state of "completed"
        taskDTOExpected.setEffectiveDateOfConclusion(LocalDateTime.of(2018, 12, 18, 0, 0));
        taskDTOExpected.setState("Completed");

        assertEquals(taskDTOExpected.toString(), taskDTOResult.toString());
    }

    @Test
    public void allTaskCollaboratorRegistryToDTO() {
        List<TaskCollaboratorRegistryRESTDTO> expected = new ArrayList<>();
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO1 = new TaskCollaboratorRegistryRESTDTO();
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO2 = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO1.setTaskId(t1.getId());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorName(user1.getName());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorEmail(user1.getEmail());
        taskCollaboratorRegistryRESTDTO1.setCollaboratorAddedToTaskDate(d1.minusYears(3));
        taskCollaboratorRegistryRESTDTO1.setRegistryStatus("ASSIGNMENTAPPROVED");
        taskCollaboratorRegistryRESTDTO2.setTaskId(t1.getId());
        taskCollaboratorRegistryRESTDTO2.setProjectCollaboratorName(user2.getName());
        taskCollaboratorRegistryRESTDTO2.setProjectCollaboratorEmail(user2.getEmail());
        taskCollaboratorRegistryRESTDTO2.setCollaboratorAddedToTaskDate(d1.minusYears(3));
        taskCollaboratorRegistryRESTDTO2.setRegistryStatus("ASSIGNMENTAPPROVED");
        expected.add(taskCollaboratorRegistryRESTDTO1);
        expected.add(taskCollaboratorRegistryRESTDTO2);
        assertEquals(expected, t1.allTaskCollaboratorRegistryToDTO());
    }

    @Test
    public void allTaskCollaboratorRegistryToDTORemovalRequest() {
        t2.addProjectCollaborator(col1);
        t2.requestRemoveProjectCollaborator(col1);

        List<TaskCollaboratorRegistryRESTDTO> expected = new ArrayList<>();
        TaskCollaboratorRegistryRESTDTO taskCollaboratorRegistryRESTDTO1 = new TaskCollaboratorRegistryRESTDTO();
        taskCollaboratorRegistryRESTDTO1.setTaskId(t2.getId());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorName(user1.getName());
        taskCollaboratorRegistryRESTDTO1.setProjectCollaboratorEmail(user1.getEmail());
        taskCollaboratorRegistryRESTDTO1.setCollaboratorAddedToTaskDate(d1.minusYears(3));
        taskCollaboratorRegistryRESTDTO1.setRegistryStatus("REMOVALREQUEST");
        expected.add(taskCollaboratorRegistryRESTDTO1);
        assertEquals(expected, t2.allTaskCollaboratorRegistryToDTO());
    }

    @Test
    public void testToDTO() {
        t2.addTaskDependency(t1);
        t2.setGanttTaskDependencies("1-1");
        TaskRestDTO testTask = t2.toDTO();

        assertEquals(testTask.getTaskId(), t2.getId());
        assertEquals(testTask.getTitle(), t2.getTitle());
        assertEquals(testTask.getDescription(), t2.getDescription());
        assertEquals(testTask.getEstimatedEffort(), t2.getEstimatedEffort(), 0.01);
        assertEquals(testTask.getDateOfCreation(), t2.getDateOfCreation());
        assertEquals(testTask.getPredictedDateOfStart(), t2.getPredictedDateOfStart());
        assertEquals(testTask.getPredictedDateOfConclusion(), t2.getPredictedDateOfConclusion());
        assertEquals(testTask.getEffectiveStartDate(), t2.getEffectiveStartDate());
        assertEquals(testTask.getEffectiveDateOfConclusion(), t2.getEfectiveDateOfConclusion());
        assertEquals(testTask.getUnitCost(), t2.getUnitCost(), 0.01);
        assertEquals(testTask.getProjectId(), t2.getProject().getId());
        assertEquals(testTask.getState(), t2.getState());
        assertEquals(testTask.getCompletedRequest(), t2.getRequestTaskCompleted());
        assertEquals(testTask.getGanttTaskDependencies(), t2.getGanttTaskDependencies());
    }

    @Test
    public void testCreateGanttTaskDependencies() {
        t2.addTaskDependency(t1);
        t2.addTaskDependency(t3);
        t2.setGanttTaskDependencies("1-1,1-3");
        TaskRestDTO testTask = t2.toDTO();

        assertEquals(testTask.getGanttTaskDependencies(), t2.getGanttTaskDependencies());
    }

}