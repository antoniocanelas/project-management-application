package project.model.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import project.model.user.UserIdVO;

public class UserIdVOTest {

	
	
	@Test
	public void testGetUserEmail() {
		
		
		UserIdVO userId =UserIdVO.create("asdrubal@switch.com");
		
		
		assertEquals("asdrubal@switch.com", userId.getUserEmail());
		
	}
	
	@Test
	public void testSetUserEmail() {
		
		
		UserIdVO userId =new UserIdVO();
		
		userId.setUserEmail("asdrubal@switch.com");
		
		
		assertEquals("asdrubal@switch.com", userId.getUserEmail());
		
	}
	
	
	 @Test
	    public void testHashCode() {
		 
		 UserIdVO userId1 =UserIdVO.create("asdrubal@switch.com");
		 UserIdVO userId2 =UserIdVO.create("maria@switch.com");
		 

	        assertEquals(userId1.hashCode(), userId1.hashCode());
	        assertNotEquals(userId1.hashCode(), userId2.hashCode());

	    }

	 
	 @Test
	    public void testEqualsSuccess() {
		 
		 UserIdVO userId1 =new UserIdVO("asdrubal@switch.com");
		 UserIdVO userId2 =new UserIdVO("asdrubal@switch.com");
		 
	        assertTrue(userId1.equals(userId2));

	    }
	 
	 
	 @Test
	    public void testEqualsSuccess2() {
		 
		 UserIdVO userId1 =new UserIdVO("asdrubal@switch.com");
		
		 
	        assertTrue(userId1.equals(userId1));

	    }
	 
	 
	 @Test
	    public void testEqualsFail() {
		 
		 UserIdVO userId1 =new UserIdVO("asdrubal@switch.com");
		 UserIdVO userId2 =new UserIdVO("maria@switch.com");
		 
	        assertFalse(userId1.equals(userId2));

	    }
	 
	 @Test
	    public void testEqualsFailNullObject() {
		 
		 UserIdVO userId1 =new UserIdVO("asdrubal@switch.com");
		 UserIdVO userId2 =null;
		 
	        assertFalse(userId1.equals(userId2));

	    }
}
