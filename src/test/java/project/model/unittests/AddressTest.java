package project.model.unittests;

import org.junit.Before;
import org.junit.Test;
import project.model.user.Address;
import project.model.user.User;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class AddressTest {

    // Address
    Address address, address2, address3;

    // Address Data
    String id = "casa";
    String street = "Rua Dr. António Bernardino de Almeida nº431";
    String postalCode = "4200-072";
    String city = "Porto";
    String country = "Portugal";

    // Dates
    LocalDate birth;

    // Users
    User user;

    @Before
    public void initialize() {
        address = new Address(id, street, postalCode, city, country);
        address2 = new Address(id, street, postalCode, city, country);
        address3 = new Address("77", street, postalCode, city, country);
    }

    @Test
    public void testSetStreet() {
        // Given
        assertEquals("Rua Dr. António Bernardino de Almeida nº431", address.getStreet());
        // When
        address.setStreet("Rua Afonso Henriques");
        // Then
        assertEquals("Rua Afonso Henriques", address.getStreet());
    }

    @Test
    public void testSetPostalCode() {
        // Given
        assertEquals("4200-072", address.getPostalCode());
        // When
        address.setPostalCode("3700-000");
        // Then
        assertEquals("3700-000", address.getPostalCode());
    }

    @Test
    public void testSetCity() {
        // Given
        assertEquals("Porto", address.getCity());
        // When
        address.setCity("Coimbra");
        // Then
        assertEquals("Coimbra", address.getCity());
    }

    @Test
    public void testSetCountry() {
        // Given
        assertEquals("Portugal", address.getCountry());
        // When
        address.setCountry("Espanha");
        // Then
        assertEquals("Espanha", address.getCountry());
    }

    @Test
    public void testSetIdSucess() {
        // Given
        assertEquals("casa", address.getAddressDescription());
        // When
        address.setAddressDescription("trabalho");
        // Then
        assertEquals("trabalho", address.getAddressDescription());
    }

    @Test
    public void testEquals() {
        // Given
        assertEquals("casa", address.getAddressDescription());
        // When
        Address address2 = new Address(id, street, postalCode, city, country);
        Address address3 = new Address("test", street, "4000-087", "Maia", country);
        // Then
        assertEquals(address, address2);
        assertNotEquals(address, address3);
    }

    @Test
    public void testFalseDifferentClass() throws AddressException {
        // Given
        assertEquals("casa", address.getAddressDescription());
        // When
        user = new User("Joaquim", "91739812379", "jj@jj.com", "132456765", birth, "casa", "Rua do Bicalho", "4500-200",
                "Porto", "Portugal");
        // Then
        boolean condition = address.equals(user);
        assertFalse(condition);
    }

    @Test
    public void testToString() {
        // Given
        assertEquals("casa", address.getAddressDescription());
        // When
        String result = address.toString();
        // Then
        assertEquals("[casa, Rua Dr. António Bernardino de Almeida nº431, 4200-072, Porto, Portugal]",
                result);
    }

    @Test
    public void testIsValidSuccess() {
        // Given
        // When
        Address address = new Address("2", "Rua ", "3890-255", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertTrue(condition);
    }

    @Test
    public void testIsValidFailureNoId() {
        // Given
        // When
        Address address = new Address("", "Rua ", "3890-255", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNullId() {
        // Given
        // When
        Address address = new Address(null, "Rua ", "3890-255", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNoStreet() {
        // Given
        // When
        Address address = new Address("2", "", "3890-255", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNullStreet() {
        // Given
        // When
        Address address = new Address("2", null, "3890-255", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNoPostalCode() {
        // Given
        // When
        Address address = new Address("2", "Rua ", "", "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNullPostalCode() {
        // Given
        // When
        Address address = new Address("2", "Rua ", null, "cidade", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNoCity() {
        // Given
        // When
        Address address = new Address("2", "Rua ", "3890-255", "", "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testIsValidFailureNullCity() {
        // Given
        // When
        Address address = new Address("2", "Rua ", "3890-255", null, "país");
        // Then
        boolean condition = address.isValid();
        assertFalse(condition);
    }

    @Test
    public void testEqualsTrueSucess() {
        // Given
        // When
        Address testAddress = new Address(id, street, postalCode, city, country);
        Address newAddress = new Address(id, street, postalCode, city, country);
        // Then
        boolean condition = testAddress.equals(newAddress);
        assertTrue(condition);
    }

    @Test
    public void testeEqualsTrue1() {
        // Given
        // When
        Address newAddress1 = new Address("2", "Rua do Bolhão", postalCode, city, "Portugal");
        Address newAddress2 = new Address("2", "Rua do Bolhão", postalCode, city, "China");
        // Then
        boolean condition = newAddress1.equals(newAddress2);
        assertTrue(condition);
    }

    @Test
    public void testEqualsTrue3() {
        // Given
        // When
        Address newAddress = new Address(null, street, postalCode, city, country);
        // Then
        boolean condition = newAddress.equals(newAddress);
        assertTrue(condition);
    }

    @Test
    public void testEqualsFalse1() {
        // Given
        // When
        Address address1 = null;
        Address newAddress = new Address(null, street, postalCode, city, country);
        // Then
        boolean condition = newAddress.equals(address1);
        assertFalse(condition);
    }

    @Test
    public void testEqualsFalse2() {
        // Given
        // When
        address = new Address(id, street, postalCode, city, country);
        // Then
        boolean condition = address.equals(null);
        assertFalse(condition);
    }

    @Test
    public void testHashCode() {
        // Given
        // When
        address = new Address(id, street, postalCode, city, country);
        address2 = new Address("casa", "Rua Dr. António Bernardino de Almeida nº431", "4200-072", "Porto", "Portugal");
        address3 = new Address("77", street, postalCode, city, country);
        // Then
        assertEquals(address.hashCode(), address2.hashCode());
        assertNotEquals(address.hashCode(), address3.hashCode());
    }

}
