package project.model.unittests;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import project.dto.rest.TaskCollaboratorRegistryRESTDTO;
import project.dto.rest.TaskRestDTO;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Report;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.TaskCollaboratorRegistry.RegistryStatus;
import project.model.task.taskstate.Created;
import project.model.task.taskstate.TaskState;
import project.model.user.User;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

public class TaskServiceTest {

    // RepositoryClass
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    // Services
    UserService userService;
    ProjectService projectService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    // Dates
    LocalDate birth = LocalDate.of(1999, 11, 11);
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    LocalDateTime d5;
    LocalDateTime d9;
    LocalDateTime d10;
    LocalDateTime d11;
    int y1 = LocalDateTime.now().getYear();
    Month m1 = LocalDateTime.now().getMonth();

    // Tasks
    Task t1;
    Task t2;
    Task t3;
    Task t4;
    Task t5;
    Task t6;
    Task t7;
    Task t8;
    Task t9;

    // Users
    User user1;
    User user2;
    User projectManager;

    // Project Collaborators
    ProjectCollaborator col1;
    ProjectCollaborator col2;
    ProjectCollaborator col3;
    // Reports
    Report report1;
    Report report2;
    Report report3;
    Report report4;
    // Lists - Reports
    List<Report> reportList;
    // Projects
    private Project project;
    private Project project2;

    @Before
    public void initialize() throws AddressException {

        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);

        projectService.addProject("1", "SWitCH");
        project = projectService.getProjectByID("1");

        projectService.addProject("2", "Teste");
        project2 = projectService.getProjectByID("2");

        d1 = LocalDateTime.of(2011, 10, 29, 0, 0);
        d2 = LocalDateTime.of(2011, 10, 30, 0, 0);
        d3 = LocalDateTime.of(2014, 5, 8, 0, 0);
        d4 = LocalDateTime.of(2014, 12, 1, 0, 0);
        d5 = LocalDateTime.of(2017, 5, 8, 0, 0);
        d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
        d11 = LocalDateTime.of(y1, m1.minus(1), 1, 0, 0);

        project.setStartDate(d5);

        taskService.addTask(project, "Test 1");
        taskService.addTask(project, "Test 2");
        taskService.addTask(project, "Test 3");
        taskService.addTask(project, "Test 4");
        taskService.addTask(project, "Test 5");
        taskService.addTask(project, "Test 6");
        taskService.addTask(project, "Test 7");
        taskService.addTask(project, "Test 8");

        t1 = project.findTaskByTitle("Test 1");
        t2 = project.findTaskByTitle("Test 2");
        t3 = project.findTaskByTitle("Test 3");
        t4 = project.findTaskByTitle("Test 4");
        t5 = project.findTaskByTitle("Test 5");
        t6 = project.findTaskByTitle("Test 6");
        t7 = project.findTaskByTitle("Test 7");
        t8 = project.findTaskByTitle("Test 8");

        t1.setPredictedDateOfStart(d1);
        t1.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(2));
        t2.setPredictedDateOfStart(d1);
        t2.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(2));
        t3.setPredictedDateOfStart(d1);
        t3.setPredictedDateOfConclusion(d3);
        t4.setPredictedDateOfStart(d1);
        t4.setPredictedDateOfConclusion(d4);
        t7.setPredictedDateOfStart(d1);
        t7.setPredictedDateOfConclusion(d3);

        userService.addUser("Pedro", "919999999", "pedro@gmail.com", "332432", birth, "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser("Joana", "929999999", "joana@gmail.com", "432893", birth, "2", "Rua ", "4433", "cidade",
                "país");
        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@gmail.com", "55555", birth, "2", "Rua ", "4433",
                "cidade", "pais");

        user1 = userService.searchUserByEmail("pedro@gmail.com");
        user2 = userService.searchUserByEmail("joana@gmail.com");
        projectManager = userService.searchUserByEmail("joana@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        projectManager.setProfileCollaborator();

        projectCollaboratorService.setProjectManager(project, projectManager);
        projectCollaboratorService.addProjectCollaborator(project, user1, 6);
        projectCollaboratorService.addProjectCollaborator(project, user2, 7);

        projectCollaboratorService.setProjectManager(project2, projectManager);
        projectCollaboratorService.addProjectCollaborator(project2, user1, 6);

        col1 = projectCollaboratorService.getProjectCollaborator(project.getId(), "pedro@gmail.com");
        col2 = projectCollaboratorService.getProjectCollaborator(project.getId(), "joana@gmail.com");

        col3 = projectCollaboratorService.getProjectCollaborator(project2.getId(), "pedro@gmail.com");

        taskService.addProjectCollaboratorToTask(col1, t1);
        taskService.addProjectCollaboratorToTask(col1, t2);
        taskService.addProjectCollaboratorToTask(col1, t3);

        taskService.addProjectCollaboratorToTask(col2, t4);
        TaskCollaboratorRegistry col2TaskRegistry = t4
                .getTaskCollaboratorRegistryByID(t4.getId() + "-" + col2.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

        taskService.addProjectCollaboratorToTask(col2, t5);

        t3.setEffectiveStartDate(LocalDateTime.of(2017, 10, 30, 0, 0));
        t4.setEffectiveStartDate(LocalDateTime.of(2017, 10, 30, 0, 0));

        taskService.addProjectCollaboratorToTask(col1, t7);

    }

    @Test
    @Transactional
    public void testNewAddTaskSuccess() {
        // Given
        // When
        taskService.addTask(project, "des");
        Task t = project.findTaskByTitle("des");
        // Then
        boolean condition1 = t instanceof Task;
        assertTrue(condition1);
        assertEquals("des", t.getTitle());
    }

    @Test
    @Transactional
    public void testNewAddTaskWithIdSuccess() {
        // Given
        // When
        taskService.addTask(project, "des", "test");
        Task t = project.findTaskByID("test");
        // Then
        boolean condition1 = t instanceof Task;
        assertTrue(condition1);
        assertEquals("test", t.getId());
    }

    @Test
    @Transactional
    public void testNewAddTaskSuccess2() {
        // Given
        // When
        taskService.addTask(project, "des", "description", d1, d2, 4, 6);
        Task t = project.findTaskByTitle("des");
        // Then
        boolean condition1 = t instanceof Task;
        assertTrue(condition1);
        assertEquals("des", t.getTitle());
    }

    @Test
    @Transactional
    public void testNewAddTaskSuccess3() {
        // Given
        // When
        taskService.addTask(project, "des", "description", 2, 3, 4, 5);
        Task t = project.findTaskByTitle("des");
        // Then
        boolean condition1 = t instanceof Task;
        assertTrue(condition1);
        assertEquals("des", t.getTitle());
    }

    @Test
    @Transactional
    public void testUpdateTaskSuccess() {
        // Given
        // Then
        Task result = taskService.findTaskByID("1-2");
        boolean condition = taskService.updateTask(result);
        assertNotNull(result);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testUpdateTaskFail() {
        // Given
        // When
        Task result = new Task(project, "Test 9");
        // Then
        boolean condition = taskService.updateTask(result);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorToTaskSuccess() {
        // Given
        // Then
        boolean condition = taskService.addProjectCollaboratorToTask(col1, t5);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorToTaskSuccess2() {
        // Given
        // Then
        boolean condition1 = taskService.addProjectCollaboratorToTask(col1, t1);
        assertFalse(condition1);
        // When
        taskService.removeProjectCollaboratorFromTask(col1, t1);
        // Then
        boolean condition2 = taskService.addProjectCollaboratorToTask(col1, t1);
        assertTrue(condition2);
    }

    @Test
    @Transactional
    public void testAddProjectCollaboratorToTaskFailCollabNotInProject() {
        // Given
        // Then
        boolean condition = taskService.addProjectCollaboratorToTask(col3, t5);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testGetProjectTasks() {
        // Given
        // Then
        List<Task> result = taskService.getProjectTasks(project);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        expected.add(t7);
        expected.add(t8);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testGetTaskByTitleSuccess() {
        // Given
        // Then
        Task result = taskService.findTaskByTitle("Test 1");
        assertEquals(t1, result);
    }

    @Test
    @Transactional
    public void testGetTaskByTitleFailureNoTaskByThatTitle() {
        // Given
        // Then
        Task result = taskService.findTaskByTitle("SWitCH");
        assertNull(result);
    }

    @Test
    @Transactional
    public void testGetTaskByIDSuccess() {
        // Given
        // Then
        Task result = taskService.findTaskByID(t2.getId());
        assertEquals(t2, result);
    }

    @Test
    @Transactional
    public void testGetTaskByIDNull() {
        // Given
        // Then
        Task result = taskService.findTaskByID("8");
        assertNull(result);
    }

    @Test
    @Transactional
    public void testGetTaskNoTaskByThatID() {
        // Given
        // Then
        Task result = taskService.findTaskByID("6");
        assertNull(result);
    }

    @Test
    @Transactional
    public void testRemoveTaskCreatedState() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertTrue(taskIsInProjectBeforeRemoval);
        assertEquals(taskService.findTaskByTitle("Test 6"), t6);
        boolean taskState = t6.getTaskState().isOnCreatedState();
        assertTrue(taskState);
        // When
        boolean removeTask = taskService.removeTask(t6);
        assertTrue(removeTask);
        // Then
        assertNull(taskService.findTaskByTitle("Test 6"));
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertFalse(taskIsInProjectAfterRemoval);

    }

    @Test
    @Transactional
    public void testRemoveTaskPlannedState() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertTrue(taskIsInProjectBeforeRemoval);
        assertEquals(taskService.findTaskByTitle("Test 6"), t6);
        t6.setPredictedDateOfStart(LocalDateTime.now());
        boolean taskState = t6.getTaskState().isOnPlannedState();
        assertTrue(taskState);
        // When
        boolean removeTask = taskService.removeTask(t6);
        assertTrue(removeTask);
        // Then
        assertNull(taskService.findTaskByTitle("Test 6"));
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertFalse(taskIsInProjectAfterRemoval);
    }

    @Test
    @Transactional
    public void testRemoveTaskAssignedState() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertTrue(taskIsInProjectBeforeRemoval);
        assertEquals(taskService.findTaskByTitle("Test 6"), t6);
        t6.setPredictedDateOfStart(LocalDateTime.now());
        t6.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(10));
        t6.addProjectCollaborator(col1);
        t6.addTaskDependency(t1);
        boolean taskState = t6.getTaskState().isOnAssignedState();
        assertTrue(taskState);
        // When
        boolean removeTask = taskService.removeTask(t6);
        assertTrue(removeTask);
        // Then
        assertNull(taskService.findTaskByTitle("Test 6"));
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t6);
        assertFalse(taskIsInProjectAfterRemoval);

    }

    @Test
    @Transactional
    public void testRemoveTaskReadyToStart() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectBeforeRemoval);
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);
        boolean taskState = t1.getTaskState().isOnReadyToStartState();
        assertTrue(taskState);
        // When
        boolean removeTask = taskService.removeTask(t1);
        assertTrue(removeTask);
        // Then
        assertNull(taskService.findTaskByTitle("Test 1"));
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertFalse(taskIsInProjectAfterRemoval);
    }

    @Test
    @Transactional
    public void testRemoveTaskInProgress() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectBeforeRemoval);
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);
        t1.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(10));
        t1.addReport(col1, 5, LocalDateTime.now().minusDays(1), LocalDateTime.now().minusHours(1));
        boolean taskState = t1.getTaskState().isOnInProgressState();
        assertTrue(taskState);
        // When
        boolean removeTask = taskService.removeTask(t1);
        assertFalse(removeTask);
        // Then
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectAfterRemoval);
    }

    @Test
    @Transactional
    public void testRemoveTaskDTOSucess() {
        // Given
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectBeforeRemoval);
        TaskRestDTO taskDto = new TaskRestDTO();
        taskDto.setTaskId(t1.getId());
        // When
        assertTrue(taskService.removeTask(taskDto));
        // Then
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertFalse(taskIsInProjectAfterRemoval);
    }

    @Test
    @Transactional
    public void testRemoveTaskDTOFail() {
        boolean taskIsInProjectBeforeRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectBeforeRemoval);
        TaskRestDTO taskDto = new TaskRestDTO();
        taskDto.setTaskId("fail");
        assertFalse(taskService.removeTask(taskDto));
        boolean taskIsInProjectAfterRemoval = projectService.getProjectByID("1").getTasksList().contains(t1);
        assertTrue(taskIsInProjectAfterRemoval);
    }

    @Test
    @Transactional
    public void testListCollaboratorCompletedTasks() {
        // Given
        t1.setEffectiveStartDate(LocalDateTime.now().minusDays(7));
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(LocalDateTime.now().minusDays(1));
        taskService.updateTask(t1);
        t3.setTaskCompleted();
        taskService.updateTask(t3);
        // When
        List<Task> result = taskService.listCollaboratorCompletedTasks(col1);
        // Then
        List<Task> expected = new ArrayList<>();
        expected.add(t3);
        expected.add(t1);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorCompletedTasksEmptyList() {
        // Given
        // When
        taskService.updateTask(t1);
        taskService.updateTask(t2);
        taskService.updateTask(t3);
        taskService.updateTask(t7);

        // Then
        List<Task> result = taskService.listCollaboratorCompletedTasks(col1);

        assertTrue(result.isEmpty());
    }

    @Test
    @Transactional
    public void testListCollaboratorTasksCompletedLastMonth() {
        // Given
        t1.setEffectiveStartDate(d9.minusDays(15));
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9.minusMinutes(2));
        taskService.updateTask(t1);
        t3.setTaskCompleted();
        t3.setEffectiveDateOfConclusion(d9);
        taskService.updateTask(t3);

        // When
        List<Task> result = taskService.listCollaboratorsCompletedTasksLastMonth(col1);
        // Then
        List<Task> expected = new ArrayList<>();
        expected.add(t3);
        expected.add(t1);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testListCollaboratorTasksCompletedLastMonthDateNextMonth() {
        // Given
        // When
        LocalDateTime testDate = LocalDateTime.of(2018, 10, 28, 0, 0);
        t3.setEffectiveDateOfConclusion(testDate);
        taskService.updateTask(t3);
        // Then
        List<Task> result = taskService.listCollaboratorsCompletedTasksLastMonth(col1);
        assertTrue(result.isEmpty());
    }

    @Test
    @Transactional
    public void listTasksThatCanBeDependencySucess() {
        // Given
        // Then
        List<Task> expected = new ArrayList<>();
        List<Task> result = taskService.listTasksThatCanBeDependency(project);
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t4);
        expected.add(t5);
        expected.add(t6);
        expected.add(t7);
        expected.add(t8);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void listTasksThatCanBeDependencyFailCancelledState() {
        // Given
        // When
        t4.setTaskCancelled();
        // Then
        boolean condition = t4.getStatus().isOnCancelledState();
        assertTrue(condition);
        List<Task> expected = new ArrayList<>();
        List<Task> result = taskService.listTasksThatCanBeDependency(project);
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t5);
        expected.add(t6);
        expected.add(t7);
        expected.add(t8);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void listTasksThatCanBeDependencyFailCompletedState() {
        // Given
        // When
        t4.setTaskCompleted();
        // Then
        boolean condition = t4.getStatus().isOnCompletedState();
        assertTrue(condition);
        List<Task> expected = new ArrayList<>();
        List<Task> result = taskService.listTasksThatCanBeDependency(project);
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t5);
        expected.add(t6);
        expected.add(t7);
        expected.add(t8);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void listTasksThatCanBeDependencyFailSuspendedState() {
        // Given
        // When
        t4.addReport(col2, 2, LocalDateTime.now().minusMonths(5), LocalDateTime.now().minusDays(25));
        t4.removeProjectCollaborator(col2);
        // Then
        boolean condition = t4.getStatus().isOnSuspendedState();
        assertTrue(condition);
        List<Task> expected = new ArrayList<>();
        List<Task> result = taskService.listTasksThatCanBeDependency(project);
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t5);
        expected.add(t6);
        expected.add(t7);
        expected.add(t8);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredSuccess() {

        t7.setPredictedDateOfConclusion(d1);
        t4.setPredictedDateOfConclusion(d2);
        t3.setPredictedDateOfConclusion(d3);

        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(t3.toDTO());
        expected.add(t4.toDTO());
        expected.add(t7.toDTO());

        taskService.updateTask(t3);
        taskService.updateTask(t4);
        taskService.updateTask(t7);

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredFailureFutureDate() {

        LocalDateTime testDate = LocalDateTime.of(2018, 10, 28, 0, 0);

        t4.setPredictedDateOfConclusion(testDate);
        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(t3.toDTO());
        expected.add(t7.toDTO());

        taskService.updateTask(t3);
        taskService.updateTask(t7);

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);

        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredFailureFutureDate2() {

        LocalDateTime testDate1 = LocalDateTime.of(2016, 10, 28, 0, 0);
        LocalDateTime testDate2 = LocalDateTime.of(2018, 10, 28, 0, 0);

        t4.setPredictedDateOfConclusion(testDate1);

        List<TaskRestDTO> expected = new ArrayList<>();
        t3.setPredictedDateOfConclusion(testDate2);
        expected.add(t4.toDTO());
        expected.add(t7.toDTO());

        taskService.updateTask(t4);
        taskService.updateTask(t7);

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);
        assertThat(result, containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredEmptyTaskList() {

        projectService.addProject("234", "projecto switch");
        Project project2 = projectService.getProjectByID("234");

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project2);
        List<TaskRestDTO> expected = new ArrayList<>();
      
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredEmptyListTaskCompleted() {

        t3.setTaskCompleted();
        taskService.updateTask(t3);

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);
        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(taskService.findTaskByID("1-4").toDTO());
        expected.add(taskService.findTaskByID("1-7").toDTO());
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetNotCompletedTasksAndWithPredictedDateOfConclusionExpiredEmptyTaskListDateNotBefore() {
        LocalDateTime testDate2 = LocalDateTime.of(2018, 10, 28, 0, 0);
        t3.setPredictedDateOfConclusion(testDate2);
        taskService.updateTask(t3);

        List<TaskRestDTO> result = taskService
                .listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(project);
        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(taskService.findTaskByID("1-4").toDTO());
        expected.add(taskService.findTaskByID("1-7").toDTO());
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listCancelledTasks() {
        List<TaskRestDTO> cancelledTasks = new ArrayList<>();
        assertTrue(t2.getTaskState().isOnReadyToStartState());
        assertTrue(t3.getTaskState().isOnInProgressState());

        TaskCollaboratorRegistry col1TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        TaskCollaboratorRegistry col2TaskRegistry = t3
                .getTaskCollaboratorRegistryByID(t3.getId() + "-" + col1.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));
        t2.addReport(col1, 2, LocalDateTime.now(), d1);
        t3.addReport(col1, 2, LocalDateTime.now(), d1);
        t3.removeProjectCollaborator(col1);

        assertTrue(t2.getTaskState().isOnInProgressState());
        assertTrue(t3.getTaskState().isOnSuspendedState());
        assertTrue(t4.getTaskState().isOnInProgressState());

        t2.setTaskCancelled();
        t3.setTaskCancelled();
        assertTrue(t2.getTaskState().isOnCancelledState());
        assertTrue(t3.getTaskState().isOnCancelledState());
        taskService.updateTask(t2);
        taskService.updateTask(t3);

        cancelledTasks.add(t2.toDTO());
        cancelledTasks.add(t3.toDTO());
        assertEquals(cancelledTasks, taskService.listProjectCancelledTasks(project));
    }

    @Test
    @Transactional
    public void listNotInitiatedTasks() {
        List<TaskRestDTO> expected = new ArrayList<>();

        t3.setTaskCompleted();
        taskService.updateTask(t3);

        t6.setPredictedDateOfStart(d1);
        taskService.updateTask(t6);

        assertTrue(t1.getTaskState().isOnReadyToStartState());
        assertTrue(t2.getTaskState().isOnReadyToStartState());
        assertTrue(t3.getTaskState().isOnCompletedState());
        assertTrue(t4.getTaskState().isOnInProgressState());
        assertTrue(t5.getTaskState().isOnReadyToStartState());
        assertTrue(t6.getTaskState().isOnPlannedState());
        t8.setPredictedDateOfStart(d1);
        t8.addTaskDependency(t4);
        t8.addProjectCollaborator(col1);
        taskService.updateTask(t8);

        assertTrue(t8.getTaskState().isOnAssignedState());
        taskService.updateTask(t1);
        taskService.updateTask(t2);
        taskService.updateTask(t4);
        taskService.updateTask(t5);
        taskService.updateTask(t7);

        taskService.addTask(project, "Test 9");
        t9 = project.findTaskByTitle("Test 9");
        assertTrue(t9.getTaskState().isOnCreatedState());
        taskService.updateTask(t9);
        expected.add(t1.toDTO());
        expected.add(t2.toDTO());
        expected.add(t5.toDTO());
        expected.add(t6.toDTO());
        expected.add(t7.toDTO());
        expected.add(t8.toDTO());
        expected.add(t9.toDTO());

        assertThat(taskService.listProjectNotInitiatedTasks(project), containsInAnyOrder(expected.toArray()));
    }

    @Test
    @Transactional
    public void testListProjectCompletedTasks() {
        d3 = LocalDateTime.of(2018, 05, 15, 0, 0);
        d4 = LocalDateTime.of(2018, 07, 11, 0, 0);

        t2.setEffectiveStartDate(d3.minusMonths(2));
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d3);

        t3.addProjectCollaborator(col1);
        t3.setEffectiveStartDate(d4.minusMonths(2));
        t3.setTaskCompleted();
        t3.setEffectiveDateOfConclusion(d4);

        taskService.updateTask(t2);
        taskService.updateTask(t3);

        List<TaskRestDTO> expected = new ArrayList<>();

        expected.add(t3.toDTO());
        expected.add(t2.toDTO());

        assertThat(expected, containsInAnyOrder(taskService.listProjectCompletedTasks(project).toArray()));

        t3.removeProjectCollaborator(col1);
    }

    @Test
    @Transactional
    public void testListProjectInitiatedButNotCompletedTasksSuccessInProgressState() {

        t1.setEffectiveStartDate(d1);
        t2.setEffectiveStartDate(d1);
        t3.setEffectiveStartDate(d1);
        t2.setTaskCompleted();
        assertTrue(t3.getStatus().isOnInProgressState());
        assertTrue(t1.getStatus().isOnInProgressState());
        assertTrue(t4.getStatus().isOnInProgressState());

        taskService.updateTask(t1);
        taskService.updateTask(t3);

        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(t1.toDTO());
        expected.add(t3.toDTO());
        expected.add(t4.toDTO());

        assertEquals(expected, taskService.listProjectIniciatedButNotCompletedTasks(project));
    }

    @Test
    @Transactional
    public void testListProjectInitiatedButNotCompletedTasksSuccessInSuspendedState() {

        t1.setEffectiveStartDate(d1);
        t2.setEffectiveStartDate(d1);
        t1.addReport(col1, 5, LocalDateTime.of(2018, 5, 25, 4, 5),
                LocalDateTime.now().minusHours(1));
        t1.removeProjectCollaborator(col1);
        t2.setTaskCompleted();
        t3.addReport(col1, 5, LocalDateTime.of(2018, 5, 25, 4, 5),
                LocalDateTime.now().minusHours(1));
        t4.addReport(col2, 5, LocalDateTime.of(2018, 5, 25, 4, 5),
                LocalDateTime.now().minusHours(1));
        t1.setTaskSuspended();
        t3.setTaskSuspended();
        t4.setTaskSuspended();
        assertTrue(t1.getStatus().isOnSuspendedState()); //DODO CHECK ERROR
        assertTrue(t3.getStatus().isOnSuspendedState());
        assertTrue(t4.getStatus().isOnSuspendedState());

        taskService.updateTask(t1);
        taskService.updateTask(t3);
        taskService.updateTask(t4);

        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(t1.toDTO());
        expected.add(t3.toDTO());
        expected.add(t4.toDTO());

        assertEquals(expected, taskService.listProjectIniciatedButNotCompletedTasks(project));
    }

    @Test
    @Transactional
    public void testListProjectInitiatedButNotCompletedTasksFailNotInCorrectState() {

        assertTrue(t1.getStatus().isOnReadyToStartState());
        assertTrue(t6.getStatus().isOnCreatedState());
        assertTrue(t3.getStatus().isOnInProgressState());
        assertTrue(t4.getStatus().isOnInProgressState());

        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(t3.toDTO());
        expected.add(t4.toDTO());

        assertEquals(expected, taskService.listProjectIniciatedButNotCompletedTasks(project));
    }

    @Test
    @Transactional
    public void testRemoveProjectCollaboratorFromAllTasksInProject() {
        // Given 1 Project with Collaborator1 that has 4 tasks in project
        assertEquals(project.hasActiveProjectCollaborator(col1), true);
        assertEquals(project.listCollaboratorTasks(col1).size(), 4);
        // When: remove collaborator from all task
        taskService.removeProjectCollaboratorFromAllTasksInProject(project, col1);
        // Then: collaborator is removed from all tasks in project
        assertEquals(project.listCollaboratorTasks(col1).size(), 0);
    }

    @Test
    @Transactional
    public void testAddTaskSuccess() {
        // Given
        // When
        taskService.addTask(project, "test8", "test8");
        Task t = project.findTaskByTitle("test8");
        // Then
        boolean condition1 = t instanceof Task;
        assertTrue(condition1);
        assertEquals("test8", t.getTitle());
    }

    @Test
    @Transactional
    public void testRemoveTaskSuccessNotInitiated() {
        // Given: task not initiated
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);
        assertEquals(t1.getTaskState().isOnReadyToStartState(), true);
        // When: try to remove
        taskService.removeTask(t1);
        // Then: task is sucessfully removed
        assertEquals(taskService.findTaskByTitle("Test 1"), null);

    }

    @Test
    @Transactional
    public void testRemoveTaskInProgressNotAllowed() {
        // Given: Task in progress
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);
        t1.listTaskCollaboratorRegistry(col1).get(0).setCollaboratorAddedToTaskDate(LocalDateTime.now().minusDays(10));
        t1.addReport(col1, 10, LocalDateTime.now().minusHours(10), LocalDateTime.now().minusHours(2));
        taskService.updateTask(t1);
        assertTrue(t1.getTaskState().isOnInProgressState());
        // When: try to remove
        taskService.removeTask(t1);
        // Then: Task is not allowed to be removed because is already start
        assertEquals(taskService.findTaskByTitle("Test 1"), t1);

    }

    @Test
    @Transactional
    public void testGetTaskState() {

        taskService.addTask(project, "getTaskState");
        Task taskTest = taskService.findTaskByTitle("getTaskState");
        TaskState taskState = new Created(taskTest);
        
        assertEquals(taskTest.getTaskState().getClass().getSimpleName(), taskState.getClass().getSimpleName());

    }

    @Test
    @Transactional
    public void testListTasksDTOByProject() {
        List<TaskRestDTO> taskRestDTOS = new ArrayList<>();
        taskRestDTOS.add(t1.toDTO());
        taskRestDTOS.add(t2.toDTO());
        taskRestDTOS.add(t3.toDTO());
        taskRestDTOS.add(t4.toDTO());
        taskRestDTOS.add(t5.toDTO());
        taskRestDTOS.add(t6.toDTO());
        taskRestDTOS.add(t7.toDTO());
        taskRestDTOS.add(t8.toDTO());
        assertEquals(taskRestDTOS, taskService.listTasksDTOByProject(project));
    }

   
    
    
   
    
    @Test
    @Transactional
    public void testListTaskRegistry() {
    	 
    	List<TaskCollaboratorRegistryRESTDTO> expected =new ArrayList<>();
    	TaskCollaboratorRegistryRESTDTO dto1=new TaskCollaboratorRegistryRESTDTO();
    	dto1.setTaskId(t1.getId());
    	dto1.setProjectCollaboratorId(col1.getId());
    	dto1.setProjectCollaboratorName(col1.getUser().getName());
    	dto1.setProjectCollaboratorEmail(col1.getUser().getEmail());
    	dto1.setCollaboratorAddedToTaskDate(t1.getLastTaskCollaboratorRegistryOf(col1).getCollaboratorAddedToTaskDate());
    	dto1.setRegistryStatus(t1.getLastTaskCollaboratorRegistryOf(col1).getRegistryStatus().toString());
    	expected.add(dto1);
    	assertEquals(expected,taskService.listActiveTaskCollaborators(t1.getId()));
    	
    	
    
    }
    
    
    @Test
    @Transactional
    public void testListTaskRegistryRemovelRequest() {
    	 
    	List<TaskCollaboratorRegistryRESTDTO> expected =new ArrayList<>();
    	TaskCollaboratorRegistryRESTDTO dto1=new TaskCollaboratorRegistryRESTDTO();
    	t1.getLastTaskCollaboratorRegistryOf(col1).setRegistryStatus(RegistryStatus.REMOVALREQUEST);

    	dto1.setTaskId(t1.getId());
    	dto1.setProjectCollaboratorId(col1.getId());
    	dto1.setProjectCollaboratorName(col1.getUser().getName());
    	dto1.setProjectCollaboratorEmail(col1.getUser().getEmail());
    	dto1.setCollaboratorAddedToTaskDate(t1.getLastTaskCollaboratorRegistryOf(col1).getCollaboratorAddedToTaskDate());
    	dto1.setRegistryStatus(t1.getLastTaskCollaboratorRegistryOf(col1).getRegistryStatus().toString());
    	expected.add(dto1);
    	assertEquals(expected,taskService.listActiveTaskCollaborators(t1.getId()));
    	
    	
    
    }
    @Test
    @Transactional
    public void testListTaskRegistryAssignementRequest() {
    	 
    	List<TaskCollaboratorRegistryRESTDTO> expected =new ArrayList<>();
    	
    	t1.getLastTaskCollaboratorRegistryOf(col1).setRegistryStatus(RegistryStatus.ASSIGNMENTREQUEST);
  
    	assertEquals(expected,taskService.listActiveTaskCollaborators(t1.getId()));
    	
    	
    
    }
    
    @Test
    @Transactional
    public void testListTaskRegistryRemovalApproved() {
    	 
    	List<TaskCollaboratorRegistryRESTDTO> expected =new ArrayList<>();
    	
    	t1.getLastTaskCollaboratorRegistryOf(col1).setRegistryStatus(RegistryStatus.REMOVALAPPROVED);
  
    	assertEquals(expected,taskService.listActiveTaskCollaborators(t1.getId()));
    	
    	
    
    }
    
    @Test
    @Transactional
    public void testListTaskRegistryAssignementCancel() {
    	 
    	List<TaskCollaboratorRegistryRESTDTO> expected =new ArrayList<>();
    	
    	t1.getLastTaskCollaboratorRegistryOf(col1).setRegistryStatus(RegistryStatus.ASSIGNMENTCANCELLED);
  
    	assertEquals(expected,taskService.listActiveTaskCollaborators(t1.getId()));

    }

    @Test
    @Transactional
    public void testMarkTaskCompleted() {
        TaskRestDTO expected = new TaskRestDTO();
        expected.setProjectId(project.getId());
        expected.setTaskId(t1.getId());
        expected.setDateOfCreation(LocalDateTime.now());
        expected.setEffectiveDateOfConclusion(LocalDateTime.now());
        expected.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(2));
        expected.setPredictedDateOfStart(d1);
        expected.setTitle(t1.getTitle());
        expected.setState("ReadyToStart");
        TaskRestDTO actual = taskService.markTaskCompleted(t1.getId());
        assertEquals(expected, actual);
    }
    
    
    
    @Test
    @Transactional
    public void testCancelMarkTaskCompleted() {
        TaskRestDTO expected = new TaskRestDTO();
        expected.setProjectId(project.getId());
        expected.setTaskId(t1.getId());
        expected.setDateOfCreation(LocalDateTime.now());
        expected.setEffectiveDateOfConclusion(LocalDateTime.now());
        expected.setPredictedDateOfConclusion(LocalDateTime.now().plusDays(2));
        expected.setPredictedDateOfStart(d1);
        expected.setTitle(t1.getTitle());
        expected.setState("ReadyToStart");
        expected.setCompletedRequest(null);
        TaskRestDTO actual = taskService.cancelMarkTaskCompleted(t1.getId());
        assertEquals(expected, actual);
        assertEquals(expected.getCompletedRequest(), actual.getCompletedRequest());
    }

    @Test
    @Transactional
    public void testListTaskDependenciesSuccess() {
        List<TaskRestDTO> expected = new ArrayList<>();
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(project.getId());
        taskRestDTO.setTaskId(t2.getId());
        taskRestDTO.setDateOfCreation(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        taskRestDTO.setPredictedDateOfConclusion(LocalDateTime.now());
        taskRestDTO.setPredictedDateOfStart(d1);
        taskRestDTO.setTitle(t2.getTitle());
        taskRestDTO.setState("ReadyToStart");
        expected.add(taskRestDTO);


        t1.addTaskDependency(t2);
        taskService.updateTask(t1);
        assertEquals(expected ,taskService.listTaskDependencies(t1.getId()));
    }

    @Test
    @Transactional
    public void testListTaskDependenciesNoTaskDependencies() {
        List<TaskRestDTO> expected = new ArrayList<>();
        assertEquals(expected ,taskService.listTaskDependencies(t1.getId()));
    }
    
    @Test
    public void testfindTaskByIDtoDTOTaskNull() {
        
    		TaskRestDTO expected = taskService.findTaskByIDtoDTO("inexistantTask");
    		
        assertNull(expected.getCompletedRequest());
        assertNull(expected.getDateOfCreation());
        assertTrue(expected.getDependenciesId().isEmpty());
        assertNull(expected.getDescription());
        assertNull(expected.getEffectiveDateOfConclusion());
        assertNull(expected.getEffectiveStartDate());
        assertEquals(0.0, expected.getEstimatedEffort(), 0.0001);
        assertNull(expected.getId());
        assertNull(expected.getMessage());
        
        
    }
}
