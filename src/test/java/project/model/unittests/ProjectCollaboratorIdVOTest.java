package project.model.unittests;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import project.model.projectcollaborator.ProjectCollaboratorIdVO;

public class ProjectCollaboratorIdVOTest {

	@Test
	public void testGetProjectCollaboratorId() {
		
		 ProjectCollaboratorIdVO projectCollaboratorId= new  ProjectCollaboratorIdVO();
		
		assertNotNull(projectCollaboratorId.getProjectCollaboratorId());
		
	}
}
