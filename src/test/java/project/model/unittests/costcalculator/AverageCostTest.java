package project.model.unittests.costcalculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import project.model.project.AverageCost;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class AverageCostTest {

	ProjectCollaboratorCostAndTimePeriod colab1 = new ProjectCollaboratorCostAndTimePeriod(5);
	ProjectCollaboratorCostAndTimePeriod colab2 = new ProjectCollaboratorCostAndTimePeriod(7);
	Report report = new Report("1234", 10, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));
	
	@Test
	public void testAverageCostSuccess() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		AverageCost averageCost = new AverageCost();
		
		assertEquals(60.0, averageCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

	@Test
	public void testAverageCostFail() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		AverageCost averageCost = new AverageCost();
		
		assertNotEquals(30.0, averageCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

	@Test
	public void testAverageCostEmpty() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();

		AverageCost averageCost = new AverageCost();

		assertEquals(0, averageCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}





}
