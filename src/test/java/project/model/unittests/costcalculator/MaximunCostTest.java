package project.model.unittests.costcalculator;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import project.model.project.MaximumTimePeriodCost;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class MaximunCostTest {

	
	LocalDateTime d1;
	LocalDateTime d2;
	
	ProjectCollaboratorCostAndTimePeriod colab1 = new ProjectCollaboratorCostAndTimePeriod(5);
	ProjectCollaboratorCostAndTimePeriod colab2 = new ProjectCollaboratorCostAndTimePeriod(7);
	ProjectCollaboratorCostAndTimePeriod colab3 = new ProjectCollaboratorCostAndTimePeriod(1);
	ProjectCollaboratorCostAndTimePeriod colab4 = new ProjectCollaboratorCostAndTimePeriod(2);
	List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
	Report report = new Report("1234", 10, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));

	
	@Test
	public void testMaximumCostTwoValues() {
		
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		
		MaximumTimePeriodCost maximumCost = new MaximumTimePeriodCost();
		
		assertEquals(70.0, maximumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);
	}
	
	
	@Test
	public void testMaximumCostManyValues() {
		
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab3);
		costAndTimePeriodList.add(colab4);
		
		MaximumTimePeriodCost maximumCost = new MaximumTimePeriodCost();
		
		assertEquals(70.0, maximumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);
	}
	
	
	@Test
	public void testMaximumCostEmptyList() {
		
		
		MaximumTimePeriodCost maximumCost = new MaximumTimePeriodCost();
		
		assertEquals(-10, maximumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);
	}
	
	

}
