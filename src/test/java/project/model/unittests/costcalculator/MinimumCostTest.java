package project.model.unittests.costcalculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import project.model.project.MinimumTimePeriodCost;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.task.Report;

public class MinimumCostTest {

	ProjectCollaboratorCostAndTimePeriod colab1 = new ProjectCollaboratorCostAndTimePeriod(5);
	ProjectCollaboratorCostAndTimePeriod colab2 = new ProjectCollaboratorCostAndTimePeriod(7);
	ProjectCollaboratorCostAndTimePeriod colab3 = new ProjectCollaboratorCostAndTimePeriod(3);
	ProjectCollaboratorCostAndTimePeriod colab4 = new ProjectCollaboratorCostAndTimePeriod(2);
	Report report = new Report("1234", 10, LocalDateTime.now().minusDays(10), LocalDateTime.now().minusDays(5));


	
	@Test
	public void testMinimumCostSuccess() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab3);
		
		MinimumTimePeriodCost minimumCost = new MinimumTimePeriodCost();
		
		assertEquals(30.0, minimumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
	
	@Test
	public void testMinimumCostSuccess2() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab3);
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab4);
		
		MinimumTimePeriodCost minimumCost = new MinimumTimePeriodCost();
		
		assertEquals(20.0, minimumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}

	@Test
	public void testMinimumCostFail() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		costAndTimePeriodList.add(colab3);
		costAndTimePeriodList.add(colab1);
		costAndTimePeriodList.add(colab2);
		costAndTimePeriodList.add(colab4);
		
		MinimumTimePeriodCost minimumCost = new MinimumTimePeriodCost();
		
		assertNotEquals(70.0, minimumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
	

	@Test
	public void testMinimumCostFailRepeatedValue() {
		List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();
		
		
		MinimumTimePeriodCost minimumCost = new MinimumTimePeriodCost();
		
		
		assertEquals(-10, minimumCost.calculateReportCost(report, costAndTimePeriodList), 0.01);

	}
	
}