package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.taskstate.Created;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class CreatedTest {

	private Task task1;
	private Task task2;
	private TaskState created;
	private ProjectCollaborator collab;
	private LocalDate birth = LocalDate.of(1999, 11, 11);
	private LocalDateTime d1 = LocalDateTime.of(2017, 11, 11, 0, 0);

	@Before
	public void setUp() throws AddressException {
		Project project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		task2.setTaskId("1-2");
		project.addTask(task1);
		project.addTask(task2);
		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		collab = new ProjectCollaborator(project, user, 10);

		created = new Created(task1);
	}

	@Test
	public void testCreated() {

		assertTrue(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testIsOnAssignedState() {

		assertTrue(task1.getTaskState().isOnCreatedState());
		assertFalse(task1.getTaskState().isOnAssignedState());
	}

	@Test
	public void testIsOnCompletedState() {

		assertTrue(task1.getTaskState().isOnCreatedState());
		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testIsOnCreatedState() {

		assertTrue(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testIsOnCancelledState() {

		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testIsOnInProgressState() {

		assertFalse(task1.getTaskState().isOnInProgressState());
	}

	@Test
	public void testIsOnReadyToStartState() {

		assertFalse(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	public void testIsOnPlannedState() {

		assertFalse(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testIsOnSuspendedState() {

		assertFalse(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testIsValid() {

		assertTrue((task1.getEffectiveStartDate() == null) && (task1.getEfectiveDateOfConclusion() == null));
		assertTrue((task1.getPredictedDateOfStart() == null) && (task1.getPredictedDateOfConclusion() == null));
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertTrue(created.isValid());
	}

	@Test
	public void testIsNotValid() {

		task1.addProjectCollaborator(collab);
		assertTrue((task1.getEffectiveStartDate() == null) && (task1.getEfectiveDateOfConclusion() == null));
		assertTrue((task1.getPredictedDateOfStart() == null) && (task1.getPredictedDateOfConclusion() == null));
		assertFalse(!task1.hasAssignedProjectCollaborator());
		assertFalse(created.isValid());
	}

	@Test
	public void testIsNotValidEffectiveDates1() {

		task1.setEffectiveStartDate(d1);
		assertTrue((task1.getEffectiveStartDate() != null) && (task1.getEfectiveDateOfConclusion() == null));
		assertTrue((task1.getPredictedDateOfStart() == null) && (task1.getPredictedDateOfConclusion() == null));
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertFalse(created.isValid());
	}

	@Test
	public void testIsNotValidEffectiveDates2() {

		task1.setEffectiveDateOfConclusion(d1);
		assertTrue((task1.getEffectiveStartDate() == null) && (task1.getEfectiveDateOfConclusion() != null));
		assertTrue((task1.getPredictedDateOfStart() == null) && (task1.getPredictedDateOfConclusion() == null));
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertFalse(created.isValid());
	}

	@Test
	public void testIsNotValidPredictedDates1() {

		task1.setPredictedDateOfStart(d1);
		assertTrue((task1.getEffectiveStartDate() == null) && (task1.getEfectiveDateOfConclusion() == null));
		assertTrue((task1.getPredictedDateOfStart() != null) && (task1.getPredictedDateOfConclusion() == null));
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertFalse(created.isValid());
	}

	@Test
	public void testIsNotValidPredictedDates2() {

		task1.setPredictedDateOfConclusion(d1);
		assertTrue((task1.getEffectiveStartDate() == null) && (task1.getEfectiveDateOfConclusion() == null));
		assertTrue((task1.getPredictedDateOfStart() == null) && (task1.getPredictedDateOfConclusion() != null));
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertFalse(created.isValid());
	}

	@Test
	public void testChangeToPlanned() {

		assertTrue(task1.getTaskState().isOnCreatedState());
		created.changeTo();
		assertFalse(task1.getTaskState().isOnPlannedState());
		task1.setPredictedDateOfStart(d1);
		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testChangeToAssigned() {
		task1.addProjectCollaborator(collab);
		task1.addTaskDependency(task2);
		assertTrue(task1.getTaskState().isOnAssignedState());
	}

	@Test
	public void testChangeTo() {

		created.changeTo();
		assertTrue(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testChangeToCancelled() {

		assertTrue(task1.getTaskState().isOnCreatedState());
		task1.setTaskCancelled();
		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testChangeToCompleted() {

		assertTrue(task1.getTaskState().isOnCreatedState());
		task1.setTaskCompleted();
		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testToString() {

		String expected = "Created";
		String result = created.toString();
		assertEquals(expected, result);
	}

	/**
	 * GIVEN: a task which is on created state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnCreatedState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".setPredictedDateOfStart");
		expected.add(nameOfTaskState + ".setPredictedDateOfConclusion");
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		expected.add(nameOfTaskState + ".addTaskDependency");
		expected.add(nameOfTaskState + ".removeTask");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}