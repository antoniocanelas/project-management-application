package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.taskstate.Planned;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class PlannedTest {

	private Task task1;
	private TaskState planned;
	private ProjectCollaborator collab;
	private LocalDate birth = LocalDate.of(1999, 11, 11);
	private LocalDateTime d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
	private LocalDateTime d2 = LocalDateTime.of(2018, 11, 11, 0, 0);

	@Before
	public void setUp() throws AddressException {
		Project project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid", "description1", d1, d2, 1.2, 1.3);
		project.addTask(task1);

		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		collab = new ProjectCollaborator(project, user, 10);
		planned = new Planned(task1);

	}

	@Test
	public void testPlanned() {

		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testIsOnAssignedState() {

		assertFalse(task1.getTaskState().isOnAssignedState());
	}

	@Test
	@Transactional
	public void testIsOnCreatedState() {

		assertFalse(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testIsOnCancelledState() {

		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testIsOnCompletedState() {

		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testIsOnInProgressState() {

		assertFalse(task1.getTaskState().isOnInProgressState());
	}

	@Test
	public void testIsOnPlannedState() {

		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testIsOnReadyToStartState() {

		assertFalse(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	public void testIsOnSuspendedState() {

		assertFalse(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testIsValidSuccess() {

		assertTrue(task1.getPredictedDateOfStart() != null);
		assertTrue(!task1.hasAssignedProjectCollaborator());
		assertTrue(planned.isValid());
	}

	@Test
	public void testIsValidNullPredictedDateOfStart() {
		task1.setPredictedDateOfStart(null);
		assertTrue(task1.getPredictedDateOfStart() == null);
		assertTrue(planned.isValid());
	}

	@Test
	public void testIsValidNullPredictedDateOfConclusion() {
		task1.setPredictedDateOfConclusion(null);
		assertTrue(task1.getPredictedDateOfConclusion() == null);
		assertTrue(planned.isValid());
	}

	@Test
	public void testIsValidNullPredictedDates() {
		task1.setPredictedDateOfConclusion(null);
		task1.setPredictedDateOfStart(null);
		assertTrue(task1.getPredictedDateOfConclusion() == null && task1.getPredictedDateOfStart() == null);
		assertFalse(planned.isValid());
	}

	@Test
	public void testConditionEffectiveDatesSucess() {
		task1.setEffectiveStartDate(null);
		task1.setEffectiveDateOfConclusion(null);
		assertTrue(task1.getEffectiveStartDate() == null && task1.getEfectiveDateOfConclusion() == null);
		assertTrue(planned.isValid());
	}

	@Test
	public void testConditionEffectiveDatesFail() {
		task1.setEffectiveStartDate(d1);
		task1.setEffectiveDateOfConclusion(d2);
		assertTrue(task1.getEffectiveStartDate() != null && task1.getEfectiveDateOfConclusion() != null);
		assertFalse(planned.isValid());
	}

	@Test
	public void testConditionEffectiveDatesFail2() {
		task1.setEffectiveStartDate(d1);
		task1.setEffectiveDateOfConclusion(null);
		assertTrue(task1.getEffectiveStartDate() != null && task1.getEfectiveDateOfConclusion() == null);
		assertFalse(planned.isValid());
	}

	@Test
	public void testConditionEffectiveDatesFail3() {
		task1.setEffectiveStartDate(null);
		task1.setEffectiveDateOfConclusion(d2);
		assertTrue(task1.getEffectiveStartDate() == null && task1.getEfectiveDateOfConclusion() != null);
		assertFalse(planned.isValid());
	}

	@Test
	public void testIsValidNotValisHasCollaboratorAssignedTo() {

		assertTrue(task1.getPredictedDateOfStart() != null);
		task1.addProjectCollaborator(collab);
		assertTrue(task1.hasAssignedProjectCollaborator());
		assertFalse(planned.isValid());
	}

	@Test
	public void testChangeToReadyToStart() {

		assertTrue(task1.getTaskState().isOnPlannedState());
		task1.addProjectCollaborator(collab);
		assertTrue(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	public void testChangeTo() {

		planned.changeTo();
		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testChangeToCancelled() {

		assertTrue(task1.getTaskState().isOnPlannedState());
		task1.setTaskCancelled();
		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testChangeToCompleted() {
		task1.setTaskCompleted();
		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testToString() {

		String expected = "Planned";
		String result = task1.getTaskState().toString();

		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN: a task which is on planned state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnPlannedState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".setPredictedDateOfStart");
		expected.add(nameOfTaskState + ".setPredictedDateOfConclusion");
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		expected.add(nameOfTaskState + ".addTaskDependency");
		expected.add(nameOfTaskState + ".removeTask");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}