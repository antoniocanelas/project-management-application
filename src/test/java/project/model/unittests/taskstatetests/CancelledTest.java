package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.taskstate.Cancelled;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class CancelledTest {

	private Task task1;
	private Task task2;
	private TaskState cancelled;
	private TaskState cancelledFalse;
	private ProjectCollaborator collab;
	private LocalDate birth = LocalDate.of(1999, 11, 11);
	private LocalDateTime d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
	private LocalDateTime d2 = LocalDateTime.of(2018, 11, 11, 0, 0);
	private LocalDateTime reportDate;

	@Before
	public void setUp() throws AddressException {
		Project project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		project.addTask(task1);
		project.addTask(task2);
		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		collab = new ProjectCollaborator(project, user, 10);
		project.addProjectCollaborator(collab);
		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task1.addProjectCollaborator(collab);
		task2.setTaskId("1-2");
		

		reportDate = LocalDateTime.now().minusMonths(14);

		TaskCollaboratorRegistry col1TaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + collab.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		task1.addReport(collab, 5, reportDate, reportDate.plusDays(5));
		task1.setTaskCancelled();

		cancelled = new Cancelled(task1);
		cancelledFalse = new Cancelled(task2);
	}

	@Test
	public void testIsValid() {
		assertTrue(task1.getTaskState().isOnCancelledState());
		
		assertTrue(cancelled.isValid());
	}

	@Test
	public void testIsNotValid() {

		assertFalse(task2.getTaskState().isOnCancelledState());
		assertFalse(cancelledFalse.isValid());
	}

	@Test
	public void testChangeToSucess() {

		task2.addProjectCollaborator(collab);
		TaskCollaboratorRegistry col1TaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + collab.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		task2.addReport(collab, 2, reportDate, reportDate.plusDays(5));
		assertTrue(task2.getTaskState().isOnInProgressState());
		task2.setTaskCancelled();
		cancelled.changeTo();
		assertTrue(task2.getTaskState().isOnCancelledState());
	}

	@Test
	public void testChangeToFail() {

		task2.addProjectCollaborator(collab);
		TaskCollaboratorRegistry col1TaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + collab.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		task2.addReport(collab, 2, reportDate, reportDate.plusDays(5));
		assertTrue(task2.getTaskState().isOnInProgressState());
		assertFalse(task2.getTaskState().isOnCancelledState());
		cancelledFalse.changeTo();
	}

	@Test
	public void testToString() {

		String expected = "Cancelled";
		String result = cancelled.toString();
		assertEquals(expected, result);
	}

	@Test
	public void testChangeTo() {
		
		task2.addProjectCollaborator(collab);
		task2.setEffectiveStartDate(d1);
		
		assertFalse(task2.getTaskState().isOnCancelledState());
		task2.setTaskCancelled();
		task2.changeStateTo();
		
		assertTrue(task2.getTaskState().isOnCancelledState());
	}
	
	/**
	 * GIVEN: a task which is on Cancelled state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnCancelledState());
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertTrue(result.isEmpty());
	}
}
