package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.task.taskstate.Completed;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class CompletedTest {

	private Task task1;
	private Task task2;
	private TaskState completed;
	private ProjectCollaborator collab;
	private LocalDate birth = LocalDate.of(1999, 11, 11);
	private LocalDateTime d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
	private LocalDateTime d2 = LocalDateTime.of(2018, 11, 11, 0, 0);

	@Before
	public void setUp() throws AddressException {
		Project project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		project.addTask(task1);
		project.addTask(task2);
		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		collab = new ProjectCollaborator(project, user, 10);
		project.addProjectCollaborator(collab);
		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task1.addProjectCollaborator(collab);
		task2.setTaskId("1-2");

		TaskCollaboratorRegistry col1TaskRegistry = task1
				.getTaskCollaboratorRegistryByID(task1.getId() + "-" + collab.getUser().getEmail());
		col1TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));

		task1.addReport(collab, 300, LocalDateTime.now().minusMonths(1), LocalDateTime.now().minusDays(25));
		task1.setTaskCompleted();
		completed = new Completed(task1);
	}

	@Test
	public void testIsOnCompletedState() {

		assertTrue(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testIsOnSuspendedState() {

		assertFalse(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testIsValid() {
		task2.setEffectiveStartDate(d1);
		task2.setEffectiveDateOfConclusion(d1);
		assertTrue(task2.getEffectiveStartDate() != null);
		assertTrue(task2.getEfectiveDateOfConclusion() != null);
		assertTrue(completed.isValid());
	}

	// @Test
	// public void testIsNotValidEffectiveStartDateNull() {
	// task2.setEffectiveStartDate(d1);
	// assertTrue(task2.getEffectiveStartDate() != null);
	// assertFalse(task2.getEfectiveDateOfConclusion() != null);
	// assertFalse(completed.isValid());
	// }
	//
	// @Test
	// public void testIsNotValidEffetiveDateConclusionNull() {
	// task2.setEffectiveDateOfConclusion(d1);
	// assertFalse(task2.getEffectiveStartDate() != null);
	// assertTrue(task2.getEfectiveDateOfConclusion() != null);
	// assertFalse(completed.isValid());
	// }

	@Test
	public void testChangeToSuspended() {
		task2.addProjectCollaborator(collab);
		TaskCollaboratorRegistry col2TaskRegistry = task2
				.getTaskCollaboratorRegistryByID(task2.getId() + "-" + collab.getUser().getEmail());
		col2TaskRegistry.setCollaboratorAddedToTaskDate(LocalDateTime.now().minusYears(2));
		task2.addReport(collab, 5, LocalDateTime.now().minusMonths(1), LocalDateTime.now().minusDays(25));
		assertTrue(task2.getTaskState().isOnInProgressState());
		task2.setTaskCompleted();
		assertTrue(task2.getTaskState().isOnCompletedState());
		task2.setEffectiveDateOfConclusion(null);
		assertTrue(task2.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testChangeTo() {
		completed.changeTo();
		assertTrue(completed.isOnCompletedState());
	}

	@Test
	public void testToString() {
		String expected = "Completed";
		String result = completed.toString();
		assertEquals(expected, result);
	}
	
	/**
	 * GIVEN: a task which is on Completed state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnCompletedState());
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertTrue(result.isEmpty());
	}
}
