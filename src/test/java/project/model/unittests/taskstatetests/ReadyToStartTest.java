package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.taskstate.ReadyToStart;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class ReadyToStartTest {


    private LocalDate birth;
    private ProjectCollaborator projectCollaborator2;
    private Task task1;
    private Task task2;
    private TaskState readyToStart;
    private LocalDateTime d1;
    private LocalDateTime d2;

    @Before
    public void setUp() throws AddressException {

        birth = LocalDate.of(1999, 11, 11);

        Project project = new Project("1", "SWitCH");
        task1 = new Task(project, "From Stupid to Solid");
        task2 = new Task(project, "Keep it Simple Stupid"); 
        task2.setTaskId("1-2");
        project.addTask(task1);
        project.addTask(task2);     
        User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city", "country");        
        projectCollaborator2 = new ProjectCollaborator(project, user, 10);
        project.addProjectCollaborator(projectCollaborator2);  

        d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
        d2 = LocalDateTime.of(2018, 05, 11, 0, 0);

        task1.setPredictedDateOfStart(d1);
        task2.setPredictedDateOfStart(d1);
        task1.setPredictedDateOfConclusion(d2);
        task2.setPredictedDateOfConclusion(d2);       
        task1.addProjectCollaborator(projectCollaborator2);
        readyToStart = new ReadyToStart(task1);
    }

    @Test
    @Transactional
    public void testReadyToStart() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void isOnAssignedState() {

        assertFalse(task1.getTaskState().isOnAssignedState());
    }

    @Test
    @Transactional
    public void isOnCancelledState() {

        assertFalse(task1.getTaskState().isOnCancelledState());
    }

    @Test
    @Transactional
    public void isOnCompletedState() {

        assertFalse(task1.getTaskState().isOnCompletedState());
    }

    @Test
    @Transactional
    public void isOnCreatedState() {

        assertFalse(task1.getTaskState().isOnCreatedState());
    }

    @Test
    @Transactional
    public void isOnInProgressState() {

        assertFalse(task1.getTaskState().isOnInProgressState());
    }

    @Test
    @Transactional
    public void isOnPlannedState() {

        assertFalse(task1.getTaskState().isOnPlannedState());
    }

    @Test
    @Transactional
    public void isOnSuspendedState() {

        assertFalse(task1.getTaskState().isOnSuspendedState());
    }

    @Test
    @Transactional
    public void isOnReadyToStartState() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void isValid() {
        assertTrue(task1.hasAssignedProjectCollaborator());
        assertTrue(task1.canStart());
        assertTrue(task1.getEffectiveStartDate() == null);
        assertTrue(task1.getEfectiveDateOfConclusion() == null);
        assertTrue(readyToStart.isValid());
    }

    @Test
    @Transactional
    public void isNotValid1() {
        task1.removeProjectCollaborator(projectCollaborator2);
        assertFalse(task1.hasAssignedProjectCollaborator());
        assertTrue(task1.canStart());
        assertTrue(task1.getEffectiveStartDate() == null);
        assertTrue(task1.getEfectiveDateOfConclusion() == null);
        assertFalse(readyToStart.isValid());
    }

    @Test
    @Transactional
    public void isNotValid2() {
        assertTrue(task1.hasAssignedProjectCollaborator());
        assertTrue(task1.getTaskState().isOnReadyToStartState());
        task1.addTaskDependency(task2);
        assertTrue(task1.getTaskState().isOnAssignedState());
        task2.addReport(projectCollaborator2, 2, LocalDateTime.now(), LocalDateTime.now().plusDays(5));
        assertFalse(task2.getTaskState().isOnCompletedState());
        assertFalse(task1.canStart());
        assertTrue(task1.getEffectiveStartDate() == null);
        assertTrue(task1.getEfectiveDateOfConclusion() == null);
        assertFalse(readyToStart.isValid());
    }

    @Test
    @Transactional
    public void isNotValid3() {
        assertTrue(task1.hasAssignedProjectCollaborator());
        assertTrue(task1.canStart());
        task1.setEffectiveStartDate(d1);
        assertTrue(task1.getEffectiveStartDate() != null);
        assertTrue(task1.getEfectiveDateOfConclusion() == null);
        assertFalse(readyToStart.isValid());
    }

    @Test
    @Transactional
    public void isNotValid4() {
        assertTrue(task1.hasAssignedProjectCollaborator());
        assertTrue(task1.canStart());
        assertTrue(task1.getEffectiveStartDate() == null);
        task1.setEffectiveDateOfConclusion(d1);
        assertTrue(task1.getEfectiveDateOfConclusion() != null);
        assertFalse(readyToStart.isValid());
    }

    @Test
    @Transactional
    public void testChangeToAssigned() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        task1.addTaskDependency(task2);
        readyToStart.changeTo();
        assertTrue(task1.getTaskState().isOnAssignedState());
    }

    @Test
    @Transactional
    public void testChangeToPlanned() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        readyToStart.changeTo();
        assertFalse(task1.getTaskState().isOnPlannedState());
        task1.setPredictedDateOfStart(d1);
        task1.removeProjectCollaborator(projectCollaborator2);
        assertTrue(task1.getTaskState().isOnPlannedState());
    }

    @Test
    @Transactional
    public void testChangeToReadyToStart() {

        readyToStart.changeTo();
        assertTrue(task1.getTaskState().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void testChangeToInProgress() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        readyToStart.changeTo();
        assertFalse(task1.getTaskState().isOnInProgressState());
        task1.setEffectiveStartDate(d1);
        assertTrue(task1.getTaskState().isOnInProgressState());
    }

    @Test
    @Transactional
    public void testChangeTo() {

        assertTrue(task1.getTaskState().isOnReadyToStartState());
        readyToStart.changeTo();
        assertTrue(task1.getTaskState().isOnReadyToStartState());
    }

    @Test
    @Transactional
    public void testToString() {

        String expected = "ReadyToStart";
        String result = readyToStart.toString();

        assertEquals(expected, result);
    }
    
    /**
	 * GIVEN: a task which is on readyToStart state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnReadyToStartState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".requestRemoveProjectCollaborator");
		expected.add(nameOfTaskState + ".removeProjectCollaborator");
		expected.add(nameOfTaskState + ".addTaskDependency");
		expected.add(nameOfTaskState + ".removeTask");
		expected.add(nameOfTaskState + ".setPredictedDateOfStart");
		expected.add(nameOfTaskState + ".setPredictedDateOfConclusion");
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		expected.add(nameOfTaskState +".addReport");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}
