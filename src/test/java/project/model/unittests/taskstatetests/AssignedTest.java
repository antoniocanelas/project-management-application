package project.model.unittests.taskstatetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.taskstate.Assigned;
import project.model.task.taskstate.TaskState;
import project.model.user.User;

public class AssignedTest {

	private Task task1;
	private Task task2;
	private TaskState assigned;
	private ProjectCollaborator collab;
	private LocalDate birth = LocalDate.of(1999, 11, 11);
	private LocalDateTime d1 = LocalDateTime.of(2017, 11, 11, 0, 0);
	private LocalDateTime d2 = LocalDateTime.of(2018, 11, 11, 0, 0);

	@Before
	public void setUp() throws AddressException {

		Project project = new Project("1", "SWitCH");
		task1 = new Task(project, "From Stupid to Solid");
		task2 = new Task(project, "Keep it Simple Stupid");
		project.addTask(task1);
		project.addTask(task2);
		User user = new User("name", "phone", "email@mail.com", "tin", birth, "1", "street", "postalCode", "city",
				"country");
		collab = new ProjectCollaborator(project, user, 10);
		project.addProjectCollaborator(collab);
		task1.setPredictedDateOfStart(d1);
		task1.setPredictedDateOfConclusion(d2);
		task1.addProjectCollaborator(collab);
		task2.setTaskId("1-2");
		task1.addTaskDependency(task2);
		assigned = new Assigned(task1);
	}

	@Test
	public void testAssigned() {

		assertTrue(assigned.isOnAssignedState());
	}

	@Test
	public void testToString() {

		String expected = "Assigned";
		String result = assigned.toString();
		assertEquals(expected, result);
	}

	@Test
	public void testIsOnAssignedState() {

		assertTrue(task1.getTaskState().isOnAssignedState());

	}

	@Test
	public void testIsOnCancelledState() {

		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testIsOnCompletedState() {

		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	@Test
	public void testIsOnCreatedState() {

		assertFalse(task1.getTaskState().isOnCreatedState());
	}

	@Test
	public void testIsOnInProgressState() {

		assertFalse(task1.getTaskState().isOnInProgressState());
	}

	@Test
	public void testIsOnPlannedState() {

		assertFalse(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testIsOnReadyToStartState() {

		assertFalse(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	public void testIsOnSuspendedState() {

		assertFalse(task1.getTaskState().isOnSuspendedState());
	}

	@Test
	public void testIsValid() {

		// task1 cannot start because it is dependent on task2
		assertFalse(task1.canStart());
		// task1 has project collaborators associated with it
		assertTrue(task1.hasAssignedProjectCollaborator());
		// task1 has a creation date and doesn't have an effective start date nor an
		// effective date of conclusion
		// (necessary conditions to be in assigned state)
		assertTrue(((Assigned) task1.getTaskState()).conditionEffectiveDates(task1));
		assertTrue(task1.isValid());
	}

	@Test
	public void testIsValidFalseTaskHasEffectiveStartDate() {

		// sets task1's effective date of start, making it ineligible to be in assigned
		// state
		task1.setEffectiveStartDate(d1);

		assertFalse(task1.isValid());
	}

	@Test
	public void testConditionEffectiveDatesTrue() {

		assertTrue(((Assigned) task1.getTaskState()).conditionEffectiveDates(task1));
	}

	@Test
	public void testConditionEffectiveDatesFalseHasEffectiveDateOfConclusion() {

		task1.setEffectiveDateOfConclusion(d1);
		assertFalse(((Assigned) task1.getTaskState()).conditionEffectiveDates(task1));
	}

	@Test
	public void testChangeToPlannedSuccess() {

		assertTrue(task1.getTaskState().isOnAssignedState());
		task1.removeProjectCollaborator(collab);
		assertTrue(task1.getTaskState().isOnPlannedState());
	}

	@Test
	public void testChangeToReadyToStart() {

		assertTrue(task1.getTaskState().isOnAssignedState());
		task2.addProjectCollaborator(collab);
		task2.setEffectiveStartDate(d1);
		task2.setTaskCompleted();
		task1.changeStateTo();
		assertTrue(task1.getTaskState().isOnReadyToStartState());
	}

	@Test
	@Transactional
	public void testChangeTo() {

		task1.changeStateTo();
		assertTrue(task1.getTaskState().isOnAssignedState());
	}

	@Test
	public void testChangeToCancelled() {

		assertTrue(task1.getTaskState().isOnAssignedState());
		task1.changeStateTo();
		assertFalse(task1.getTaskState().isOnCancelledState());
	}

	@Test
	public void testChangeToCompleted() {
		task1.setTaskCompleted();
		assertFalse(task1.getTaskState().isOnCompletedState());
	}

	/**
	 * GIVEN: a task which is on assigned state and a list of possible actions
	 * WHEN: we ask for the list of available actions,
	 * THEN: we obtain that list
	 */
	@Test
	public void testListAvailableActions() {
		//Given
		assertTrue(task1.getTaskState().isOnAssignedState());
		String nameOfTaskState = task1.getTaskState().getClass().getSimpleName();
		List<String> expected = new ArrayList<>();
		expected.add(nameOfTaskState + ".addTaskDependency");
		expected.add(nameOfTaskState + ".removeTask");
		expected.add(nameOfTaskState + ".requestRemoveProjectCollaborator");
		expected.add(nameOfTaskState + ".removeProjectCollaborator");
		expected.add(nameOfTaskState + ".setPredictedDateOfStart");
		expected.add(nameOfTaskState + ".setPredictedDateOfConclusion");
		expected.add(nameOfTaskState + ".requestAddProjectCollaborator");
		expected.add(nameOfTaskState + ".addProjectCollaborator");
		
		//When
		List<String> result = task1.getTaskState().listAvailableActions();
		
		//Then
		assertEquals(expected, result);
	}
}
