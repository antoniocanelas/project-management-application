package project.model.unittests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;
import project.model.user.User;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class ProjectCollaboratorServiceTest {

    // Services
    ProjectRepositoryClass projectRepositoryClass;
    UserRepositoryClass userRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;

    ProjectService projectService;
    UserService userService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    // Dates
    LocalDate birth;
    LocalDate birth1;
    LocalDate birth2;

    LocalDateTime d1;
    LocalDateTime d2;

    // Users
    User user1;
    User user2;
    User user3;
    User user4;
    User projectManager;

    // Project Collaborators
    ProjectCollaborator col1;
    ProjectCollaborator col2;
    ProjectCollaborator col3;

    // Projects
    Project p1;
    Project p2;
    Project p3;

    @Before
    public void initialize() throws AddressException {

        projectRepositoryClass = new ProjectRepositoryClass();
        userRepositoryClass = new UserRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();

        projectService = new ProjectService(projectRepositoryClass);
        userService = new UserService(userRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        ProjectCollaborator.setStartIdGenerator(1);
        projectService.addProject("1", "project1");
        projectService.addProject("2", "project2");
        projectService.addProject("3", "project3");

        p1 = projectService.getProjectByID("1");
        p2 = projectService.getProjectByID("2");
        p3 = projectService.getProjectByID("3");

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d2 = LocalDateTime.of(2017, 12, 18, 0, 0);

        taskService.addTask(p1, "Test 1", "task-1", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 22", "task-2", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 333", "task-3", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 666666", "task-6", d1, d2, 1.2, 1.3);

        birth = LocalDate.of(1989, 5, 11);
        birth1 = LocalDate.of(1999, 9, 11);
        birth2 = LocalDate.of(1975, 12, 5);

        userService.addUser("Pedro", "919999999", "pedro@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "pais");
        userService.addUser("Joana", "929999999", "teste@teste.com", "432893", birth2, "2", "Rua ", "4433", "cidade",
                "pais");
        userService.addUser("Joao", "929999999", "jopgomes@net.com", "432893", birth2, "2", "Rua ", "4433",
                "cidade", "pais");
        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@gmail.com", "55555", birth, "2", "Rua ",
                "4433", "cidade", "pais");
        userService.addUser("Firmino", "99 555 333 22", "firmino@gmail.com", "44444", birth, "2", "Rua ",
                "4433", "cidade", "pais");

        user1 = userService.searchUserByEmail("pedro@gmail.com");
        user2 = userService.searchUserByEmail("teste@teste.com");
        user3 = userService.searchUserByEmail("jopgomes@net.com");
        user4 = userService.searchUserByEmail("firmino@gmail.com");
        projectManager = userService.searchUserByEmail("asdrubal@gmail.com");
        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        user4.setProfileDirector();
        projectManager.setProfileCollaborator();

        projectCollaboratorService.setProjectManager(p1, projectManager);
        projectCollaboratorService.addProjectCollaborator(p1, user1, 7);
        projectCollaboratorService.addProjectCollaborator(p1, user2, 5);
        projectCollaboratorService.addProjectCollaborator(p1, user4, 5);

        col1 = p1.findProjectCollaborator(user1);
        col2 = p1.findProjectCollaborator(user2);
        col3 = p1.findProjectCollaborator(projectManager);

    }
    
    
    @Test
    @Transactional
    public void getAndSetIdIncrementTest() {
        // Given
        assertEquals(col2.getId(),3);
        // When
        ProjectCollaborator.setStartIdGenerator(10);
        projectCollaboratorService.addProjectCollaborator(p1, user3, 10);
        //Then
        ProjectCollaborator col4 = projectCollaboratorService.getProjectCollaborator(p1.getId(), user3.getEmail());
        assertEquals(col4.getId(),10);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorFailProjManagNullTest() {
        // Given
        boolean condition1 = p2.getProjectManager() == null;
        boolean condition2 = projectCollaboratorService.listUsersByProject("2").contains(user1);
        assertTrue(condition1);
        assertFalse(condition2);
        // When
        boolean condition3 = projectCollaboratorService.addProjectCollaborator(p2, user1, 1.5);
        // Then
        assertFalse(condition3);
        boolean condition4 = projectCollaboratorService.listUsersByProject("2").contains(user1);
        assertFalse(condition4);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorFailUserInactiveTest() {
        // Given
        user3.setInactive();
        boolean condition1 = user3.isActive();
        boolean condition2 = p1.getProjectManager() != null;
        boolean condition3 = projectCollaboratorService.listUsersByProject("1").contains(user3);
        assertFalse(condition1);
        assertTrue(condition2);
        assertFalse(condition3);
        // When
        boolean condition4 = projectCollaboratorService.addProjectCollaborator(p1, user3, 1.5);
        // Then
        assertFalse(condition4);
        boolean condition5 = projectCollaboratorService.listUsersByProject("2").contains(user3);
        assertFalse(condition5);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorFailUserNotCollabTest() {
        // Given
        user1.setProfileRegisteredUser();
        // When
        boolean condition1 = projectCollaboratorService.addProjectCollaborator(p1, user1, 1.5);
        // Then
        assertFalse(condition1);
        boolean condition2 = projectCollaboratorService.listUsersByProject("2").contains(user3);
        assertFalse(condition2);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorFailNoCollabProfile() {
        // Given
        boolean condition1 = projectCollaboratorService.listUsersByProject("1").contains(user3);
        assertFalse(condition1);
        // When
        boolean condition2 = projectCollaboratorService.addProjectCollaborator(p1, user4, 1.5);
        // Then
        assertFalse(condition2);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorSuccessTest() {
        // Given
        boolean condition1 = projectCollaboratorService.listUsersByProject("1").contains(user3);
        assertFalse(condition1);
        // When
        boolean condition2 = projectCollaboratorService.addProjectCollaborator(p1, user3, 1.5);
        // Then
        assertTrue(condition2);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorSuccessTestUserIsNotActive() {
        // Given
        col2 = p1.findProjectCollaborator(user2);
        boolean condition1 = projectCollaboratorService.listActiveUsersByProject(p1.getId()).contains(user2);
        assertTrue(condition1);
        col2.getCostAndTimePeriodList().get(col2.getCostAndTimePeriodList().size() - 1).setEndDate(LocalDate.now().minusDays(2));
        boolean condition2 = projectCollaboratorService.listActiveUsersByProject(p1.getId()).contains(user2);
        assertFalse(condition2);
        // When
        boolean condition3 = projectCollaboratorService.addProjectCollaborator(p1, user2, 1.5);
        // Then
        assertTrue(condition3);
    }

    @Test
    @Transactional
    public void addProjectCollaboratorSuccessFailExistingUser() {
        // Given
        boolean condition1 = projectCollaboratorService.listUsersByProject("1").contains(user1);
        assertTrue(condition1);
        // When
        boolean condition2 = projectCollaboratorService.addProjectCollaborator(p1, user1, 1.5);
        // Then
        assertFalse(condition2);
    }

    @Test
    @Transactional
    public void setProjectManagerFailUserNull() {
        // Given
        boolean condition1 = p1.getProjectManager() != null;
        assertTrue(condition1);
        // When
        boolean condition2 = projectCollaboratorService.setProjectManager(p1, null);
        // Then
        assertFalse(condition2);
        boolean condition3 = p1.getProjectManager() != null;
        assertTrue(condition3);
    }

    @Test
    @Transactional
    public void setProjectManagerFailUserNotActive() {
        // Given
        user1.setInactive();
        boolean condition1 = p1.getProjectManager() != null;
        boolean condition2 = p1.getProjectManager().getUser() != user1;
        assertTrue(condition1);
        assertTrue(condition2);
        // When
        boolean condition3 = projectCollaboratorService.setProjectManager(p1, user1);
        // Then
        assertFalse(condition3);
        boolean condition4 = p1.getProjectManager().getUser() != user1;
        assertTrue(condition4);
    }

    @Test
    @Transactional
    public void setProjectManagerSuccess() {
        // Given
        boolean condition1 = p1.getProjectManager().getUser() != user1;
        assertTrue(condition1);
        // When
        boolean condition2 = projectCollaboratorService.setProjectManager(p1, user1);
        // Then
        assertTrue(condition2);
        boolean condition3 = p1.getProjectManager().getUser() == user1;
        assertTrue(condition3);
    }

    @Test
    @Transactional
    public void updateProjectCollaboratorSuccess() {

        // Given
        assertEquals(7.0, col1.getCurrentCost(), 0.001);

        // When 
        col1.addNewCostAndAssociatedPeriod(5.0);
        projectCollaboratorService.updateProjectCollaborator(col1);
        // Then
        assertEquals(5.0, col1.getCurrentCost(), 0.001);
    }

    @Test
    @Transactional
    public void getProjectCollaboratorTest() {
        assertNotNull(projectCollaboratorService.getProjectCollaborator(1));
        assertEquals(projectCollaboratorRepositoryClass.getOneById(1), projectCollaboratorService.getProjectCollaborator(1));
    }

    @Test
    @Transactional
    public void getProjectCollaborator() {
        // Given
        ProjectCollaborator projCollabExpected = col1;
        String email = "pedro@gmail.com";
        ProjectCollaborator projCollabResult = projectCollaboratorService.getProjectCollaborator(p1.getId(), email);
        boolean condition1 = email.equals(col1.getUser().getEmail());
        boolean condition2 = p1.getId().equals(col1.getProject().getId());
        // Then
        assertTrue(condition1);
        assertTrue(condition2);
        assertEquals(projCollabExpected, projCollabResult);
    }

    @Test
    @Transactional
    public void getProjectCollaboratorFailCondition1Failed() {
        // Given
        String email = "pedroo@gmail.com";
        ProjectCollaborator projCollabResult = projectCollaboratorService.getProjectCollaborator(p1.getId(), email);
        boolean condition1 = email.equals(col1.getUser().getEmail());
        boolean condition2 = p1.getId().equals(col1.getProject().getId());
        // Then
        assertFalse(condition1);
        assertTrue(condition2);
        assertNull(projCollabResult);
    }

    @Test
    @Transactional
    public void getProjectCollaboratorFailCondition2Failed() {
        // Given
        String email = "pedro@gmail.com";
        ProjectCollaborator projCollabResult = projectCollaboratorService.getProjectCollaborator(p2.getId(), email);
        boolean condition1 = email.equals(col1.getUser().getEmail());
        boolean condition2 = p2.getId().equals(col1.getProject().getId());
        // Then
        assertTrue(condition1);
        assertFalse(condition2);
        assertNull(projCollabResult);
    }

    @Test
    @Transactional
    public void removeProjectCollaboratorSuccess() {
        // Given
        boolean condition1 = projectCollaboratorService.listUsersByProject("1").contains(user1);
        assertTrue(condition1);
        // When
        boolean condition2 = projectCollaboratorService.removeProjectCollaborator(p1, "pedro@gmail.com");
        // Then
        assertTrue(condition2);
        boolean condition3 = projectCollaboratorService.listActiveUsersByProject("1").contains(user1);
        assertFalse(condition3);

    }

    @Test
    @Transactional
    public void removeProjectCollaboratorFailProjCollabNull() {
        // Given
    	
        // When
        boolean condition = projectCollaboratorService.removeProjectCollaborator(p1, "assss@asass.asa");
        // Then
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void listProjectCollaboratorsByProjectSuccess() {
        // Given
        List<ProjectCollaborator> result = projectCollaboratorService.listProjectCollaboratorsByProject(p1.getId());
        List<ProjectCollaborator> expected = new ArrayList<>();
        expected.add(col3);
        expected.add(col1);
        expected.add(col2);
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listProjectCollaboratorsByProjectFail() {
        // Given
        List<ProjectCollaborator> result = projectCollaboratorService.listProjectCollaboratorsByProject(p3.getId());
        List<ProjectCollaborator> expected = new ArrayList<>();
        expected.add(col3);
        // Then
        assertNotEquals(expected, result);
    }

    @Test
    @Transactional
    public void listProjectCollaboratorsByProjectFailUserIsNotActive() {
        // Given
        List<ProjectCollaborator> result = projectCollaboratorService.listProjectCollaboratorsByProject(p1.getId());
        List<ProjectCollaborator> expected = new ArrayList<>();
        expected.add(col3);
        expected.add(col1);
        expected.add(col2);
        assertEquals(expected, result);
        // When
        col2.getCostAndTimePeriodList().get(col2.getCostAndTimePeriodList().size() - 1).setEndDate(LocalDate.now().minusDays(2));
        assertFalse(col2.isActiveInProject());
        result = projectCollaboratorService.listProjectCollaboratorsByProject(p1.getId());
        // Then
        expected.remove(col2);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listUsersByProject() {
        // Given
        List<User> result = projectCollaboratorService.listUsersByProject(p1.getId());
        List<User> expected = new ArrayList<>();
        expected.add(projectManager);
        expected.add(user1);
        expected.add(user2);
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listActiveUsersByProject() {
        // Given
        List<User> result = projectCollaboratorService.listActiveUsersByProject(p1.getId());
        List<User> expected = new ArrayList<>();
        expected.add(projectManager);
        expected.add(user1);
        expected.add(user2);
        assertEquals(expected, result);
        //When
        assertEquals(col2.getUser(), user2);
        col2.getCostAndTimePeriodList().get(col2.getCostAndTimePeriodList().size() - 1).setEndDate(LocalDate.now().minusDays(2));
        assertFalse(col2.isActiveInProject());
        result = projectCollaboratorService.listActiveUsersByProject(p1.getId());
        expected.remove(user2);
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void listActiveUsersByProjectUserProjectIdNotEqualsProjectId() {
        // Given
        List<User> result = projectCollaboratorService.listActiveUsersByProject(p3.getId());
        List<User> expected = new ArrayList<>();
        assertEquals(expected, result);

        //When
        p3.addProjectCollaborator(col2);
        assertNotEquals(col2.getProject().getId(), p3.getId());
        result = projectCollaboratorService.listActiveUsersByProject(p3.getId());
        expected = new ArrayList<>();

        //Then
        assertEquals(expected, result);

    }
    
    @Test
    @Transactional
    public void updateProjectCollaboratorNotRegistered() {
        // Given
       ProjectCollaborator collab = new ProjectCollaborator(p2, user3, 10);
       collab.setId(70);
        //When
        
        projectCollaboratorService.updateProjectCollaborator(collab);

        //Then
        assertEquals(projectCollaboratorService.getProjectCollaborator(70),null);
        assertEquals(projectCollaboratorService.getProjectCollaborator(p2.getId(), user3.getEmail()),null);

    }
    
    @Test
    @Transactional
    public void updateProjectManagerTest() {
    	// Given
        boolean condition1 = p1.getProjectManager().getUser() != user1;
        assertTrue(condition1);
        // When
        boolean condition2 = projectCollaboratorService.setProjectManager(p1, user1);
        // Then
        assertTrue(condition2);
        boolean condition3 = p1.getProjectManager().getUser() == user1;
        assertTrue(condition3);
        ProjectCollaborator pm = p1.getProjectManager();
        assertEquals(p1.listActiveProjectCollaborators().contains(pm),true);
        //and When
        assertEquals(projectCollaboratorService.setProjectManager(p1, user3),true);
        //Then
        assertEquals(p1.listActiveProjectCollaborators().contains(pm),false);
        assertEquals(p1.listActiveProjectCollaborators().contains(p1.findProjectCollaborator(user3)),true);

    }
    
    
    
    

}
