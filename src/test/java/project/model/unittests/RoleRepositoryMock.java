package project.model.unittests;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import project.model.user.roles.Role;
import project.model.user.roles.RoleName;
import project.jparepositories.RoleRepository;

import java.util.*;

public class RoleRepositoryMock implements RoleRepository {


    Set<Role> list = new LinkedHashSet<>();

    @Override
    public Role getOneByName(RoleName roleName) {
        return null;
    }

    @Override
    public List<Role> findAll() {
        return new ArrayList<Role>(list);
    }

    @Override
    public List<Role> findAll(Sort sort) {
        return new ArrayList<Role>(list);
    }

    @Override
    public Page<Role> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Role> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Role role) {

    }

    @Override
    public void deleteAll(Iterable<? extends Role> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Role> S save(S entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public <S extends Role> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Role> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Role> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Role> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Role getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Role> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Role> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Role> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Role> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Role> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Role> boolean exists(Example<S> example) {
        return list.contains(example);
    }

	@Override
	public boolean existsByName(RoleName roleName) {
		
		return list.contains(roleName);
	}
}
