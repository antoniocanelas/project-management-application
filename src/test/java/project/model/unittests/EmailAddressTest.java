package project.model.unittests;

import static org.junit.Assert.assertEquals;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.junit.Before;
import org.junit.Test;

import project.model.user.EmailAddress;

public class EmailAddressTest {

    // Email Addresses
    EmailAddress email1;
    EmailAddress email2;
    EmailAddress email3;

    @Before
    public void setUp() throws AddressException {
        email1 = new EmailAddress("lisaggjhdgjh@switch.com");
        email2 = new EmailAddress("lisaAdelaide@switch.com");
    }

    @Test
    public void testEmailEqualsGetAndSet() throws AddressException {
        // Given
        assertEquals(new InternetAddress("lisaggjhdgjh@switch.com"), email1.getEmailAddre());
        // When
        email1.setEmailAddre("lisa@switch.com");
        // Then
        assertEquals(new InternetAddress("lisa@switch.com"), email1.getEmailAddre());
    }

    @Test
    public void testEmailEqualsFalse() throws AddressException {
        // Given
        assertEquals(true, new EmailAddress("lisaggjhdgjh@switch.com").equals(email1));
        // When
        EmailAddress expected = new EmailAddress("lish@switch.com");
        InternetAddress otherClass = new InternetAddress("dsa@sds.com");
        // Then
        assertEquals(false, expected.equals(email1));
        assertEquals(false, expected.equals(otherClass));
    }

    @Test
    public void testEmailEqualsTrue() throws AddressException {
        // Given
        // When
        EmailAddress mail = new EmailAddress("lisaggjhdgjh@switch.com");
        // Then
        assertEquals(mail, email1);
        // When
        EmailAddress expected = new EmailAddress("lisaggjhdgjh@switch.com");
        // Then
        assertEquals(true, mail.equals(expected));
        assertEquals(true, mail.equals(mail));
    }

    @Test
    public void testEmailEqualsNullCases() throws AddressException {
        // Given
        // When
        EmailAddress mail = new EmailAddress("lisaggjhdgjh@switch.com");
        // Then
        assertEquals(mail, email1);
        assertEquals(false, mail.equals(null));
    }

    @Test
    public void testHassCode() throws AddressException {
        // Given
        // When
        EmailAddress mail = new EmailAddress("lisaggjhdgjh@switch.com");
        // Then
        assertEquals(mail.hashCode(), email1.hashCode());
        assertEquals(email1.hashCode(), -949244899);
    }

    @Test
    public void testToString() throws AddressException {
        // Given
        // When
        EmailAddress mail = new EmailAddress("lisaggjhdgjh@switch.com");
        // Then
        assertEquals("lisaggjhdgjh@switch.com",mail.toString());
     
    }
    
    @Test(expected = AddressException.class)
    public void badEmailFormat() throws AddressException {
        // When
        new EmailAddress("emailWithoutAt");
    }

    @Test(expected = AddressException.class)
    public void badEmailFormatTwoAts() throws AddressException {
        // When
        new EmailAddress("email@@WithoutAt");
    }

    @Test(expected = AddressException.class)
    public void badEmailFormatNoDomain() throws AddressException {
        // When
        new EmailAddress("email@");
    }

    @Test(expected = AddressException.class)
    public void badEmailFormatNoPart() throws AddressException {
        // When
        new EmailAddress("@email");
    }

}
