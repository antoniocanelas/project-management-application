package project.model.unittests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;

import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.projectcollaborator.ProjectCollaboratorCostAndTimePeriod;
import project.model.user.User;

public class ProjectCollaboratorTest {

    // Users
    private User user1;
    private User user2;
    private User user3;
    private User user4;
    private User user5;
    private User user6;

    // Project Collaborators
    private ProjectCollaborator collab1;
    private ProjectCollaborator collab2;
    private ProjectCollaborator collab3;
    private ProjectCollaborator collab4;
    private ProjectCollaborator collab5;
    private ProjectCollaborator collab6;


    // Projects
    private Project project;
    private Project project4;

    @Before
    public void initialize() throws AddressException {
        project = mock(Project.class);
        project4 = null;

        user1 = mock(User.class);
        user2 = mock(User.class);
        user3 = mock(User.class);
        user4 = null;
        user5 = new User("Ana", "123", "ana@switch.com", "2345", LocalDate.of(2018, 5, 31), "1", "Rua ", "4433",
                "cidade", "país");
        user6 = new User("pedro", "321", "pedro@switch.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433",
                "cidade", "país");

        collab1 = new ProjectCollaborator(project, user1, 10);
        collab2 = new ProjectCollaborator(project, user2, 20);
        collab3 = new ProjectCollaborator(project, user1, 10);
        collab4 = new ProjectCollaborator(project4, user4, 0);
        collab5 = new ProjectCollaborator(project4, user5, 1);
        collab6 = new ProjectCollaborator(project4, user6, 2);
    }

    @Test
    public void testGetCurrenttCost() {
        // Given
        // Then
        assertEquals(10.0, collab1.getCurrentCost(), 0.1);
    }

    @Test
    public void testGetCurrenttCost_twoEntries() {
        // Given
        assertEquals(10.0, collab1.getCurrentCost(), 0.1);
        // When
        assertTrue(collab1.addNewCostAndAssociatedPeriod(45.0));
        // Then
        assertEquals(45.0, collab1.getCurrentCost(), 0.1);
    }

    @Test
    public void testGetId() {
        // Given
        // Then
        assertEquals(0, collab1.getId());
    }

    @Test
    public void getIdTest() {
        // Given
        ProjectCollaborator pc = mock(ProjectCollaborator.class);
        // When
        when(pc.getId()).thenReturn(10);
        // Then
        assertEquals(10, pc.getId());
    }

    @Test
    public void isActiveInProjectTest() {
        // Given
        // Then
        boolean condition = collab1.isActiveInProject();
        assertTrue(condition);
    }

    @Test
    public void isActiveInProjectTest_false() {
        // Given
        boolean condition1 = collab1.isActiveInProject();
        assertTrue(condition1);
        // When
        collab1.getCostAndTimePeriodList().get(0).setEndDate(LocalDate.now());
        // Then
        boolean condition2 = collab1.isActiveInProject();
        assertFalse(condition2);
    }

    @Test
    public void getProjectTest() {
        // Given
        // When
        Project projectTest = collab1.getProject();
        // Then
        assertEquals(project, projectTest);
    }

    @Test
    public void testSetAndGetUser() {
        // Given
        // When
        collab1.setUser(user3);
        // Then
        assertEquals(user3, collab1.getUser());
    }

    @Test
    public void testSetCostAndTimePeriodList() {
        // Given
        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodList = new ArrayList<>();

        // When
        collab1.setCostAndTimePeriodList(costAndTimePeriodList);
        // Then
        assertEquals(costAndTimePeriodList, collab1.getCostAndTimePeriodList());
    }

    @Test
    public void testTrue1() {
        // Given
        boolean condition = collab1.equals(collab1);
        assertTrue(condition);
    }

    @Test
    public void testTrue2() {
        // Given
        boolean condition = collab1.equals(collab3);
        assertTrue(condition);
    }

    @Test
    public void testFalse1() {
        // Given
        boolean condition = collab1.equals(collab2);
        assertFalse(condition);
    }

    @Test
    public void testFalse2Null() {
        // Given
        // When
        collab1 = null;
        // Then
        boolean condition = collab2.equals(collab1);
        assertFalse(condition);
    }

    @Test
    public void testFalse5DifferentClass() {
        // Given
        boolean condition = collab1.equals(user2);
        assertFalse(condition);
    }

    @Test
    public void testToString() {
        // Given
        // When
        when(user1.getEmail()).thenReturn("something@something.new");
        when(user1.getName()).thenReturn("NoOne");
        // Then
        assertEquals("\nProject ROLE_COLLABORATOR Name=NoOne, Email=something@something.new, cost=10.0\n", collab1.toString());
    }

    @Test
    public void testEquals() {
        // Given
        boolean condition = collab4.equals(collab1);
        assertFalse(condition);
    }

    @Test
    public void testEquals_nullProject() {
        // Given
        boolean condition1 = collab4.equals(collab3);
        assertFalse(condition1);
        boolean condition2 = collab3.equals(collab4);
        assertFalse(condition2);
        boolean condition3 = collab5.equals(collab6);
        assertFalse(condition3);
        // When
        collab3 = null;
        // Then
        boolean condition4 = collab4.equals(collab3);
        assertFalse(condition4);
    }

    @Test
    public void testEquals_diferentProject() {
        // Given
        Project p2 = mock(Project.class);
        // When
        ProjectCollaborator pc2 = new ProjectCollaborator(p2, user1, 10);
        // Then
        boolean condition = collab1.equals(pc2);
        assertFalse(condition);
    }

    @Test
    public void testIsActiveTrue() {
        assertEquals(true, collab1.isActiveInProject());
    }

    @Test
    public void testIsActiveFalse() {
        collab1.getCostAndTimePeriodList().get(0).setEndDate(LocalDate.now());
        assertEquals(false, collab1.isActiveInProject());
    }

    @Test
    public void testGetLastCostTimeAndPeriod() {
        assertEquals(collab1.getLastCostAndTimePeriod(), collab1.getCostAndTimePeriodList().get(0));
        collab1.addNewCostAndAssociatedPeriod(20);
        assertEquals(collab1.getLastCostAndTimePeriod(), collab1.getCostAndTimePeriodList().get(1));

    }

    /**
     * GIVEN: a collaborator's cost and time period,
     * WHEN: we add a new cost and associated time period
     * THEN: the former period is closed with today's date and the
     * next period starts tomorrow.
     */
    @Test
    public void testAddNewCostAndAssociatedPeriod() {
        //Given
        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodListInitial = new ArrayList<>();
        List<ProjectCollaboratorCostAndTimePeriod> costAndTimePeriodListFinal = new ArrayList<>();
        assertNull(collab2.getLastCostAndTimePeriod().getEndDate());
        assertEquals(1, collab2.getCostAndTimePeriodList().size());
        costAndTimePeriodListInitial.add(collab2.getLastCostAndTimePeriod());
        assertEquals(costAndTimePeriodListInitial, collab2.getCostAndTimePeriodList());

        //When
        collab2.addNewCostAndAssociatedPeriod(20);

        //Then
        assertEquals(collab2.getCostAndTimePeriodList().get(0).getEndDate(), LocalDate.now());
        assertEquals(collab2.getLastCostAndTimePeriod().getStartDate(), LocalDate.now().plusDays(1));
        assertEquals(2, collab2.getCostAndTimePeriodList().size());
        costAndTimePeriodListFinal.add(collab2.getCostAndTimePeriodList().get(0));
        costAndTimePeriodListFinal.add(collab2.getCostAndTimePeriodList().get(1));
        assertEquals(costAndTimePeriodListFinal, collab2.getCostAndTimePeriodList());

    }

    @Test
    public void testAddNewCostAndAssociatedPeriod2() {
        List<ProjectCollaboratorCostAndTimePeriod> initialList = new ArrayList<>();
        List<ProjectCollaboratorCostAndTimePeriod> finalList = new ArrayList<>();
        ProjectCollaboratorCostAndTimePeriod initialCostAndTime = collab6.getLastCostAndTimePeriod();
        initialList.add(initialCostAndTime);
        finalList.add(initialCostAndTime);
        assertEquals(initialList, finalList);
        collab6.addNewCostAndAssociatedPeriod(5);
        ProjectCollaboratorCostAndTimePeriod finalCostAndTime = collab6.getLastCostAndTimePeriod();
        initialList.add(finalCostAndTime);
        assertNotEquals(initialList, finalList);
    }

    @Test
    @Transactional
    public void testHashCode() {
        User userTest = null;
        try {
            userTest = new User("Ana", "123", "ana@gmail.com", "2345", LocalDate.now(), "2", "Rua ", "4433", "cidade", "país");
        } catch (AddressException e) {
            e.printStackTrace();
        }

        Project projectTest = new Project("Test", "Project Test");
        ProjectCollaborator projectCollaboratorTest = new ProjectCollaborator(projectTest, userTest, 9.0);
        projectCollaboratorTest.setId(50);


        assertEquals(1768775231, projectCollaboratorTest.hashCode());

    }

    @Test
    @Transactional
    public void testHashCode2() {
        User userTest = null;

        Project projectTest = new Project("Test", "Project Test");
        ProjectCollaborator projectCollaboratorTest = new ProjectCollaborator(projectTest, userTest, 9.0);
        projectCollaboratorTest.setId(50);


        assertEquals(2651236, projectCollaboratorTest.hashCode());

    }

    @Test
    @Transactional
    public void testHashCode3() throws AddressException {
        User userTest = new User("Ana", "123", "ana@gmail.com", "2345", LocalDate.now(), "2", "Rua ", "4433", "cidade", "país");

        Project projectTest = null;
        ProjectCollaborator projectCollaboratorTest = new ProjectCollaborator(projectTest, userTest, 9.0);
        projectCollaboratorTest.setId(50);


        assertEquals(1766172045, projectCollaboratorTest.hashCode());

    }

}
