package project.model.unittests;

import jparepositoriestest.ProjectCollaboratorRepositoryClass;
import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.TaskRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.rest.*;
import project.model.UserService;
import project.model.project.Project;
import project.model.project.Project.ProjectUnits;
import project.model.project.ProjectService;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.task.Task;
import project.model.task.TaskCollaboratorRegistry;
import project.model.user.User;
import project.model.user.UserIdVO;
import project.services.ProjectCollaboratorService;
import project.services.TaskService;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class ProjectServiceTest {

    // Services
    ProjectService projectService;
    UserService userService;
    TaskService taskService;
    ProjectCollaboratorService projectCollaboratorService;

    // Repositories
    UserRepositoryClass userRepositoryClass;
    ProjectRepositoryClass projectRepositoryClass;
    ProjectCollaboratorRepositoryClass projectCollaboratorRepositoryClass;
    TaskRepositoryClass taskRepositoryClass;

    // Dates
    LocalDate birth;
    LocalDate birth1;
    LocalDate birth2;
    LocalDateTime d1;
    LocalDateTime d2;
    LocalDateTime d3;
    LocalDateTime d4;
    LocalDateTime d5;
    LocalDateTime d6;
    LocalDateTime d7;
    LocalDateTime d8;
    LocalDate dn1;
    LocalDate dn2;

    // Users
    User user1;
    User user2;
    User user3;
    User projectManager;


    // Project Collaborators
    ProjectCollaborator col1;
    ProjectCollaborator col2;

    // Projects
    Project p1;
    Project p2;
    Project p3;
    Project p4;

    // Tasks
    Task t1;
    Task t2;
    Task t3;
    Task t4;
    Task t5;
    Task t6;

    // Lists - Users and Projects
    List<User> expectedUser;
    List<Project> expectedProject;

    UserIdVO userIdVO;
    String msg;

    @Before
    public void initialize() throws AddressException {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectCollaboratorRepositoryClass = new ProjectCollaboratorRepositoryClass();
        taskRepositoryClass = new TaskRepositoryClass();

        userService = new UserService(userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);
        projectCollaboratorService = new ProjectCollaboratorService(projectCollaboratorRepositoryClass);
        taskService = new TaskService(taskRepositoryClass);

        projectService.addProject("1", "project1");
        projectService.addProject("2", "project2");
        projectService.addProject("3", "project3");
        projectService.addProject("4", "project4");
        p1 = projectService.getProjectByID("1");
        p2 = projectService.getProjectByID("2");
        p3 = projectService.getProjectByID("3");
        p4 = projectService.getProjectByID("4");

        d1 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d2 = LocalDateTime.of(2017, 12, 18, 0, 0);
        d3 = LocalDateTime.of(2020, 12, 18, 0, 0);
        d4 = LocalDateTime.of(2017, 12, 1, 0, 0);
        d5 = LocalDateTime.of(2018, 5, 18, 0, 0);

        taskService.addTask(p1, "Test 1", "task-1", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 22", "task-2", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 333", "task-3", d1, d2, 1.2, 1.3);
        taskService.addTask(p2, "Test task 2-1", "task-2-1", d1, d2, 1.2, 1.3);
        taskService.addTask(p2, "Test task 2-222", "task-2-222", d1, d2, 1.2, 1.3);
        taskService.addTask(p1, "Test 666666", "task-6", d1, d2, 1.2, 1.3);

        t1 = p1.findTaskByTitle("Test 1");
        t2 = p1.findTaskByTitle("Test 22");
        t3 = p1.findTaskByTitle("Test 333");
        t4 = p2.findTaskByTitle("Test task 2-1");
        t5 = p2.findTaskByTitle("Test task 2-222");
        t6 = p1.findTaskByTitle("Test 666666");

        birth = LocalDate.of(1989, 5, 11);
        birth1 = LocalDate.of(1999, 9, 11);
        birth2 = LocalDate.of(1975, 12, 5);

        userService.addUser("Pedro", "919999999", "pedro@gmail.com", "332432", birth1, "2", "Rua ", "4433", "cidade",
                "pais");

        userService.addUser("Joana", "929999999", "teste@teste.com", "432893", birth2, "2", "Rua ", "4433", "cidade",
                "pais");

        userService.addUser("Joao", "929999999", "jopgomes@net.com", "432893", birth2, "2", "Rua ", "4433", "cidade",
                "pais");

        userService.addUser("Asdrubal", "99 555 666 22", "asdrubal@gmail.com", "55555", birth, "2", "Rua ", "4433",
                "cidade", "pais");

        user1 = userService.searchUserByEmail("pedro@gmail.com");
        user2 = userService.searchUserByEmail("teste@teste.com");
        user3 = userService.searchUserByEmail("jopgomes@net.com");
        projectManager = userService.searchUserByEmail("asdrubal@gmail.com");

        user1.setProfileCollaborator();
        user2.setProfileCollaborator();
        user3.setProfileCollaborator();
        projectManager.setProfileCollaborator();

        projectCollaboratorService.setProjectManager(p1, projectManager);
        projectCollaboratorService.addProjectCollaborator(p1, user1, 7);
        projectCollaboratorService.addProjectCollaborator(p1, user2, 5);

        col1 = p1.findProjectCollaborator(user1);
        col2 = p1.findProjectCollaborator(user2);

        t1.addProjectCollaborator(col1);
        t1.addProjectCollaborator(col2);
        t2.addProjectCollaborator(col2);

        expectedUser = new ArrayList<>();
        expectedProject = new ArrayList<>();
    }

    @Test
    @Transactional
    public void removeTaskToUserListSucessComparingListEmpty() {
        // Given
        // When
        t1.removeProjectCollaborator(col1);
        List<Task> expected = new ArrayList<>();
        // Then
        assertEquals(expected, projectService.listUserTasksInAllProjects(user1));
    }

    @Test
    @Transactional
    public void removeTaskToUserListSucessComparingList() {
        // Given
        // When
        t2.addProjectCollaborator(col1);
        t3.addProjectCollaborator(col1);
        t1.removeProjectCollaborator(col1);
        List<Task> expected = new ArrayList<>();
        expected.add(t2);
        expected.add(t3);
        // Then
        assertEquals(expected, projectService.listUserTasksInAllProjects(user1));
    }

    @Test
    @Transactional
    public void removeTaskToUserListSucess() {
        // Given
        boolean condition = t1.removeProjectCollaborator(col1);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void removeTaskToUserListFailureProjectCollaboratorNotInTask() {
        // Given
        boolean condition = t2.removeProjectCollaborator(col1);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void removeTaskToUserListFailureProjectHasntProjectCollaborator() {
        // Given
        // When
        projectCollaboratorService.removeProjectCollaborator(p1, "pedro@gmail.com");
        // Then
        boolean condition = t2.removeProjectCollaborator(col1);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testAddProjectTrue1() {
        // Given
        boolean condition = projectService.addProject("100", "Initial Project");
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testAddProjectTrueSameName() {
        // Given
        // When
        projectService.addProject("101", "Initial Project");
        // Then
        boolean condition = projectService.addProject("102", "Initial Project");
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testAddProjectFalse() {
        // Given
        // When
        projectService.addProject("103", "Initial Project");
        // Then
        boolean condition = projectService.addProject("103", "Second Project");
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testAddProjectManagerTrue() {
        // Given
        // When
        userService.addUser(projectManager);
        // Then
        boolean condition = projectCollaboratorService.setProjectManager(p1, projectManager);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testAddProjectManagerFalse() {
        // Given
        boolean condition = projectCollaboratorService.setProjectManager(null, null);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testListAllActiveProjectsSuccess1() {
        // Given
        // When
        projectService.getProjectList().clear();
        p2.setInactive();
        List<Project> expected = new ArrayList<>();
        expected.add(p3);
        expected.add(p4);
        expected.add(p1);
        // Then
        assertThat(expected, containsInAnyOrder(projectService.listAllActiveProjects().toArray()));
    }

    @Test
    @Transactional
    public void testListAllActiveProjectsSuccess2() {
        // Given
        // When
        p2.setInactive();
        // Then
        boolean condition1 = projectService.listAllActiveProjects().contains(p1);
        boolean condition2 = projectService.listAllActiveProjects().contains(p3);
        boolean condition3 = projectService.listAllActiveProjects().contains(p4);
        boolean condition4 = projectService.listAllActiveProjects().contains(p2);
        assertTrue(condition1);
        assertTrue(condition2);
        assertTrue(condition3);
        assertFalse(condition4);
    }

    @Test
    @Transactional
    public void testListAllActiveProjectsAllFalse() {
        // Given
        // When
        p1.setInactive();
        p2.setInactive();
        p3.setInactive();
        projectService.addProject("1", "project1");
        projectService.addProject("2", "project2");
        projectService.addProject("3", "project3");
        // Then
        boolean condition1 = projectService.listAllActiveProjects().contains(p1);
        boolean condition2 = projectService.listAllActiveProjects().contains(p2);
        boolean condition3 = projectService.listAllActiveProjects().contains(p3);
        assertFalse(condition1);
        assertFalse(condition2);
        assertFalse(condition3);

    }

    @Test
    @Transactional
    public void testListAllActiveProjectsEmptyList() {
        // Given
        // When
        p1.setInactive();
        // Then
        boolean condition = projectService.listAllActiveProjects().contains(p1);
        assertFalse(condition);

    }

    @Test
    @Transactional
    public void testGetProjectManagerEquals() {
        // Given
        // When
        projectCollaboratorService.setProjectManager(p1, user1);
        // Then
        assertEquals(user1, p1.getProjectManager().getUser());
    }

    @Test
    @Transactional
    public void testGetAllProjectManagersSuccess() {
        // Given
        // When
        projectCollaboratorService.setProjectManager(p1, user1);
        projectCollaboratorService.setProjectManager(p2, user2);
        List<User> expected = new ArrayList<>();
        expected.add(user1);
        expected.add(user2);
        List<User> result = projectService.listAllProjectManager();
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetAllProjectManagersSuccessOneUserHasMultipleProjectManagersProfile() {
        // Given
        // When
        projectCollaboratorService.setProjectManager(p1, user1);
        projectCollaboratorService.setProjectManager(p2, user1);
        List<User> expected = new ArrayList<>();
        expected.add(user1);
        List<User> result = projectService.listAllProjectManager();
        // Then
        boolean condition = expected.equals(result);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testGetUserProjects() {
        // Given
        List<Project> result = projectService.listUserProjects(user1);
        List<Project> expected = new ArrayList<>();
        expected.add(p1);
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserTasks() {

        // Given
        List<Task> result1 = projectService.listUserTasksInAllProjects(user1);
        List<Task> expected1 = new ArrayList<>();
        expected1.add(t1);
        // Then
        assertEquals(expected1, result1);
    }

    @Test
    @Transactional
    public void testGetUserCompletedTasks() {
        // Given
        // When
        d1 = LocalDateTime.of(2017, 12, 3, 0, 0);
        d2 = LocalDateTime.of(2017, 12, 2, 0, 0);
        d3 = LocalDateTime.of(2017, 12, 1, 0, 0);

        t1.addProjectCollaborator(col1);
        t2.addProjectCollaborator(col1);
        t3.addProjectCollaborator(col1);
        t1.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d1);
        t2.setEffectiveStartDate(d2);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d2);
        t3.setEffectiveStartDate(d3);
        t3.setTaskCompleted();
        t3.setEffectiveDateOfConclusion(d3);
        // Then
        List<Task> result = projectService.listUserCompletedTasksInAllProjectsSortedByReverseOrder(user1);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserPendentTasksInAllProjects() {
        // Given
        // When
        t2.addProjectCollaborator(col1);
        t3.addProjectCollaborator(col1);
        t4.addProjectCollaborator(col1);
        t1.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t2.setPredictedDateOfConclusion(d3);
        t3.setPredictedDateOfConclusion(d2);
        t4.setPredictedDateOfConclusion(d4);
        // Then
        List<Task> result = projectService.listUserPendentTasksInAllProjects(user1);
        List<Task> expected = new ArrayList<>();
        expected.add(t3);
        expected.add(t2);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserPendentTasksInAllProjectsNoPredictedDates() {
        // Given
        // When
        projectService.addProject("10", "Projeto Teste p3");
        Project pp3 = projectService.getProjectByID("10");
        projectCollaboratorService.setProjectManager(pp3, projectManager);
        user3.setProfileCollaborator();
        // Then
        boolean condition = projectCollaboratorService.addProjectCollaborator(pp3, user3, 5);
        assertTrue(condition);
        // When
        ProjectCollaborator c3 = pp3.findProjectCollaborator(user3);
        taskService.addTask(pp3, "test only title");
        taskService.addTask(pp3, "test date 1", "With date predict", d7, d8, 10, 20);
        taskService.addTask(pp3, "test date 2", "With date predict", d7, d1, 10, 20);
        taskService.addTask(pp3, "test 02 only title");
        Task tt1 = pp3.findTaskByTitle("test only title");
        Task tt2 = pp3.findTaskByTitle("test date 1");
        Task tt3 = pp3.findTaskByTitle("test date 2");
        Task tt4 = pp3.findTaskByTitle("test 02 only title");
        tt1.addProjectCollaborator(c3);
        tt2.addProjectCollaborator(c3);
        tt3.addProjectCollaborator(c3);
        tt4.addProjectCollaborator(c3);
        List<Task> result = projectService.listUserPendentTasksInAllProjects(user3);
        List<Task> expected = new ArrayList<>();
        tt3.addProjectCollaborator(col1);
        tt3.addReport(col1, 5, d1, d1.plusDays(5));
        expected.add(tt3);
        expected.add(tt1);
        expected.add(tt2);
        expected.add(tt4);
        // Then
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserCompletedTasksLastMonth0() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d11 = LocalDateTime.of(y1, m1.minus(1), 1, 0, 0);
        LocalDateTime d12 = LocalDateTime.of(y1, m1.minus(1), 27, 0, 0);
        // When
        t2.addProjectCollaborator(col1);
        t4.addProjectCollaborator(col2);
        t1.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        t2.setEffectiveStartDate(d1);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d11);
        t4.setEffectiveStartDate(d1);
        t4.setTaskCompleted();
        t4.setEffectiveDateOfConclusion(d12);
        // Then
        List<Task> result = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(user1);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserCompletedTasksLastMonth1() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d11 = LocalDateTime.of(y1, m1.minus(1), 1, 0, 0);
        LocalDateTime d12 = LocalDateTime.of(y1, m1.minus(1), 27, 0, 0);
        // When
        t2.addProjectCollaborator(col1);
        t4.addProjectCollaborator(col1);
        t2.addProjectCollaborator(col2);
        t4.addProjectCollaborator(col2);
        t1.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        t2.setEffectiveStartDate(d1);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d11);
        t4.setEffectiveStartDate(d1);
        t4.setTaskCompleted();
        t4.setEffectiveDateOfConclusion(d12);
        // Then
        List<Task> result = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(user1);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserCompletedTasksLastMonth2() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d11 = LocalDateTime.of(y1, m1.minus(1), 1, 0, 0);
        LocalDateTime d12 = LocalDateTime.of(y1, m1.minus(1), 27, 0, 0);
        // When
        t2.addProjectCollaborator(col1);
        t4.addProjectCollaborator(col1);
        t2.addProjectCollaborator(col2);
        t4.addProjectCollaborator(col2);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d11);
        t4.setTaskCompleted();
        t4.setEffectiveDateOfConclusion(d12);
        p2.getProjectCollaboratorList().add(col2);
        // Then
        List<Task> result = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(user2);
        List<Task> expected = new ArrayList<>();
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUserCompletedTasksLastMonth3() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d11 = LocalDateTime.of(y1, m1.minus(1), 1, 0, 0);
        LocalDateTime d12 = LocalDateTime.of(y1, m1.minus(1), 27, 0, 0);
        // When
        t2.addProjectCollaborator(col2);
        t1.setEffectiveStartDate(d1);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        t2.setEffectiveStartDate(d1);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d11);
        t4.setEffectiveStartDate(d1);
        t4.setTaskCompleted();
        t4.setEffectiveDateOfConclusion(d12);
        // Then
        List<Task> result = projectService.listUserCompletedTasksLastMonthInAllProjectsSortedByReverseOrder(user1);
        List<Task> expected = new ArrayList<>();
        expected.add(t1);

        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testGetUsersHoursCompletedTasksLastMonth() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
        // When
        t1.addProjectCollaborator(col2);
        t2.addProjectCollaborator(col1);

        TaskCollaboratorRegistry col1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        TaskCollaboratorRegistry col2TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + col1.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t2.addReport(col1, 2, d9, d9.plusDays(5));
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d10);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        // Then
        double result = projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user1, ProjectUnits.HOURS);
        assertEquals(7.0, result, 0.01);
    }

    @Test
    @Transactional
    public void testGetUsersHoursCompletedTasksLastMonth2() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(2), 18, 0, 0);
        p1.getProjectCollaboratorList().add(col2);
        p2.getProjectCollaboratorList().add(col2);
        // When
        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t2.addReport(col1, 2, d9, d9.plusDays(5));
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d10);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        // Then
        double resultHours = projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user2, ProjectUnits.HOURS);
        assertEquals(0.0, resultHours, 0.01);
        double resultPM = projectService.calculateUsersHoursCompletedTasksLastMonthInAllProjects(user2, ProjectUnits.PEOPLE_MONTH);
        assertEquals(0.0, resultPM, 0.01);
    }

    @Test
    @Transactional
    public void testGetUsersAverageHoursCompletedTasksLastMonth() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
        // When
        t2.addProjectCollaborator(col1);

        TaskCollaboratorRegistry col1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        TaskCollaboratorRegistry col2TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + col1.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t2.addReport(col1, 2, d9, d9.plusDays(5));
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d10);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        // Then
        double result = projectService.calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(user1);
        double expected = 3.5;
        assertEquals(expected, result, 0.01);
    }

    @Test
    @Transactional
    public void testGetUsersAverageHoursCompletedTasksLastMonthNoProjects() {
        // Given
        Month m1 = LocalDateTime.now().getMonth();
        int y1 = LocalDateTime.now().getYear();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
        // When
        t2.addProjectCollaborator(col1);

        TaskCollaboratorRegistry col1TaskRegistry = t1
                .getTaskCollaboratorRegistryByID(t1.getId() + "-" + col1.getUser().getEmail());
        col1TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        TaskCollaboratorRegistry col2TaskRegistry = t2
                .getTaskCollaboratorRegistryByID(t2.getId() + "-" + col1.getUser().getEmail());
        col2TaskRegistry.setCollaboratorAddedToTaskDate(d1.minusYears(2));

        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d10);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        // Then
        double result = projectService.calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(user1);
        assertEquals(0, result, 0.01);
    }

    @Test
    @Transactional
    public void testGetUsersAverageHoursCompletedTasksLastMonth2() {
        // Given
        int y1 = LocalDateTime.now().getYear();
        Month m1 = LocalDateTime.now().getMonth();
        LocalDateTime d9 = LocalDateTime.of(y1, m1.minus(1), 22, 0, 0);
        LocalDateTime d10 = LocalDateTime.of(y1, m1.minus(1), 18, 0, 0);
        p1.getProjectCollaboratorList().add(col2);
        p2.getProjectCollaboratorList().add(col2);
        // When
        t2.addProjectCollaborator(col1);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d10);
        t1.setTaskCompleted();
        t1.setEffectiveDateOfConclusion(d9);
        t1.addReport(col1, 5, d9, d9.plusDays(5));
        t2.addReport(col1, 2, d9, d9.plusDays(5));
        // Then
        double result = projectService.calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(user2);
        assertEquals(0.0, result, 0.01);
    }

    @Test
    @Transactional
    public void testGetUsersAverageHoursCompletedTasksLastMonthUserNoTasks() {
        // Given
        // Then
        assertEquals(0.0, projectService.calculateUsersAverageHoursCompletedTasksLastMonthInAllProjects(user3), 0.01);
    }

    @Test
    @Transactional
    public void testGetProjectById() {
        // Given
        // Then
        assertEquals(p1, projectService.getProjectByID("1"));
        assertEquals(p2, projectService.getProjectByID("2"));
    }

    @Test
    @Transactional
    public void testGetProjectByIdFalse() {
        // Given
        // Then
        assertNull(projectService.getProjectByID("6"));
        assertNull(projectService.getProjectByID("7"));
    }

    @Test
    @Transactional
    public void testProjectManagerProjectsList() {
        // Given
        // When
        projectService.getProjectList().add(p3);
        projectService.getProjectList().add(p4);
        projectCollaboratorService.setProjectManager(p1, user1);
        projectCollaboratorService.setProjectManager(p2, user1);
        projectCollaboratorService.setProjectManager(p3, user2);
        projectCollaboratorService.setProjectManager(p4, user1);
        // Then
        List<Project> result = projectService.projectManagerProjectsList(user1);
        List<Project> expected = new ArrayList<>();
        expected.add(p1);
        expected.add(p2);
        expected.add(p4);
        assertEquals(expected, result);
    }

    @Test
    @Transactional
    public void testUpdateProject() {
        // Given
        // When
        projectCollaboratorService.addProjectCollaborator(p3, user1, 3);
        // Then
        boolean condition = projectService.updateProject(p3);
        assertTrue(condition);
    }

    @Test
    @Transactional
    public void testUpdateProjectFalse() {
        // Given
        // When
        Project p8 = new Project("8", "test8");
        // Then
        boolean condition = projectService.updateProject(p8);
        assertFalse(condition);
    }

    @Test
    @Transactional
    public void testListProjectCostCalculationOptionsSuccess() {
        // Given
        String choice3 = "AverageCost";
        String choice1 = "MaximumCost";
        String choice2 = "MinimumCost";

        projectService.addProject("1001", "SETI");
        Project project = projectService.getProjectByID("1001");

        // When
        List<String> expected = project.getCostCalculationOptions();
        expected.add(choice1);
        expected.add(choice2);
        expected.add(choice3);
        project.setCostCalculationOptions(expected);
        projectService.updateProject(project);

        List<String> result = projectService.listProjectCostCalculationOptions(project.getId());

        // Then
        assertThat(expected, containsInAnyOrder(result.toArray()));
    }

    @Test
    @Transactional
    public void testListProjectCostCalculationOptionsOnlyOneOptionWhenCreatingProject() {

        //Given
        projectService.addProject("1001", "SETI");
        Project project = projectService.getProjectByID("1001");

        //Then
        assertTrue(project.getCostCalculationOptions().size() == 1);
    }

    @Test
    @Transactional
    public void testSetProjectCostCalculationMechanismSuccess() {

        //Given
        projectService.addProject("1001", "SETI");
        Project project = projectService.getProjectByID("1001");

        String choice = "MaximumCost";

        //When
        projectService.setProjectCostCalculationMechanism(project.getId(), choice);

        //Then
        assertTrue(choice.equals(project.getCostCalculatorPersistence()));
//		assertTrue(choice.equals(project.getCostCalculator().getClass().getSimpleName()));
    }

    @Test
    @Transactional
    public void testListAllCostCalculationOptions() {

        //Given
        List<String> expected = new ArrayList<>();
        expected.add("AverageCost");
        expected.add("FirstTimePeriodCost");
        expected.add("LastTimePeriodCost");
        expected.add("MinimumTimePeriodCost");
        expected.add("MaximumTimePeriodCost");

        //When
        List<String> result = projectService.listAllCostCalculationOptions();

        //Then
        assertThat(expected, containsInAnyOrder(result.toArray()));
    }

    @Test
    @Transactional
    public void setListOfCostCalculationOptions() {

        //Given
        List<String> expected = new ArrayList<>();
        expected.add("AverageCost");
        expected.add("FirstTimePeriodCost");
        expected.add("LastTimePeriodCost");

        projectService.addProject("1002", "SOLID to STUPID");
        Project project = projectService.getProjectByID("1002");

        //When
        project.setCostCalculationOptions(expected);

        //Then
        assertThat(expected, containsInAnyOrder(project.getCostCalculationOptions().toArray()));
    }

    /**
     * Test method that returns a list of a user's completed tasks by reverse order
     * <p>
     * GIVEN: a user that exists in the database and has one completed task
     * WHEN: get the list of tasks completed
     * THEN: assert that the user has one completed task in the returned list
     */
    @Test
    public void testlistUserCompletedTasksInAllProjectsSortedByReverseOrderToDTO() {

        // GIVEN
        userIdVO = UserIdVO.create("pedro@gmail.com");
        assertTrue(userRepositoryClass.existsById(userIdVO));
        t2.addProjectCollaborator(col1);
        t2.setEffectiveStartDate(d1);
        t2.setTaskCompleted();
        t2.setEffectiveDateOfConclusion(d1.plusMonths(6));
        TaskRestDTO taskRestDTO = t2.toDTO();
        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(taskRestDTO);

        // WHEN
        List<TaskRestDTO> result = projectService.listUserCompletedTasksInAllProjectsSortedByReverseOrderToDTO(user1);

        // THEN
        assertEquals(expected, result);
    }

    /**
     * Test method that returns a list of a project's unassigned tasks
     * <p>
     * GIVEN: a project that exists in the database and that task (t3) and task (t6) have no assigned project collaborators
     * WHEN: get the list of unassigned tasks
     * THEN: assert that the project has two unassigned task in the returned list
     */
    @Test
    public void testlistUnassignedTasks() {

        // GIVEN
        assertNotNull(projectService.getProjectByID(p1.getId()));
        assertFalse(t3.hasAssignedProjectCollaborator());
        assertFalse(t6.hasAssignedProjectCollaborator());

        TaskRestDTO task3RestDTO = t3.toDTO();
        TaskRestDTO task6RestDTO = t6.toDTO();
        List<TaskRestDTO> expected = new ArrayList<>();
        expected.add(task3RestDTO);
        expected.add(task6RestDTO);

        // WHEN
        List<TaskRestDTO> result = projectService.listUnassignedTasksToDTO(p1.getId());

        // THEN
        assertEquals(expected, result);
    }

    /**
     * Test method to set a list of costOptions to  a project
     * <p>
     * GIVEN: a project that exists in the database with costoption available: LastTimePeriodCost
     * WHEN: set cost options : AverageCost, MaximumCost
     * THEN: assert that the project has that two cost Options that was setted, and the outDTO expected is correct.
     */
    @Test
    public void testSetCostOptionsDTOSuccess() {

        // GIVEN
        assertNotNull(projectService.getProjectByID(p1.getId()));
        List<String> costOptions = new ArrayList<>();
        costOptions.add("LastTimePeriodCost");
        assertEquals(projectService.getProjectByID(p1.getId()).getCostCalculationOptions(), costOptions);

        // WHEN
        List<String> newCostOptions = new ArrayList<>();
        newCostOptions.add("AverageCost");
        newCostOptions.add("MaximumCost");
        ProjectCostOptionsDTO expectedDTO = new ProjectCostOptionsDTO();
        expectedDTO.setCostCalculationOptions(newCostOptions);
        expectedDTO.setProjectId(p1.getId());
        ProjectCostOptionsDTO resultDTO = projectService.setListOfCostCalculationOptions(expectedDTO);

        // THEN
        assertEquals(expectedDTO, resultDTO);
        assertEquals(p1.getCostCalculationOptions(), resultDTO.getCostCalculationOptions());
    }

    /**
     * Test method to set a list of costOptions to  a project
     * <p>
     * GIVEN: a project that exists in the database with costoption available: LastTimePeriodCost
     * WHEN: set empty list
     * THEN: assert that the project still got the same cost Options, and the outDTO expected is null.
     */
    @Test
    public void testSetCostOptionsDTOEmptyList() {

        // GIVEN
        assertNotNull(projectService.getProjectByID(p1.getId()));
        List<String> costOptions = new ArrayList<>();
        costOptions.add("LastTimePeriodCost");
        assertEquals(projectService.getProjectByID(p1.getId()).getCostCalculationOptions(), costOptions);

        // WHEN
        List<String> newCostOptions = new ArrayList<>();
        ProjectCostOptionsDTO expectedDTO = new ProjectCostOptionsDTO();
        expectedDTO.setCostCalculationOptions(newCostOptions);
        expectedDTO.setProjectId(p1.getId());
        ProjectCostOptionsDTO resultDTO = projectService.setListOfCostCalculationOptions(expectedDTO);

        // THEN
        assertEquals(null, resultDTO);
        assertEquals(p1.getCostCalculationOptions(), costOptions);
    }

    @Test
    @Transactional
    public void testGetProjectDTOByIdTrue() {
        // Given
        ProjectDTO expected = new ProjectDTO();
        expected.setProjectId("1");
        expected.setName("project1");
        expected.setDescription("<no project description>");
        expected.setUserEmail("asdrubal@gmail.com");
        expected.setUnit("HOURS");
        // Then
        assertEquals(expected, projectService.getProjectDTOByID("1"));

    }

    @Test
    @Transactional
    public void testlistUserAllTasksInAllProjects() {
        TaskRestDTO taskRestDTO = new TaskRestDTO();
        taskRestDTO.setProjectId(p1.getId());
        taskRestDTO.setTaskId(t1.getId());
        taskRestDTO.setDateOfCreation(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        taskRestDTO.setPredictedDateOfConclusion(d1);
        taskRestDTO.setPredictedDateOfStart(d1);
        taskRestDTO.setTitle(t1.getTitle());
        taskRestDTO.setDescription(t1.getDescription());
        taskRestDTO.setEstimatedEffort(1.3);
        taskRestDTO.setUnitCost(1.2);
        taskRestDTO.setState(t1.getState());

        assertEquals("[" + taskRestDTO + "]", projectService.listUserAllTasksInAllProjects(user1).toString());
    }

    @Test
    @Transactional
    public void listAllUserProjects() {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectId(p1.getId());
        projectDTO.setName(p1.getName());
        projectDTO.setDescription(p1.getDescription());
        projectDTO.setUserEmail(projectManager.getEmail());
        projectDTO.setUnit(p1.getUnit().toString());
        projectDTO.setGlobalBudget(0.0);
        projectDTO.setIsActive(true);

        assertEquals("[" + projectDTO + "]", projectService.listAllUserProjects(projectManager).toString());
    }

    /**
     * Test method to set a list of costOptions to  a project
     * <p>
     * GIVEN: A list of strings that represent all classes that implement the CostCalculation interface
     * WHEN: list all available cost options to a DTO
     * THEN: assert that the DTO contains a list with all available cost options
     */
    @Test
    public void testListAllCostCalculationOptionsToDTO() {

        // GIVEN
        assertNotNull(projectService.listAllCostCalculationOptions());
        List<String> resultcostOptions = projectService.listAllCostCalculationOptions();

        // WHEN
        List<String> expected = new ArrayList<>();
        expected.add("AverageCost");
        expected.add("FirstTimePeriodCost");
        expected.add("LastTimePeriodCost");
        expected.add("MaximumTimePeriodCost");
        expected.add("MinimumTimePeriodCost");
        AvailableCostOptionsDTO resultDTO = projectService.listAllCostCalculationOptionsToDTO();

        // THEN
        assertEquals(expected, resultDTO.getAvailableCalculationOptions());
    }

    @Test
    @Transactional
    public void listProjectCostCalculationOptionsToDTOTest() {

        List<String> expected = p1.getCostCalculationOptions();
        expected.add("AverageCost");
        expected.add("FirstTimePeriodCost");
        expected.add("LastTimePeriodCost");
        expected.add("MaximumTimePeriodCost");
        expected.add("MinimumTimePeriodCost");

        p1.setCostCalculationOptions(expected);

        AvailableCostOptionsDTO resultDTO = projectService.listProjectCostCalculationOptionsToDTO(p1.getId());

        assertEquals(expected, resultDTO.getAvailableCalculationOptions());
    }

    @Test
    @Transactional
    public void setProjectCostCalculationMechanismToDTOTest() {

        String choice1 = "AverageCost";
        String choice2 = "LastTimePeriodCost";

        List<String> costOptions = p1.getCostCalculationOptions();
        costOptions.add(choice1);
        costOptions.add(choice2);

        AvailableCostOptionsDTO availableCostOptionsDTO = new AvailableCostOptionsDTO();
        availableCostOptionsDTO.setAvailableCalculationOptions(costOptions);

        assertEquals(choice2, p1.getCostCalculatorPersistence());

        projectService.setProjectCostCalculationMechanismToDTO(p1.getId(), choice1);
        assertEquals(choice1, p1.getCostCalculatorPersistence());

        projectService.setProjectCostCalculationMechanismToDTO(p1.getId(), choice2);
        assertEquals(choice2, p1.getCostCalculatorPersistence());

        AvailableCostOptionsDTO resultDTO = projectService.listProjectCostCalculationOptionsToDTO(p1.getId());
        assertEquals(costOptions, resultDTO.getAvailableCalculationOptions());
    }

    @Test
    @Transactional
    public void setProjectCostCalculationMechanismToDTO2Test() {

        String choice1 = "AverageCost";
        String choice2 = "LastTimePeriodCost";

        List<String> costOptions = p1.getCostCalculationOptions();
        costOptions.add(choice1);
        costOptions.add(choice2);

        AvailableCostOptionsDTO expected = new AvailableCostOptionsDTO();
        expected.setCalculationOption(choice2);
        AvailableCostOptionsDTO result;
        result = projectService.setProjectCostCalculationMechanismToDTO(p1.getId(), choice2);

        assertEquals(expected, result);

    }

    @Test
    @Transactional
    public void testGetProjectTasksCostToDTOSuccess() {
        List<TaskCostRestDTO> expected = new ArrayList<>();
        TaskCostRestDTO taskCostRestDTO1 = new TaskCostRestDTO();
        TaskCostRestDTO taskCostRestDTO2 = new TaskCostRestDTO();
        taskCostRestDTO1.setTitle(t4.getTitle());
        taskCostRestDTO1.setTaskId(t4.getId());
        taskCostRestDTO1.setCost(t4.calculateTotalCostsoFar(p2.getCostCalculator()));
        taskCostRestDTO2.setTitle(t5.getTitle());
        taskCostRestDTO2.setTaskId(t5.getId());
        taskCostRestDTO2.setCost(t5.calculateTotalCostsoFar(p2.getCostCalculator()));
        assertEquals(0.0, taskCostRestDTO2.getCost(), 0.01);
        expected.add(taskCostRestDTO1);
        expected.add(taskCostRestDTO2);

        assertEquals(expected.toString(), projectService.getProjectTasksCostToDTO(p2.getId()).toString());
    }

    @Test
    @Transactional
    public void testGetProjectTasksCostToDTONoProject() {
        List<TaskCostRestDTO> expected = new ArrayList<>();

        assertEquals(expected.toString(), projectService.getProjectTasksCostToDTO("testFail").toString());
    }

    @Test
    @Transactional
    public void testGetProjectTasksCostToDTONoTasksInProject() {
        List<TaskCostRestDTO> expected = new ArrayList<>();

        assertEquals(expected.toString(), projectService.getProjectTasksCostToDTO(p4.getId()).toString());
    }

    @Test
    @Transactional
    public void testListUnassignedProjectCollaborators() {
        List<ProjectCollaborator> expected = new ArrayList<>();

        assertEquals(expected, projectService.listUnassignedProjectCollaborators(p1.getId()));
    }

    @Test
    @Transactional
    public void testListUserTaskCollabRegistryInAllProjectsOrderByLastReportNoReports() {
        t2.addProjectCollaborator(col1);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        expected.add(t1.getLastTaskCollaboratorRegistryOf(col1));
        expected.add(t2.getLastTaskCollaboratorRegistryOf(col1));

        assertEquals(expected, projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(user1));
    }

    @Test
    @Transactional
    public void testListUserTaskCollabRegistryInAllProjectsOrderByLastReportWithReports() {
        t2.addProjectCollaborator(col1);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        col1.getLastCostAndTimePeriod().setStartDate(d1.toLocalDate());
        t1.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t1.addReport(col1, 5, d1.plusDays(1), d1.plusDays(2));
        t1.addReport(col1, 15, d1.plusDays(5), d1.plusDays(7));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(0).setReportCreationDate(LocalDateTime.now().minusDays(5));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(1).setReportCreationDate(LocalDateTime.now().minusDays(4));
        t2.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t2.addReport(col1, 55, d1.plusDays(1), d1.plusDays(2));

        expected.add(t2.getLastTaskCollaboratorRegistryOf(col1));
        expected.add(t1.getLastTaskCollaboratorRegistryOf(col1));

        assertEquals(expected, projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(user1));
    }

    @Test
    @Transactional
    public void testListUserTaskCollabRegistryInAllProjectsOrderByLastReportWithReports2() {
        t2.addProjectCollaborator(col1);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        col1.getLastCostAndTimePeriod().setStartDate(d1.toLocalDate());
        t1.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t1.addReport(col1, 5, d1.plusDays(1), d1.plusDays(2));
        t1.addReport(col1, 15, d1.plusDays(5), d1.plusDays(7));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(0).setReportCreationDate(LocalDateTime.now().minusDays(5));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(1).setReportCreationDate(LocalDateTime.now().minusDays(4));
        t2.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t2.addReport(col1, 55, d1.plusDays(1), d1.plusDays(2));
        t2.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(0).setReportCreationDate(LocalDateTime.now().minusDays(9));
        expected.add(t1.getLastTaskCollaboratorRegistryOf(col1));
        expected.add(t2.getLastTaskCollaboratorRegistryOf(col1));

        assertEquals(expected, projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(user1));
    }

    @Test
    @Transactional
    public void testListUserTaskCollabRegistryInAllProjectsOrderByLastReportEmptyReportsInOneTask() {
        t2.addProjectCollaborator(col1);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        col1.getLastCostAndTimePeriod().setStartDate(d1.toLocalDate());
        t1.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t1.addReport(col1, 5, d1.plusDays(1), d1.plusDays(2));
        t1.addReport(col1, 15, d1.plusDays(5), d1.plusDays(7));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(0).setReportCreationDate(LocalDateTime.now().minusDays(5));
        t1.getLastTaskCollaboratorRegistryOf(col1).getReportList().get(1).setReportCreationDate(LocalDateTime.now().minusDays(4));

        expected.add(t1.getLastTaskCollaboratorRegistryOf(col1));
        expected.add(t2.getLastTaskCollaboratorRegistryOf(col1));

        assertEquals(expected, projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(user1));
    }

    @Test
    @Transactional
    public void testListUserTaskCollabRegistryInAllProjectsOrderByLastReportEmptyReportsInOneTask2() {
        t2.addProjectCollaborator(col1);
        List<TaskCollaboratorRegistry> expected = new ArrayList<>();
        col1.getLastCostAndTimePeriod().setStartDate(d1.toLocalDate());
        t2.getLastTaskCollaboratorRegistryOf(col1).setCollaboratorAddedToTaskDate(d1);
        t2.addReport(col1, 55, d1.plusDays(1), d1.plusDays(2));

        expected.add(t2.getLastTaskCollaboratorRegistryOf(col1));
        expected.add(t1.getLastTaskCollaboratorRegistryOf(col1));

        assertEquals(expected, projectService.listUserTaskCollabRegistryInAllProjectsOrderByLastReport(user1));
    }
}
