package project.model.user.roles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RoleTest {
    Role administrator;
    Role director;
    Role collaborator;
    Role registeredUser;
    Role nullRole;


    @Before
    public void setUp() throws Exception {
        administrator = new Role(RoleName.ROLE_ADMINISTRATOR);
        director = new Role(RoleName.ROLE_DIRECTOR);
        collaborator = new Role(RoleName.ROLE_COLLABORATOR);
        registeredUser = new Role(RoleName.ROLE_REGISTEREDUSER);
        nullRole = new Role();

    }


    @Test
    public void getSetId() {
        //Given
        assertNull(administrator.getId());
        //when
        administrator.setId(1L);
        //then
        long result = administrator.getId();
        assertEquals(1L, result);
    }

    @Test
    public void getSetName() {
        assertTrue(collaborator.isCollaborator());
        collaborator.setName(RoleName.ROLE_DIRECTOR);
        assertTrue(collaborator.isDirector());
    }

    @Test
    public void isRole() {
        assertTrue(collaborator.isCollaborator());
        assertTrue(administrator.isAdministrator());
        assertTrue(registeredUser.isRegisteredUser());
        assertTrue(administrator.isAdministrator());
    }


    @Test
    public void toStringTest() {
        assertEquals("ROLE_COLLABORATOR", collaborator.toString());
    }

    @Test
    public void equalsTest() {
        Role admin2 = new Role(RoleName.ROLE_ADMINISTRATOR);
        Role admin3 = new Role(RoleName.ROLE_COLLABORATOR);

        assertTrue(administrator.equals(admin2));
        assertFalse(administrator.equals(admin3));
        assertTrue(administrator.equals(administrator));
   
        assertNotNull(collaborator);
        assertNotEquals(administrator, administrator.hashCode());
    }

    @Test
    public void hashCodeTest() {
        assertEquals(administrator.hashCode(), administrator.hashCode());
        assertNotEquals(administrator.hashCode(), collaborator.hashCode());
    }
}