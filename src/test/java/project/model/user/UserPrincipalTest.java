package project.model.user;

import org.junit.Before;
import org.junit.Test;
import project.security.UserPrincipal;

import javax.mail.internet.AddressException;
import java.time.LocalDate;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class UserPrincipalTest {

    UserPrincipal userPrincipal;
    UserPrincipal userPrincipal2;
    User user;
    User user2;

    @Before
    public void setUp() throws AddressException {
        LocalDate birth = LocalDate.of(1999, 10, 11);

        user = new User("Pedro", "919999999", "pedro@gmail.com", "332432", birth, "2", "Rua ", "4433", "cidade",
                "país");
        user.setPassword("12345");

        user2 = new User("Alberto", "918888888", "alberto@gmail.com", "323432", birth, "2", "Rua ", "4433", "cidade",
                "país");
        user2.setPassword("54321");

        userPrincipal = UserPrincipal.create(user, null, null, null);
        userPrincipal2 = UserPrincipal.create(user2, null, null, null);
    }

    @Test
    public void create() {
        assertEquals(userPrincipal.getEmail(), user.getEmail());
    }

    @Test
    public void getEmail() {
        assertEquals(userPrincipal.getEmail(), user.getEmail());
    }

    @Test
    public void getPassword() {
        assertEquals(userPrincipal.getPassword(), user.getPassword());
    }

    @Test
    public void getName() {
        assertEquals(userPrincipal.getName(), user.getName());
    }

    @Test
    public void getUsername() {
        assertEquals(userPrincipal.getUsername(), user.getUsername());
    }

    @Test
    public void getAuthorities() {
        assertTrue(userPrincipal.getAuthorities().isEmpty());
    }

    @Test
    public void isAccountNonExpired() {
        assertTrue(userPrincipal.isAccountNonExpired());
    }

    @Test
    public void isAccountNonLocked() {
        assertTrue(userPrincipal.isAccountNonLocked());
    }

    @Test
    public void isCredentialsNonExpired() {
        assertTrue(userPrincipal.isCredentialsNonExpired());
    }

    @Test
    public void isEnabled() {
        assertTrue(userPrincipal.isEnabled());
    }

    @Test
    public void equals() {
        assertTrue(userPrincipal.equals(userPrincipal));
        assertFalse(userPrincipal.equals(userPrincipal2));
        assertFalse(userPrincipal.equals(null));
        assertTrue(userPrincipal.getClass().equals(userPrincipal2.getClass()));
        assertFalse(userPrincipal.equals(user));
    }

    @Test
    public void testHashCode() {
        assertEquals(userPrincipal.hashCode(), userPrincipal.hashCode());
        assertNotEquals(userPrincipal.hashCode(), userPrincipal2.hashCode());
    }
}