package project.model.project;

import jparepositoriestest.ProjectRepositoryClass;
import jparepositoriestest.UserRepositoryClass;
import org.junit.Before;
import org.junit.Test;
import project.dto.rest.ProjectDTO;
import project.dto.rest.SetProjectManagerDTO;
import project.model.user.User;
import project.services.ProjectUserService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectUserServiceTest {

    ProjectUserService projectUserService;

    ProjectRepositoryClass projectRepositoryClass;
    UserRepositoryClass userRepositoryClass;
    ProjectService projectService;

    ProjectDTO projectInDTO;
    ProjectDTO projectOutDTO;
    ProjectDTO projectInDTOGlobalBudgetNaN;
    ProjectDTO projectOutDTOGlobalBudgetNaN;
    SetProjectManagerDTO setProjectManagerDTO;
    User user;
    User userTest;

    @Before
    public void setUp() throws Exception {
        userRepositoryClass = new UserRepositoryClass();
        projectRepositoryClass = new ProjectRepositoryClass();
        projectUserService = new ProjectUserService(projectRepositoryClass, userRepositoryClass);
        projectService = new ProjectService(projectRepositoryClass);

        user = new User("Ana", "123", "ana@switch.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433", "cidade", "país");
        userTest = new User("UserTest", "123", "userTest@switch.com", "2345", LocalDate.of(2018, 5, 31), "2", "Rua ", "4433", "cidade", "país");

        user.setProfileCollaborator();
        user.setActive();
        userRepositoryClass.save(user);

        projectInDTO = new ProjectDTO();
        projectInDTO.setProjectId("Project Created");
        projectInDTO.setName("Project name");
        projectInDTO.setDescription("Project description");
        projectInDTO.setStartDate(LocalDateTime.of(2018, 5, 1, 0, 0));
        projectInDTO.setFinalDate(LocalDateTime.of(2018, 8, 1, 0, 0));
        projectInDTO.setUserEmail("ana@switch.com");
        projectInDTO.setGlobalBudget(90.0);
        projectInDTO.setIsActive(true);
        projectInDTO.setUnit("PEOPLE_MONTH");

        setProjectManagerDTO = new SetProjectManagerDTO();
        setProjectManagerDTO.setEmail(user.getUserIdVO().getUserEmail());
        setProjectManagerDTO.setProjectId(projectInDTO.getProjectId());

    }

    @Test
    @Transactional
    public void createProjectSuccess() {

         /*
        //GIVEN: An projectDTO with correct data to create project.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get created project DTO.
         */

        //GIVEN
        projectInDTO.setProjectId("Project Created");

        //WHEN
        projectOutDTO = projectUserService.createProject(projectInDTO);

        //THEN
        assertEquals("Project Created", projectOutDTO.getProjectId());
        assertEquals(projectInDTO.toString(), projectOutDTO.toString());
    }

    @Test
    @Transactional
    public void createProjectFailUserNotExist() {
        /*
        //GIVEN: An projectDTO with a user invalid.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get null return.
         */

        //GIVEN
        projectInDTO.setUserEmail("test@switch.com");

        //WHEN
        projectOutDTO = projectUserService.createProject(projectInDTO);

        //THEN
        assertNull(projectOutDTO);
    }

    @Test
    @Transactional
    public void createProjectFailWithoutId() {
        /*
        //GIVEN: An projectDTO without projectId.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get null return.
         */

        //GIVEN
        projectInDTO.setProjectId(null);

        //WHEN
        projectOutDTO = projectUserService.createProject(projectInDTO);

        //THEN
        assertNull(projectOutDTO);
    }

    @Test
    @Transactional
    public void testAssignProjectManagerAndAddToProjectCollaboratorListFail() {
         /*
        //GIVEN: An projectDTO with inactive user.
        //WHEN: We post these projectDTO in HTTP client.
        //THEN We get null return.
         */

        //GIVEN
        user.setInactive();
        userRepositoryClass.save(user);

        //WHEN
        projectOutDTO = projectUserService.createProject(projectInDTO);

        //THEN
        assertNull(projectOutDTO);

    }

    /**
     * test a successful call to the setProjectManagerDTOSuccess method
     * <p>
     * GIVEN: a project and a user that exist in mock database
     * WHEN: set the project's project manager
     * THEN: return a DTO with project manager information
     */
    @Test
    @Transactional
    public void testSetProjectManagerDTOSuccess() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        userRepositoryClass.save(user);
        userTest.setProfileCollaborator();
        userRepositoryClass.save(userTest);
        setProjectManagerDTO.setEmail(userTest.getEmail());
        assertEquals(projectService.getProjectByID(setProjectManagerDTO.getProjectId()).listActiveProjectCollaborators().size(),1);
        // WHEN
        SetProjectManagerDTO setProjectManagerDTO1 = projectUserService.setProjectManager(setProjectManagerDTO);

        // THEN
        assertEquals(setProjectManagerDTO, setProjectManagerDTO1);
        assertEquals(projectService.getProjectByID(setProjectManagerDTO.getProjectId()).listActiveProjectCollaborators().size(),2);
    }
   

    /**
     * test a failed call to the setProjectManagerDTOSuccess method
     * <p>
     * GIVEN: a project that exist in mock database and a user that does not exist
     * WHEN: set the project's project manager
     * THEN: return null
     */
    @Test
    @Transactional
    public void testSetProjectManagerDTOFail1() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        setProjectManagerDTO.setEmail("nouser@gmail.com");

        // WHEN
        SetProjectManagerDTO setProjectManagerDTO1 = projectUserService.setProjectManager(setProjectManagerDTO);

        // THEN
        assertNull(setProjectManagerDTO1);

    }

    /**
     * test a failed call to the setProjectManagerDTOSuccess method
     * <p>
     * GIVEN: a project and a user that exist in mock database and user is inactive
     * WHEN: set the project's project manager
     * THEN: return null
     */
    @Test
    @Transactional
    public void testSetProjectManagerDTOFail2() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        user.setInactive();
        userRepositoryClass.save(user);

        // WHEN
        SetProjectManagerDTO setProjectManagerDTO1 = projectUserService.setProjectManager(setProjectManagerDTO);

        // THEN
        assertNull(setProjectManagerDTO1);

    }

    /**
     * test a successful call to the setProjectManagerDTOSuccess method
     * <p>
     * GIVEN: a project and a user that exist in mock database but user is not a project collaborator
     * WHEN: set the project's project manager
     * THEN: return a DTO with project manager information
     */
    @Test
    @Transactional
    public void testSetProjectManagerDTOSuccess2() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        assertTrue(projectRepositoryClass.existsById(projectInDTO.getProjectId()));
        setProjectManagerDTO.setEmail(userTest.getUserIdVO().getUserEmail());
        userRepositoryClass.save(userTest);

        // WHEN
        SetProjectManagerDTO setProjectManagerDTO1 = projectUserService.setProjectManager(setProjectManagerDTO);

        // THEN
        assertEquals(setProjectManagerDTO, setProjectManagerDTO1);

    }

    @Test
    @Transactional
    public void testSetProjectManagerFailNoProject() {
        assertFalse(projectRepositoryClass.existsById(projectInDTO.getProjectId()));
        setProjectManagerDTO.setEmail(userTest.getUserIdVO().getUserEmail());
        userRepositoryClass.save(userTest);
        SetProjectManagerDTO setProjectManagerDTOFail = projectUserService.setProjectManager(setProjectManagerDTO);
        assertNull(setProjectManagerDTOFail);
    }

    /**
     * test a successful call to the listAllProjects method
     * <p>
     * GIVEN: a project that exist in mock database
     * WHEN: list all projects
     * THEN: return a list of projects
     */
    @Test
    @Transactional
    public void testListAllProjectsSuccess() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        assertTrue(projectRepositoryClass.existsById(projectInDTO.getProjectId()));

        // WHEN
        List<ProjectDTO> expected = new ArrayList<>();
        expected.add(projectInDTO);
        List<ProjectDTO> dtoList = projectUserService.listAllProjects();

        // THEN
        assertEquals(projectInDTO, dtoList.get(0));

    }

    /**
     * test a successful call to the getProject method
     * <p>
     * GIVEN: a project that exist in mock database
     * WHEN: get project
     * THEN: return the project
     */
    @Test
    @Transactional
    public void testGetProjectSuccess() {

        // GIVEN
        projectUserService.createProject(projectInDTO);
        assertTrue(projectRepositoryClass.existsById(projectInDTO.getProjectId()));

        // WHEN
        ProjectDTO result = projectUserService.getProject(projectInDTO.getProjectId());

        // THEN
        assertEquals(projectInDTO, result);

    }

    @Test
    @Transactional
    public void createProjectGlobalBudgetNaNTest() {

        projectInDTOGlobalBudgetNaN = new ProjectDTO();
        projectInDTOGlobalBudgetNaN.setProjectId("Project Created");
        projectInDTOGlobalBudgetNaN.setName("Project name");
        projectInDTOGlobalBudgetNaN.setDescription("Project description");
        projectInDTOGlobalBudgetNaN.setStartDate(LocalDateTime.of(2018, 5, 1, 0, 0));
        projectInDTOGlobalBudgetNaN.setFinalDate(LocalDateTime.of(2018, 8, 1, 0, 0));
        projectInDTOGlobalBudgetNaN.setUserEmail("ana@switch.com");
        projectInDTOGlobalBudgetNaN.setGlobalBudget(Math.sqrt(-1));
        projectInDTOGlobalBudgetNaN.setIsActive(true);
        projectInDTOGlobalBudgetNaN.setUnit("PEOPLE_MONTH");

        setProjectManagerDTO = new SetProjectManagerDTO();
        setProjectManagerDTO.setEmail(user.getUserIdVO().getUserEmail());
        setProjectManagerDTO.setProjectId(projectInDTOGlobalBudgetNaN.getProjectId());

        //GIVEN
        projectInDTOGlobalBudgetNaN.setProjectId("Project Id");

        //WHEN
        projectOutDTOGlobalBudgetNaN = projectUserService.createProject(projectInDTOGlobalBudgetNaN);

        //THEN
        assertEquals("Project Id", projectOutDTOGlobalBudgetNaN.getProjectId());
        assertNotEquals(projectInDTOGlobalBudgetNaN.toString(), projectOutDTOGlobalBudgetNaN.toString());
    }
}