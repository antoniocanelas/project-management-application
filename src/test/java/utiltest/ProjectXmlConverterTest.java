package utiltest;

import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import project.dto.ProjectDTO;
import project.util.ProjectConverter;
import project.util.ProjectXmlConverter;

public class ProjectXmlConverterTest {

	ProjectConverter projectXmlConverter;
	
	@Before
	public void setUp() throws Exception {
		
		projectXmlConverter = new ProjectXmlConverter();
	}

	/**
	 * GIVEN a wrong input file name
	 * WHEN we try to parseToProject using that wrong file name
	 * THEN we receive a null DTO.
	 */
	@Test
	public void testParseToProjectFailureWrongInputFile() {
		//Given
		String wrongInputFile = "wrongName";
		
		//When
		ProjectDTO result = projectXmlConverter.parseToProject(wrongInputFile);
		
		//Then
		assertNull(result);
	}

}
