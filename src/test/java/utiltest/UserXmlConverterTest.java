package utiltest;

import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import project.dto.UserRegistryDTO;
import project.util.UserConverter;
import project.util.UserXmlConverter;

public class UserXmlConverterTest {

	UserConverter userXmlConverter;
	
	@Before
	public void setUp() throws Exception {
		
	userXmlConverter = new UserXmlConverter();	
	}

	/**
	 * GIVEN a wrong input file name
	 * WHEN we try to parseToUser using that wrong file name
	 * THEN we receive a null DTO.
	 */
	@Test
	public void testParseToUserFailureWrongInputFile() {
		//Given
		String wrongInputFile = "wrongName";
		
		//When
		UserRegistryDTO result = userXmlConverter.parseToUser(wrongInputFile);
		
		//Then
		assertNull(result);
	}
}
