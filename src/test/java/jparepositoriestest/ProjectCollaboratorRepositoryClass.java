package jparepositoriestest;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import project.jparepositories.ProjectCollaboratorRepository;
import project.model.project.Project;
import project.model.projectcollaborator.ProjectCollaborator;
import project.model.user.User;

public class ProjectCollaboratorRepositoryClass implements ProjectCollaboratorRepository {
    Set<ProjectCollaborator> list = new LinkedHashSet<>();
    //save

    @Override
    public List<ProjectCollaborator> findAll() {
        return new ArrayList<ProjectCollaborator>(list);
    }

    @Override
    public List<ProjectCollaborator> findAll(Sort sort) {
        //Auto-generated method stub
        return new ArrayList<>();

    }

    @Override
    public List<ProjectCollaborator> findAllById(Iterable<Integer> ids) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public <S extends ProjectCollaborator> List<S> saveAll(Iterable<S> entities) {
        //Auto-generated method stub
        return new ArrayList<>();

    }

    @Override
    public void flush() {
        //Auto-generated method stub

    }

    @Override
    public <S extends ProjectCollaborator> S saveAndFlush(S entity) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<ProjectCollaborator> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAllInBatch() {
        //Auto-generated method stub

    }

    @Override
    public ProjectCollaborator getOne(Integer id) {
        for (ProjectCollaborator pc : list) {
            if (pc.getId() == (id)) {
                return pc;
            }
        }
        return null;
    }

    @Override
    public <S extends ProjectCollaborator> List<S> findAll(Example<S> example) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public <S extends ProjectCollaborator> List<S> findAll(Example<S> example, Sort sort) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public Page<ProjectCollaborator> findAll(Pageable pageable) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public <S extends ProjectCollaborator> S save(S entity) {
    	if (existsById(entity.getId()))
    	{
    		list.remove(entity);
    		list.add(entity);
    		return entity;
    	}
    	entity.setId(ProjectCollaborator.getAndIncrementId());
        list.add(entity);
        return entity;
    }

    @Override
    public Optional<ProjectCollaborator> findById(Integer id) {
        //Auto-generated method stub
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer id) {

        for (ProjectCollaborator pc : list) {
            if (pc.getId() == (id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long count() {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public void deleteById(Integer id) {
        //Auto-generated method stub

    }

    @Override
    public void delete(ProjectCollaborator projectCollaborator) {
        for (ProjectCollaborator pc : list) {
            if (pc.equals(projectCollaborator)) {
                list.remove(pc);
            }
        }

    }

    @Override
    public void deleteAll(Iterable<? extends ProjectCollaborator> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAll() {
        //Auto-generated method stub

    }

    @Override
    public <S extends ProjectCollaborator> Optional<S> findOne(Example<S> example) {
        //Auto-generated method stub
        return Optional.empty();
    }

    @Override
    public <S extends ProjectCollaborator> Page<S> findAll(Example<S> example, Pageable pageable) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public <S extends ProjectCollaborator> long count(Example<S> example) {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public <S extends ProjectCollaborator> boolean exists(Example<S> example) {
        //Auto-generated method stub
        return false;
    }

    @Override
    public List<ProjectCollaborator> findByProject(Project project) {
        for (ProjectCollaborator pc : list) {
            if (pc.getProject().equals(project)) {
                list.add(pc);
            }
        }
        return new ArrayList<>(list);
    }

    @Override
    public ProjectCollaborator getOneById(Integer id) {
        for (ProjectCollaborator pc : list) {
            if (pc.getId() == id) {
                return pc;
            }
        }
        return null;
    }
}
