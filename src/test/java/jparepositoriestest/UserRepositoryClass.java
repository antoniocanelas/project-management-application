package jparepositoriestest;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import project.jparepositories.UserRepository;
import project.model.user.User;
import project.model.user.UserIdVO;

public class UserRepositoryClass implements UserRepository {
    Set<User> list = new LinkedHashSet<>();

    @Override
    public List<User> findAll() {
        return new ArrayList<User>(list);
    }

    @Override
    public List<User> findAll(Sort sort) {
        return new ArrayList<User>(list);
    }

    @Override
    public List<User> findAllById(Iterable<UserIdVO> ids) {

        return new ArrayList<>();
    }

    @Override
    public <S extends User> List<S> saveAll(Iterable<S> entities) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public void flush() {
        //Auto-generated method stub

    }

    @Override
    public <S extends User> S saveAndFlush(S entity) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<User> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAllInBatch() {
        //Auto-generated method stub

    }

    @Override
    public User getOne(UserIdVO userIdVO) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public User getOneByUserIdVO(UserIdVO id) {
        for (User userA : list) {
            if (userA.getUserIdVO().equals(id)) {
                return userA;
            }
        }
        return null;
    }

    @Override
    public boolean existsByUserIdVO(UserIdVO userIdVO) {
        return false;
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
        //Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public <S extends User> S save(S entity) {
        list.add(entity);
        return entity;

    }

    @Override
    public Optional<User> findById(UserIdVO id) {

        for (User userA : list) {
            if (userA.getUserIdVO().equals(id)) {
                return Optional.of(userA);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(UserIdVO id) {
        for (User userA : list) {
            if (userA.getUserIdVO().getUserEmail().equals(id.getUserEmail())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long count() {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public void deleteById(UserIdVO id) {
        //Auto-generated method stub

    }

    @Override
    public void delete(User entity) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAll(Iterable<? extends User> entities) {
        //Auto-generated method stub

    }

    @Override
    public void deleteAll() {
        list.clear();

    }

    @Override
    public <S extends User> Optional<S> findOne(Example<S> example) {
        //Auto-generated method stub
        return Optional.empty();    }

    @Override
    public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
        //Auto-generated method stub
        return null;
    }

    @Override
    public <S extends User> long count(Example<S> example) {
        //Auto-generated method stub
        return 0;
    }

    @Override
    public <S extends User> boolean exists(Example<S> example) {
        //Auto-generated method stub
        return false;
    }

}
