import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  cancelAssignmentRequestCollab,
  cancelRemovalRequestCollab,
  loadAssignmentRequests,
  loadCompletedRequests,
  loadRemovalRequests
} from "../../actions/projectActions";
import {
  addTaskCollaborators,
  markCompleted,
  removeCollaboratorFromTask,
  cancelRequestTaskCompletedCollab
} from "../../actions/actions_task";
import { Avatar, Icon, List, message, Modal, Alert } from "antd";
import ContentHeader from "../../templates/contentHeader/index";
import "./ManageRequests.css";
import { getAvatarColor } from "../../util/Colors";
import { loadCollabRequests } from "../../actions/actions_task_array";

class ListRequests extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      collabRequests: [],
      removalRequests: [],
      ModalText: "",
      visible: false,
      confirmLoading: false
    };
  }

  componentWillMount() {
    const { loadCollabRequests } = this.props;

    loadCollabRequests(this.props.currentUser.email);
  }

  showModal = text => {
    this.setState({
      ModalText: (
        <p>
          <strong>Action:</strong>{" "}
          {text.isApproved ? <span>ACCEPTING REQUEST</span> : <span>DECLINE REQUEST</span>} <br />{" "}
          <strong>Type:</strong> {text.type.split("REQUEST")}
          <br />
          <strong>Collaborator:</strong> {text.name}
          <small>({text.email})</small>
        </p>
      ),
      CollabEmail: text.email,
      CollabName: text.name,
      taskId: text.taskId,
      id: text.id,
      isApproved: text.isApproved,
      ProcessingRequest: text.type,
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: `Processing..`,
      confirmLoading: true
    });

    const {
      cancelAssignmentRequestCollab,
      cancelRemovalRequestCollab,
      cancelRequestTaskCompletedCollab
    } = this.props;

    this.state.ProcessingRequest === "ASSIGNMENTREQUEST" && !this.state.isApproved
      ? cancelAssignmentRequestCollab(
          this.props.project.projectId,
          this.state.taskId,
          this.state.CollabEmail,
          this.state.id,
          this.props.currentUser.email
        )
      : this.state.ProcessingRequest === "REMOVALREQUEST" && !this.state.isApproved
        ? cancelRemovalRequestCollab(
            this.props.project.projectId,
            this.state.taskId,
            this.state.CollabEmail,
            this.state.id,
            this.props.currentUser.email
          )
        : cancelRequestTaskCompletedCollab(
            this.state.taskId,
            this.props.project.projectId,
            this.props.currentUser.email
          );

    setTimeout(() => {
      //   const {loadAssignmentRequests} = this.props
      // const {loadRemovalRequests} = this.props
      //   loadAssignmentRequests(this.props.project.projectId)
      // loadRemovalRequests(this.props.project.projectId)
      this.setState({
        visible: false,
        confirmLoading: false
      });
      this.success();
    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  success = () => {
      message.info(`Request Removed! ${this.state.ProcessingRequest}`);
  };

  render() {
    const { visible, confirmLoading, ModalText } = this.state;
    const data = this.props.collabRequests;
    if (this.props.collabRequests.length === 0) {
      return (
        <div>
          <ContentHeader title="REQUESTS" />
          <Alert message="You don't have any requests" type="info" showIcon />
        </div>
      );
    } else {
      return (
        <div>
          <ContentHeader title="REQUESTS" />
          <List
            itemLayout="horizontal"
            dataSource={data}
            renderItem={item => (
              <List.Item
                actions={[
                  <button
                    onClick={() =>
                      this.showModal({
                        taskId: item.taskId,
                        email: item.projectCollaboratorEmail,
                        name: item.projectCollaboratorName,
                        id: item.projectCollaboratorId,
                        type: item.registryStatus,
                        isApproved: false
                      })
                    }
                  >
                    <Icon className="icon-cancel" type="close-circle" />
                  </button>
                ]}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      className="user-avatar-circle2"
                      style={{ backgroundColor: getAvatarColor(this.props.currentUser.name) }}
                    >
                      {this.props.currentUser.name[0].toUpperCase()}
                    </Avatar>
                  }
                  title={
                    <strong>
                      {this.props.currentUser.name} (
                      <small>{item.projectCollaboratorEmail}</small>
                      )
                    </strong>
                  }
                  description={`Task ID: ${item.taskId}`}
                />
                <div className="request-type">{item.registryStatus}</div>
              </List.Item>
            )}
          />
          <Modal
            title="Do you want to confirm this operation?"
            visible={visible}
            onOk={this.handleOk}
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
          >
            <div>{ModalText}</div>
          </Modal>
        </div>
      );
    }
  }
}
const mapStateToProps = state => ({
  collabRequests: state.tasks.collabRequests,
  currentUser: state.authentication.currentUser,
  project: state.projects.selectedProject,
  assignmentRequests: state.projects.assignmentRequests,
  removalRequests: state.projects.removalRequests,
  completeTaskRequests: state.projects.completeTaskRequests
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadCollabRequests,

      loadAssignmentRequests,
      loadRemovalRequests,
      addTaskCollaborators,
      loadCompletedRequests,
      removeCollaboratorFromTask,
      cancelAssignmentRequestCollab,
      cancelRemovalRequestCollab,
      markCompleted,
      cancelRequestTaskCompletedCollab
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListRequests);
