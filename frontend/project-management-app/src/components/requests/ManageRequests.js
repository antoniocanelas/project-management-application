import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  cancelAssignmentRequest,
  cancelRemovalRequest,
  loadAssignmentRequests,
  loadCompletedRequests,
  loadRemovalRequests
} from "../../actions/projectActions";
import {
  addTaskCollaborators,
  markCompleted,
  removeCollaboratorFromTask,
  cancelRequestTaskCompleted
} from "../../actions/actions_task";
import { Avatar, Icon, List, message, Modal, Alert } from "antd";
import "./ManageRequests.css";
import { getAvatarColor } from "../../util/Colors";

class ManageRequests extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      assignmentRequests: [],
      removalRequests: [],
      completeRequests: [],
      ModalText: "",
      visible: false,
      confirmLoading: false
    };
  }

  componentWillMount() {
    const { loadAssignmentRequests } = this.props;
    const { loadRemovalRequests, loadCompletedRequests } = this.props;
    loadAssignmentRequests(this.props.project.projectId);
    loadRemovalRequests(this.props.project.projectId);
    loadCompletedRequests(this.props.project.projectId);
  }

  showModal = text => {
    this.setState({
      ModalText: (
        <p>
          <strong>Action:</strong>{" "}
          {text.isApproved ? <span>ACCEPTING REQUEST</span> : <span>DECLINE REQUEST</span>} <br />{" "}
          <strong>Type:</strong> {text.type.split("REQUEST")}
          <br />
          <strong>Collaborator:</strong> {text.name}
          <small>({text.email})</small>
        </p>
      ),
      CollabEmail: text.email,
      CollabName: text.name,
      taskId: text.taskId,
      id: text.id,
      isApproved: text.isApproved,
      ProcessingRequest: text.type,
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: `Processing..`,
      confirmLoading: true
    });

    const {
      addTaskCollaborators,
      removeCollaboratorFromTask,
      cancelAssignmentRequest,
      cancelRemovalRequest,
      markCompleted,
      cancelRequestTaskCompleted
    } = this.props;

    this.state.ProcessingRequest === "ASSIGNMENTREQUEST" && this.state.isApproved
      ? addTaskCollaborators(
          this.state.taskId,
          this.state.CollabEmail,
          this.props.project.projectId,
          this.props.currentUser.email
        )
      : this.state.ProcessingRequest === "REMOVALREQUEST" && this.state.isApproved
        ? removeCollaboratorFromTask(
            this.state.taskId,
            this.state.CollabEmail,
            this.props.project.projectId,
            this.props.currentUser.email
          )
        : this.state.ProcessingRequest === "COMPLETEDREQUEST" && this.state.isApproved
          ? markCompleted(
              this.state.taskId,
              this.props.project.projectId,
              this.props.currentUser.email,
              this.props.currentUser.email
            )
          : this.state.ProcessingRequest === "ASSIGNMENTREQUEST" && !this.state.isApproved
            ? cancelAssignmentRequest(
                this.props.project.projectId,
                this.state.taskId,
                this.state.CollabEmail,
                this.state.id,
                this.props.currentUser.email
              )
            : this.state.ProcessingRequest === "REMOVALREQUEST" && !this.state.isApproved
              ? cancelRemovalRequest(
                  this.props.project.projectId,
                  this.state.taskId,
                  this.state.CollabEmail,
                  this.state.id,
                  this.props.currentUser.email
                )
              : cancelRequestTaskCompleted(this.state.taskId, this.props.project.projectId, this.props.currentUser.email);

    setTimeout(() => {
      //   const {loadAssignmentRequests} = this.props
      // const {loadRemovalRequests} = this.props
      //   loadAssignmentRequests(this.props.project.projectId)
      // loadRemovalRequests(this.props.project.projectId)
      this.setState({
        visible: false,
        confirmLoading: false
      });
      this.success();
    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  success = () => {
    message.success(`Request Accepted! ${this.state.ProcessingRequest}`);
  };

  render() {
    const { visible, confirmLoading, ModalText } = this.state;
    const data = this.props.assignmentRequests
      .concat(this.props.removalRequests)
      .concat(this.props.completeTaskRequests);

    return (data.length === 0 ? <div className="content"> <Alert message="You have no requests to manage" type="info" showIcon /></div> :
      <div className="content">
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item
              actions={[
                <button
                  onClick={() =>
                    this.showModal({
                      taskId: item.taskId,
                      email: item.projectCollaboratorEmail,
                      name: item.projectCollaboratorName,
                      id: item.projectCollaboratorId,
                      type: item.registryStatus,
                      isApproved: true
                    })
                  }
                >
                  <Icon className="icon-accept" type="check-circle" />
                </button>,
                <button
                  onClick={() =>
                    this.showModal({
                      taskId: item.taskId,
                      email: item.projectCollaboratorEmail,
                      name: item.projectCollaboratorName,
                      id: item.projectCollaboratorId,
                      type: item.registryStatus,
                      isApproved: false
                    })
                  }
                >
                  <Icon className="icon-cancel" type="close-circle" />
                </button>
              ]}
            >
              <List.Item.Meta
                avatar={
                  <Avatar
                    className="user-avatar-circle2"
                    style={{ backgroundColor: getAvatarColor(item.projectCollaboratorName) }}
                  >
                    {item.projectCollaboratorName[0].toUpperCase()}
                  </Avatar>
                }
                title={
                  <strong>
                    {item.projectCollaboratorName} (
                    <small>{item.projectCollaboratorEmail}</small>
                    )
                  </strong>
                }
                description={`Task ID: ${item.taskId}`}
              />
              <div className="request-type">{item.registryStatus}</div>
            </List.Item>
          )}
        />
        <Modal
          title="Do you want to confirm this operation?"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <div>{ModalText}</div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  project: state.projects.selectedProject,
  assignmentRequests: state.projects.assignmentRequests,
  removalRequests: state.projects.removalRequests,
  completeTaskRequests: state.projects.completeTaskRequests,
  currentUser: state.authentication.currentUser
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadAssignmentRequests,
      loadRemovalRequests,
      addTaskCollaborators,
      loadCompletedRequests,
      removeCollaboratorFromTask,
      cancelAssignmentRequest,
      cancelRemovalRequest,
      markCompleted,
      cancelRequestTaskCompleted
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageRequests);
