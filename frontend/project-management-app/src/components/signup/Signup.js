import React, { Component } from "react";
import { checkEmailAvailability, checkUsernameAvailability, signup } from "../../util/APIUtils";
import "./Signup.css";
import { Link } from "react-router-dom";
import LoadingIndicator from '../../common/LoadingIndicator'
import {
  EMAIL_MAX_LENGTH,
  NAME_MAX_LENGTH,
  NAME_MIN_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH
} from "../../constants";

import { BackTop, Button, Checkbox, Form, Icon, Input, Modal, notification } from "antd";

const FormItem = Form.Item;

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: { value: "" },
      email: { value: "" },
      password: { value: "" },
      phone: { value: "" },
      taxPayerId: { value: "" },
      birthDate: { value: "" },
      addressId: { value: "" },
      street: { value: "" },
      postalCode: { value: "" },
      city: { value: "" },
      country: { value: "" },
      acceptConditions: { value: false },
      isLoading: false
    };

    notification.config({
      placement: "topRight",
      top: 70,
      duration: 5
    });

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateUsernameAvailability = this.validateUsernameAvailability.bind(this);
    this.validateEmailAvailability = this.validateEmailAvailability.bind(this);
    this.isFormInvalid = this.isFormInvalid.bind(this);
    this.onChangeCheckBox = this.onChangeCheckBox.bind(this);
  }

  handleInputChange(event, validationFun) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: {
        value: inputValue,
        ...validationFun(inputValue)
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const signupRequest = {
      name: this.state.name.value,
      email: this.state.email.value,
      password: this.state.password.value,
      phone: this.state.phone.value,
      taxPayerId: this.state.taxPayerId.value,
      birthDate: this.state.birthDate.value,
      addressId: this.state.addressId.value,
      street: this.state.street.value,
      postalCode: this.state.postalCode.value,
      city: this.state.city.value,
      country: this.state.country.value,
      acceptConditions: this.state.acceptConditions.value
    };
    this.setState({isLoading:true})

    signup(signupRequest)
      .then(response => {
        this.setState({isLoading:false})
        notification.success({
          message: "PM App",
          description:
            "Thank you! You're successfully registered. Please check your email and login to verify your account!"
        });
        this.props.history.push("/login");
      })
      .catch(error => {
        notification.error({
          message: "PM App",
          description: error.message || "Sorry! Something went wrong. Please try again!"
        });
      });
  }

  info() {
    Modal.info({
      title: "Terms and Conditions",
      content: (
        <div className="signupDiv">
          <p>Welcome to Project Manager App.</p>
          <p>
            {" "}
            These Terms of Use (“Terms”, “Agreement”) govern your use of our web site (“Site”) and
            its contents.
          </p>
          <p> You should carefully read these Terms of Use.</p>
          <p>
            By acception these terms, or by using this site, your agree to be legally bound to these
            terms of use, our privacy policy, and our copyrighy policy. You agree to receive
            required notices and to transact with us electronically.
          </p>
          <p>
            If you do not agree to any of these terms or statements, do accept these terms and do
            not use this site.
          </p>
        </div>
      ),
      onOk() {}
    });
  }

  isFormInvalid() {
    return !(
      this.state.name.validateStatus === "success" &&
      this.state.email.validateStatus === "success" &&
      this.state.password.validateStatus === "success" &&
      this.state.birthDate.validateStatus === "success" &&
      !this.state.acceptConditions
    );
  }

  function;

  onChangeCheckBox(e) {
    console.log(`acceptConditions = ${e.target.checked}`);
    this.setState({ acceptConditions: !this.state.acceptConditions });
  }

  render() {
     
    return (
      this.state.isLoading ?<div className="loading-indicator"><LoadingIndicator tip="Validating data ..."/></div> :
      <div className="signup-container">
        <h1 className="page-title">Sign Up</h1>
        <div className="signup-content">
          <Form onSubmit={this.handleSubmit} className="signup-form">
            <FormItem
              label="Full Name"
              hasFeedback
              validateStatus={this.state.name.validateStatus}
              help={this.state.name.errorMsg}
            >
              <Input
                prefix={<Icon type="user" />}
                size="large"
                name="name"
                autoComplete="off"
                placeholder="Your full name"
                value={this.state.name.value}
                onChange={event => this.handleInputChange(event, this.validateName)}
              />
            </FormItem>
            <FormItem
              label="Email"
              hasFeedback
              validateStatus={this.state.email.validateStatus}
              help={this.state.email.errorMsg}
            >
              <Input
                prefix={<Icon type="mail" />}
                size="large"
                name="email"
                type="email"
                autoComplete="off"
                placeholder="Your email"
                value={this.state.email.value}
                onBlur={this.validateEmailAvailability}
                onChange={event => this.handleInputChange(event, this.validateEmail)}
              />
            </FormItem>
            <FormItem
              label="Password"
              hasFeedback
              validateStatus={this.state.password.validateStatus}
              help={this.state.password.errorMsg}
            >
              <Input
                prefix={<Icon type="safety" />}
                size="large"
                name="password"
                type="password"
                autoComplete="off"
                placeholder="A password between 6 to 20 characters"
                value={this.state.password.value}
                onChange={event => this.handleInputChange(event, this.validatePassword)}
              />
            </FormItem>
            <FormItem
              label="Phone"
              validateStatus={this.state.phone.validateStatus}
              help={this.state.phone.errorMsg}
            >
              <Input
                prefix={<Icon type="phone" />}
                size="large"
                name="phone"
                autoComplete="off"
                placeholder="Enter your phone"
                value={this.state.phone.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>

            <FormItem
              label="Tax Payer ID"
              validateStatus={this.state.taxPayerId.validateStatus}
              help={this.state.taxPayerId.errorMsg}
            >
              <Input
                prefix={<Icon type="credit-card" />}
                size="large"
                name="taxPayerId"
                autoComplete="off"
                placeholder="Enter your tax payer ID"
                value={this.state.taxPayerId.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>
            <FormItem
              label="Birth Date"
              hasFeedback
              validateStatus={this.state.birthDate.validateStatus}
              help={this.state.birthDate.errorMsg}
            >
              <Input
                prefix={<Icon type="gift" />}
                size="large"
                name="birthDate"
                type="date"
                autoComplete="off"
                placeholder="Enter your birth date"
                value={this.state.birthDate.value}
                onChange={event => this.handleInputChange(event, this.validateDate)}
              />
            </FormItem>

            <FormItem
              label="Address ID"
              validateStatus={this.state.addressId.validateStatus}
              help={this.state.addressId.errorMsg}
            >
              <Input
                prefix={<Icon type="info-circle-o" />}
                size="large"
                name="addressId"
                autoComplete="off"
                placeholder="Enter your address ID"
                value={this.state.addressId.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>

            <FormItem
              label="Street"
              validateStatus={this.state.street.validateStatus}
              help={this.state.street.errorMsg}
            >
              <Input
                prefix={<Icon type="home" />}
                size="large"
                name="street"
                autoComplete="off"
                placeholder="Enter your street name"
                value={this.state.street.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>

            <FormItem
              label="Postal Code"
              validateStatus={this.state.postalCode.validateStatus}
              help={this.state.postalCode.errorMsg}
            >
              <Input
                prefix={<Icon type="star-o" />}
                size="large"
                name="postalCode"
                autoComplete="off"
                placeholder="Enter your postal code"
                value={this.state.postalCode.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>

            <FormItem
              label="City"
              validateStatus={this.state.city.validateStatus}
              help={this.state.city.errorMsg}
            >
              <Input
                prefix={<Icon type="pushpin-o" />}
                size="large"
                name="city"
                autoComplete="off"
                placeholder="Enter your city"
                value={this.state.city.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>

            <FormItem
              label="Country"
              validateStatus={this.state.country.validateStatus}
              help={this.state.country.errorMsg}
            >
              <Input
                prefix={<Icon type="global" />}
                size="large"
                name="country"
                autoComplete="off"
                placeholder="Enter your country"
                value={this.state.country.value}
                onChange={event => this.handleInputChange(event, this.validateData)}
              />
            </FormItem>
            <FormItem>
              <Button
                type="primary"
                htmlType="submit"
                size="large"
                className="signup-form-button"
                disabled={this.isFormInvalid()}
              >
                Sign up
              </Button>
              Already registed? <Link to="/login">Login now!</Link>
            </FormItem>
            <FormItem>
              <Checkbox
                name="conditionsCheckBox"
                acceptConditions={this.state.acceptConditions}
                onChange={e => this.onChangeCheckBox(e)}
              >
                I agree with the
              </Checkbox>
              <Button onClick={this.info} type="primary" ghost>
                Terms and Conditions
              </Button>
            </FormItem>
          </Form>
        </div>
        <BackTop>
          <div className="ant-back-top-inner">UP</div>
        </BackTop>
      </div>
    );
  }

  // Validation Functions

  validateName = name => {
    if (name.length < NAME_MIN_LENGTH) {
      return {
        validateStatus: "error",
        errorMsg: `Name is too short (Minimum ${NAME_MIN_LENGTH} characters needed.)`
      };
    } else if (name.length > NAME_MAX_LENGTH) {
      return {
        validationStatus: "error",
        errorMsg: `Name is too long (Maximum ${NAME_MAX_LENGTH} characters allowed.)`
      };
    } else {
      return {
        validateStatus: "success",
        errorMsg: null
      };
    }
  };

  validateDate = date => {
    const today = new Date();
    today.getDate;
    const todaysFormatted = this.formatDate(today);
    if (date === "") {
      return {
        validateStatus: "error",
        errorMsg: "Birth Date may not be empty!"
      };
    } else if (date > todaysFormatted) {
      return {
        validateStatus: "error",
        errorMsg: "Birth Date should be valid"
      };
    } else
      return {
        validateStatus: "success",
        errorMsg: null
      };
  };

  formatDate(date) {
    const d = new Date(date);
    let month = "" + (d.getMonth() + 1),
      day = "" + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  validateEmail = email => {
    if (!email) {
      return {
        validateStatus: "error",
        errorMsg: "Email may not be empty"
      };
    }

    const EMAIL_REGEX = RegExp("[^@ ]+@[^@ ]+\\.[^@ ]+");
    if (!EMAIL_REGEX.test(email)) {
      return {
        validateStatus: "error",
        errorMsg: "Email not valid"
      };
    }

    if (email.length > EMAIL_MAX_LENGTH) {
      return {
        validateStatus: "error",
        errorMsg: `Email is too long (Maximum ${EMAIL_MAX_LENGTH} characters allowed)`
      };
    }

    return {
      validateStatus: null,
      errorMsg: null
    };
  };

  validateUsername = username => {
    if (username.length < USERNAME_MIN_LENGTH) {
      return {
        validateStatus: "error",
        errorMsg: `Username is too short (Minimum ${USERNAME_MIN_LENGTH} characters needed.)`
      };
    } else if (username.length > USERNAME_MAX_LENGTH) {
      return {
        validationStatus: "error",
        errorMsg: `Username is too long (Maximum ${USERNAME_MAX_LENGTH} characters allowed.)`
      };
    } else {
      return {
        validateStatus: null,
        errorMsg: null
      };
    }
  };

  validateData = data => {
    return {
      validateStatus: null,
      errorMsg: null
    };
  };

  validateUsernameAvailability() {
    // First check for client side errors in username
    const usernameValue = this.state.username.value;
    const usernameValidation = this.validateUsername(usernameValue);

    if (usernameValidation.validateStatus === "error") {
      this.setState({
        username: {
          value: usernameValue,
          ...usernameValidation
        }
      });
      return;
    }

    this.setState({
      username: {
        value: usernameValue,
        validateStatus: "validating",
        errorMsg: null
      }
    });

    checkUsernameAvailability(usernameValue)
      .then(response => {
        if (response.available) {
          this.setState({
            username: {
              value: usernameValue,
              validateStatus: "success",
              errorMsg: null
            }
          });
        } else {
          this.setState({
            username: {
              value: usernameValue,
              validateStatus: "error",
              errorMsg: "This username is already taken"
            }
          });
        }
      })
      .catch(error => {
        // Marking validateStatus as success, Form will be recchecked at server
        this.setState({
          username: {
            value: usernameValue,
            validateStatus: "success",
            errorMsg: null
          }
        });
      });
  }

  validateEmailAvailability() {
    // First check for client side errors in email
    const emailValue = this.state.email.value;
    const emailValidation = this.validateEmail(emailValue);

    if (emailValidation.validateStatus === "error") {
      this.setState({
        email: {
          value: emailValue,
          ...emailValidation
        }
      });
      return;
    }

    this.setState({
      email: {
        value: emailValue,
        validateStatus: "validating",
        errorMsg: null
      }
    });

    checkEmailAvailability(emailValue)
      .then(response => {
        if (response.available) {
          this.setState({
            email: {
              value: emailValue,
              validateStatus: "success",
              errorMsg: null
            }
          });
        } else {
          this.setState({
            email: {
              value: emailValue,
              validateStatus: "error",
              errorMsg: "This email or username is already registered"
            }
          });
        }
      })
      .catch(error => {
        // Marking validateStatus as success, Form will be recchecked at server
        this.setState({
          email: {
            value: emailValue,
            validateStatus: "success",
            errorMsg: null
          }
        });
      });
  }

  validatePassword = password => {
    if (password.length < PASSWORD_MIN_LENGTH) {
      return {
        validateStatus: "error",
        errorMsg: `Password is too short (Minimum ${PASSWORD_MIN_LENGTH} characters needed.)`
      };
    } else if (password.length > PASSWORD_MAX_LENGTH) {
      return {
        validationStatus: "error",
        errorMsg: `Password is too long (Maximum ${PASSWORD_MAX_LENGTH} characters allowed.)`
      };
    } else {
      return {
        validateStatus: "success",
        errorMsg: null
      };
    }
  };
}

export default Signup;
