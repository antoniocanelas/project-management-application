import { Icon, Layout, Menu } from "antd";
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

const { SubMenu } = Menu;
const { Sider } = Layout;

class SideBar extends Component {
  render() {
    return (
      <Sider className="sideBar" id="side-bar" width={200}>
        <Menu
          mode="inline"
          defaultSelectedKeys={[this.props.location.pathname]}
          defaultOpenKeys={["sub0"]}
          style={{ height: "100%" }}
        >
          <Menu.Item key="/">
            <Link to="/">
              <Icon type="dashboard" />Dashboard
            </Link>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="folder" />My Projects
              </span>
            }
          >
            <Menu.Item key="/projects/collaborator">
              <Link to="/projects/collaborator">As Collaborator</Link>
            </Menu.Item>
            <Menu.Item key="/projects/projectmanager">
              <Link to="/projects/projectmanager">As Project Manager</Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="profile" /> My Tasks
              </span>
            }
          >
            <Menu.Item key="/tasks/lists">
              <Link to="/tasks/lists">View All</Link>
            </Menu.Item>
            <Menu.Item key="/tasks/requests">
              <Link to="/tasks/requests"> My Requests</Link>
            </Menu.Item>
          </SubMenu>
          <Menu.Item key="/reports/all">
            <Link to="/reports/all">
              <Icon type="solution" />My Reports
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default withRouter(SideBar);
