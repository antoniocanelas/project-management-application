import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loadUser } from "../../actions/loginActions";
import "./Login.css";
import { withRouter } from "react-router-dom";
import { Form } from "antd";
import LoginForm from "./LoginForm";

class Login extends Component {
  constructor(props) {
    super(props);

    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin() {
    this.props.history.push("/");
  }

  render() {
    const AntWrappedLoginForm = Form.create()(LoginForm);
    return (
      <div className="login-container">
        <h1 className="page-title">Login</h1>
        <div className="login-content">
          <AntWrappedLoginForm onLogin={this.handleLogin} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.authentication.isAuthenticated,
  needActivation: state.authentication.needActivation,
  currentUser: state.authentication.currentUser,
  isLoading: state.authentication.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators({ loadUser }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
