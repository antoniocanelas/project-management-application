import React, {Component} from 'react';
import './AppFooter.css';
import switchIcon from '../../switchImg.svg';
import {Layout} from 'antd';
import {Link} from "react-router-dom";

const Footer = Layout.Footer;

class AppFooter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Footer className="app-footer">
                <div className="imageFooter">
                    <img src={switchIcon} alt="switchIcon"/>
                </div>
                <div className="text">Created by <Link to="/team">Group 1</Link> - SWitCH Program 2018</div>
            </Footer>
        );
    }
}


export default AppFooter;