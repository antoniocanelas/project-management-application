import React, { Fragment, Component } from "react";
import { Tag, Icon, Card, Col, Row, Tooltip, Progress } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./Dashboard.css";
import ContentHeader from "../../templates/contentHeader";
import {
  getProjectById,
  getProjectsAsPManager,
  getProjects,
  cancelAssignmentRequest,
  cancelRemovalRequest,
  loadAssignmentRequests,
  loadCompletedRequests,
  loadRemovalRequests
} from "../../actions/projectActions";
import {
  loadedCollaboratorPendingTasks,
  loadUserRecentTasksActivity
} from "../../actions/actions_task_array";
import { collaboratorEffortInCompletedTasksLastMonth } from "../../actions/actions_report_array";
import moment from "moment";
import { colorChangeState } from "../../util/Helpers";
import TaskProgress from "../../mainapp/task/taskprogress/TaskProgress";
import { withRouter } from "react-router-dom";

class DashboardCollaborator extends Component {
  state = {
    selectedValue: moment(Date.now())
  };

  componentWillMount() {
    const { getProjects, getProjectsAsPManager } = this.props;
    getProjects(this.props.currentUser.email);
    getProjectsAsPManager(this.props.currentUser.email);

    const { loadedCollaboratorPendingTasks } = this.props;
    loadedCollaboratorPendingTasks(this.props.currentUser.email);

    const { collaboratorEffortInCompletedTasksLastMonth } = this.props;
    collaboratorEffortInCompletedTasksLastMonth(this.props.currentUser.email);

    const { loadUserRecentTasksActivity } = this.props;
    loadUserRecentTasksActivity(this.props.currentUser.email);


  }

  countRequests (){
    const{loadAssignmentRequests, loadRemovalRequests, loadCompletedRequests}=this.props;


    let sum = 0;
    this.props.PMprojects.map(p=>
            sum=sum+(p.requests)

    )
      return sum;

  }
  onSelect = value => {
    this.setState({
      selectedValue: value
    });
  };

  handleClick = value => {
    const { getProjectById, currentUser, history } = this.props;
    getProjectById(value, currentUser.email);
    history.push("/projects/selected");
  };

  getActivityTime = date => {
    if (date == null) {
      return (
        <span>
          <small>
            <strong>NO ACTIVITY</strong>
          </small>
        </span>
      );
    } else {
      let dateReport = moment(new Date(date));
      let dateToday = moment(Date.now());
      let difSeconds = dateToday.diff(dateReport, "seconds") % 60;
      let difMinutes = dateToday.diff(dateReport, "minutes") % 60;
      let difHours = dateToday.diff(dateReport, "hours") % 24;
      let difDays = dateToday.diff(dateReport, "days");

      return difDays > 0 ? (
        <span>
          <small>
            <strong>
              {difDays}d {difHours}h {difMinutes}m {difSeconds}s
            </strong>
          </small>
        </span>
      ) : difHours > 0 ? (
        <span>
          <small>
            <strong>
              {difHours}h {difMinutes}m {difSeconds}s
            </strong>
          </small>
        </span>
      ) : difMinutes > 0 ? (
        <span>
          <small>
            <strong>
              {difMinutes}m {difSeconds}s
            </strong>
          </small>
        </span>
      ) : (
        <span>
          <small>
            <strong>{difSeconds}s</strong>
          </small>
        </span>
      );
    }
  };




  render() {
    const {
      myProjects,
      collaboratorPendingTasks,
      hoursLastMonth,
      PMprojects,

    } = this.props;

    const projectLength = parseInt(myProjects.length + PMprojects.length);
    const taskLength = parseInt(collaboratorPendingTasks.length);
    const effort = hoursLastMonth;
    const projects = myProjects.concat(PMprojects);
    const projs = PMprojects.length;
    const sumRequests = this.countRequests();
    let i;
    let len = 16;
    if (this.props.recentTaskActivity.length === 0) {len=24};



    return (
      <Fragment>
        <ContentHeader title="DASHBOARD" />
        <div className="cards-header">
          <Row gutter={24}>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="folder-open" />
                <h3 className="title-card">My Projects</h3>
                <div className="header-col">
                  <span className="content-card">{projectLength}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="profile" />
                <h3 className="title-card">My Pending Tasks</h3>
                <div className="header-col">
                  <span className="content-card">{taskLength}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="exclamation-circle-o" />
                <h3 className="title-card">Manager Requests</h3>
                <div className="header-col">
                  <span className="content-card">{sumRequests}</span>
                </div>
              </Card>
            </Col>
            <Col span={6}>
              <Card className="card-head">
                <Icon className="icon" type="clock-circle-o" />
                <h3 className="title-card">Last Month's Effort</h3>
                <div className="header-col">
                  <span className="content-card">{effort}</span>
                </div>
              </Card>
            </Col>
          </Row>

          <Row id="dashboard" gutter={24} className="gridStyle">
            <Col span={len}>
              <Card title="Recent Projects" className="height" bordered={false}>
                {projects.map(proj => (
                  <Card.Grid key={proj.projectId}>
                  {proj.name.length > 26 ? <strong>{proj.name}</strong> :
                <strong>{proj.name}</strong>}

                    <br />
                    {proj.userEmail === this.props.currentUser.email ? (
                      <Tag color="#65655f" style={{marginTop:'0.5em'}}>Project Manager</Tag>
                    ) : (
                      <Tag color="#1852a6" style={{marginTop:'0.5em'}}>Collaborator</Tag>
                    )}
                    <Tooltip
                      title={ "Tasks: " +
                        proj.completedTasks +
                        " Completed / " +
                        proj.initiatedButNotCompletedTasks +
                        " In Progress / " +
                        (proj.notInitiatedTasks - proj.canceledTasks) +
                        " To Do"
                      }
                    ><br/><br/>Task Resume:
                      <Progress className="yellow-bar"
                        percent={
                          Math.round(
                            ((proj.initiatedButNotCompletedTasks + proj.completedTasks) /
                              (proj.completedTasks +
                                proj.initiatedButNotCompletedTasks +
                                proj.notInitiatedTasks -
                                proj.canceledTasks)) *
                              100 *
                              100
                          ) / 100
                        }
                        successPercent={
                          (proj.completedTasks /
                            (proj.completedTasks +
                              proj.initiatedButNotCompletedTasks +
                              proj.notInitiatedTasks -
                              proj.canceledTasks)) *
                          100
                        }
                        size="small"
                      />
                    </Tooltip>
                    <div className="dash-separator" />
                    <small>
                      Tasks: <strong>{proj.tasks}</strong>
                    </small>
                    <br />
                    <small>
                      Collaborators: <strong>{proj.collaborators}</strong>
                    </small>
                    {proj.userEmail === this.props.currentUser.email ? (
                      <div>
                        <small>
                          Current Cost:{" "}
                          <strong>
                            {Math.round(proj.currentCost * 100) / 100} {proj.unit}
                          </strong>
                        </small>

                        {proj.unassignedCollaborators > 0 ? (
                          <div>
                            <Tag color="rgb(255, 201, 4)" style={{ marginTop: "1em" }}>
                              {proj.unassignedCollaborators} unassigned collaborator(s)
                            </Tag>{" "}
                          </div>
                        ) : null}
                      </div>
                    ) : <div><br/></div>}

                    <br />
                    <span>
                      {proj.endDate ? (
                        <Tag color="#87d068">Closed</Tag>
                      ) : (
                        <Tag color="#2db7f5">In Progress</Tag>
                      )}
                    </span>
                    <span className="btn-info">
                      <button onClick={() => this.handleClick(proj.projectId)}>
                        INFO <Icon className="icon-info" type="right-square" />
                      </button>
                    </span>
                  </Card.Grid>
                ))}
              </Card>
            </Col>
            <Col span={8}>
              {this.props.recentTaskActivity.length > 0 && (
                <Card title="Recent Reports Activity" className="height" bordered={false}>
                  {this.props.recentTaskActivity.map(task => (
                    <div key={task.taskId}>
                      <Tooltip
                        title={"[" + task.taskId + "]  -  Description: " + task.description}
                        key={task.taskId}
                      >
                        <br />
                          <strong>Task:</strong> {task.title}{" "}
                          <small>(project: {task.projectId})</small>
                          <Tag style={{float: "right" }} color="volcano">
                            {this.getActivityTime(task.lastReportCreationDate)}
                          </Tag>
                      </Tooltip>
                      <br />
                      <Tag color={colorChangeState(task.state)} style={{marginTop:'0.5em'}}>{task.state} </Tag>
                      <TaskProgress taskProgress={task} />
                      {task.lastReportHours === 0 ? (
                        <p>
                          <small> [ No reports in this task ]</small>
                        </p>
                      ) : (
                        <p>
                          <strong>Reported:</strong> {task.lastReportHours} hours{" "}
                          <small> ( at {moment(task.lastReportDate).format("YYYY-MM-DD")} )</small>
                        </p>
                      )}

                      <div style={{ borderBottom: "1px solid #d9d9d9", paddingTop: "1em" }} />
                    </div>
                  ))}
                </Card>
              )}
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  myProjects: state.projects.projectsAsCollaborator,
  currentUser: state.authentication.currentUser,
  collaboratorPendingTasks: state.tasks.collaboratorPendingTasks,
  assignmentRequests: state.projects.assignmentRequests,
  removalRequests: state.projects.removalRequests,
  completeTaskRequests: state.projects.completeTaskRequests,
  hoursLastMonth: state.reports.collaboratorHoursCompletedTasksLastMonth,
  PMprojects: state.projects.projectsAsManager,
  recentTaskActivity: state.tasks.tasksOrderByLastReport
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProjects,
      getProjectById,
      loadedCollaboratorPendingTasks,
      loadAssignmentRequests,
      loadRemovalRequests,
      loadCompletedRequests,
      cancelAssignmentRequest,
      cancelRemovalRequest,
      collaboratorEffortInCompletedTasksLastMonth,
      getProjectsAsPManager,
      loadUserRecentTasksActivity
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashboardCollaborator)
);
