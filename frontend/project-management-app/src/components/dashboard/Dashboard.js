import React, { Component } from "react";
import { connect } from "react-redux";
import "./Dashboard.css";
import { getProfileName } from "../../util/Helpers";
import DashboardAdmin from "./DashboardAdmin";
import DashboardDirector from "./DashboardDirector";
import DashboardCollaborator from "./DashboardCollaborator";
import DashboardRegisteredUser from "./DashboardRegisteredUser";

class Dashboard extends Component {
  render() {
    const profile = getProfileName(this.props.currentUser.profile);

    return profile === "COLLABORATOR" ? (
      <DashboardCollaborator />
    ) : profile === "ADMINISTRATOR" ? (
      <DashboardAdmin />
    ) : profile === "DIRECTOR" ? (
      <DashboardDirector />
    ) : (
      <DashboardRegisteredUser />
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.authentication.currentUser
});

export default connect(mapStateToProps)(Dashboard);
