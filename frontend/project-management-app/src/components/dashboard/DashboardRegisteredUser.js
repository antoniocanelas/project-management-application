import React, { Fragment } from "react";
import { Calendar, Card, Col, Row, Icon, Avatar } from "antd";
import "./Dashboard.css";
import ContentHeader from "../../templates/contentHeader";
import {getAvatarColor} from "../../util/Colors";

const { Meta } = Card;

function onPanelChange(value, mode) {
  console.log(value, mode);
}

class DashboardRegisteredUser extends React.Component {
  render() {
    return (
      <Fragment>
        <ContentHeader title="DASHBOARD" />
          <div className="cards-header">
              <Row gutter={24} className="flex">
                  <Col span={14}>
                      <Card title={<Meta
                          avatar={<Icon type="star-o"/>}
                          title="Welcome Message"
                          className="margin"
                      />} bordered={false} className="height">
                          <p>You are registered user.</p>
                          <p>Please wait for the application administrator's authorization to have access to the features corresponding to your profile.</p>
                      </Card>
                  </Col>
                  <Col span={10}>
                      <Card title="Calendar" bordered={false}>
                          <div style={{ border: "1px solid #d9d9d9", borderRadius: 4 }}>
                              <Calendar fullscreen={false} onPanelChange={onPanelChange} />
                          </div>
                      </Card>
                  </Col>
              </Row>
          </div>

      </Fragment>
    );
  }
}

export default DashboardRegisteredUser;
