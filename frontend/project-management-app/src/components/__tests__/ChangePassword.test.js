import {shallow} from 'enzyme';
import React from 'react';
import ChangePassword from "../login/ChangePassword";


it('Snapshot ChangePassword', () => {
    const wrapped = shallow(<ChangePassword/>);
    expect(wrapped).toMatchSnapshot();
});
