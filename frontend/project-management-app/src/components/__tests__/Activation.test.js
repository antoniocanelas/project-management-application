import {shallow} from 'enzyme';
import React from 'react';
import Activation from "../activation/Activation";


it('test App snapshot', () => {
    const wrapped = shallow(<Activation />);
    expect(wrapped).toMatchSnapshot();
});