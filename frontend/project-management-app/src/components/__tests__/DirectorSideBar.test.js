import {shallow} from 'enzyme';
import React from 'react';
import DirectorSideBar from "../sidebar/DirectorSideBar";


it('Snapshot Profile', () => {
    const wrapped = shallow(<DirectorSideBar/>);
    expect(wrapped).toMatchSnapshot();
});
