import {shallow} from 'enzyme';
import React from 'react';
import DashboardRegisteredUser from "../dashboard/DashboardRegisteredUser";


it('test DashboardRegisteredUser snapshot', () => {
    const wrapped = shallow(<DashboardRegisteredUser />);
    expect(wrapped).toMatchSnapshot();
});