import {shallow} from 'enzyme';
import React from 'react';
import AppHeader from "../header/AppHeader";


it('Snapshot AppHeader', () => {
    const wrapped = shallow(<AppHeader/>);
    expect(wrapped).toMatchSnapshot();
});
