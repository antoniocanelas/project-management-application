import {shallow} from 'enzyme';
import React from 'react';
import Signup from "../signup/Signup";
import {Form} from 'antd';
import {Layout} from "antd/lib/index";

const Footer = Layout.Footer;


it('test PrivateRoute snapshot', () => {
    const wrapped = shallow(<Signup/>);
    expect(wrapped).toMatchSnapshot();
});

it('Renders component', () => {
    const wrapper = shallow(<Signup/>);
    expect(wrapper.contains(<button className="signupDiv"/>));
});

it('Check if if component exist', () => {
    const wrapped = shallow(<Signup/>);
    expect(wrapped.find(Form).length).toEqual(1);
});
