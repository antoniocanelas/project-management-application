import {shallow} from 'enzyme';
import React from 'react';
import ServerError from "../common/ServerError/ServerError";
import {Button} from "antd";


it('test ServerError snapshot', () => {
    const wrapped = shallow(<ServerError/>);
    expect(wrapped).toMatchSnapshot();
});


it('Renders component', () => {
    const wrapper = shallow(<ServerError/>);
    expect(wrapper.contains(<div className="server-error-page"/>));
});


it('Renders component', () => {
    const wrapper = shallow(<ServerError/>);
    expect(wrapper.contains(<div className="server-error-desc"/>));
});

it('Renders component', () => {
    const wrapper = shallow(<ServerError/>);
    expect(wrapper.contains(<h1 className="server-error-title"/>));
});

it('Renders component', () => {
    const wrapper = shallow(<ServerError/>);
    expect(wrapper.contains(<Button className="server-error-go-back-btn"/>));
});




