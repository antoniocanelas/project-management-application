import {shallow} from 'enzyme';
import React from 'react';
import App from "../../app/App";


it('test App snapshot', () => {
    const wrapped = shallow(<App/>);
    expect(wrapped).toMatchSnapshot();
});
