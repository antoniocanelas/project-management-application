import {shallow} from 'enzyme';
import React from 'react';
import SimpleReactFileUpload from "../importData/SimpleReactFileUpload";
import onFormSubmit from "../importData/SimpleReactFileUpload";

it('Snapshot SimpleReactFileUpload', () => {
    const wrapped = shallow(<SimpleReactFileUpload/>);
    expect(wrapped).toMatchSnapshot();
});

it('Renders component', () => {
    const wrapper = shallow(<SimpleReactFileUpload/>);
    expect(wrapper.contains(<div className="principalDiv"/>));
});


it('Renders component', () => {
    const wrapper = shallow(<SimpleReactFileUpload/>);
    expect(wrapper.contains(<form className="uploadForm"/>));
});

it('Renders component', () => {
    const wrapper = shallow(<SimpleReactFileUpload/>);
    expect(wrapper.contains(<input className="inputName"/>));
});

it('Renders component', () => {
    const wrapper = shallow(<SimpleReactFileUpload/>);
    expect(wrapper.contains(<button className="uploadButton"/>));
});




