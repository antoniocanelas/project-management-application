import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {ACCESS_TOKEN} from '../../constants';
import {logout} from '../../actions/loginActions'
import './AppHeader.css';
import {Icon, Layout, Menu, notification} from 'antd';
import ProfileDropdownMenu from "./ProfileDropDownMenu";

const Header = Layout.Header;

class AppHeader extends Component {
    constructor(props) {
        super(props);
        this.handleMenuClick = this.handleMenuClick.bind(this);
        this.handleLogout = this.handleLogout.bind(this);

        notification.config({
            placement: 'topRight',
            top: 70,
            duration: 3,
          });    
    }

    handleMenuClick({key}) {
        if (key === "logout") {
            this.handleLogout();
        }
    }

    handleLogout(obj={}, redirectTo="/login", notificationType="success", description="You're successfully logged out.") {

        const {logout} = this.props;
        localStorage.removeItem(ACCESS_TOKEN);
        logout();
        this.props.history.push(redirectTo);
        
        notification[notificationType]({
          message: 'PM App',
          description: description,
        });
    
        
    }

    render() {
        let menuItems;
        const role = this.props.currentUser.profile;
        
        if (this.props.isAuthenticated) {
            menuItems = [
                <Menu.Item key="/">
                    <Link to="/">
                        <Icon type="home" className="nav-icon"/>
                    </Link>
                </Menu.Item>,
                //<Menu.Item key="/search" disabled={true}>
                  //<Link to="/search">
                    //    <Icon type="search" />
                    //</Link>
                //</Menu.Item>,
                <Menu.Item key="/profile" className="profile-menu">
                    <ProfileDropdownMenu
                        currentUser={this.props.currentUser}
                        handleMenuClick={this.handleMenuClick}/>
                </Menu.Item>,
                <Menu.Item key="logout">
                    <Link to="/logout">
                        <Icon type="poweroff" />
                    </Link>
                </Menu.Item>
            ];
        } else {
            menuItems = [
                <Menu.Item key="/login">
                    <Link to="/login">Login</Link>
                </Menu.Item>,
                <Menu.Item key="/signup">
                    <Link to="/signup">Signup</Link>
                </Menu.Item>
            ];
        }

        return (
            <Header className="app-header">
                <div className="app-title">
                    <Link to="/">PROJECT MANAGEMENT APP</Link>
                </div>
                <Menu onClick={this.handleMenuClick}
                    className="app-menu"
                    mode="horizontal"
                    selectedKeys={[this.props.location.pathname]}
                    style={{lineHeight: '64px'}}>
                    {menuItems}
                </Menu>
            </Header>
        );
    }
}


const mapStateToProps = state => ({currentUser: state.authentication.currentUser, isAuthenticated: state.authentication.isAuthenticated });

const mapDispatchToProps = dispatch =>
bindActionCreators({logout},dispatch);

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(AppHeader));