import {
  addDependenciestoTask,
  addTaskCollaborator,
  availableTaskCollaboratorToAdd,
  cancelTask,
  completedTask,
  createTask,
  getTask,
  listAllTaskCollaborators,
  listTaskCollaborators,
  makeAssignementRequest,
  markTaskCompleted,
  removeTask,
  removeTaskCollaborator,
  requestMarkTaskCompleted,
  cancelCompletedRequests,
  makeRemovalRequest
} from "../util/APIUtils";
import {
  loadCollabRequests,
  loadedCollaboratorPendingTasks,
  loadedCollaboratorTasks,
  loadedProjectTasks,
  loadedTasksDependencies,
  loadedTasksDependenciesToAdd
} from "./actions_task_array";
import {
  getProjectById,
  loadAssignmentRequests,
  loadCompletedRequests,
  loadedProjectCollaboratorsList,
  loadRemovalRequests
} from "./projectActions";
import { message } from "antd";

export const loadTask = taskid => {
  return dispatch => {
    getTask(taskid).then(resp =>
      dispatch({
        type: "CURRENT_TASK",
        payload: resp
      })
    );
  };
};

export const loadLinks = taskid => {
  return dispatch => {
    getTask(taskid).then(resp =>
      dispatch({
        type: "LOAD_LINKS",
        payload: resp._links
      })
    );
  };
};

export const requestTaskCompleted = (taskid, email) => {
  return dispatch => {
    requestMarkTaskCompleted(taskid, email)
      .then(resp =>
        dispatch({
          type: "REQUEST_MARK_COMPLETED_TASK",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedCollaboratorPendingTasks(email)))
      .then(resp => dispatch(loadedCollaboratorTasks(email)))
      .then(resp => message.success('Complete task request successfully sent!'))
  };
};

export const loadAddTask = (projectId, title, description, effort, startDate, endDate) => {
  return dispatch => {
    createTask(projectId, title, description, effort, startDate, endDate)
      .then(resp =>
        dispatch({
          type: "CREATE_TASK",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedProjectTasks(projectId)));
  };
};

export const loadAddDependencies = (taskId, dependencies) => {
  return dispatch => {
    addDependenciestoTask(taskId, dependencies)
      .then(resp =>
        dispatch({
          type: "ADD_DEPENDENCIES",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedTasksDependencies(taskId)))
      .then(resp => dispatch(loadedTasksDependenciesToAdd(taskId)))
      .then(resp => dispatch(loadTask(taskId)));
  };
};

export const loadTaskCollaborators = taskId => {
  return dispatch => {
    listTaskCollaborators(taskId).then(resp =>
      dispatch({
        type: "LOAD_TASK_COLLABORATORS",
        payload: resp
      })
    );
  };
};

export const loadAllTaskCollaborators = taskId => {
  return dispatch => {
    listAllTaskCollaborators(taskId).then(resp =>
      dispatch({
        type: "LOAD_ALL_TASK_COLLABORATORS",
        payload: resp
      })
    );
  };
};
export const loadAvailableTaskCollaborators = taskId => {
  return dispatch => {
    availableTaskCollaboratorToAdd(taskId)
      .then(resp =>
        dispatch({
          type: "LOAD_AVAILABLE_TO_ADD_TASK_COLLABORATORS",
          payload: resp
        })
      )
      .then(resp => dispatch(loadTaskCollaborators(taskId)));
  };
};

export const addTaskCollaborators = (taskid, email, projectId, currentUser) => {
  return dispatch => {
    addTaskCollaborator(taskid, email)
      .then(resp =>
        dispatch({
          type: "ADD_TASK_COLLABORATORS",
          payload: resp
        })
      )
      .then(resp => dispatch(loadTaskCollaborators(taskid)))
      .then(resp => dispatch(loadAllTaskCollaborators(taskid)))
      .then(resp => dispatch(loadAssignmentRequests(projectId)))
      .then(resp => dispatch(loadAvailableTaskCollaborators(taskid)))
      .then(resp => dispatch(loadTask(taskid)))
      .then(resp => dispatch(getProjectById(projectId, currentUser)));
  };
};

export const removeTasks = (taskid, projectId) => {
  return dispatch => {
    removeTask(taskid)
      .then(resp =>
        dispatch({
          type: "REMOVE_TASK",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedProjectTasks(projectId)));
  };
};

export const cancelTasks = (taskid, projectId) => {
  return dispatch => {
    cancelTask(taskid, projectId)
      .then(resp =>
        dispatch({
          type: "CANCEL_TASK",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedProjectTasks(projectId)))
      .then(resp => dispatch(loadTask(taskid)));
  };
};

export const completedTasks = (taskid, projectId) => {
  return dispatch => {
    completedTask(taskid, projectId)
      .then(resp =>
        dispatch({
          type: "COMPLETED_TASK",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedProjectTasks(projectId)))
      .then(resp => dispatch(loadTask(taskid)));
  };
};

export const removeCollaboratorFromTask = (taskid, email, projectId, currentUser) => {
  return dispatch => {
    removeTaskCollaborator(taskid, email)
      .then(resp =>
        dispatch({
          type: "REMOVE_TASK_COLLABORATORS",
          payload: resp
        })
      )
      .then(resp => dispatch(loadRemovalRequests(projectId)))
      .then(resp => dispatch(loadTaskCollaborators(taskid)))
      .then(resp => dispatch(loadAllTaskCollaborators(taskid)))
      .then(resp => dispatch(loadAvailableTaskCollaborators(taskid)))
      .then(resp => dispatch(loadTask(taskid)))
      .then(resp => dispatch(getProjectById(projectId, currentUser)));
  };
};

export const markCompleted = (taskid, projectId, currentUser) => {
  return dispatch => {
    markTaskCompleted(taskid)
      .then(resp =>
        dispatch({
          type: "MARK_COMPLETED",
          payload: resp
        })
      )
      .then(resp => dispatch(loadedProjectCollaboratorsList(projectId)))
      .then(resp => dispatch(loadCompletedRequests(projectId)))
      .then(resp => dispatch(getProjectById(projectId, currentUser)))
      
  };
};

export const makeAssignment = (taskid, email) => {
  return dispatch => {
    makeAssignementRequest(taskid, email).then(resp =>
      dispatch({
        type: "MAKE_ASSIGNMENT_REQUEST",
        payload: resp
      })
    )
    .then(resp => message.success('Assignment request successfully sent!'));
  };
};

export const makeRemoval = (taskid, email) => {
  return dispatch => {
    makeRemovalRequest(taskid, email).then(resp =>
      dispatch({
        type: "MAKE_REMOVAL_REQUEST",
        payload: resp
      })
    )
    .then(resp => message.success('Removal request successfully sent!'));
  };
};

export const cancelRequestTaskCompleted = (taskid, projectId, currentUser) => {
  return dispatch => {
    cancelCompletedRequests(taskid)
      .then(resp =>
        dispatch({
          type: "CANCEL_REQUEST_MARK_COMPLETED_TASK",
          payload: resp
        })
      )

      .then(resp => dispatch(loadCompletedRequests(projectId)))
      .then(resp => dispatch(loadedProjectTasks(projectId)))
  .then(resp => dispatch(getProjectById(projectId, currentUser)));

  };
};

export const cancelRequestTaskCompletedCollab = (taskid, projectId, email, currentUser) => {
  return dispatch => {
    cancelCompletedRequests(taskid)
      .then(resp =>
        dispatch({
          type: "CANCEL_REQUEST_MARK_COMPLETED_TASK_COLLAB",
          payload: resp
        })
      )

      .then(resp => dispatch(loadCollabRequests(email)))
      .then(resp => dispatch(loadedProjectTasks(projectId)))
        .then(resp => dispatch(getProjectById(projectId, currentUser)));

  };
};
