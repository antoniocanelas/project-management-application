import {
    getProjectTasks,
    listAllTasks,
    listCompletedTasks,
    listCompletedTasksLastMonth,
    listDependenciesTasks,
    listTasks,
    listTasksThatCanBeAddedAsDependencies,
    listProjectCompletedTasks,
    listProjectInitiatedButNotCompletedTasks,
    listProjectCancelledTasks,
    listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks,
    listProjectCompletedTasksLastMonth,
    listCollaboratorTasksOrderByLastReport, getCollabRequests
} from "../util/APIUtils";

export const loadedProjectTasks = projectId => {
  return dispatch => {
    getProjectTasks(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_TASKS",
        payload: resp
      })
    );
  };
};

export const loadCollabRequests = email => {
    return dispatch => {
        getCollabRequests(email).then(resp =>
            dispatch({
                type: "REQUESTS_LOADED",
                payload: resp
            })
        );
    };
};

export const loadUserRecentTasksActivity = email => {
  return dispatch => {
    listCollaboratorTasksOrderByLastReport(email).then(resp =>
      dispatch({
        type: "LOADED_USER_RECENT_ACTIVITY",
        payload: resp
      })
    );
  };
};

export const loadedProjectCompletedTasks = projectId => {
  return dispatch => {
    listProjectCompletedTasks(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_COMPLETED_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedProjectCompletedTasksLastMonth = projectId => {
  return dispatch => {
    listProjectCompletedTasksLastMonth(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_COMPLETED_TASKS_LAST_MONTH",
        payload: resp
      })
    );
  };
};

export const loadedProjectInitiatedButNotCompletedTasks = projectId => {
  return dispatch => {
    listProjectInitiatedButNotCompletedTasks(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_INITIATED_BUT_NOT_COMPLETED_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedProjectCancelledTasks = projectId => {
  return dispatch => {
    listProjectCancelledTasks(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_CANCELLED_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedProjectOverdueTasks = projectId => {
  return dispatch => {
    listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(projectId).then(resp =>
      dispatch({
        type: "LOADED_PROJECT_OVERDUE_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorTasks = useremail => {
  return dispatch => {
    listAllTasks(useremail).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_PROJECT_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorPendingTasks = useremail => {
  return dispatch => {
    listTasks(useremail).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_PENDING_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorCompletedTasks = useremail => {
  return dispatch => {
    listCompletedTasks(useremail).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_COMPLETED_TASKS",
        payload: resp
      })
    );
  };
};

export const loadedCollaboratorCompletedTasksLastMonth = useremail => {
  return dispatch => {
    listCompletedTasksLastMonth(useremail).then(resp =>
      dispatch({
        type: "LOADED_COLLABORATOR_COMPLETED_TASKS_LAST_MONTH",
        payload: resp
      })
    );
  };
};

export const loadedTasksDependencies = taskid => {
  return dispatch => {
    listDependenciesTasks(taskid).then(resp =>
      dispatch({
        type: "LOADED_TASKS_DEPENDENCIES",
        payload: resp
      })
    );
  };
};

// export const loadedAddTasksDependencies = (taskid,addDependencies) => {
//     return dispatch => {
//         addDependenciestoTask(taskid, addDependencies)
//             .then(resp => dispatch({
//                     type: 'LOADED_ADD_TASKS_DEPENDENCIES',
//                     payload: resp.data
//                 })
//             )
//     }
// };

export const loadedTasksDependenciesToAdd = taskId => {
  return dispatch => {
    listTasksThatCanBeAddedAsDependencies(taskId).then(resp =>
      dispatch({
        type: "LOADED_AVAILABLE_TASKS_DEPENDENCIES_TO_ADD",
        payload: resp
      })
    );
  };
};
