import { combineReducers } from "redux";

import loginReducer from "./loginReducer";
import projectReducer from "./projectReducer";

import reducer_task_array from "./reducer_task_array";
import reducer_task from "./reducer_task";

import reducer_report_array from "./reducer_report_array";
import reducer_report from "./reducer_report";
import reducer_user_array from "./reducer_user_array";

const appReducer = combineReducers({
  authentication: loginReducer,
  projects: projectReducer,
  tasks: reducer_task_array,
  task: reducer_task,
  reports: reducer_report_array,
  report: reducer_report,
  users: reducer_user_array
});

const rootReducer = (state, action) => {
  if (action.type === "LOGGED_OUT") {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
