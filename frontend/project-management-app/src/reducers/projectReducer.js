const INITIAL_STATE = {
  selectedProject: "",
  isProjectManager: false,
  currentCost: "",
  availableCostOptions: [],
  assignmentRequests: [],
  removalRequests: [],
  completeTaskRequests: [],
  projectsAsCollaborator: [],
  projectsAsManager: [],
  projectsAsDirector: [],
  projectsCollaboratorList: [],
  projectsCollaboratorNotInProjectList: [],
  projectCostOptions: [],
  unassignedProjectsCollaboratorList: [],
  projectTasksCostList: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "PROJECT_SELECTED":
      return {
        ...state,
        selectedProject: action.payload,
        isProjectManager: Boolean(action.payload.userEmail === action.email)
      };
    case "PROJECTS_LOADED":
      return { ...state, projectsAsCollaborator: action.payload };
    case "PROJECTS_AS_PM_LOADED":
      return { ...state, projectsAsManager: action.payload };
    case "PROJECTS_AS_DIR_LOADED":
      return { ...state, projectsAsDirector: action.payload };
    case "PROJECTS_COLLABORATORS_LIST_LOADED":
      return { ...state, projectsCollaboratorList: action.payload };
    case "PROJECTS_COLLAB_LIST_UNASSIGNED":
      return { ...state, unassignedProjectsCollaboratorList: action.payload };
    case "PROJECTS_COLLAB_LIST_NOT_IN_PROJECT_LOADED":
      return { ...state, projectsCollaboratorNotInProjectList: action.payload };
    case "PROJECT_COST_OPTIONS_LOADED":
      return { ...state, projectCostOptions: action.payload };
    case "CURRENT_COST_LOADED":
      return { ...state, currentCost: action.payload };
    case "AVAILABLE_COST_OPTIONS_LOADED":
      return { ...state, availableCostOptions: action.payload.availableCalculationOptions };
    case "COST_SELECTED":
      return { ...state };
    case "ADD_PROJECT_COLLABORATOR":
      return { ...state };
    case "ASSIGNEMENT_REQUESTS_LOADED":
      return { ...state, assignmentRequests: action.payload };
    case "REMOVAL_REQUESTS_LOADED":
      return { ...state, removalRequests: action.payload };
    case "COMPLETE_TASK_REQUESTS_LOADED":
      return { ...state, completeTaskRequests: action.payload };
    case "ASSIGNMENT_REQUEST_CANCELLED":
      return { ...state };
    case "REMOVAL_REQUEST_CANCELLED":
      return { ...state };
      case "ASSIGNMENT_REQUEST_CANCELLED_COLLAB":
          return { ...state };
      case "REMOVAL_REQUEST_CANCELLED_COLLAB":
          return { ...state };
    case "TASKS_COST_LOADED":
      return { ...state, projectTasksCostList: action.payload };
    default:
      return state;
  }
}
