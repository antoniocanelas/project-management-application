const INITIAL_STATE = {
  users: [],
  collaborators: [],
    registeredusers:[]
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOADED_USERS":
      return { ...state, users: action.payload };
    case "LOADED_COLLABORATORS":
      return { ...state, collaborators: action.payload };
    case "LOADED_DIRECTORS":
      return { ...state, directors: action.payload };
    case "LOADED_REGISTEREDUSERS":
      return { ...state, registeredusers: action.payload };
    case "LOADED_ADMINISTRATORS":
      return { ...state, administrators: action.payload };
    case "PROFILE_UPDATED":
      return { ...state };
    case "USERS_IMPORTED":
      return { ...state, users: action.payload };
    default:
      return state;
  }
};
