const INITIAL_STATE = {
  taskReports: [],
  collaboratorTaskReports: [],
  collaboratorAllTasksReports: [],
  collaboratorHoursCompletedTasksLastMonth: 0,
  collaboratorPeopleMonthCompletedTasksLastMonth: 0
};

export default function(state = INITIAL_STATE, action) {
  switch ((state, action.type)) {
    case "LOADED_TASK_REPORTS":
      return { ...state, taskReports: action.payload };
    case "LOADED_COLLABORATOR_REPORT_ALL_TASKS":
      return { ...state, collaboratorAllTasksReports: action.payload };
    case "LOADED_COLLABORATOR_TASK_REPORTS":
      return { ...state, collaboratorTaskReports: action.payload };
    case "LOADED_COLLABORATOR_HOUR_REPORTS_LAST_MONTH":
      return { ...state, collaboratorHoursCompletedTasksLastMonth: action.payload.hours, collaboratorPeopleMonthCompletedTasksLastMonth: action.payload.peopleMonth };
  }
  return state;
}
