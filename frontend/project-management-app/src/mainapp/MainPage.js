import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { loadUser } from "../actions/loginActions";
import LoadingIndicator from "../common/LoadingIndicator";
import { Layout } from "antd";
import { withRouter } from "react-router-dom";
import "./MainPage.css";
import UserList from "./UserList";
import ReportList from "./reports/CollaboratorReportList";
import SideBar from "../components/sidebar";
import MyProjects from "./project/MyProjects";
import PmProjects from "./project/ProjManagerProject/PmProjects";
import DirProjects from "./project/DirProjects";
import Dashboard from "../components/dashboard/Dashboard";
import AppContent from "../components/content/AppContent";
import GetProjectCost from "./project/GetProjectCost";

import Login from "../components/login/Login";
import SearchTool from "../components/search";
import DirectorSideBar from "../components/sidebar/DirectorSideBar";
import AdministratorSideBar from "../components/sidebar/AdministratorSideBar";
import CreateProject from "./project/CreateProject";
import Activation from "../components/activation";
import ChangePassword from "../components/login/ChangePassword";
import AllUsersRedux from "./AllUsersRedux";
import Project from "../mainapp/project/Project";
import Task from "./task/task/Task";
import MyCompletedTasks from "./task/CollaboratorTasks/MyCompletedTasks";
import CollaboratorTasks from "./task/CollaboratorTasks/CollaboratorTasks";
import RegisteredUserSideBar from "../components/sidebar/RegisteredUserSideBar";
import SimpleReactFileUpload from "../components/importData/SimpleReactFileUpload";
import ListRequests from "../components/requests/ListRequests";

const { Content } = Layout;

class MainPage extends Component {
  renderSwitch(param) {
    switch (param) {
      case 0:
        return <Dashboard />;
      case 1:
        return <MyProjects />;
      case 2:
        return <PmProjects />;
      case 3:
        return <GetProjectCost />;
      case 4:
        return <CollaboratorTasks />;
        case 5:
          return <ListRequests/>;
      case 7:
        return <ReportList />;
      case 8:
        return <UserList />;
      case 9:
        return <SearchTool />;
      case 10:
        return <AllUsersRedux />;
      case 11:
        return <DirProjects email={this.props.currentUser.email} />;
      case 12:
        return <CreateProject />;
      case 13:
        return <Project />;
      case 14:
        return <Task />;
      case 15:
        return <SimpleReactFileUpload />;
      default:
        return <Login />;
    }
  }

  renderSwitchSideBar(param) {
    switch (param) {
      case "[ROLE_COLLABORATOR]":
        return <SideBar />;
      case "[ROLE_DIRECTOR]":
        return <DirectorSideBar />;
      case "[ROLE_ADMINISTRATOR]":
        return <AdministratorSideBar />;
      case "[ROLE_REGISTEREDUSER]":
        return <RegisteredUserSideBar />;
      default:
        return <Login />;
    }
  }

  render() {
    if (
      this.props.isAuthenticated &&
      !this.props.needActivation &&
      !this.props.needToChangePassword
    ) {
      return (
        <Layout>
          {this.renderSwitchSideBar(this.props.profile)}
          <Layout style={{ padding: "0 24px 24px" }}>
            <Content className="app-content">
              <div>{this.renderSwitch(this.props.opt)}</div>
            </Content>
          </Layout>
        </Layout>
      );
    } else if (
      this.props.isAuthenticated &&
      this.props.needActivation &&
      !this.props.needToChangePassword
    ) {
      return (
        <div>
          <Activation />
        </div>
      );
    } else if (
      this.props.isAuthenticated &&
      !this.props.needActivation &&
      this.props.needToChangePassword
    ) {
      return (
        <div>
          <ChangePassword />
        </div>
      );
    } else {
      if (this.props.isLoading) {
        return <LoadingIndicator />;
      } else {
        return <AppContent />;
      }
    }
  }
}

const mapStateToProps = state => ({
  currentUser: state.authentication.currentUser,
  isAuthenticated: state.authentication.isAuthenticated,
  needActivation: state.authentication.needActivation,
  needToChangePassword: state.authentication.needToChangePassword,
  isLoading: state.authentication.isLoading,
  profile: state.authentication.currentUser.profile
});

const mapDispatchToProps = dispatch => bindActionCreators({ loadUser }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MainPage)
);
