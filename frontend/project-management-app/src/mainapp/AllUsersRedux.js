import React from "react";
import { Button, Dropdown, Icon, Input, Menu, Table } from "antd";
import { loadedUsers, profileUpdated } from "../actions/actions_user_array";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ContentHeader from "../templates/contentHeader";
import "./AllUsersRedux.css";

const values = {
  profile: "",
  profileAction: ""
};

class AllUsersRedux extends React.Component {
  menu = links => {
    let selectedKeysUrls = [];
    links.map(link => {
      {
        selectedKeysUrls.push(link.href);
      }
    });
    return (
      <Menu selectedKeys={selectedKeysUrls} onClick={this.onClickProfile}>
        <Menu.Item key="1" disabled={this.handleMenu(links, "ADMINISTRATOR")}>
          Change profile to Administrator
        </Menu.Item>
        <Menu.Item key="2" disabled={this.handleMenu(links, "DIRECTOR")}>
          Change profile to Director{" "}
        </Menu.Item>
        <Menu.Item key="3" disabled={this.handleMenu(links, "COLLABORATOR")}>
          Change profile to Collaborator
        </Menu.Item>
        <Menu.Item key="4" disabled={this.handleMenu(links, "REGISTEREDUSER")}>
          Change profile to Registered User
        </Menu.Item>
      </Menu>
    );
  };

  constructor(props) {
    super(props);
    this.state = {
      filteredInfo: null,
      filterDropdownVisible: false,
      searchText: "",
      filtered: false,
      usersFiltered: "",
      selectedKey: ""
    };
  }

  handleMenu = (links, profile) => {
    let found = true;
    links.map(link => {
      if (link.rel === profile) {
        found = false;
      }
    });
    return found;
  };

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  onClickProfile = e => {
    const { profileUpdated } = this.props;
    values.profileAction = e.item.props.selectedKeys[1];

    if (e.key === "1") {
      values.profile = "ADMINISTRATOR";
    } else if (e.key === "2") {
      values.profile = "DIRECTOR";
    } else if (e.key === "3") {
      values.profile = "COLLABORATOR";
    } else if (e.key === "4") {
      values.profile = "REGISTEREDUSER";
    }

    profileUpdated(values);

    setTimeout(() => this.onSearch(), 1000);
  };
  onSearch = () => {
    const { searchText } = this.state;
    const reg = new RegExp(searchText, "gi");
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      usersFiltered: this.props.users
        .map(record => {
          const match = record.email.match(reg);
          if (!match) {
            return null;
          }
          return {
            ...record,
            email: (
              <span>
                {record.email.split(new RegExp(`(?=${searchText})`, "i")).map(
                  (text, i) =>
                    text.toLowerCase() === searchText.toLowerCase() ? (
                      <span key={i} className="highlight">
                        {text}
                      </span>
                    ) : (
                      text
                    ) // eslint-disable-line
                )}
              </span>
            )
          };
        })
        .filter(record => !!record)
    });
  };
  handleChange = filters => {
    this.setState({
      filteredInfo: filters
    });
  };

  componentWillMount() {
    const { loadedUsers } = this.props;
    loadedUsers();
  }

  render() {
    let { filteredInfo } = this.state;
    filteredInfo = filteredInfo || {};

    const title = [
      {
        title: "User email",
        dataIndex: "email",
        key: "email",
        filterDropdown: (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => {
                return (this.searchInput = ele);
              }}
              placeholder="Search email"
              value={this.state.searchText}
              onChange={this.onInputChange}
              onPressEnter={this.onSearch}
            />
            <Button type="primary" onClick={this.onSearch}>
              <Icon type="search" />Search
            </Button>
          </div>
        ),
        filterIcon: (
          <Icon type="search" style={{ color: this.state.filtered ? "#108ee9" : "#aaa" }} />
        ),
        filterDropdownVisible: this.state.filterDropdownVisible,
        onFilterDropdownVisibleChange: visible => {
          this.setState(
            {
              filterDropdownVisible: visible
            },
            () => this.searchInput && this.searchInput.focus()
          );
        }
      },
      {
        title: "Name",
        dataIndex: "name",
        key: "name"
      },
      {
        title: "Profile",
        dataIndex: "profile",
        key: "profile",

        render: (text, record) => (
          <span>
            <Dropdown overlay={this.menu(record.links)}>
              <a href="#">
                {record.profile
                  .substring(6)
                  .replace("]", "")
                  .toLocaleLowerCase()}{" "}
                <Icon type="down" />
              </a>
            </Dropdown>
          </span>
        ),
        filters: [
          { text: "Director", value: "[ROLE_DIRECTOR]" },
          { text: "Administrator", value: "[ROLE_ADMINISTRATOR]" },
          { text: "Collaborator", value: "[ROLE_COLLABORATOR]" },
          { text: "Registered", value: "[ROLE_REGISTEREDUSER]" }
        ],
        onFilter: (value, record) => record.profile.includes(value)
      },
      {
        title: "Phone",
        dataIndex: "phone",
        key: "phone"
      },
      {
        title: "Birth date",
        dataIndex: "birthDate",
        key: "birthDate"
      }
    ];
    if (this.state.usersFiltered.length === 0) {
      return (
        <Table
          rowKey={record => record.email}
          dataSource={this.props.users}
          columns={title}
          onChange={this.handleChange}
        />
      );
    }
    return (
      <div>
        <ContentHeader title="USERS" />
        <Table
          rowKey={record => record.email}
          dataSource={this.state.usersFiltered}
          columns={title}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users.users,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadedUsers,
      profileUpdated
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllUsersRedux);
