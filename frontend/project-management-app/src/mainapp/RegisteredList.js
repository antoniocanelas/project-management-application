import React, { Component } from "react";
import { Avatar, List } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAvatarColor } from "../util/Colors";
import "./RegisteredList.css";
import {loadRegisteredUsers} from "../actions/actions_user_array";


class RegisteredList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0,
      columns: []
    };

  }

  componentWillMount() {
     const {loadRegisteredUsers} =this.props;
      loadRegisteredUsers();

  }


  render() {
        return (
              <List
                className="listReg"
                itemLayout="Horizontal"
                dataSource={this.props.registeredusers}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={
                        <Avatar
                          className="user-avatar-circle2"
                          style={{ backgroundColor: getAvatarColor(item.name) }}
                        >
                          {item.name[0].toUpperCase()}
                        </Avatar>
                      }
                      title={item.name}
                      description={item.email}

                    />
                      <div>
                          {item.phone}
                      </div>

                  </List.Item>
                )}
              />
        );


    }
  }


const mapStateToProps = state => ({
    registeredusers:state.users.registeredusers,

});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
        loadRegisteredUsers
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisteredList);
