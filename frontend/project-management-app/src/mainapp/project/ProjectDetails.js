import React from "react";
import { Col, Row, Tag } from "antd";
import "./ProjectDetails.css";
import { timeTask } from "../task/task/TaskDetails";

export default function ProjectDetails({ project }) {
  return (
    <Row gutter={36}>
      <Col span={16}>
        <br />
        <br />
        <h3>Description: </h3>
        <p>{project.description}</p>
        <h3>Tasks Summary: </h3>
        <p>Total:<strong> {project.tasks}</strong><br/>
        Completed:<strong> {project.completedTasks}</strong><br/>
        Initiated:<strong> {project.initiatedButNotCompletedTasks}</strong><br/>
        Not Initiated:<strong> {project.notInitiatedTasks}</strong><br/>
        </p>
        <h3>Collaborators Summary: </h3>
        <p>Total:<strong> {project.collaborators}</strong><br/>
        Active:<strong> {project.collaborators}</strong><br/>
        Unassined:<strong> {project.unassignedCollaborators}</strong><br/>
        </p>
    
      </Col>
      <Col span={6}>
        <br />
        <br />
        <h2>
          Project Status:<span>
            {project.active ? (
              <Tag className="status" color="green">
                {" "}
                ACTIVE
              </Tag>
            ) : (
              <Tag color="red">INACTIVE</Tag>
            )}
          </span>
        </h2>
        <br />
        Project Manager Contact:
        <h3>{project.userEmail}</h3>
        <br />
        Project Unit:
        <h3>{project.unit}</h3>
        <br />
        {timeTask(null, project.startDate, null, project.endDate)}
      </Col>
    </Row>
  );
}
