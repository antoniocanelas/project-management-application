import { Tabs, Badge } from "antd";
import React, { Component } from "react";
import TaskList from "../task/TaskList";
import GetProjectCost from "./GetProjectCost";
import { withRouter } from "react-router-dom";
import ProjectDetails from "./ProjectDetails";
import CollaboratorsList from "./CollaboratorsList";
import { connect } from "react-redux";
import ManageRequests from "../../components/requests";
import "./ProjectTabs.css";

const TabPane = Tabs.TabPane;

class ProjectTabs extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.history.goBack();
  }

  render() {
    const project = this.props.project;
    const role = this.props.currentUser.profile;

    const isProjectManager = Boolean(this.props.isManager);

    return (
      <div className="tab-container">
        <Tabs defaultActiveKey="1">
          <TabPane tab="PROJECT INFORMATION" key="1">
            <div className="content-tab">
              {/* <TabHeader title="Project Details"/> */}
              <ProjectDetails project={project} />
            </div>
          </TabPane>

          <TabPane tab="TASKS" key="2">
            <div className="content-tab">
              {/* <div> */}
              {/* <TabHeader title="Tasks"/> */}
              <TaskList
                projectId={project.projectId}
                isProjectManager={isProjectManager}
                role={role}
              />
            </div>
            {/* </div> */}
          </TabPane>

          <TabPane tab="COLLABORATORS" key="3">
            <div className="content-tab">
              <CollaboratorsList />
            </div>
          </TabPane>

          {this.props.isManager ? (
            <TabPane tab="MANAGE PROJECT COST" key="4">
              <div className="content-tab">
                {/*<TabHeader title="Project Cost Options"/>*/}
                <GetProjectCost />
              </div>
            </TabPane>
          ) : null}

          {this.props.isManager ? (
            <TabPane
              tab={
                <span>
                  REQUESTS <Badge count={this.props.project.requests} />
                </span>
              }
              key="5"
            >
              <div className="content-tab">
                <ManageRequests />
              </div>
            </TabPane>
          ) : null}
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isManager: state.projects.isProjectManager,
  project: state.projects.selectedProject,
  currentUser: state.authentication.currentUser
});

export default withRouter(connect(mapStateToProps)(ProjectTabs));
