import React, { Component } from "react";
import "./CreateProjectForm.css";
import { Button, Checkbox, DatePicker, Form, Input, Modal, message, Select } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loadCollaborators } from "../../actions/actions_user_array";
import { listAvailableCostOptions } from "../../actions/projectActions";
import { createProject, setProjectCostOptions } from "../../util/APIUtils";

const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;

class CreateProjectForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      formValues: { dates: [], costOptions: [] }
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  componentWillMount() {
    const { loadCollaborators } = this.props;
    loadCollaborators();

    const { listAvailableCostOptions } = this.props;
    listAvailableCostOptions();
  }

  handleOk = e => {
    this.setState({
      visible: false
    });

    const values = {
      projectId: this.state.formValues.projectId,
      name: this.state.formValues.projectName,
      description: this.state.formValues.projectDescription,
      startDate: this.state.formValues.dates[0] + " 00:00:00",
      endDate: this.state.formValues.dates[1] + " 00:00:00",
      userEmail: this.state.formValues.projectManager
    };

    createProject(values)
      .then(response => {
        setProjectCostOptions(this.state.formValues.projectId, this.state.formValues.costOptions)
          .then(response => {
            this.props.history.push("/projects/director");
          })
          .catch(error => {
            message.error("Sorry! Invalid cost options. Please try again!");
          });
        message.success("Project was successfully created");
      })
      .catch(error => {
        message.error("Sorry! Something went wrong. Please try again!");
      });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.showModal();

      // Should format date value before submit.
      const rangeValue = fieldsValue["range-picker"];
      const projectID = fieldsValue["projectID"];
      const projectName = fieldsValue["projectName"];
      const projectDescription = fieldsValue["projectDescription"];
      const projectManager = fieldsValue["projectManager"];
      const values = {
        ...fieldsValue,
        dates: [rangeValue[0].format("YYYY-MM-DD"), rangeValue[1].format("YYYY-MM-DD")]
      };

      this.setState({ formValues: values });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    const options = this.props.projectCostOptions.availableCalculationOptions;

    const rangeConfig = {
      rules: [{ type: "array", required: true, message: "Please select start and end dates" }]
    };

    return (
      <Form className="createForm" onSubmit={this.handleSubmit}>
        <FormItem {...formItemLayout} label="Project ID">
          {getFieldDecorator("projectId", {
            rules: [
              {
                required: true,
                message: "Please input project id"
              }
            ]
          })(<Input placeholder="Please input project id" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Name">
          {getFieldDecorator("projectName", {
            rules: [
              {
                required: true,
                message: "Title between 4 and 30 characters",
                min: 4,
                max: 30
              }
            ]
          })(<Input placeholder="Please input project name" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Project description">
          {getFieldDecorator("projectDescription", {
            rules: [
              {
                required: false,
                message: "Please input project description text"
              }
            ]
          })(<Input placeholder="Please input project description text" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Start and End Date">
          {getFieldDecorator("range-picker", rangeConfig)(<RangePicker />)}
        </FormItem>

        <FormItem {...formItemLayout} label="Project Manager">
          {getFieldDecorator("projectManager", {
            rules: [
              {
                required: true,
                message: "Please input project manager name"
              }
            ]
          })(
            <Select
              showSearch
              style={{ width: 300 }}
              placeholder="Select a collaborator"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {this.props.collaborators.map(collab => (
                <Option key={collab.email} value={collab.email}>
                  {collab.name}
                  <small> ({collab.email})</small>
                </Option>
              ))}
            </Select>
          )}
        </FormItem>

        <FormItem {...formItemLayout} label="Cost Calculation Options">
          {getFieldDecorator("costOptions", {
            valuePropName: "checkeds",
            rules: [
              {
                required: true,
                message: "Please Select at least one cost"
              }
            ]
          })(<CheckboxGroup options={options} />)}
        </FormItem>

        <FormItem
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: { span: 16, offset: 8 }
          }}
        >
          <Button type="primary" htmlType="submit">
            Create
          </Button>
          <Modal
            title="Do you want to confirm this information?"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <p>
              <strong>Project ID:</strong> {this.state.formValues.projectId}
            </p>
            <p>
              <strong>Project Name:</strong> {this.state.formValues.projectName}
            </p>
            <p>
              <strong>Predictable Start:</strong> {this.state.formValues.dates[0]}
            </p>
            <p>
              <strong>Predictable Finish:</strong> {this.state.formValues.dates[1]}
            </p>
            <p>
              <strong>ProjectManager:</strong> {this.state.formValues.projectManager}
            </p>
            <p>
              <strong>Available Cost Calculation:</strong>
              <br />
              {this.state.formValues.costOptions.map(cost => (
                <span key={cost} style={{ padding: 50 }}>
                  - {cost}
                  <br />
                </span>
              ))}
            </p>
          </Modal>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  collaborators: state.users.collaborators,
  projectCostOptions: state.projects.projectCostOptions
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadCollaborators, listAvailableCostOptions }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CreateProjectForm)
);
