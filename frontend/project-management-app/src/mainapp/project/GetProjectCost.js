import React, { Component } from "react";
import {
  getTaskCosts,
  loadCostOptions,
  loadCurrentProjectCost,
  selectCostOption
} from "../../actions/projectActions";
import { connect } from "react-redux";
import { BackTop, Card, Col, Progress, Radio, Row, Table, Select } from "antd";
import { bindActionCreators } from "redux";
import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from "recharts";

const RadioGroup = Radio.Group;
const Option = Select.Option;

class GetProjectCost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }

  columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Cost: (" + this.props.project.unit + ")",
      dataIndex: "cost",
      key: "cost"
    }
  ];

  componentWillMount() {
    const { loadCostOptions, loadCurrentProjectCost, getTaskCosts } = this.props;
    loadCostOptions(this.props.project.projectId);
    loadCurrentProjectCost(this.props.project.projectId);
    getTaskCosts(this.props.project.projectId);
  }

  onChange = value => {
    this.setState({
      value: value
    });
    const { selectCostOption } = this.props;
    selectCostOption(this.props.project.projectId, value);
  };

  render() {
    let percentDone =
      100 -
      Math.floor(
        ((this.props.project.globalBudget - this.props.cost) / this.props.project.globalBudget) *
          100
      );

    return (
      <div>
        <Row type="flex" justify="space-between">
          <Col span={12} style={{paddingLeft:18, marginTop:30}}>
            <h3>Selected Option Cost: </h3>
            {/* <RadioGroup
              onChange={this.onChange}
              value={this.state.value}
              options={this.props.options}
              style={{ marginBottom: 12 }}

            /> */}
             <Select defaultValue={this.props.project.costOptionSelected} style={{ width: 240 }} onChange={this.onChange}>
            {this.props.options.map(opt => (
              <Option value={opt} key={opt}>{opt}</Option>))}
            </Select>
            <br />
            <br />
              <div>
                  Global Budget:{" "}
                  <b>
                      {this.props.project.globalBudget} {this.props.project.unit}
                  </b>
                  <br />
                  Current Total Cost:{" "}
                  <b>
                      {this.props.cost} {this.props.project.unit}
                  </b>
              </div>
          </Col>

          <Col span={12} style={{paddingLeft:190}}>
            <br/>
            <Progress type="circle" percent={percentDone} />
          </Col>
        </Row>

        <Row type="flex" justify="space-between">
          <Col span={10}>
            <br />
            <br />
            <Table columns={this.columns} dataSource={this.props.tasksCost} pagination={false} rowKey={record => record.taskId} />
            <br />

            <BackTop>
              <div className="ant-back-top-inner">UP</div>
            </BackTop>
          </Col>
          <Col span={14}>
            <br />
            <br />
            <br />

            <BarChart
              width={600}
              height={300}
              data={this.props.tasksCost}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="title" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="cost" fill={"#2035d8"} />
            </BarChart>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isManager: state.projects.isProjectManager,
  project: state.projects.selectedProject,
  cost: state.projects.currentCost,
  options: state.projects.availableCostOptions,
  tasksCost: state.projects.projectTasksCostList
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { loadCurrentProjectCost, loadCostOptions, selectCostOption, getTaskCosts },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GetProjectCost);
