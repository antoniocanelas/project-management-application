import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import ContentHeader from "../../templates/contentHeader";
import ProjecTabs from "./ProjectTabs";
import "./Project.css";

class Project extends Component {
  render() {
    const result = `(ID: ${this.props.selectedProject.projectId})`;
    return (
      <div>
        <ContentHeader title={"Project: " + this.props.selectedProject.name} subtitle={result} />
        <ProjecTabs />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject
});

export default withRouter(connect(mapStateToProps)(Project));
