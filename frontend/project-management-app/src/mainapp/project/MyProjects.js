import React, { Component } from "react";
import "./MyProjects.css";
import { Icon, Popover, Table, Alert } from "antd";
import ContentHeader from "../../templates/contentHeader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProjectById, getProjects } from "../../actions/projectActions";
import { withRouter } from "react-router-dom";
import { recordslice } from "../task/task/TaskDetails";

class MyProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: []
    };
  }

  componentWillMount() {
    const { getProjects } = this.props;
    getProjects(this.props.currentUser.email);

    const columnsCollaborator = [
      {
        title: "Title",
        dataIndex: "name",
        key: "name",
        width: 200
      },
      {
        title: "Project Manager",
        dataIndex: "userEmail",
        key: "userEmail"
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Start Date",
        dataIndex: "startDate",
        key: "startDate",
        render: (text, record) => recordslice(record.startDate)
      },
      {
        title: "Final Date",
        dataIndex: "finalDate",
        key: "finalDate",
        render: (text, record) => recordslice(record.finalDate)
      },
      {
        title: "Action",
        dataIndex: "",
        key: "action",
        render: (text, record) => (
          <Popover content="View Details">
            <button onClick={() => this.viewDetails(record.projectId)} value={record.projectId}>
              <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
            </button>
          </Popover>
        )
      }
    ];

    this.setState({
      columns: columnsCollaborator
    });
  }

  viewDetails(projectId) {
    const { getProjectById } = this.props;
    getProjectById(projectId, this.props.currentUser.email);
    this.props.history.push("/projects/selected")  }

  seeList(param) {
    return (
      <div>
        <ContentHeader title="MY PROJECTS" subtitle="As Collaborator" />
        {param.length === 0 ? (
          <Alert message="You have no Projects as Project Collaborator" type="info" showIcon />
        ) : (
          <Table
            rowKey={record => record.projectId}
            dataSource={this.props.myProjects}
            columns={this.state.columns}
          />
        )}
      </div>
    );
  }



  render() {
    return <div>{this.seeList(this.props.myProjects)}</div>;
  }
}

const mapStateToProps = state => ({
  currentUser: state.authentication.currentUser,
  myProjects: state.projects.projectsAsCollaborator
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ getProjects, getProjectById }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MyProjects)
);
