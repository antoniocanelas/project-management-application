import React, { Component } from "react";
import { Button, Form, Icon, InputNumber, Mention, Modal, Select } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  addProjectCollaborator,
  loadedProjectCollaboratorsListNotInProject
} from "../../../actions/projectActions";
import { bindActionCreators } from "redux";
import "./AddProjectCollaborator.css";

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
  class extends Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;

      return (
        <Modal
          visible={visible}
          title="Add Project Collaborator"
          okText="Add"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Project Collaborator">
              {getFieldDecorator("email", {
                rules: [
                  {
                    required: true,
                    message: "Please input project collaborator"
                  }
                ]
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a collaborator"
                  optionFilterProp="children"
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                  filterOption={(input, option) =>
                    option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.props.collaboratorsNotInProject.map(collab => (
                    <Option key={collab.email} value={collab.email}>
                      {collab.name}
                      <small> ({collab.email})</small>
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Collaborator Cost">
              {getFieldDecorator("cost", {
                rules: [
                  {
                    required: true,
                    message: "Please input project collaborator cost"
                  }
                ]
              })(<InputNumber style={{ width: 200 }} min={0} type="textarea" />)}
            </FormItem>
            <FormItem label="Project Unit">
              <Mention style={{ width: 200 }} placeholder={this.props.projectUnit} disabled />
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

class AddProjectCollaborator extends React.Component {
  state = {
    visible: false
  };
  showModal = () => {
    const { loadedProjectCollaboratorsListNotInProject } = this.props;
    this.setState({ visible: true });
    loadedProjectCollaboratorsListNotInProject(this.props.selectedProject.projectId);
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };
  handleCreate = () => {
    const { addProjectCollaborator } = this.props;

    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      addProjectCollaborator(this.props.selectedProject.projectId, values.email, values.cost);
      console.log("Received values of form: ", values);

      form.resetFields();
      this.setState({ visible: false });

      // confirm({
      //     title: 'Do you want add collaborator to this project?',
      //     content: (
      //         <div>
      //             <br/>
      //             <p><strong>Project ID:</strong> {this.props.selectedProject.projectId}</p>
      //             <p><strong>Project Collaborator:</strong> {values.email}</p>
      //             <p><strong>Collaborator Cost:</strong> {values.cost}</p>
      //             <p><strong>Project Unit:</strong> {this.props.selectedProject.unit}</p>
      //         </div>
      //     ),
      //     onOk() {
      //         console.log('OK');
      //     },
      //     onCancel() {
      //         console.log('Cancel');
      //     },
      // });
    });
  };
  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button className="button-addCollab" type="primary" onClick={this.showModal}>
          <Icon type="user-add" />Add Project Collaborator
        </Button>
        <br />
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          collaboratorsNotInProject={this.props.collaboratorsNotInProject}
          projectUnit={this.props.selectedProject.unit}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  collaboratorsNotInProject: state.projects.projectsCollaboratorNotInProjectList
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { loadedProjectCollaboratorsListNotInProject, addProjectCollaborator },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddProjectCollaborator)
);
