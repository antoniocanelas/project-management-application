import React, { Component } from "react";
import TaskTabs from "./TaskTabs";
import "./Task.css";
import TaskState from "../taskstate/TaskState";
import ContentHeader from "../../../templates/contentHeader";

import { connect } from "react-redux";


class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  


  render() {
      const result = `(ID: ${this.props.selectedTask.taskId})`;

      return (
      <div>
          <ContentHeader title={ "Task: " + this.props.selectedTask.title} subtitle={result} />
        <br />
        <TaskState />
        <p />
        <TaskTabs />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});

export default connect(
  mapStateToProps,
)(Task);
