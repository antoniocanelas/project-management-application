import React from "react";
import { Timeline } from "antd";

export function timeTask(
  predStartDate,
  effectiveStartDate,
  preditctedConclusionDate,
  effectiveConclusionDate
) {
  return (
    <span>
      <Timeline>
        {predStartDate ? (
          <Timeline.Item color="#949a97" style={{ color: "#949a97" }}>
            Predicted Start Date {recordslice(predStartDate)}
          </Timeline.Item>
        ) : null}
        {effectiveStartDate ? (
          <Timeline.Item color="green">
            Effective Start Date {recordslice(effectiveStartDate)}
          </Timeline.Item>
        ) : null}
        {preditctedConclusionDate ? (
          <Timeline.Item color="#949a97" style={{ color: "#949a97" }}>
            Predicted Conclusion date {recordslice(preditctedConclusionDate)}
          </Timeline.Item>
        ) : null}
        {effectiveConclusionDate ? (
          <Timeline.Item color="green">
            EffectiveConclusion date {recordslice(effectiveConclusionDate)}
          </Timeline.Item>
        ) : null}
      </Timeline>
    </span>
  );
}

export function recordslice(record) {
  if (!record) {
    return;
  }
  return record.slice(0, 10);
}
