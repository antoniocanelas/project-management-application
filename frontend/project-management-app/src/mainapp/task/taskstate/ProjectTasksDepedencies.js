import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Chart } from "react-google-charts";

class ProjectTasksDepedencies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  columns = [
    { id: "Task ID", type: "string" },
    { id: "Task Name", type: "string" },
    { id: "Start Date", type: "date" },
    { id: "End Date", type: "date" },
    { id: "Duration", type: "number" },
    { id: "Percent Complete", type: "number" },
    { id: "Dependencies", type: "string" }
  ];

  endDate(task) {
    if (task.effectiveDateOfConclusion) {
      return new Date(task.effectiveDateOfConclusion);
    } else if (task.predictedDateOfConclusion) {
      return new Date(task.predictedDateOfConclusion);
    } else {
      return null;
    }
  }

  startDate(task) {
    if (task.effectiveStartDate) {
      return new Date(task.effectiveStartDate);
    } else if (task.predictedDateOfStart) {
      return new Date(task.predictedDateOfStart);
    } else {
      return null;
    }
  }

  progress(task) {
    if (task.state === "Completed") return 100;
    else if (task.state === "InProgress") {
      const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
      const totalDays = Math.round(
        Math.abs((this.endDate(task).getTime() - this.startDate(task).getTime()) / oneDay)
      );
      const daysLeft = Math.round((this.endDate(task).getTime() - Date.now()) / oneDay);
      const percentDays = Math.floor(((totalDays - daysLeft) / totalDays) * 100);
      return percentDays;
    } else {
      return 0;
    }
  }

  projectTaskDependencies() {
    let dependencies = [];

    for (let i = 0; i < this.props.tasks.length; i++) {
      let transformed = [
        this.props.tasks[i].taskId,
        this.props.tasks[i].title,
        this.startDate(this.props.tasks[i]),
        this.endDate(this.props.tasks[i]),
        null,
        this.progress(this.props.tasks[i]),
        this.props.tasks[i].ganttTaskDependencies
      ];
      dependencies.push(transformed);
    }
    return dependencies;
  }

  render() {
    return (
      <div>
        <Chart
          chartType="Gantt"
          columns={this.columns}
          rows={this.projectTaskDependencies()}
          width="100%"
          options={{
            gantt: {
              labelStyle: {
                fontName: "Arial",
                fontSize: 16
              },
              barCornerRadius: 5,
              barHeight: 15,
              criticalPathEnabled: false, // Critical path arrows will be the same as other arrows.
              arrow: {
                angle: 30,
                width: 2,
                color: "#f6780f",
                radius: 25,
                spaceAfter: 0
              },
              innerGridHorizLine: {
                stroke: "#949495",
                strokeWidth: 0
              }
            }
          }}
          chartPackages={["gantt"]}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task,
  tasks: state.tasks.tasks
});

export default withRouter(connect(mapStateToProps)(ProjectTasksDepedencies));
