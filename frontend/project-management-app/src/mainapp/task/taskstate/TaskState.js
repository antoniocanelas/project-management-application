import React, { Component } from "react";
import {Icon, Steps, Tag} from "antd";
import { connect } from "react-redux";
import "./TaskState.css";

const Step = Steps.Step;

class TaskState extends Component {
  taskStateProgress(state) {
    if (state === 6) {
      return <h1 className="state-title-susp">Suspended</h1>
    } else if (state === 5) {
      return  <h1 className="state-title">Cancelled</h1>

    } else {
      return (
        <Steps current={state}>
          <Step title="Created/Planned" />
          <Step title="Assigned" />
          <Step title="Ready to Start" />
          <Step title="InProgress" />
          <Step title="Completed" />
        </Steps>
      );
    }
  }

  stateSwitch(param) {
    switch (param) {
      case "Created":
        return 0;
      case "Planned":
        return 0;
      case "Assigned":
        return 1;
      case "ReadyToStart":
        return 2;
      case "InProgress":
        return 3;
      case "Completed":
        return 4;
      case "Cancelled":
        return 5;
      case "Suspended":
        return 6;
      default:
        return 0;
    }
  }

  render() {
    return (
      <div>
        {this.taskStateProgress(this.stateSwitch(this.props.selectedTask.state))}
        <br />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedTask: state.task.task
});

export default connect(mapStateToProps)(TaskState);
