import React, { Component } from "react";
import { Alert, Icon, message, Popconfirm, Popover, Table, Tag } from "antd";
import "./SelfPendingTasks.css";
import { bindActionCreators } from "redux";
import { loadedCollaboratorPendingTasks } from "../../../actions/actions_task_array";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadTask, requestTaskCompleted, makeRemoval } from "../../../actions/actions_task";
import TaskProgress from "../taskprogress/TaskProgress";
import AddReport2 from "../../reports/AddReport2";
import { colorChangeState } from "../../../util/Helpers";

class SelfPendingTasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0,
      visible: false
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    const { loadedCollaboratorPendingTasks } = this.props;
    loadedCollaboratorPendingTasks(this.props.currentUser.email);
  }

  columns = [
    {
      title: "Project ID",
      dataIndex: "projectId",
      key: "projectId"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 200
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",
      filters: [
        {
          text: "ReadyToStart",
          value: "ReadyToStart"
        },
        {
          text: "InProgress",
          value: "InProgress"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      render: (text, record) => <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
    },
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Completed Request",
      dataIndex: "completedRequest",
      key: "completedRequest"
    },
    {
      title: "Actions",
      children: [
        {
          title: "",
          dataIndex: "",
          key: "info",
          width: 10,
          render: (text, record) => (
            <span>
              <Popover content="View Details">
                <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
                  <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
                </button>
              </Popover>
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "addReport",
          width: 10,

          render: (text, record) => (
            <span>
              <AddReport2
                value={record.taskId}
                projectIdPending={record.projectId}
                taskSelected={this.addReport}
              />
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "completed",
          width: 10,

          render: (text, record) => (
            <span>
              {this.getLinksRequestMarkCompleted(
                record.links,
                record.taskId,
                record.completedRequest
              )}
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "requestRemove",
          width: 10,

          render: (text, record) => (
            <span>{this.getLinksRequestRemoval(record.links, record.taskId)}</span>
          )
        }
      ]
    }
  ];

  addReport = e => {
    e.preventDefault();
    const { loadTask } = this.props;
    loadTask(e.target.value);
    this.setState({ option: 0 });
  };

  getLinksRequestMarkCompleted(links, taskId, requestTaskCompleted) {
    if (links.some(e => e.rel === "Request Set Task Completed") && !requestTaskCompleted) {
      return (
        <div>
          <Popconfirm
            title="Are you sure mark this task completed?"
            onConfirm={() => {
              this.onSubmit(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Mark Task Completed">
              <button>
                <Icon type="check-circle" style={{ fontSize: 20, color: "#9ea624" }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksRequestRemoval(links, taskId) {
    if (links.some(e => e.rel === "Request Remove Collaborator")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure send request to be removed from this task?"
            onConfirm={() => {
              this.requestRemove(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Removal">
              <button>
                <Icon type="user-delete" style={{ fontSize: 25 }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  onSubmit(taskId) {
    const { requestTaskCompleted } = this.props;
    requestTaskCompleted(taskId, this.props.currentUser.email);
  }

  viewDetails(taskId) {
    const { loadTask } = this.props;
    loadTask(taskId);
    this.props.history.push("/tasks/selected");
  }

  requestRemove(taskId) {
    const { makeRemoval } = this.props;
    makeRemoval(taskId, this.props.currentUser.email);
  }

  renderSwitch(param) {
    switch (param) {
      case 0:
        if (this.props.tasks.length === 0) {
          return (
            <div>
              <br />
              <Alert message="You have no pending tasks!" type="info" showIcon />
            </div>
          );
        } else {
          return (
            <div>
              <br />
              <br />
              <Table
                rowKey={record => record.taskId}
                dataSource={this.props.tasks}
                columns={this.columns}
              />
            </div>
          );
        }
      case 1:
        return this.props.history.push("/tasks/selected");

      default:
        return <div />;
    }
  }

  render() {
    return (
      <div>
        <div>{this.renderSwitch(this.state.option)}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks.collaboratorPendingTasks,
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { loadedCollaboratorPendingTasks, loadTask, requestTaskCompleted, makeRemoval },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SelfPendingTasks)
);
