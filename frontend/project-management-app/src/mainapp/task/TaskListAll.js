import React, { Component } from "react";
import { Alert, Icon, Popconfirm, Popover, Table, Tag } from "antd";
import { loadedProjectTasks } from "../../actions/actions_task_array";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TaskProgress from "./taskprogress/TaskProgress";
import { loadTask, makeAssignment, makeRemoval } from "../../actions/actions_task";
import { withRouter } from "react-router-dom";
import { message } from "antd/lib/index";
import { colorChangeState } from "../../util/Helpers";
import AddReport2 from "../reports/AddReport2";
import "./TaskListAll.css";
import { recordslice } from "../task/task/TaskDetails";

class TaskListAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: 0
    };
  }

  componentWillMount() {
    const { loadedProjectTasks } = this.props;
    loadedProjectTasks(this.props.selectedProject.projectId);
  }

  viewDetails(taskId) {
    const { loadTask } = this.props;
    loadTask(taskId);
    this.props.history.push("/tasks/selected");
  }

  title = [
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      width: 200
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",

      filters: [
        {
          text: "Created",
          value: "Created"
        },
        {
          text: "Planned",
          value: "Planned"
        },
        {
          text: "Assigned",
          value: "Assigned"
        },
        {
          text: "ReadyToStart",
          value: "ReadyToStart"
        },
        {
          text: "InProgress",
          value: "InProgress"
        },
        {
          text: "Completed",
          value: "Completed"
        },
        {
          text: "Cancelled",
          value: "Cancelled"
        },
        {
          text: "Suspended",
          value: "Suspended"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      render: (text, record) => <Tag color={colorChangeState(record.state)}>{record.state} </Tag>
    },
    {
      title: "Effective start date",
      dataIndex: "effectiveStartDate",
      key: "effectiveStartDate",
      width: 200,
      render: (text, record) => recordslice(record.effectiveStartDate)
    },
    {
      title: "Effective conclusion date",
      dataIndex: "effectiveDateOfConclusion",
      key: "effectiveDateOfConclusion",
      width: 180,
      render: (text, record) => recordslice(record.effectiveDateOfConclusion)
    },
    {
      title: "Actions",
      children: [
        {
          title: "",
          dataIndex: "",
          key: "viewDetails",
          width: 100,

          render: (text, record) => (
            <span>
              <Popover content="View Details">
                <button onClick={() => this.viewDetails(record.taskId)} value={record.taskId}>
                  <Icon type="info-circle" style={{ fontSize: 20, color: "#08c" }} />
                </button>
              </Popover>
            </span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "requestAdd",
          width: 5,
          render: (text, record) => (
            <span>{this.isTaskCollaborator(record.links, record.taskId, record.projectId)}</span>
          )
        },
        {
          title: "",
          dataIndex: "",
          key: "requestRemove",
          width: 10,

          render: (text, record) => (
            <span>{this.getLinksRequestRemoval(record.links, record.taskId)}</span>
          )
        }
      ]
    }
  ];

  isTaskCollaborator(links, taskId, projectId) {
    if (links.some(e => e.rel === "Request Add Collaborator")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure send request to assign this task ?"
            onConfirm={() => {
              this.sendRequest(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Assignment">
              <button>
                <Icon type="user-add" style={{ fontSize: 25 }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    }

    if (links.some(e => e.rel === "Add Report")) {
      return (
        <div>
          <AddReport2 projectIdPending={projectId} value={taskId} />
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksRequestRemoval(links, taskId) {
    if (links.some(e => e.rel === "Request Remove Collaborator")) {
      return (
        <div>
          <Popconfirm
            title="Are you sure send request to be removed from this task?"
            onConfirm={() => {
              this.requestRemove(taskId);
            }}
            onCancel={() => {
              message.error("You cancel this option");
            }}
            okText="Yes"
            cancelText="No"
          >
            <Popover content="Request Removal">
              <button>
                <Icon type="user-delete" style={{ fontSize: 25 }} />
              </button>
            </Popover>
          </Popconfirm>
        </div>
      );
    } else {
      return null;
    }
  }

  getLinksAddReport(links, taskId, projectIdPending) {
    if (links.some(e => e.rel === "Add Report")) {
      return (
        <div>
          <AddReport2
            value={taskId}
            projectIdPending={projectIdPending}
            taskSelected={this.addReport}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  sendRequest(taskId) {
    const { makeAssignment } = this.props;

    makeAssignment(taskId, this.props.currentUser.email);
  }
  requestRemove(taskId) {
    const { makeRemoval } = this.props;
    makeRemoval(taskId, this.props.currentUser.email);
  }

  render() {
    if (this.props.tasks.length === 0) {
      return <Alert message="There are no tasks to display" type="info" showIcon />;
    } else {
      return (
        <div>
          <Table
            rowKey={record => record.taskId}
            dataSource={this.props.tasks}
            footer={() => <div />}
            columns={this.title}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  selectedProject: state.projects.selectedProject,
  tasks: state.tasks.tasks,
  selectedTask: state.task.task,
  currentUser: state.authentication.currentUser
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadedProjectTasks, loadTask, makeAssignment, makeRemoval }, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TaskListAll)
);
