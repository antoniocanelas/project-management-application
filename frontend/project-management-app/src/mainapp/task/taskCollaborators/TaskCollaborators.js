import React, {Component} from "react";
import {Icon, message, Popconfirm, Popover, Radio, Table} from "antd";

import ContentHeader from "../../../templates/contentHeader";
import {
    loadAllTaskCollaborators,
    loadAvailableTaskCollaborators,
    loadLinks,
    loadTaskCollaborators,
    removeCollaboratorFromTask
} from "../../../actions/actions_task";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import AddTaskCollaborator from "./AddTaskCollaborator";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class TaskCollaborators extends Component {
    constructor(props) {
        super(props);
        this.state = {
            option: 0,
            collabs: []
        };
    }

    componentWillMount() {
        const {loadTaskCollaborators} = this.props;
        loadTaskCollaborators(this.props.selectedTask.taskId);

        const {loadAllTaskCollaborators} = this.props;
        loadAllTaskCollaborators(this.props.selectedTask.taskId);

        const {loadLinks} = this.props;
        loadLinks(this.props.selectedTask.taskId);

        const {loadAvailableTaskCollaborators} = this.props;
        loadAvailableTaskCollaborators(this.props.selectedTask.taskId);
    }

    title = [
        {
            title: "Id",
            dataIndex: "projectCollaboratorId",
            key: "projectCollaboratorId"
        },
        {
            title: "Name",
            dataIndex: "projectCollaboratorName",
            key: "projectCollaboratorName"
        },
        {
            title: "Email",
            dataIndex: "projectCollaboratorEmail",
            key: "projectCollaboratorEmail"
        },
        {
            title: "Added To Task Date",
            dataIndex: "collaboratorAddedToTaskDate",
            key: "collaboratorAddedToTaskDate",
            render: (text, record) => this.recordslice(record.collaboratorAddedToTaskDate)
        },
        {
            title: "Removed From Task Date",
            dataIndex: "collaboratorRemovedFromTaskDate",
            key: "collaboratorRemovedFromTaskDate",
            render: (text, record) => this.recordslice(record.collaboratorRemovedFromTaskDate)
        },
        {
            title: "Registry Status",
            dataIndex: "registryStatus",
            key: "registryStatus"
        },

        {
            title: "Action",
            dataIndex: "remove",
            key: "remove",

            render: (text, record) => {
                if (this.props.selectedTask._links && this.props.selectedTask._links["Remove Collaborator"]) {
                    return (
                        <div>
                            <Popconfirm
                                title="Remove Collaborator from Task?"
                                onConfirm={() => this.removeCollab(record.projectCollaboratorEmail)}
                                onCancel={() => {
                                    message.error("You cancel this option");
                                }}
                                okText="Yes"
                                cancelText="No"
                            >
                                <Popover content="Remove Collaborator">
                                    <button>
                                        <Icon type="user-delete" style={{fontSize: 20}}/>{" "}
                                    </button>
                                </Popover>
                            </Popconfirm>
                        </div>
                    );
                }
            }
        } //<Popconfirm title="Are you sure send request to assigonConfirm={ () => this.removeCollab(record.projectCollaboratorEmail)} onCancel={()=>{message.error('You cancel this option')}} okText="Yes" cancelText="No">
    ];

    recordslice(record) {
        if (!record) {
            return;
        }
        return record.slice(0, 10);
    }

    onChange = e => {
        if (e.target.value === "all") {
            this.setState({option: 1});
        } else {
            this.setState({option: 0});
        }
    };

    removeCollab(email) {
        const {removeCollaboratorFromTask} = this.props;
        removeCollaboratorFromTask(this.props.selectedTask.taskId, email,this.props.selectedProject.projectId, this.props.currentUser.email);
    }

    renderSwitch(param) {
        let button;

        if (this.props.links && this.props.links["Add Collaborator"]) {
            button = <AddTaskCollaborator taskId={this.props.selectedTask.taskId}/>;
        } else button = null;
        switch (param) {
            case 0:
                return (
                    <div>
                        {button}

                        <ContentHeader title="Collaborators List"/>
                        <div className="radio-filter">
                            <RadioGroup onChange={this.onChange} defaultValue="active">
                                <RadioButton value="active">Active</RadioButton>
                                <RadioButton value="all">All</RadioButton>
                            </RadioGroup>
                        </div>

                        <br/>
                        <Table
                            rowKey={record => record.projectCollaboratorId}
                            dataSource={this.props.taskCollaborators}
                            columns={this.title}
                        />
                    </div>
                );

            case 1:
                return (
                    <div>
                        {button}

                        <ContentHeader title="Collaborators List"/>

                        <div className="radio-filter">
                            <RadioGroup onChange={this.onChange} defaultValue="active">
                                <RadioButton value="active">Active</RadioButton>
                                <RadioButton value="all">All</RadioButton>
                            </RadioGroup>
                        </div>

                        <br/>
                        <Table
                            rowKey={record => record.projectCollaboratorId}
                            dataSource={this.props.allTaskCollaborators}
                            columns={this.title}
                        />
                    </div>
                );
            default:
                return <div/>;
        }
    }

    render() {
        return this.renderSwitch(this.state.option);
    }
}

const mapStateToProps = state => ({
    selectedTask: state.task.task,
    collaboratorsNotInTask: state.task.taskAvailableCollaborators,
    links: state.task.links,
    taskCollaborators: state.task.taskCollaborators,
    allTaskCollaborators: state.task.allTaskCollaborators,
    currentUser: state.authentication.currentUser,
    selectedProject: state.projects.selectedProject
});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            loadLinks,
            loadAvailableTaskCollaborators,
            loadTaskCollaborators,
            loadAllTaskCollaborators,
            removeCollaboratorFromTask
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TaskCollaborators);
