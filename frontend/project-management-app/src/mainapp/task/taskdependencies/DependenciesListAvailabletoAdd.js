import React, { Component } from "react";
import { Button, Icon, Table } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TaskProgress from "../taskprogress/TaskProgress";
import { loadedTasksDependenciesToAdd } from "../../../actions/actions_task_array";
import { loadAddDependencies, loadTask } from "../../../actions/actions_task";

class DependenciesListAvailabletoAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: []
    };
    this.addCheckedDependencies = this.addCheckedDependencies.bind(this);
  }

  componentWillMount() {
    const { loadTask } = this.props;
    loadTask(this.props.taskid);

    const { loadedTasksDependenciesToAdd } = this.props;
    loadedTasksDependenciesToAdd(this.props.taskid);
  }

  title = [
    {
      title: "Progress",
      dataIndex: "",
      render: (text, record) => <TaskProgress taskProgress={record} />
    },
    {
      title: "TaskId",
      dataIndex: "taskId",
      key: "taskId"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state"
    },
    {
      title: "predictedDateOfConclusion",
      dataIndex: "predictedDateOfConclusion",
      key: "predictedDateOfConclusion"
    }
  ];

  selectRow = record => {
    const selectedRowKeys = [...this.state.selectedRowKeys];
    if (selectedRowKeys.indexOf(record.key) >= 0) {
      selectedRowKeys.splice(selectedRowKeys.indexOf(record.key), 1);
    } else {
      selectedRowKeys.push(record.key);
    }
    this.setState({ selectedRowKeys });
  };
  onSelectedRowKeysChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  addCheckedDependencies(event) {
    event.preventDefault();
    const { loadAddDependencies } = this.props;
    loadAddDependencies(this.props.taskid, this.state.selectedRowKeys);
  }

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectedRowKeysChange
    };
    console.log(this.state.selectedRowKeys);

    if (
      this.props.task.state === "Completed" ||
      this.props.task.state === "Canceled" ||
      this.props.task.state === "Suspended"
    ) {
      return;
    } else {
      console.log(this.props.task.taskid);

      return (
        <div>
          <Table
            rowKey={record => record.taskId}
            rowSelection={rowSelection}
            dataSource={this.props.tasks}
            columns={this.title}
            onRow={record => ({
              onClick: () => {
                this.selectRow(record);
              }
            })}
          />
          <Button type="primary" onClick={this.addCheckedDependencies}>
            <Icon type="plus-circle-o" />Add Dependencies
          </Button>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks.dependenciesToAdd,
  currentUser: state.authentication.currentUser,
  task: state.task.task
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ loadedTasksDependenciesToAdd, loadAddDependencies, loadTask }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DependenciesListAvailabletoAdd);
