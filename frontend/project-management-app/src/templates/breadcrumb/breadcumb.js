import React from 'react'
import {Breadcrumb, Icon} from 'antd';

import './breadcrumb.css'


export default props => (
    <section className='top-breadcrumb'>
        <Breadcrumb>
            <Breadcrumb.Item href="">
                <Icon type="home" />
            </Breadcrumb.Item>
            <Breadcrumb.Item href="">
                <Icon type="user" />
                <span>Application List</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                Application
            </Breadcrumb.Item>
        </Breadcrumb>
    </section>
    
)


