import React from 'react'
import {Button, Icon} from 'antd';

import './goBackButton.css'

const ButtonGroup = Button.Group;

export default props => (
        <Button type="primary" onClick={() => props.onClick()}>
            <Icon type="left" />{props.name}
        </Button>
    
)