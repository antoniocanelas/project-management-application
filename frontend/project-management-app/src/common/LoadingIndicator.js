import React from 'react';
import {Icon, Spin} from 'antd';

export default function LoadingIndicator(props) {
    const antIcon = <Icon type="loading-3-quarters" spin />;
    return (<div style = {{textAlign: 'center', marginTop: '10em'}}>
        <Spin indicator={antIcon} size="large" tip={props.tip} spinning={props.spinning}/>
    </div>                                    
        
    );

   
}