import React, { Component } from "react";
import "./App.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import AppHeader from "../components/header";
import LoadingIndicator from "../common/LoadingIndicator";
import AppFooter from "../components/footer/AppFooter";
import { Layout } from "antd";
import { MainRoutes } from "../routes/MainRoutes";

class App extends Component {
  render() {
    if (this.props.isLoading) {
      return <LoadingIndicator />;
    }

    return (
      <Layout>
        <AppHeader />
        <MainRoutes />
        <AppFooter />
      </Layout>
    );
  }
}

const mapStateToProps = state => ({ isLoading: state.authentication.isLoading });

export default withRouter(connect(mapStateToProps)(App));
