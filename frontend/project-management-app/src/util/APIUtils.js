import { ACCESS_TOKEN, API_BASE_URL } from "../constants";

export const request = options => {
  const headers = new Headers({
    "Content-Type": "application/json"
  });

  if (localStorage.getItem(ACCESS_TOKEN)) {
    headers.append("Authorization", "Bearer " + localStorage.getItem(ACCESS_TOKEN));
  }

  const defaults = { headers: headers };
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options).then(response =>
    response.json().then(json => {
      if (response.ok) {
        return json;
      }
      return Promise.reject(json);
    })
  );
};

export function listUsersByProfile(profile) {
  return request({
    url: API_BASE_URL + "/users?profile=ROLE_" + profile,
    method: "GET"
  });
}

export function listUsers() {
  return request({
    url: API_BASE_URL + "/users",
    method: "GET"
  });
}

export function listprojects(useremail) {
  return request({
    url: API_BASE_URL + "/users/" + useremail + "/projects",
    method: "GET"
  });
}

export function listTasks(useremail) {
  return request({
    url: API_BASE_URL + "/users/" + useremail + "/tasks/pending",
    method: "GET"
  });
}

export function listAllTasks(useremail) {
  return request({
    url: API_BASE_URL + "/users/" + useremail + "/tasks",
    method: "GET"
  });
}

export function removeTask(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId,
    method: "DELETE"
  });
}

export function completedTask(taskId, projectId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/approvecompleted",
    method: "PUT"
  });
}

export function cancelTask(taskId, projectId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/canceled",
    method: "PUT"
  });
}

export function listProjectCompletedTasks(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/completed",
    method: "GET"
  });
}

export function listProjectCompletedTasksLastMonth(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/completedlastmonth",
    method: "GET"
  });
}

export function listProjectInitiatedButNotCompletedTasks(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/initiated-not-completed",
    method: "GET"
  });
}

export function listProjectCancelledTasks(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/cancelled",
    method: "GET"
  });
}

export function listProjectNotCompletedTasksAndWithPredictedDateOfConclusionExpiredTasks(
  projectId
) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/overdue",
    method: "GET"
  });
}

export function listCompletedTasks(useremail) {
  return request({
    url: API_BASE_URL + "/users/" + useremail + "/tasks/completed",
    method: "GET"
  });
}

export function listCompletedTasksLastMonth(useremail) {
  return request({
    url: API_BASE_URL + "/users/" + useremail + "/tasks/completedlastmonth",
    method: "GET"
  });
}

export function listDependenciesTasks(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/dependencies",
    method: "GET"
  });
}

export function addDependenciestoTask(taskId, addDependencies) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/dependenciestoadd",
    method: "POST",
    body: JSON.stringify({
      taskId: taskId,
      dependenciesId: addDependencies
    })
  });
}

export function listTasksThatCanBeAddedAsDependencies(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/dependenciestoadd",
    method: "GET"
  });
}

export function requestMarkTaskCompleted(taskId, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/request-completed",
    method: "PUT",
    body: JSON.stringify({
      email: email
    })
  });
}

export function listReports(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/reports",
    method: "GET"
  });
}

export function listCollaboratorReports(email) {
  return request({
    url: API_BASE_URL + "/tasks/reports/" + email,
    method: "GET"
  });
}

export function listCollaboratorReportsByTask(email, taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/reports/" + email,
    method: "GET"
  });
}

export function getCostProject(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/costs",
    method: "GET"
  });
}

export function getTask(taskid) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid,
    method: "GET"
  });
}

export function getProject(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/show",
    method: "GET"
  });
}

export function getProjectTasks(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/all",
    method: "GET"
  });
}

export function editReport(taskid, reportId, reportQuantity, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid + "/reports2/" + reportId,
    method: "PUT",
    body: JSON.stringify({
      quantity: reportQuantity,
      collabEmail: email,
      taskID: taskid,
      reportId: reportId
    })
  });
}

export function addReportCollab(taskId, quantity, startDate, endDate, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/reports2",
    method: "POST",
    body: JSON.stringify({
      quantity: quantity,
      startDate: startDate,
      endDate: endDate,
      projectCollaboratorEmail: email,
      taskID: taskId
    })
  });
}

export function createTask(projectId, title, description, effort, startDate, endDate) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks",
    method: "POST",
    body: JSON.stringify({
      projectId: projectId,
      title: title,
      description: description,
      estimatedEffort: effort,
      predictedDateOfStart: startDate + " 00:00:00",
      predictedDateOfConclusion: endDate + " 00:00:00"
    })
  });
}

export function login(loginRequest) {
  return request({
    url: API_BASE_URL + "/api/auth/signin",
    method: "POST",
    body: JSON.stringify(loginRequest)
  });
}

export function signup(signupRequest) {
  return request({
    url: API_BASE_URL + "/api/auth/signup",
    method: "POST",
    body: JSON.stringify(signupRequest)
  });
}

export function checkUsernameAvailability(email) {
  return request({
    url: API_BASE_URL + "/api/user/checkUsernameAvailability?email=" + email,
    method: "GET"
  });
}

export function checkEmailAvailability(email) {
  return request({
    url: API_BASE_URL + "/api/user/checkEmailAvailability?email=" + email,
    method: "GET"
  });
}

export function getCurrentUser() {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }

  return request({
    url: API_BASE_URL + "/api/user/me",
    method: "GET"
  });
}

export function getUserProfile(email) {
  return request({
    url: API_BASE_URL + "/api/users/" + email,
    method: "GET"
  });
}

export function getPmProjects(email) {
  return request({
    url: API_BASE_URL + "/users/" + email + "/projectsaspm",
    method: "GET"
  });
}

export function getAllActiveProjects() {
  return request({
    url: API_BASE_URL + "/projects?isActive=true",
    method: "GET"
  });
}

export function getProjectCollaboratorsList(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/collaborators",
    method: "GET"
  });
}

export function getUnassignedProjectCollaboratorsList(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/unassignedcollaborators",
    method: "GET"
  });
}

export function getCollaboratorsListNotInProject(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/collaboratorsnotinproject",
    method: "GET"
  });
}

export function getAvailableCostOptions() {
  return request({
    url: API_BASE_URL + "/projects/availableCostOptions",
    method: "GET"
  });
}

export function activateUser(confirmationCode) {
  return request({
    url: API_BASE_URL + "/api/auth/activate",
    method: "POST",
    body: confirmationCode
  });
}

export function addProjectCollaborator2(projectId, email, cost) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId,
    method: "POST",
    body: JSON.stringify({
      userId: email,
      projectId: projectId,
      cost: cost
    })
  });
}

export function setProjectCostOptions(projectId, costOptions) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/costOptions",
    method: "PUT",
    body: JSON.stringify({
      projectId: projectId,
      costCalculationOptions: costOptions
    })
  });
}

export function createProject(projectInfo) {
  return request({
    url: API_BASE_URL + "/projects",
    method: "POST",
    body: JSON.stringify(projectInfo)
  });
}

export function setUserProfile(values) {
  return request({
    url: API_BASE_URL + values.profileAction,
    method: "PUT",
    body: JSON.stringify({
      profile: values.profile
    })
  });
}

export function listTaskCollaborators(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/taskcollaborators",
    method: "GET"
  });
}

export function listAllTaskCollaborators(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskId + "/alltaskcollaborators",
    method: "GET"
  });
}

export function getProjectCostOptions(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/costcalculation",
    method: "GET"
  });
}

export function selectProjectCostOption(projectId, costOption) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/costcalculation",
    method: "PUT",
    body: JSON.stringify({
      calculationOption: costOption
    })
  });
}

export function availableTaskCollaboratorToAdd(taskid) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid + "/collaboratorsavailable",
    method: "GET"
  });
}

export function addTaskCollaborator(taskid, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid,
    method: "POST",
    body: JSON.stringify({
      email: email
    })
  });
}

export function getCollabRequests(email) {
    return request({
        url: API_BASE_URL + "/users/" + email + "/requests",
        method: "GET"
    });
}

export function getAssignmentRequests(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/assignmentrequests",
    method: "GET"
  });
}

export function getRemovalRequests(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/removalrequests",
    method: "GET"
  });
}

export function getCompletedRequests(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/completedrequests",
    method: "GET"
  });
}

export function cancelAssignmentRequests(projectId, taskId, userEmail, collabId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/cancelassignment",
    method: "PUT",
    body: JSON.stringify({
      taskId: taskId,
      projectCollaboratorId: collabId,
      projectCollaboratorEmail: userEmail
    })
  });
}

export function cancelCompletedRequests(taskId) {
  return request({
    url: API_BASE_URL + "/tasks/"+taskId+"/cancelcompleted",
    method: "PUT",
   
  });
}

export function cancelRemovalRequests(projectId, taskId, userEmail, collabId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/cancelremoval",
    method: "PUT",
    body: JSON.stringify({
      taskId: taskId,
      projectCollaboratorId: collabId,
      projectCollaboratorEmail: userEmail
    })
  });
}

export function listCollaboratorTasksOrderByLastReport(email) {
  return request({
    url: API_BASE_URL + "/users/" + email + "/recentActivity",
    method: "GET"
  });
}

// export function setTaskCancelled()

// export function setTaskRemoved()

export function removeTaskCollaborator(taskid, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid,
    method: "PUT",
    body: JSON.stringify({
      email: email
    })
  });
}

export function resendEmail() {
  return request({
    url: API_BASE_URL + "/api/auth/activate/resendmail",
    method: "GET"
  });
}

export function markTaskCompleted(taskid) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid + "/approvecompleted",
    method: "PUT"
  });
}

export function makeAssignementRequest(taskid, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid + "/makeassignement",
    method: "PUT",
    body: JSON.stringify({
      projectCollaboratorEmail: email
    })
  });
}


export function makeRemovalRequest(taskid, email) {
  return request({
    url: API_BASE_URL + "/tasks/" + taskid + "/makeremoval",
    method: "PUT",
    body: JSON.stringify({
      projectCollaboratorEmail: email
    })
  });
}

export function getProjectTasksCost(projectId) {
  return request({
    url: API_BASE_URL + "/projects/" + projectId + "/tasks/costs",
    method: "GET"
  });
}

export function collaboratorHoursInCompletedTasksLastMonth(email) {
  return request({
    url: API_BASE_URL + "/users/" + email + "/hours",
    method: "GET"
  });
}

export function populateUsersXMLAPI(filename, path) {
  return request({
    url: API_BASE_URL + "/users/populate",
    method: "POST",
    body: JSON.stringify({
      filename: filename,
      path, path
    })
  });
}

export function populateProjectsXMLAPI(filename, path) {
    return request({
      url: API_BASE_URL + "/projects/populate",
      method: "POST",
      body: JSON.stringify({
        filename: filename,
        path, path
      })
    });
  }

export function changePassword(oldPassword, newPassword) {
  return request({
    url: API_BASE_URL + "/users/changePassword",
    method: "PUT",
    body: JSON.stringify({
      oldPassword: oldPassword,
      newPassword: newPassword
    })
  });
}
